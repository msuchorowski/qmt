#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"


#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


double get_energy(const gsl_vector* v,void *params);

//std::vector <double> minmize_ANMSA(const gsl_vector* v,void *params);
std::vector <double> minimize(const gsl_vector* vv,void *params);
std::vector <double> dE(const gsl_vector* v,void *params,double da);



int main(int argc,const char* argv[])
{
bool flag=1;
int N=15;
for(size_t i = 0U; i < N;i++)
{
  double dR=6.0/N;

  double R = 0.1 + i * dR;
  if(flag==0) {R+=dR*0.5; flag=1;}
  else if( (R>=1 && R<2) && flag ) {flag=0; i--; }
   
  gsl_vector *x;
  
  x = gsl_vector_alloc (2);
  gsl_vector_set (x, 0, 1.1);
  gsl_vector_set (x, 1, 1.1);

  std::vector<double> En=minimize(x,&R);

  std::cout<< R << " ";
  for(int i=0; i<x->size; i++){
    std::cout << En[i] <<" ";}
  
  std::cout << En[x->size] << std::endl;
  
  
}
  
	return 0;
}

std::vector <double> dE(const gsl_vector* v,void *params,double da){
  std::cout << "#calculating gradient" << std::endl;
  std::vector<double> de;
  double e=get_energy(v,params);
  for(int i=0; i<v->size; i++){
      gsl_vector *temp= gsl_vector_alloc (v->size);
      gsl_vector_memcpy(temp,v);
      gsl_vector_set (temp, i, gsl_vector_get(v,i)+da);
      double eplus=get_energy(temp,params);
      de.push_back((eplus-e)/da);
    }
  de.push_back(e);
  return de;

}

std::vector <double> minimize(const gsl_vector* vv,void *params){
  //gsl_vector_get(v,0)
  //
  size_t n=vv->size;
  gsl_vector *v= gsl_vector_alloc (n);
  gsl_vector_memcpy (v, vv);
  
  double amin=0.5, amax=2.5;

  double s=1;
  std::vector<double> grad (n,0);
  double E;
  while(s>10e-6){
    std::cout << "#ITER" << std::endl;
    for(size_t i=0; i<n; i++){
      gsl_vector_set (v, i, gsl_vector_get(v,i)-grad[i]*1.0);
      if(gsl_vector_get (v, i)<amin) gsl_vector_set (v, i, amin);
      if(gsl_vector_get (v, i)>amax) gsl_vector_set (v, i, amax);
      }

    grad=dE(v,params,0.001);
    E=grad.back();
    grad.pop_back();

    s=0;
    for(auto it = grad.begin(); it != grad.end(); ++it)
         s += (*it)*(*it);
    std::cout << "#length of grad(E) = " << s << std::endl << "#";
    std::cout << "#energy E = " << E << std::endl << "#";
    for(int i=0; i<n; i++)  
      std::cout << gsl_vector_get(v,i) << " ";
    std::cout << std::endl;
  
  }
  std::vector <double> results;
  for(int i=0; i<n; i++)
    results.push_back(gsl_vector_get(v,i));
  results.push_back(E);
  return results;
}



double get_energy(const gsl_vector* v,void *params){
  std::cout << "#Get energy for R=" << *(static_cast<double*>(params)) <<  std::endl;
std::cout << "#" << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << std::endl;

      //qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<8> ("ham.dat",1U,1U,200000U);
            std::vector<double> one_body;
            std::vector<double> two_body;
      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

      double R = *(static_cast<double*>(params));
      system->set_parameters(std::vector<double>({gsl_vector_get(v,0),gsl_vector_get(v,1)}),qmt::QmtVector(1,1,R));
//      system->set_parameters(std::vector<double>({1.0,1.0}),qmt::QmtVector(1,1,R));
            
            for(auto i = 0; i < system->get_one_body_number();++i) {
               double integral = 0.0; 
               integral = system->get_one_body_integral(i);  
        //     std::cout<<"#one_body_integral#"<<i<<" = "<<integral<<std::endl;
               one_body.push_back(integral);
               
             }
                   
            for(auto i = 0; i < system->get_two_body_number();++i){
               auto integral =  system->get_two_body_integral(i);
        //     std::cout<<"#two_body_integral#"<<i<<" = "<<integral<<std::endl;
               two_body.push_back(integral);
              }
    
std::cout<<"#start VMC"<<std::endl;
double result =  (diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)+2/R);
//double result =   diagonalizer_gsl->Diagonalize(one_body,two_body);

//delete diagonalizer_gsl;
delete diagonalizer_vmc_gsl;
delete system;

std::cout << "#energy: "<< result << " " << result-2/R << std::endl;
return result;
}
