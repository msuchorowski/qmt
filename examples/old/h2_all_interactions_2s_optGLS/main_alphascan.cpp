#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double get_energy(const gsl_vector* v,void *params){


      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
      
            std::vector<double> one_body;
            std::vector<double> two_body;
      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

      double R = *(static_cast<double*>(params));
      system->set_parameters(std::vector<double>({gsl_vector_get(v,0),gsl_vector_get(v,1)}),qmt::QmtVector(1,1,R));
//      system->set_parameters(std::vector<double>({1.0,1.0}),qmt::QmtVector(1,1,R));
            
            for(auto i = 0; i < system->get_one_body_number();++i) {
               double integral = 0.0; 
               integral = system->get_one_body_integral(i);  
//             std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
               one_body.push_back(integral);
               
             }
                   
            for(auto i = 0; i < system->get_two_body_number();++i){
               auto integral =  system->get_two_body_integral(i);
//             std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
               two_body.push_back(integral);
              }
    


double result =   diagonalizer_gsl->Diagonalize(one_body,two_body);

delete diagonalizer_gsl;
delete system;

return result;
}





int main(int argc,const char* argv[])
{
bool flag=1;
for(size_t i = 0U; i < 20;i++)
{

size_t iter = 0;
  int status;

  const gsl_multimin_fminimizer_type *T;
  gsl_multimin_fminimizer *s;

 /* Set initial step sizes to 1 */
  gsl_vector *ss = gsl_vector_alloc (2);
  gsl_vector_set_all (ss, 1.0);
  
  double R = 1.35 + i * 0.01;
 // if(flag==0) {R+=0.01; flag=1;}
  //else if( (R>=1.35 && R<1.45) && flag ) {flag=0; i--; }
  //std::cout << R << std::endl;
   
  gsl_vector *x;
  gsl_multimin_function my_func;

  my_func.n = 2;
  my_func.f = get_energy;
  my_func.params = &R;
  
  /* Starting point, x = (5,7) */
  
  x = gsl_vector_alloc (2);
  gsl_vector_set (x, 0, 1.1);
  gsl_vector_set (x, 1, 1.1);

  T = gsl_multimin_fminimizer_nmsimplex2;
  s = gsl_multimin_fminimizer_alloc (T, 2);

    gsl_multimin_fminimizer_set (s, &my_func, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if (status)
        break;

      double size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);


    }
  while (status == GSL_CONTINUE && iter < 100);

  std::cout<<R<<" "<<gsl_vector_get (s->x, 0)<<" "<<gsl_vector_get (s->x, 1)<<" "<<s->fval<<std::endl;
  gsl_multimin_fminimizer_free (s);
  gsl_vector_free (x);
  
}
  
	return 0;
}

