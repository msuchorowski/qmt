#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double get_energy(const gsl_vector* v,void *params){
std::cout << "#Get energy for R=" << *(static_cast<double*>(params)) <<  std::endl;
std::cout << "#" << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << " " << gsl_vector_get(v,2) << std::endl;
      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
 
            std::vector<double> one_body;
            std::vector<double> two_body;
      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

      double R = *(static_cast<double*>(params));
      system->set_parameters(std::vector<double>({gsl_vector_get(v,0),gsl_vector_get(v,1),gsl_vector_get(v,2)}),qmt::QmtVector(1,1,R));
//      system->set_parameters(std::vector<double>({1.0,1.0}),qmt::QmtVector(1,1,R));
            
            for(auto i = 0; i < system->get_one_body_number();++i) {
               double integral = 0.0; 
               integral = system->get_one_body_integral(i);  
 //            std::cout<<"#one_body_integral#"<<i<<" = "<<integral<<std::endl;
               one_body.push_back(integral);
               
             }
  //          std::cout << system->get_two_body_number() << std::endl;       
            for(auto i = 0; i < system->get_two_body_number();++i){
               auto integral =  system->get_two_body_integral(i);
  //           std::cout<<"#two_body_integral#"<<i<<" = "<<integral<<std::endl;
               two_body.push_back(integral);
              }
    
  

std::cout << "#start diagonalization" << std::endl;
double result =   diagonalizer_gsl->Diagonalize(one_body,two_body);

delete diagonalizer_gsl;
delete system;

return result;
}





int main(int argc,const char* argv[])
{

bool flag=1;
size_t N=10;
for(size_t i = 0U; i < N;i++)
{

size_t iter = 0;
  int status;

  const gsl_multimin_fminimizer_type *T;
  gsl_multimin_fminimizer *s;

 /* Set initial step sizes to 1 */
  gsl_vector *ss = gsl_vector_alloc (3);
  gsl_vector_set_all (ss, 1.0);
  double dR=6.0/N;

  double R = 0.1 + i * dR;
  if(flag==0) {R+=dR*0.5; flag=1;}
  else if( (R>=1 && R<2) && flag ) {flag=0; i--; }
  //std::cout << R << std::endl;
  
  gsl_vector *x;
  gsl_multimin_function my_func;

  my_func.n = 3;
  my_func.f = get_energy;
  my_func.params = &R;
 
  /* Starting point, x = (5,7) */
 
  x = gsl_vector_alloc (3);
  gsl_vector_set (x, 0, 1.1);
  gsl_vector_set (x, 1, 1.1);
  gsl_vector_set (x, 2, 1.1);

  T = gsl_multimin_fminimizer_nmsimplex2;
  s = gsl_multimin_fminimizer_alloc (T, 3);
  
    gsl_multimin_fminimizer_set (s, &my_func, x, ss);

  do
    {
      std::cout << "#ITER " << iter << " FOR R=" << R << std::endl;  
      //std::cout<<gsl_vector_get (s->x, 0)<<" "<<gsl_vector_get (s->x, 1) << " " << gsl_vector_get (s->x, 2) <<" "<<s->fval<<std::endl;
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if (status)
        break;

      double size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);


    }
  while (status == GSL_CONTINUE && iter < 100);

  std::cout << R << " " <<gsl_vector_get (s->x, 0)<<" "<<gsl_vector_get (s->x, 1) << " " << gsl_vector_get (s->x, 2) <<" "<<s->fval<<std::endl;
  gsl_multimin_fminimizer_free (s);
  gsl_vector_free (x);
  
}
  
	return 0;
}

