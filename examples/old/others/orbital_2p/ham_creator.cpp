#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <utility>

using namespace std;


bool sortcmp_newnum( const  vector<int>& v1, 
               const  vector<int>& v2 ) { 
 return v1[1] < v2[1]; 
} 


int main(){
    int N=10; 
    
     //orbitals
     string o1s0="(0,  0,  0,  0)";
     string o1s1="(0,  0,  0,  1)";
     string o1s2="(0,  0,  0,  2)";
     string o1s3="(0,  0,  0,  3)"; 
     string o1s4="(0,  0,  0,  4)";
     string o2s5="(0,  0,  1.0,  5)";
     string o2s6="(0,  0,  1.0,  6)";
     string o2s7="(0,  0,  1.0,  7)";
     string o2s8="(0,  0,  1.0,  8)";
     string o2s9="(0,  0,  1.0,  9)";
       string orbitals[10]={o1s0, o1s1, o1s2, o1s3, o1s4, o2s5, o2s6, o2s7, o2s8, o2s9};




  


   ofstream file2("ham_2p_full.dat");
    
    file2 << "one_body" << endl;    

       for(int j=0; j<N; j++)
         for(int i=0; i<N; i++){
           file2 << i << "cup,    " << j << "aup,    " << "1.0,   " << j*4+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
           file2 << i << "cdown,  " << j << "adown,  " << "1.0,   " << j*4+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
    }



    file2 << "end_one_body" << endl << endl << "two_body" << endl;

    vector < vector <int> > final_integral;
    
    for(int i=0; i<(N*N*N*(N-1)+N*N*(N-1)+N*(N-1)+N); i++){
       vector <int> temp;
       temp.push_back(-1);
       temp.push_back(-1);
       final_integral.push_back(temp);

    }
    cout << final_integral.size() << endl;

    int n;
    int integral_number=0;
    for(int l=0; l<N; l++)
    for(int k=0; k<N; k++)
    for(int j=0; j<N; j++)
    for(int i=0; i<N; i++){
           n=(l*N*N*N+k*N*N+j*N+i);
           final_integral[n][0]=n;
           if(final_integral[n][1]==-1){
              final_integral[n][1]=integral_number;
              final_integral[(k*N*N*N+l*N*N+j*N+i)][1]=integral_number;
              final_integral[(k*N*N*N+l*N*N+i*N+j)][1]=integral_number;
              final_integral[(l*N*N*N+k*N*N+i*N+j)][1]=integral_number;
              integral_number++;
           }
    }
    //cout << "Test" << endl;
    vector<string> ham_str;
    for(int l=0; l<N; l++)
    for(int k=0; k<N; k++)
    for(int j=0; j<N; j++)
    for(int i=0; i<N; i++){
           n=(l*N*N*N+k*N*N+j*N+i);

           
           ham_str.push_back(to_string(i)+"cup,    "+to_string(j)+"cup,    "+to_string(k)+"aup,    "+ to_string(l)+"aup,    "+"0.5,   "+ to_string(int(final_integral[n][1]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
          
           ham_str.push_back(to_string(i)+"cdown,  "+to_string(j)+"cdown,  "+to_string(k)+"adown,  "+ to_string(l)+"adown,  "+"0.5,   "+ to_string(int(final_integral[n][1]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
          
           ham_str.push_back(to_string(i)+"cup,    "+to_string(j)+"cdown,  "+to_string(k)+"adown,  "+ to_string(l)+"aup,    "+"0.5,   "+ to_string(int(final_integral[n][1]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
         
           ham_str.push_back(to_string(i)+"cdown,  "+to_string(j)+"cup,    "+to_string(k)+"aup,    "+ to_string(l)+"adown,  "+"0.5,   "+ to_string(int(final_integral[n][1]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");

    }
    cout << final_integral.size() << endl;
    cout << ham_str.size() << endl;

    sort(final_integral.begin(), final_integral.end(),sortcmp_newnum);



  for (int i=0; i<final_integral.size(); i++) {
    file2 << ham_str[final_integral[i][0]*4] <<  endl;
    file2 << ham_str[final_integral[i][0]*4+1] <<  endl;
    file2 << ham_str[final_integral[i][0]*4+2] <<  endl;
    file2 << ham_str[final_integral[i][0]*4+3] <<  endl;
  }
   
   file2 << "end_two_body" << endl;
   file2.close();
}





