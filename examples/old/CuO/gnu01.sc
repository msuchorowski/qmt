while (1) {
       set multiplot layout 2,1
       plot "optimization.log" u 2 w lp pt 6 ps 2 title "Energy"
       plot "optimization.log" u 3 w lp pt 6 ps 2 title  "lambda_0"
       unset multiplot
       pause 5      # waiting time in seconds
    
}
