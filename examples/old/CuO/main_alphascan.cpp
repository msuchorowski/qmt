#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



#define SIZE 96



int main(int argc,const char* argv[])
{


int NEL = atoi(argv[1]);

/* Engine - most important */

qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<SIZE> ("ham.dat",(NEL/2),(NEL/2),200000U);

auto &stat = (static_cast<qmt::vmc::QmtVmcGslMetropolisDiagonalizer<SIZE>*>(diagonalizer_vmc_gsl))->get_stat();




{
std::cout<<"VAR "<<" E_tot "<<" N ";
auto avs_names = stat.get_names();
for(const auto item : avs_names)
std::cout<<" "<<item;
}
std::cout<<std::endl;


double eps_d = 0;
double eps_p = -3.57;

double t_pd = 1.13;
double t_pp = 0.49;

double U_d = 11.0;//7.85;
double U_p = 4.1;



std::vector<double> one_body;

std::vector<double> two_body;

two_body.push_back(U_d);
two_body.push_back(U_p);



one_body.push_back(0);
one_body.push_back(eps_d); //0
one_body.push_back(-t_pd); //1
one_body.push_back(t_pd);  //2
one_body.push_back(eps_p); //3
one_body.push_back(t_pp);  //4
one_body.push_back(-t_pp); //5





std::cout<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)<<" "<<NEL<<" ";//<<hl1;//<<" "<<hl2;


for(const auto item : stat.get_values())
std::cout<<" "<<item;

std::cout<<std::endl;

 	
delete diagonalizer_vmc_gsl;

return 0;
}
