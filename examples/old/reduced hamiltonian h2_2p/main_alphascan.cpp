#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"


#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <fstream>
//this version of program takes already calculated values of integrals from file log_integrals.dat

bool sortcmp_value( const std::vector<double>& v1, 
               const std::vector<double>& v2 ) { 
 return v1[1] < v2[1]; 
} 

bool sortcmp_oldnum( const std::vector<double>& v1, 
               const std::vector<double>& v2 ) { 
 return v1[0] < v2[0]; 
} 

bool sortcmp_newnum( const std::vector<double>& v1, 
               const std::vector<double>& v2 ) { 
 return v1[3] < v2[3]; 
} 


std::list<double> integrals_file;


std::vector<std::vector<double>> get_energy(const gsl_vector* v,void *params, bool flag_file){

qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);


      std::vector<double> two_body;
       
      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

      double R = *(static_cast<double*>(params));
     // std::cout<<R<<" "<<gsl_vector_get (v, 0)<<" "<<gsl_vector_get (v, 1)<< " " << gsl_vector_get(v,2) << std::endl;
      system->set_parameters(std::vector<double>({gsl_vector_get(v,0),gsl_vector_get(v,1),gsl_vector_get(v,2)}),qmt::QmtVector(1,1,R));

                   
      std::vector<std::vector<double>> integrals;
      std::vector<double> temp;

      for(auto i = 0; i < system->get_two_body_number();i+=4){
            // get intergral from file if flag_file is True
            double integral;
            if(flag_file){
            integral =integrals_file.front();
            integrals_file.pop_front();}
            else integral =  system->get_two_body_integral(i);

            std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;

            temp.push_back(i);
            temp.push_back(integral);
            integrals.push_back(temp);
            temp.clear();

            temp.push_back(i+1);
            temp.push_back(integral);
            integrals.push_back(temp);
            temp.clear();


            temp.push_back(i+2);
            temp.push_back(integral);
            integrals.push_back(
              temp);
            temp.clear();

            temp.push_back(i+3);
            temp.push_back(integral);
            integrals.push_back(temp);
            temp.clear();


              }
     sort(integrals.begin(), integrals.end(),sortcmp_value);

    double n;
    for (int i=0; i<integrals.size(); i++) {
      
      if(!i) {n=0; integrals[i].push_back(0);}
      else if(/*integrals[i][1]==integrals[i-1][1]*/ abs(integrals[i][1]-integrals[i-1][1]<10e-20)) {integrals[i].push_back(0);}
      else {n+=1;
            integrals[i].push_back(1);}

      integrals[i].push_back(n);
      
      //std::cout << "#" << integrals[i][0] << "   " << integrals[i][1] << "   " << integrals[i][2] << std::endl;
      }

return integrals;

}


int main(int argc,const char* argv[])
{

std::ifstream file("log_integrals.dat");
std::string line;
while (std::getline(file, line)) {
    double int_file;
    std::size_t found=line.find('=');
    int_file = std::stod(line.substr (found+1) );
  //  std::cout << int_file << std::endl;
    integrals_file.push_back(int_file);
}




std::vector<std::vector<double>> final_integral;
std::vector<std::vector<std::vector<double>>> reduce;
//Put flag_file as true if integrals for that point were already calculated. Do not change its order! Add new point as last.
gsl_vector *aa = gsl_vector_alloc (3);
gsl_vector_set(aa, 0, 1.2);
gsl_vector_set(aa, 1, 1.3);
gsl_vector_set(aa, 2, 1.5);
double R = 1.0;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.7);
gsl_vector_set(aa, 1, 1.1);
gsl_vector_set(aa, 2, 2.1);
R=2;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 2.4);
gsl_vector_set(aa, 1, 2.1);
gsl_vector_set(aa, 2, 1.2);
R=0.7;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.2);
gsl_vector_set(aa, 1, 1.8);
gsl_vector_set(aa, 2, 2.2);
R=0.7;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.1);
gsl_vector_set(aa, 1, 1.6);
gsl_vector_set(aa, 2, 2.0);
R=1.2;
reduce.push_back(get_energy(aa,&R,1));


gsl_vector_set(aa, 0, 1.5);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 2.5);
R=1.2;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.3);
gsl_vector_set(aa, 2, 2.3);
R=1.42;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.1);
gsl_vector_set(aa, 1, 1.1);
gsl_vector_set(aa, 2, 2.1);
R=1.5;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.4);
gsl_vector_set(aa, 1, 1.4);
gsl_vector_set(aa, 2, 4.4);
R=1.3;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.3);
gsl_vector_set(aa, 2, 1.3);
R=1.42;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.4);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.6);
R=1.3;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.4);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.6);
R=1.2;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.4);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.6);
R=1.5;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.4);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.6);
R=1.6;
reduce.push_back(get_energy(aa,&R,1));

//new points
gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.7);
R=0.9;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.7);
R=0.4;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.7);
R=1.4;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.7);
R=1.9;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.7);
R=2.2;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.7);
R=3.9;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.7);
R=3.4;
reduce.push_back(get_energy(aa,&R,1));

gsl_vector_set(aa, 0, 1.3);
gsl_vector_set(aa, 1, 1.5);
gsl_vector_set(aa, 2, 1.7);
R=4.8;
reduce.push_back(get_energy(aa,&R,1));
/*

gsl_vector_set(aa, 0, 1.35);
gsl_vector_set(aa, 1, 1.55);
gsl_vector_set(aa, 2, 1.75);
R=1.1;
reduce.push_back(get_energy(aa,&R,0));

gsl_vector_set(aa, 0, 1.35);
gsl_vector_set(aa, 1, 1.55);
gsl_vector_set(aa, 2, 1.75);
R=2.7;
reduce.push_back(get_energy(aa,&R,0));

gsl_vector_set(aa, 0, 1.35);
gsl_vector_set(aa, 1, 1.55);
gsl_vector_set(aa, 2, 1.75);
R=3.8;
reduce.push_back(get_energy(aa,&R,0));

gsl_vector_set(aa, 0, 1.35);
gsl_vector_set(aa, 1, 1.55);
gsl_vector_set(aa, 2, 1.75);
R=4.3;
reduce.push_back(get_energy(aa,&R,0));
*/
int temp_max;
int a,b,c,d;



for (int i=0; i<reduce[0].size(); i++) {
  double flag=0;
  
  for(int k=1; k<reduce.size();k++){
    if( (!reduce[0][i][2]) && reduce[k][i][2]) {flag=1; break;} 
  }

  if( flag ) {
    for (int j=i; j<reduce[0].size(); j++)
          reduce[0][j][3]++;
            }
  }
  final_integral=reduce[0];    
    


    sort(final_integral.begin(), final_integral.end(),sortcmp_oldnum);



//orbitals
     std::string o1s0="(0,  0,  0,  0)";
     std::string o1s1="(0,  0,  0,  1)";
     std::string o1s2="(0,  0,  0,  2)";
     std::string o1s3="(0,  0,  0,  3)"; 
     std::string o1s4="(0,  0,  0,  4)";
     std::string o2s5="(0,  0,  1.0,  5)";
     std::string o2s6="(0,  0,  1.0,  6)";
     std::string o2s7="(0,  0,  1.0,  7)";
     std::string o2s8="(0,  0,  1.0,  8)";
     std::string o2s9="(0,  0,  1.0,  9)";
       std::string orbitals[10]={o1s0, o1s1, o1s2, o1s3, o1s4, o2s5, o2s6, o2s7, o2s8, o2s9};


   std::ofstream file2("ham.dat");
    
    file2 << "one_body" << std::endl;    

       for(int j=0; j<10; j++)
         for(int i=0; i<10; i++){
           file2 << i << "cup,    " << j << "aup,    " << "1.0,   " << j*10+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << std::endl;
           file2 << i << "cdown,  " << j << "adown,  " << "1.0,   " << j*10+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << std::endl;
    }



    file2 << "end_one_body" << std::endl << std::endl << "two_body" << std::endl;


std::vector<std::string> ham_str; //texts to hamiltonian in order of old numbers
       int n=0;
    for(int l=0; l<10; l++)
    for(int k=0; k<10; k++)
    for(int j=0; j<10; j++)
    for(int i=0; i<10; i++){
           n=(l*10*10*10+k*10*10+j*10+i)*4;

           
           ham_str.push_back(std::to_string(i)+"cup,    "+std::to_string(j)+"cup,    "+std::to_string(k)+"aup,    "+std::to_string(l)+"aup,    "+"0.5,   "+std::to_string(int(final_integral[n][3]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
          
           ham_str.push_back(std::to_string(i)+"cdown,  "+std::to_string(j)+"cdown,  "+std::to_string(k)+"adown,  "+std::to_string(l)+"adown,  "+"0.5,   "+std::to_string(int(final_integral[n+1][3]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
          
           ham_str.push_back(std::to_string(i)+"cup,    "+std::to_string(j)+"cdown,  "+std::to_string(k)+"adown,  "+std::to_string(l)+"aup,    "+"0.5,   "+std::to_string(int(final_integral[n+2][3]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
         
           ham_str.push_back(std::to_string(i)+"cdown,  "+std::to_string(j)+"cup,    "+std::to_string(k)+"aup,    "+std::to_string(l)+"adown,  "+"0.5,   "+std::to_string(int(final_integral[n+3][3]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");

    }
  
  sort(final_integral.begin(), final_integral.end(),sortcmp_newnum);

  for (int i=0; i<final_integral.size(); i++) {
    file2 << ham_str[final_integral[i][0]] << std::endl;
  }


   file2 << "end_two_body" << std::endl;
   file2.close();



	return 0;
}