// to work orginal hamiltonian need to number each integral with diffrent number

#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <fstream>


bool sortcmp_value( const std::vector<double>& v1, 
               const std::vector<double>& v2 ) { 
 return v1[1] < v2[1]; 
} 

bool sortcmp_oldnum( const std::vector<double>& v1, 
               const std::vector<double>& v2 ) { 
 return v1[0] < v2[0]; 
} 

bool sortcmp_newnum( const std::vector<double>& v1, 
               const std::vector<double>& v2 ) { 
 return v1[3] < v2[3]; 
} 



std::vector<std::vector<double>> get_energy(const gsl_vector* v,void *params){
      

      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
      
      std::vector<double> two_body;

      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

      double R = *(static_cast<double*>(params));
      //std::cout<<R<<" "<<gsl_vector_get (v, 0)<<" "<<gsl_vector_get (v, 1)<<std::endl;
      system->set_parameters(std::vector<double>({gsl_vector_get(v,0),gsl_vector_get(v,1)}),qmt::QmtVector(1,1,R));

                   
      std::vector<std::vector<double>> integrals;
      std::vector<double> temp;

      for(auto i = 0; i < system->get_two_body_number();++i){
            auto integral =  system->get_two_body_integral(i);
             std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
            temp.push_back(i);
            temp.push_back(integral);
            integrals.push_back(temp);
            temp.clear();

              }
     sort(integrals.begin(), integrals.end(),sortcmp_value);

    double n;
    int counter=1; //for 2s there are 32 integrals with the same value
    for (int i=0; i<integrals.size(); i++) {
      
      if(!i) {n=0; integrals[i].push_back(0);}
      else if( abs(integrals[i][1]-integrals[i-1][1]<10e-20)) {counter++; integrals[i].push_back(0);}
      else {n+=1;
            //std::cout << n-1 << ": " << counter << std::endl;
            counter=0;
            integrals[i].push_back(1);}

      integrals[i].push_back(n);
      
      //std::cout << "#" << integrals[i][0] << "   " << integrals[i][1] << "   " << integrals[i][2] << std::endl;
      }

return integrals;
}




int main(int argc,const char* argv[])
{

std::vector<std::vector<double>> final_integral;
std::vector<std::vector<std::vector<double>>> reduce;

gsl_vector *aa = gsl_vector_alloc (2);
gsl_vector_set(aa, 0, 1.2);
gsl_vector_set(aa, 1, 1.3);
double R = 1.0;
reduce.push_back(get_energy(aa,&R));

gsl_vector_set(aa, 0, 1.7);
gsl_vector_set(aa, 1, 1.1);
R=2;
reduce.push_back(get_energy(aa,&R));

gsl_vector_set(aa, 0, 2.4);
gsl_vector_set(aa, 1, 2.1);
R=0.7;
reduce.push_back(get_energy(aa,&R));

gsl_vector_set(aa, 0, 1.2);
gsl_vector_set(aa, 1, 1.8);
R=0.7;
reduce.push_back(get_energy(aa,&R));

// vector reduce: vector< old_number value flag_border new_number  (sorted by new_number) >

//for (int i=0; i<reduce[0].size(); i++)
//  std::cout << reduce[0][i][3] << std::endl;



int temp_max;
int a,b,c,d;



for (int i=0; i<reduce[0].size(); i++) {
  //std::cout << reduce1[i][2] << " " << reduce2[i][2] << " " << reduce3[i][2] << " " << reduce4[i][2]  << std::endl;
  double flag=0;
  
  for(int k=1; k<reduce.size();k++){
    if( (!reduce[0][i][2]) && reduce[k][i][2]) {flag=1; break;} 
  }

  if( flag ) {
    for (int j=i; j<reduce[0].size(); j++)
          reduce[0][j][3]++;
            }
  }
  final_integral=reduce[0];    
    


    sort(final_integral.begin(), final_integral.end(),sortcmp_oldnum);



//orbitals
     std::string o1s0="(0,  0,  0,  0)";
     std::string o1s1="(0,  0,  0,  1)";
     std::string o2s2="(0,  0,  1.0,  2)";
     std::string o2s3="(0,  0,  1.0,  3)";
     std::string orbitals[4]={o1s0, o1s1, o2s2, o2s3};


   std::ofstream file2("ham.dat");
    
    file2 << "one_body" << std::endl;    

       for(int j=0; j<4; j++)
         for(int i=0; i<4; i++){
           file2 << i << "cup,    " << j << "aup,    " << "1.0,   " << j*4+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << std::endl;
           file2 << i << "cdown,  " << j << "adown,  " << "1.0,   " << j*4+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << std::endl;
    }



    file2 << "end_one_body" << std::endl << std::endl << "two_body" << std::endl;


std::vector<std::string> ham_str; //texts to hamiltonian in order of old numbers

       int n=0;
    for(int l=0; l<4; l++)
    for(int k=0; k<4; k++)
    for(int j=0; j<4; j++)
    for(int i=0; i<4; i++){
           n=(l*4*4*4+k*4*4+j*4+i)*4;

           
           
           //cout << n << endl;
           
           ham_str.push_back(std::to_string(i)+"cup,    "+std::to_string(j)+"cup,    "+std::to_string(k)+"aup,    "+std::to_string(l)+"aup,    "+"0.5,   "+std::to_string(int(final_integral[n][3]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
           
           ham_str.push_back(std::to_string(i)+"cdown,  "+std::to_string(j)+"cdown,  "+std::to_string(k)+"adown,  "+std::to_string(l)+"adown,  "+"0.5,   "+std::to_string(int(final_integral[n+1][3]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
           
           ham_str.push_back(std::to_string(i)+"cup,    "+std::to_string(j)+"cdown,  "+std::to_string(k)+"adown,  "+std::to_string(l)+"aup,    "+"0.5,   "+std::to_string(int(final_integral[n+2][3]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
           
           ham_str.push_back(std::to_string(i)+"cdown,  "+std::to_string(j)+"cup,    "+std::to_string(k)+"aup,    "+std::to_string(l)+"adown,  "+"0.5,   "+std::to_string(int(final_integral[n+3][3]))+",     "+orbitals[i] + ",      "+orbitals[j]+",      "+orbitals[l]+",      "+orbitals[k]+";");
           
    }

  sort(final_integral.begin(), final_integral.end(),sortcmp_newnum);
  for (int i=0; i<final_integral.size(); i++) {
    file2 << ham_str[final_integral[i][0]] << std::endl;
  }
  std::cout << final_integral.size() << std::endl;

   file2 << "end_two_body" << std::endl;
   file2.close();



	return 0;
}

