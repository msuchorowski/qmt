#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>

using namespace std;


int main(){
     
   
     //orbitals
     std::string o1s0="(0,  0,  0,  0)";
     std::string o2s1="(0,  0,  1,  1)";
     std::string o3s2="(0,  1,  0.5,  2)";
     std::string o3s3="(0,  1,  0.5,  3)"; 
     std::string o3s4="(0,  1,  0.5,  4)";
     std::string o3s5="(0,  1,  0.5,  5)";
     std::string o3s6="(0,  1,  0.5,  6)";
       string orbitals[7]={o1s0, o2s1, o3s2, o3s3, o3s4, o3s5, o3s6};



  


   ofstream file2("ham_h2o_full.dat");
    
    file2 << "one_body" << endl;    

       for(int j=0; j<7; j++)
         for(int i=0; i<7; i++){
           file2 << i << "cup,    " << j << "aup,    " << "1.0,   " << j*7+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
           file2 << i << "cdown,  " << j << "adown,  " << "1.0,   " << j*7+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
    }



    file2 << "end_one_body" << endl << endl << "two_body" << endl;
    int a,b,c,d;
       int n=0;
    for(int l=0; l<7; l++)
    for(int k=0; k<7; k++)
    for(int j=0; j<7; j++)
    for(int i=0; i<7; i++){
           n=(l*7*7*7+k*7*7+j*7+i)*4;
/*            if(l<=4) d=0;
            else d=1;
            if(k<=4) c=0;
          else c=1;
            if(j<=4) b=0;
            else b=1;
            if(i<=4) a=0;
            else a=1;
           n=(d*2*2*2+c*2*2+b*2+a);*/
           file2 << i << "cup,    " << j << "cup,    " << k << "aup,    " << l << "aup,    " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cdown,  " << k << "adown,  " << l << "adown,  " << "0.5,   " << n+1 << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cup,    " << j << "cdown,  " << k << "adown,  " << l << "aup,    " << "0.5,   " << n+2 << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cup,    " << k << "aup,    " << l << "adown,  " << "0.5,   " << n+3 << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;

    }



   file2 << "end_two_body" << endl;
   file2.close();




}


