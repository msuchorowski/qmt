#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double get_energy(const gsl_vector* v,void *params, void *params2);

//std::vector <double> minmize_ANMSA(const gsl_vector* v,void *params);
std::vector <double> minimize(const gsl_vector* vv,void *params,void *params2);
std::vector <double> dE(const gsl_vector* v,void *params,void *params2,double da);





int sign(double a){
  if(a>=0) return 1;
  else return -1;
 }





int main(int argc,const char* argv[])
{

bool flag=1;
int N=3;
for(size_t j = 0U; j < N;j++){
  double dr=0.3/N;

  double r = 1.3 + j * dr;
  
  for(size_t i = 0U; i < N;i++)
    {
    double dR=0.3/N;

    double R = 1.0 + i * dR;
    //if(flag==0) {R+=dR*0.5; flag=1;}
    //else if( (R>=1 && R<2) && flag ) {flag=0; i--; }
   
    gsl_vector *x;
  
    x = gsl_vector_alloc (4);
    gsl_vector_set (x, 0, 1);
    gsl_vector_set (x, 1, 1);
    gsl_vector_set (x, 2, 1);
    gsl_vector_set (x, 3, 1);

    std::vector<double> En=minimize(x,&R,&r);
    std::cout << "Results for: "; 
    std::cout<< R << " " << r << " ";
    for(int i=0; i<x->size; i++){
      std::cout << En[i] <<" ";}
  
  std::cout << En[x->size] << std::endl;
  
  
}
}
	return 0;
}


std::vector <double> dE(const gsl_vector* v,void *params, void *params2,double da){
  //std::cout << "calculating gradient" << std::endl;
  std::vector<double> de;
  double e=get_energy(v,params,params2);
  for(int i=0; i<v->size; i++){
      gsl_vector *temp= gsl_vector_alloc (v->size);
      gsl_vector_memcpy(temp,v);
      gsl_vector_set (temp, i, gsl_vector_get(v,i)+da);
      double eplus=get_energy(temp,params,params2);
      de.push_back((eplus-e)/da);
    }
  de.push_back(e);
  return de;

}

std::vector <double> minimize(const gsl_vector* vv,void *params, void *params2){
  //gsl_vector_get(v,0)
  //
  size_t n=vv->size;
  gsl_vector *v= gsl_vector_alloc (n);
  gsl_vector_memcpy (v, vv);
  
  double amin=1, amax=2;
//sign(grad[i])*std::min(fabs(grad[i]),0.1)
  double s=1;
  std::vector<double> grad (n,0);
  double E;
  while(sqrt(s)>10e-6){
    std::cout << "#ITER" << std::endl;
    for(size_t i=0; i<n; i++){
      gsl_vector_set (v, i, gsl_vector_get(v,i)-sign(grad[i])*std::min(fabs(grad[i]),0.1));
      if(gsl_vector_get (v, i)<amin) gsl_vector_set (v, i, amin);
      if(gsl_vector_get (v, i)>amax) gsl_vector_set (v, i, amax);
      }

    grad=dE(v,params,params2,0.001);
    E=grad.back();
    grad.pop_back();

    s=0;
    for(auto it = grad.begin(); it != grad.end(); ++it){
         std::cout << *it << std::endl;
         s += (*it)*(*it);}
    std::cout << "#length of grad(E) = " << sqrt(s) << std::endl << "#";
    std::cout << "#energy E = " << E << std::endl << "#";
    for(int i=0; i<n; i++)  
      std::cout << gsl_vector_get(v,i) << " ";
    std::cout << std::endl;
  
  }
  std::vector <double> results;
  for(int i=0; i<n; i++)
    results.push_back(gsl_vector_get(v,i));
  results.push_back(E);
  return results;
}

double get_energy(const gsl_vector* v,void *params, void *params2){
std::cout << "#Get energy for R=" << *(static_cast<double*>(params)) << " and r=" <<  *(static_cast<double*>(params2)) << std::endl;
//std::cout << "#" << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << " " << gsl_vector_get(v,2) << std::endl;
      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
 
            std::vector<double> one_body;
            std::vector<double> two_body;
      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

      double R = *(static_cast<double*>(params));
      double r = *(static_cast<double*>(params2));
      system->set_parameters(std::vector<double>({gsl_vector_get(v,0),gsl_vector_get(v,1),gsl_vector_get(v,2),gsl_vector_get(v,3)}),qmt::QmtVector(1,r,R));
//      system->set_parameters(std::vector<double>({1.0,1.0}),qmt::QmtVector(1,1,R));
            
            for(auto i = 0; i < system->get_one_body_number();++i) {
               double integral = 0.0; 
               integral = system->get_one_body_integral(i);  
             //std::cout<<"#one_body_integral#"<<i<<" = "<<integral<<std::endl;
               one_body.push_back(integral);
               
             }
             std::cout<<"#one_body_integrals finished"<<std::endl;
          //  std::cout << system->get_two_body_number() << std::endl;       
            for(auto i = 0; i < system->get_two_body_number();++i){
               if(int(i)%60==0) std::cout<<"#"<<i*100.0/system->get_two_body_number()<<"%"; 
               auto integral =  system->get_two_body_integral(i);
          //   std::cout<<"#two_body_integral#"<<i<<" = "<<integral<<std::endl;
               two_body.push_back(integral);
              }
              std::cout<<std::endl<<"#two_body_integrals finished"<<std::endl;
    
  

std::cout << "#start diagonalization" << std::endl << "#";
double result =   diagonalizer_gsl->Diagonalize(one_body,two_body);

delete diagonalizer_gsl;
delete system;
std::cout  << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << " " << gsl_vector_get(v,2) << " " << gsl_vector_get(v,3) << " " <<result <<  std::endl;
return result;
}