#define GSL_LIBRARY


#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>


double get_energy(const gsl_vector* v,void *params);

//std::vector <double> minmize_ANMSA(const gsl_vector* v,void *params);
std::vector <double> minimize(const gsl_vector* vv,void *params);
std::vector <double> dE(const gsl_vector* v,void *params,double da);

int sign(double a){
  if(a>=0) return 1;
  else return -1;
 }

     
bool sortcmp_energy( const std::vector<double>& v1, const std::vector<double>& v2 ) { 
 return v1.back() < v2.back(); 
} 


int main(int argc,const char* argv[])
{
bool flag=1;
int N=1;
for(size_t i = 0U; i < N;i++)
{
  double dR=6.0/N;

  double R = 1.3 + i * dR;
  //if(flag==0) {R+=dR*0.5; flag=1;}
  //else if( (R>=1 && R<2) && flag ) {flag=0; i--; }
   
  gsl_vector *x;
  
  x = gsl_vector_alloc (2);
  gsl_vector_set (x, 0, 1.1);
  gsl_vector_set (x, 1, 1.1);

  std::vector<double> En=minimize(x,&R);

  std::cout<< R << " ";
  for(int i=0; i<x->size; i++){
    std::cout << En[i] <<" ";}
  
  std::cout << En[x->size] << std::endl;
  
  
}
  
	return 0;
}



std::vector <double> minimize(const gsl_vector* vv,void *params){
  //gsl_vector_get(v,0)
  //
  size_t n=vv->size;
  gsl_vector *va= gsl_vector_alloc (n);
  gsl_vector_memcpy (va, vv);
  
  double amin=0.5, amax=2.5;

  double E;
  //parametry
  double alpha=1, beta=0.5, gamma=1;

  //zdefiniowanie poczatkowego sympleksu 
  std::vector <std::vector<double>> symplex;
  std::vector <double> part_symplex;
  for(int i=0; i<=n;i++){
    for(int j=0; j<n; j++){
      double a=std::min(1.0+0.1*i+0.001*j,amax);
      a=std::max(a,amin);
      part_symplex.push_back(a);
      gsl_vector_set (va, j, a);
    }
    part_symplex.push_back(get_energy(va,params));
    symplex.push_back(part_symplex);
    part_symplex.clear();
  }


  //wektory pomocnicze
  std::vector<double> u;
  std::vector<double> v;
  std::vector<double> w;

  //szukanie nowego symplexu
  int iter=0;
  while(iter<30){
  iter++;
  std::cout << "#ITER " << iter << std::endl; 
  sort(symplex.begin(), symplex.end(),sortcmp_energy);
  
  for(auto wsp=0; wsp<n; wsp++){
    double suma=0;
    for(auto punkty=0; punkty<=n;punkty++){
      suma+=symplex[punkty][wsp];
    }
    u.push_back(suma/n);

    v.push_back( (1+alpha)*u[wsp]-alpha*symplex[0][wsp] );
    w.push_back( (1+gamma)*v[wsp] - gamma*u[wsp] );
  }

  //energia w punkcie v i w
  for(int i=0; i<n; i++){
      gsl_vector_set (va, i, v[i]);
    }
  v.push_back(get_energy(va, params));
  for(int i=0; i<n; i++){
        gsl_vector_set (va, i, w[i]);
      }
  w.push_back(get_energy(va, params));

  //ekspansja symplexu
  if(v.back()<symplex[n].back()){
      if(w.back()<symplex[n].back()) symplex[0]=w;
      else symplex[0]=v;
  }
  else if(v.back()<=symplex[1].back()) symplex[0]=v;
  else {//if(v.back()>symplex[1].back())
    if(v.back()<=symplex[0].back()) {
      symplex[0]=v;
      for(int i=0;i<n;i++){
        w[i]=beta*symplex[0][i]+(1-beta)*u[i];
      }
    }
    else if(w.back()<=symplex[0].back()) symplex[0]=w;
    else {
      for(auto wsp=0; wsp<n; wsp++){
        for(auto punkty=0; punkty<=n;punkty++){
          symplex[punkty][wsp]=0.5*(symplex[punkty][wsp]+symplex.back()[wsp]);
        }
      }
      
    }
  }


  u.clear();
  v.clear();
  w.clear();
  }

  return symplex.back();
}


double get_energy(const gsl_vector* v,void *params){


      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
      
            std::vector<double> one_body;
            std::vector<double> two_body;
      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

      double R = *(static_cast<double*>(params));
      system->set_parameters(std::vector<double>({gsl_vector_get(v,0),gsl_vector_get(v,1)}),qmt::QmtVector(1,1,R));
      std::cout << "#Get energy for: " << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << std::endl;
//      system->set_parameters(std::vector<double>({1.0,1.0}),qmt::QmtVector(1,1,R));
            
            for(auto i = 0; i < system->get_one_body_number();++i) {
               double integral = system->get_one_body_integral(i); 
               integral = system->get_one_body_integral(i);  
//             std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
               one_body.push_back(integral);
               
             }
                   
            for(auto i = 0; i < system->get_two_body_number();++i){
               auto integral =  system->get_two_body_integral(i);
//             std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
               two_body.push_back(integral);
              }
    

//std::cout << "#Start diagonalization" << std::endl;
double result =   diagonalizer_gsl->Diagonalize(one_body,two_body);
std::cout << "#Energy is " << result << std::endl;
delete diagonalizer_gsl;
delete system;

return result;
}
