#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>

using namespace std;
#define N 10

int main(){
     
   
     //orbitals
     string o1s0="(0,  0,  0,  0)";
     string o1s1="(0,  0,  0,  1)";
     string o1s2="(0,  0,  0,  2)";
     string o1s3="(0,  0,  0,  3)";
     string o1s4="(0,  0,  0,  4)";
     string o2s5="(0,  0,  1.0,  5)"; 
     string o2s6="(0,  0,  1.0,  6)";
     string o2s7="(0,  0,  1.0,  7)"; 
     string o2s8="(0,  0,  1.0,  8)";
     string o2s9="(0,  0,  1.0,  9)";
     string orbitals[N]={o1s0, o1s1, o1s2, o1s3,o1s4,o2s5,o2s6,o2s7,o2s8,o2s9};

    

  


   ofstream file2("ham_2p.dat");
    
    file2 << "one_body" << endl;    

       for(int j=0; j<N; j++)
         for(int i=0; i<N; i++){
           file2 << i << "cup,    " << j << "aup,    " << "1.0,   " << j*N+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
           file2 << i << "cdown,  " << j << "adown,  " << "1.0,   " << j*N+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
    }



    file2 << "end_one_body" << endl << endl << "two_body" << endl;
       int n=0;
    for(int l=0; l<N; l++)
    for(int k=0; k<N; k++)
    for(int j=0; j<N; j++)
    for(int i=0; i<N; i++){
           n=(l*N*N*N+k*N*N+j*N+i);
           file2 << i << "cup,    " << j << "cup,    " << k << "aup,    " << l << "aup,    " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cdown,  " << k << "adown,  " << l << "adown,  " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cup,    " << j << "cdown,  " << k << "adown,  " << l << "aup,    " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cup,    " << k << "aup,    " << l << "adown,  " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;

    }



   file2 << "end_two_body" << endl;
   file2.close();




}


