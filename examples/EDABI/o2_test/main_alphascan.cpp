#include "mpi.h"
#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"
//#include "../../../headers/qmtmpipool.h"
#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
//#include <stdio.h>
#include <stdlib.h>

#define N_a 3

double get_energy(const gsl_vector* v,void *params);





int main(int argc,const char* argv[])
{

MPI_Init(NULL, NULL);
  
int n_proc, id;
MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
MPI_Comm_rank ( MPI_COMM_WORLD, &id );

bool flag=1;


int N=1;
for(size_t i = 0U; i < N;i++)
{


  gsl_vector *ss = gsl_vector_alloc (N_a);
  gsl_vector_set_all (ss, 1.0);
  
  double dR=2.0/N;

  double R = 4.5 + i * dR;

  double p[1]={R};
  double E=get_energy(ss,p);

  
  if(id==0) {
	std::cout<<R<<" ";
	for(int i=0; i<N_a; i++)
		std::cout <<gsl_vector_get (ss, i)<<" ";
	std::cout << E << std::endl;
  }

  gsl_vector_free (ss);
  
}

  MPI_Finalize();  
  return 0;
}


double get_energy(const gsl_vector* v,void *params){
  
  for(int i=0; i<N_a; i++)
	if(gsl_vector_get(v,i)<=0) return 10000000; 
  int n_proc, id;
  MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
  MPI_Comm_rank ( MPI_COMM_WORLD, &id );

  //

  double result;
  qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat");
  double R = static_cast<double*>(params)[0];
     
  std::vector<double> alphas;
  for(int i=0; i<N_a; i++)	
  	alphas.push_back(gsl_vector_get(v,i));
  system->set_parameters(alphas,qmt::QmtVector(1,1,R)); 
  //std::cout << "#R=" << R << " ID: " << id << " a: " << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << std::endl;
  int integral_number = system->get_two_body_number();
  int istep;
  istep=int(integral_number/n_proc)+1;
  std::vector<double> two_body;
  if(id==0){
    two_body.resize(n_proc*istep,0);
    //std::cout << "# " << n_proc << " " << istep << std::endl;
  }
            
  std::vector<double> process_intergrals(istep,0);
//id*istep+i - numer calki liczonej przez proces id
  for(int i=0; (i<istep && (id*istep+i)<integral_number); i++){
    auto integral =  system->get_two_body_integral(id*istep+i);
//  std::cout<<"two_body_integral#"<<id*istep+i<<" = "<<integral<<std::endl;
    process_intergrals[i]=integral;
    }
    
    MPI_Gather(process_intergrals.data(), istep, MPI_DOUBLE, two_body.data(), istep, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    if(id==0){
      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
      std::vector<double> one_body;     
      for(auto i = 0; i < system->get_one_body_number();++i) {
        double integral = system->get_one_body_integral(i); 
        integral = system->get_one_body_integral(i);  
//      std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
        one_body.push_back(integral);
        }
//    ostatni proces liczyl mniejsza liczbe calek i zamiast nich przekazal do Gather wartosc 0
      two_body.resize(integral_number);
      result =   diagonalizer_gsl->Diagonalize(one_body,two_body);
      delete diagonalizer_gsl;
    }
  
  MPI_Bcast(&result,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  delete system;

  return result;
}

