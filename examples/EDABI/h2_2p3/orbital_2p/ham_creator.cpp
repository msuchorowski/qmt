#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>

using namespace std;


int main(){
     
   
     //orbitals
     string o1s0="(0,  0,  0,  0)";
     string o1s1="(0,  0,  0,  1)";
     string o1s2="(0,  0,  0,  2)";
     string o1s3="(0,  0,  0,  3)"; 
     string o1s4="(0,  0,  0,  4)";
     string o2s5="(0,  0,  1.0,  5)";
     string o2s6="(0,  0,  1.0,  6)";
     string o2s7="(0,  0,  1.0,  7)";
     string o2s8="(0,  0,  1.0,  8)";
     string o2s9="(0,  0,  1.0,  9)";
       string orbitals[10]={o1s0, o1s1, o1s2, o1s3, o1s4, o2s5, o2s6, o2s7, o2s8, o2s9};



  


   ofstream file2("ham_2p.dat");
    
    file2 << "one_body" << endl;    

       for(int j=0; j<10; j++)
         for(int i=0; i<10; i++){
           file2 << i << "cup,    " << j << "aup,    " << "1.0,   " << j*10+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
           file2 << i << "cdown,  " << j << "adown,  " << "1.0,   " << j*10+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
    }



    file2 << "end_one_body" << endl << endl << "two_body" << endl;
    int a,b,c,d;
       int n=0;
    for(int l=0; l<10; l++)
    for(int k=0; k<10; k++)
    for(int j=0; j<10; j++)
    for(int i=0; i<10; i++){
           n=(l*10*10*10+k*10*10+j*10+i);
/*            if(l<=4) d=0;
            else d=1;
            if(k<=4) c=0;
          else c=1;
            if(j<=4) b=0;
            else b=1;
            if(i<=4) a=0;
            else a=1;
           n=(d*2*2*2+c*2*2+b*2+a);*/
           file2 << i << "cup,    " << j << "cup,    " << k << "aup,    " << l << "aup,    " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cdown,  " << k << "adown,  " << l << "adown,  " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cup,    " << j << "cdown,  " << k << "adown,  " << l << "aup,    " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cup,    " << k << "aup,    " << l << "adown,  " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;

    }



   file2 << "end_two_body" << endl;
   file2.close();




}


