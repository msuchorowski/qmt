set terminal postscript eps enhanced color


set size ratio 0.7
set xlabel 'R [a_0]'
set ylabel '{/Symbol a} [a_0^{-1}]'
set xrange [0.1:4]
set yrange [0.7: 1.7]
set arrow from 0.1, 1 to 4, 1 nohead dt (50,6,2,6)
set out 'QMTalfa.eps'
plot 'data_EDABI.dat' u 1:3 w l lt rgb 'red' lw 3 t '1s EDABI'
