#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>

using namespace std;
#define N 15

int main(){
     
   
     //orbitals
     	std::string o1s0="(0,  0,  0,  0)";
     	std::string o1s1="(0,  0,  0,  1)";
	std::string o1s2="(0,  0,  0,  2)";
	std::string o1s3="(0,  0,  0,  3)";
	std::string o1s4="(0,  0,  0,  4)";

     	std::string o2s0="(0,  0,  1,  5)";
     	std::string o2s1="(0,  0,  1,  6)";
     	std::string o2s2="(0,  0,  1,  7)";
     	std::string o2s3="(0,  0,  1,  8)";
     	std::string o2s4="(0,  0,  1,  9)";

     	std::string o3s0="(0,  1,  0.5,  10)";
     	std::string o3s1="(0,  1,  0.5,  11)"; 
     	std::string o3s2="(0,  1,  0.5,  12)";
     	std::string o3s3="(0,  1,  0.5,  13)";
     	std::string o3s4="(0,  1,  0.5,  14)";
       	string orbitals[N]={o1s0, o1s1, o1s2, o1s3, o1s4,o2s0, o2s1, o2s2, o2s3, o2s4,o3s0, o3s1, o3s2, o3s3, o3s4};



  


   ofstream file2("ham_h2o.dat");
    
    file2 << "one_body" << endl;    

       for(int j=0; j<N; j++)
         for(int i=0; i<N; i++){
           file2 << i << "cup,    " << j << "aup,    " << "1.0,   " << j*N+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
           file2 << i << "cdown,  " << j << "adown,  " << "1.0,   " << j*N+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
    }



    file2 << "end_one_body" << endl << endl << "two_body" << endl;
    int a,b,c,d;
       int n=0;
    for(int l=0; l<N; l++)
    for(int k=0; k<N; k++)
    for(int j=0; j<N; j++)
    for(int i=0; i<N; i++){
           n=(l*N*N*N+k*N*N+j*N+i);
/*            if(l<=4) d=0;
            else d=1;
            if(k<=4) c=0;
          else c=1;
            if(j<=4) b=0;
            else b=1;
            if(i<=4) a=0;
            else a=1;
           n=(d*2*2*2+c*2*2+b*2+a);*/
           file2 << i << "cup,    " << j << "cup,    " << k << "aup,    " << l << "aup,    " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cdown,  " << k << "adown,  " << l << "adown,  " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cup,    " << j << "cdown,  " << k << "adown,  " << l << "aup,    " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cup,    " << k << "aup,    " << l << "adown,  " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;

    }



   file2 << "end_two_body" << endl;
   file2.close();




}


