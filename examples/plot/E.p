set terminal postscript eps enhanced color
set size ratio 0.7
set xlabel 'R [a_0]'
set ylabel 'E_G [Ry]'
set yrange [ -4 : 0 ]
set xrange [0 : 6]
set arrow from 1.43, graph 0 to 1.43, graph 1 nohead lw 1.5 lt 2 dt 2
set arrow from 0, -2.29587 to 8, -2.29587 nohead lw 1.5 lt 2 dt 2
set arrow from 0, -2 to 8, -2 nohead dt (50,6,2,6)
set out 'QMTE.eps'
plot 'data_EDABI.dat' u 1:2 w l lt rgb 'red' lw 0.5 t 'EDABI','data.dat' u 1:3 w l lt 1 lw 0.5 t 'QMT EDABI 1s','h2_2s.o377397' u 1:4 w l lt rgb 'red' lw 0.5 t 'QMT EDABI 2s','data_VMC.dat' u 1:5 w p lt 2 ps 1 t 'QMT VMC 1s','data2px.dat' u 1:5 w p lt 8 ps 1 t 'QMT EDABI 1s2s2p_x','data_2s_VMC.dat' u 1:4 w p lt 9 ps 1 t 'QMT 2s VMC'




