set terminal postscript eps enhanced color
set size ratio 0.7
set xlabel 'R [a_0]'
set ylabel 'E_G [Ry]'
set yrange [ -2.4 : 0 ]
set xrange [0 : 6]
set arrow from 1.43, graph 0 to 1.43, graph 1 nohead lw 1.5 lt 2 dt 2
set arrow from 0, -2.29587 to 8, -2.29587 nohead lw 1.5 lt 2 dt 2
set arrow from 0, -2 to 8, -2 nohead dt (50,6,2,6)
set out 'QMTE.eps'
plot 'data2s.dat' u 1:4 w p lt 7 ps 0.5 t 'QMT EDABI 2s'
