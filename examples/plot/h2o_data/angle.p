set terminal postscript eps enhanced color
set size ratio 0.7
set xlabel 'angle [^o]'
set ylabel 'E [Ry]'
set out 'angle.eps'
set nokey
plot 'data.dat' u 9:7 w p lt 8 ps 1 
