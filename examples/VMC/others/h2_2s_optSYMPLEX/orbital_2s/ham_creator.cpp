#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>

using namespace std;


int main(){
     
   
     //orbitals
     string o1s0="(0,  0,  0,  0)";
     string o1s1="(0,  0,  0,  1)";
     string o2s2="(0,  0,  1.0,  2)";
     string o2s3="(0,  0,  1.0,  3)";
       string orbitals[4]={o1s0, o1s1, o2s2, o2s3};



  


   ofstream file2("ham_2s_full.dat");
    
    file2 << "one_body" << endl;    

       for(int j=0; j<4; j++)
         for(int i=0; i<4; i++){
           file2 << i << "cup,    " << j << "aup,    " << "1.0,   " << j*4+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
           file2 << i << "cdown,  " << j << "adown,  " << "1.0,   " << j*4+i << ",     " << orbitals[i] << ",      " << orbitals[j] << ";" << endl;
    }



    file2 << "end_one_body" << endl << endl << "two_body" << endl;
       int n=0;
    for(int l=0; l<4; l++)
    for(int k=0; k<4; k++)
    for(int j=0; j<4; j++)
    for(int i=0; i<4; i++){
           n=(l*4*4*4+k*4*4+j*4+i)*2;
           file2 << i << "cup,    " << j << "cup,    " << k << "aup,    " << l << "aup,    " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cdown,  " << k << "adown,  " << l << "adown,  " << "0.5,   " << n << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cup,    " << j << "cdown,  " << k << "adown,  " << l << "aup,    " << "0.5,   " << n+1 << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;
           file2 << i << "cdown,  " << j << "cup,    " << k << "aup,    " << l << "adown,  " << "0.5,   " << n+1 << ",     " << orbitals[i] << ",      " << orbitals[j] << ",      " << orbitals[l] << ",      " << orbitals[k] << ";" << endl;

    }



   file2 << "end_two_body" << endl;
   file2.close();




}


