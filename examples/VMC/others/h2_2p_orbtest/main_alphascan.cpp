#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <utility>
#include <algorithm>

double get_energy(const gsl_vector* v,void *params);



int main(int argc,const char* argv[])
{

 double R = 1.32;

  gsl_vector *x;
  
  x = gsl_vector_alloc (3);
  gsl_vector_set (x, 0, 1.22);
  gsl_vector_set (x, 1, 1.2);
  gsl_vector_set (x, 2, 1.2);


  
  std::string orbital1[5]={"m","m","p","p",""};
  std::string orbital2[5]={"m","p","m","p",""};
  double En[4]={0};
  
  for(int i=0; i<5; i++){
    std::ofstream file2("defs.dat");
    for(int j=0; j<6; j++){
      file2 << "{[" << j << "] [0,0,";
      if(j<3) file2 << 0;
      else file2 << 1;
      file2 << ".0] (0) [1s, 0, 0, 1, 0, 5] (1) [1s,0,0,0,0,5]  (2) [2s, 0, 0, 0, 1, 7] (3) [2s,0,0,1,1,7] (4) [2p_z";
      file2 << orbital1[i] << ",0,0,1,2,9] (5) [2p_z" << orbital2[i] << ",0,0,0,2,9]}" << std::endl;
    }
    file2 << "< 0 , 0 , 1 >" << std::endl << "< 0 , 1 , 0 >" << std::endl << "< 1 , 0 , 0 >" << std::endl << "< 0 , 1 , 0 >" << std::endl << "< 0 , 0 , 1 >" << 
    std::endl << "< 0 , 1 , 0 >" << std::endl << "open_betas" << std::endl << 
    "1.0 , 0.5, 0.0, 0.0, 1.0 , 0.5" << std::endl << "close_betas" << std::endl << "open_atomic_numbers" << std::endl << "1,1,1,1,1,1" << std::endl << "close_atomic_numbers";  
    file2.close();
    En[i]=get_energy(x,&R);
    //std::cout << En[i] << std::endl;
  }
  for(int i=0; i<5; i++) std::cout << En[i] << std::endl;
  std::cout << "MIN E=" <<  std::min(std::min(std::min(En[0],En[1]),En[2]),En[3]) << std::endl;
	
  std::cout << std::max(std::max(std::max(En[0],En[1]),En[2]),En[3])-std::min(std::min(std::min(En[0],En[1]),En[2]),En[3]) << std::endl;
	return 0;
}



double get_energy(const gsl_vector* v,void *params){
std::cout << "#Get energy for R=" << *(static_cast<double*>(params)) <<  std::endl;
std::cout << "#And alpha:" << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << " " << gsl_vector_get(v,2) << std::endl;
      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
 
            std::vector<double> one_body;
            std::vector<double> two_body;
      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 

      double R = *(static_cast<double*>(params));
      system->set_parameters(std::vector<double>({gsl_vector_get(v,0),gsl_vector_get(v,1),gsl_vector_get(v,2)}),qmt::QmtVector(1,1,R));
//      system->set_parameters(std::vector<double>({1.0,1.0}),qmt::QmtVector(1,1,R));
            
            for(auto i = 0; i < system->get_one_body_number();++i) {
               double integral = 0.0; 
               integral = system->get_one_body_integral(i);  
             std::cout<<"#one_body_integral#"<<i<<" = "<<integral<<std::endl;
               one_body.push_back(integral);
               
             }
  //          std::cout << system->get_two_body_number() << std::endl;       
            for(auto i = 0; i < system->get_two_body_number();++i){
               auto integral =  system->get_two_body_integral(i);
             std::cout<<"#two_body_integral#"<<i<<" = "<<integral<<std::endl;
               two_body.push_back(integral);
              }
    
  

std::cout << "#start diagonalization" << std::endl;
double result =   diagonalizer_gsl->Diagonalize(one_body,two_body);

delete diagonalizer_gsl;
delete system;
std::cout  << "#Res: E=" << result << std::endl;
return result;
}