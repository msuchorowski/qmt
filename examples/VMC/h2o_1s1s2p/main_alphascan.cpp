#include "mpi.h"
#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"
//#include "../../../headers/qmtmpipool.h"
#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
//#include <stdio.h>
#include <stdlib.h>

#define N_a 4

double get_energy(const gsl_vector* v,void *params);





int main(int argc,const char* argv[])
{

MPI_Init(NULL, NULL);
  
int n_proc, id;
MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
MPI_Comm_rank ( MPI_COMM_WORLD, &id );

int N=20;

double r_s,r_max,R_s,R_max;

 
	r_s=1.05;
	r_max=1.05;
	R_s=2.87;
	R_max=2.87;


for(double R = R_s; R <= R_max;R+=0.05){
  	
	for(double r = r_s; r <= r_max;r+=0.02){
		size_t iter = 0;
  		int status;

  		const gsl_multimin_fminimizer_type *T;
  		gsl_multimin_fminimizer *s;

 		/* Set initial step sizes to 1 */
  		gsl_vector *ss = gsl_vector_alloc (N_a);
  		gsl_vector_set_all (ss, 1.0);
  
   
  		gsl_vector *x;
  		gsl_multimin_function my_func;

  		my_func.n = N_a;
  		my_func.f = get_energy;
  		double p[2]={R,r};
  		my_func.params = p;
  
  
  		x = gsl_vector_alloc (N_a);
  		//gsl_vector_set_all (x, 1.5);
		//1.01931 7.42182 8.8552 2.3047
  		gsl_vector_set (x, 0, 1.01931);
		gsl_vector_set (x, 1, 7.42182);
		gsl_vector_set (x, 2, 8.8552);
		gsl_vector_set (x, 3, 2.3047);
  

  		T = gsl_multimin_fminimizer_nmsimplex2;
  		s = gsl_multimin_fminimizer_alloc (T, N_a);
  		if (id==0) std::cout <<"#SETTING MINMAX" << std::endl;
  		gsl_multimin_fminimizer_set (s, &my_func, x, ss);
		if (id==0) std::cout <<"#DONE" << std::endl;
  		MPI_Barrier(MPI_COMM_WORLD);
		break;
  		do
  		  {
  		    //if (id==0) std::cout <<"#ITER " << iter <<  std::endl;
  		    iter++;
  		    status = gsl_multimin_fminimizer_iterate(s);
  		    MPI_Barrier(MPI_COMM_WORLD);
  		    if (status)
  		      break;

  		    double size = gsl_multimin_fminimizer_size (s);
  		    status = gsl_multimin_test_size (size, 1e-2);
		
  		  }
  		while (status == GSL_CONTINUE && iter < 100);
  
  		if(id==0) {
			std::cout<<R<<" " << r << " ";
			for(int i=0; i<N_a; i++)
				std::cout <<gsl_vector_get (s->x, i)<<" ";
			std::cout << s->fval << " " << 2*atan(R/(2*r)) << " " << 2*atan(R/(2*r))*180/M_PI << std::endl;
  		}
  		gsl_multimin_fminimizer_free (s);
  		gsl_vector_free (x);
  
		}
	}
  		MPI_Finalize();  
  		return 0;
}


double get_energy(const gsl_vector* v,void *params){
  
  for(int i=0; i<N_a; i++)
	if(gsl_vector_get(v,i)<=0) return 10000000; 
  int n_proc, id;
  MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
  MPI_Comm_rank ( MPI_COMM_WORLD, &id );
  double result;
  qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat");
  double R = static_cast<double*>(params)[0];
  double r = static_cast<double*>(params)[1];
  //R=2.7;
  //r=0.85;
  //double myints[] = {1.25203, 7.63489, 2.379, 2.17258};
  //std::vector<double> alphas (myints, myints + sizeof(myints) / sizeof(double) );   
  std::vector<double> alphas;
  for(int i=0; i<N_a; i++)	
  	alphas.push_back(gsl_vector_get(v,i));
  system->set_parameters(alphas,qmt::QmtVector(1,r,R)); 
  //std::cout << "#R=" << R << " ID: " << id << " a: " << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << std::endl;
  int integral_number = system->get_two_body_number();
  int istep;
  istep=int(integral_number/n_proc)+1;
  std::vector<double> two_body;
  if(id==0){
    two_body.resize(n_proc*istep,0);
    //std::cout << "# " << n_proc << " " << istep << std::endl;
  }
            
  std::vector<double> process_intergrals(istep,0);
//id*istep+i - numer calki liczonej przez proces id
  for(int i=0; (i<istep && (id*istep+i)<integral_number); i++){
    auto integral =  system->get_two_body_integral(id*istep+i);
//  std::cout<<"two_body_integral#"<<id*istep+i<<" = "<<integral<<std::endl;
    process_intergrals[i]=integral;
    }
    
    MPI_Gather(process_intergrals.data(), istep, MPI_DOUBLE, two_body.data(), istep, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    if(id==0){
      //qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<28> ("ham_h2o.dat",1U,1U,200000U);
      std::vector<double> one_body;     
      for(auto i = 0; i < system->get_one_body_number();++i) {
        double integral = system->get_one_body_integral(i); 
        integral = system->get_one_body_integral(i);  
//      std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
        one_body.push_back(integral);
        }
//    ostatni proces liczyl mniejsza liczbe calek i zamiast nich przekazal do Gather wartosc 0
      two_body.resize(integral_number);
      result =   diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)+2/R+2*2*8/(sqrt((0.5*R)*(0.5*R)+r*r));
      delete diagonalizer_vmc_gsl;
    }
  
  MPI_Bcast(&result,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  delete system;
  std::cout << "E_test=" << result << std::endl;
  return result;
}







