#include "mpi.h"
#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"
//#include "../../../headers/qmtmpipool.h"
#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
//#include <stdio.h>
#include <stdlib.h>

#define N_a 3

double get_energy(const gsl_vector* v,void *params);





int main(int argc,const char* argv[])
{

MPI_Init(NULL, NULL);
  
int n_proc, id;
MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
MPI_Comm_rank ( MPI_COMM_WORLD, &id );

bool flag=1;


int N=15;
for(size_t i = 0U; i < N;i++)
{
size_t iter = 0;
  int status;

  const gsl_multimin_fminimizer_type *T;
  gsl_multimin_fminimizer *s;

 /* Set initial step sizes to 1 */
  gsl_vector *ss = gsl_vector_alloc (N_a);
  gsl_vector_set_all (ss, 1.0);
  
  double dR=6.0/N;

  double R = 0.1 + i * dR;
  if(flag==0) {R+=dR*0.25; flag=1;}
  else if( (R>=1 && R<2) && flag ) {flag=0; i--; }
   
  gsl_vector *x;
  gsl_multimin_function my_func;

  my_func.n = N_a;
  my_func.f = get_energy;
  double p[1]={R};
  my_func.params = p;
  
  /* Starting point, x = (5,7) */
  
  x = gsl_vector_alloc (N_a);
  gsl_vector_set_all (x, 1.5);
  //gsl_vector_set (x, i, 1.5);
  

  T = gsl_multimin_fminimizer_nmsimplex2;
  s = gsl_multimin_fminimizer_alloc (T, N_a);
  //if (id==0) std::cout <<"#SETTING MINMAX" << std::endl;
  gsl_multimin_fminimizer_set (s, &my_func, x, ss);
  MPI_Barrier(MPI_COMM_WORLD);
  do
    {
      //if (id==0) std::cout <<"#ITER " << iter <<  std::endl;
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);
      MPI_Barrier(MPI_COMM_WORLD);
      if (status)
        break;

      double size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);


    }
  while (status == GSL_CONTINUE && iter < 100);
  
  if(id==0) {
	std::cout<<R<<" ";
	for(int i=0; i<N_a; i++)
		std::cout <<gsl_vector_get (s->x, i)<<" ";
	std::cout << s->fval << std::endl;
  }
  gsl_multimin_fminimizer_free (s);
  gsl_vector_free (x);
  
}

  MPI_Finalize();  
  return 0;
}


double get_energy(const gsl_vector* v,void *params){
  
  for(int i=0; i<N_a; i++)
	if(gsl_vector_get(v,i)<=0) return 10000000; 
  int n_proc, id;
  MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
  MPI_Comm_rank ( MPI_COMM_WORLD, &id );

  //

  double result;
  qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat");
  double R = static_cast<double*>(params)[0];
     
  std::vector<double> alphas;
  for(int i=0; i<N_a; i++)	
  	alphas.push_back(gsl_vector_get(v,i));
  system->set_parameters(alphas,qmt::QmtVector(1,1,R)); 
  //std::cout << "#R=" << R << " ID: " << id << " a: " << gsl_vector_get(v,0) << " " << gsl_vector_get(v,1) << std::endl;
  int integral_number = system->get_two_body_number();
  int istep;
  istep=int(integral_number/n_proc)+1;
  std::vector<double> two_body;
  if(id==0){
    two_body.resize(n_proc*istep,0);
    //std::cout << "# " << n_proc << " " << istep << std::endl;
  }
            
  std::vector<double> process_intergrals(istep,0);
//id*istep+i - numer calki liczonej przez proces id
  for(int i=0; (i<istep && (id*istep+i)<integral_number); i++){
    auto integral =  system->get_two_body_integral(id*istep+i);
//  std::cout<<"two_body_integral#"<<id*istep+i<<" = "<<integral<<std::endl;
    process_intergrals[i]=integral;
    }
    
    MPI_Gather(process_intergrals.data(), istep, MPI_DOUBLE, two_body.data(), istep, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    if(id==0){
      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
      std::vector<double> one_body;     
      for(auto i = 0; i < system->get_one_body_number();++i) {
        double integral = system->get_one_body_integral(i); 
        integral = system->get_one_body_integral(i);  
//      std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
        one_body.push_back(integral);
        }
//    ostatni proces liczyl mniejsza liczbe calek i zamiast nich przekazal do Gather wartosc 0
      two_body.resize(integral_number);
      result =   diagonalizer_gsl->Diagonalize(one_body,two_body);
      delete diagonalizer_gsl;
    }
  
  MPI_Bcast(&result,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  delete system;

  return result;
}

