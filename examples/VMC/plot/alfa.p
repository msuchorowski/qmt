set terminal postscript eps enhanced color


set size ratio 0.7
set xlabel 'R [a_0]'
set ylabel '{/Symbol a} [a_0^{-1}]'
set xrange [0.1:4]
set yrange [0.7: 1.7]
set arrow from 0.1, 1 to 4, 1 nohead dt (50,6,2,6)
set out 'QMTalfa.eps'
plot 'data_EDABI.dat' u 1:3 w l lt rgb 'red' lw 0.5 t 'EDABI','data.dat' u 1:2 w l lt 1 lw 0.5 t 'QMT EDABI 1s','data2s.dat' u 1:2 w p lt 7 ps 0.5 t 'QMT EDABI 2s a_1', 'data2s.dat' u 1:3 w p lt 8 ps 0.5 t 'QMT EDABI 2s a_2','data2px.dat' u 1:2 w p lt 3 ps 1 t 'QMT EDABI 1s2s2p_x a_1','data2px.dat' u 1:3 w p lt 4 ps 1 t 'QMT EDABI 1s2s2p_x a_2','data2px.dat' u 1:4 w p lt 5 ps 1 t 'QMT EDABI 1s2s2p_x a_3'
