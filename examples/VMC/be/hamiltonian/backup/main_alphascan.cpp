#define GSL_LIBRARY
#include "../../../headers/qmtlncdiag.h"
#include "../../../qmtvmc/qmtvmcdiag.h"
#include "../../../headers/qmtsystemstandard.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



int main(int argc,const char* argv[])
{
  double Rmin=0.01;
  double Rmax=4.0;
  int n=40;
  

  for(double R=Rmin; R<Rmax; R=R+(Rmax-Rmin)/n ){
      
      qmt::QmtSystem *system = new qmt::QmtSystemStandard("conf.dat"); 
      
      system->set_parameters(std::vector<double>({1.0,1.0}),qmt::QmtVector(1,1,R));
      
      double alpha0=1;//atof(argv[1]);;
  //  qmt::QmtDiagonalizer *diagonalizer_vmc_gsl = new qmt::vmc::QmtVmcGslMetropolisDiagonalizer<4> ("ham.dat",1U,1U,20000);
      qmt::QmtDiagonalizer *diagonalizer_gsl = new qmt::QmtLanczosDiagonalizer("conf.dat",0);
      
      double Eed, Evmc;
      double EminED, EminVM, aminED1, aminED2;
      double alpha1, alpha2;
      
      for(double j = 0; j <0.7; j= j + 0.05){
         
         alpha2=alpha0+j;
         for(double k = 0; k < 0.7; k = k + 0.05) {
            
            alpha1 = alpha0 + k;
            std::vector<double> one_body;
            std::vector<double> two_body;
            system->set_parameters(std::vector<double>({alpha1,alpha2}),qmt::QmtVector(1,1,R));
            
            for(auto i = 0; i < system->get_one_body_number();++i) {
               double integral = 0.0; 
               integral = system->get_one_body_integral(i);  
//             std::cout<<"one_body_integral#"<<i<<" = "<<integral<<std::endl;
               one_body.push_back(integral);
               
             }
    
            for(auto i = 0; i < system->get_two_body_number();++i){
               auto integral =  system->get_two_body_integral(i);
//             std::cout<<"two_body_integral#"<<i<<" = "<<integral<<std::endl;
               two_body.push_back(integral);
              }
    
            Eed=diagonalizer_gsl->Diagonalize(one_body,two_body);
  //        Evmc=(diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)+2/R);
            if( (k==0 && j==0) || Eed<EminED) {
               EminED=Eed;
               aminED1=alpha1;
               aminED2=alpha2;
               }
 //         if(k==0 || Evmc<Emin2){
  //           Emin2=Evmc;
  //           amin2=alpha;}
	    //    std::cout<<R<<" "<<alpha1<<"  "<<alpha2<<"  "<< Eed <<std::endl;
	//        std::cout<<R<<" "<<alpha <<" "<<diagonalizer_vmc_gsl->Diagonalize(one_body,two_body)/10<<std::endl;

        }
      }
      std::cout << R << " " << aminED1 << " " << aminED2 << " " <<EminED /*<< " " << amin2 << " " << Emin2 */<< std::endl;
   }
//delete diagonalizer_vmc_gsl;
//delete system;
	return 0;
}

