#include "qmtvmclocalnn.h"


qmt::vmc::QmtVmcLocalNN::QmtVmcLocalNN(size_t id1, size_t id2):_id1(id1),_id2(id2){

}

/*
/QmtVmcLocalObservable implementation
*/

double qmt::vmc::QmtVmcLocalNN::get_value(const QmtConfigurationState& x){
 double v1 = 0;
 double v2 = 0;
	if(x.is_occupied_up(_id1)) v1 += 1.0;
	if(x.is_occupied_down(_id1)) v1 += 1.0;
        if(x.is_occupied_up(_id2)) v2 += 1.0;
	if(x.is_occupied_down(_id2)) v2 += 1.0;
 return v1*v2;
}

/*
/print
*/

std::ostream& qmt::vmc::QmtVmcLocalNN::print(std::ostream& s) const {
 s<<"<N"<<_id1<<"_N"<<_id2<<">";
return s;
}
