#ifndef QMT_VMC_RANDOM_HOPPING_H
#define QMT_VMC_RANDOM_HOPPING_H 

#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include <vector>
#include <memory>
#include <chrono>
#include <ctime>


namespace qmt {
namespace vmc {

class QmtVmcRandomHopping {

size_t N;

std::vector<size_t> up_ones;
std::vector<size_t> up_zeros;

std::vector<size_t> down_ones;
std::vector<size_t> down_zeros;


size_t last_one;
size_t last_zero;

size_t last_sector; //0 - up, 1 - down

size_t seed;

std::default_random_engine generator;
typedef std::chrono::high_resolution_clock myclock;

void prepare_seed();
size_t rnd(size_t max);

std::shared_ptr<qmt::vmc::QmtConfigurationState> state;

public:

QmtVmcRandomHopping(std::shared_ptr<qmt::vmc::QmtConfigurationState> st);

std::shared_ptr<qmt::vmc::QmtConfigurationState> get();

void update(); 


};


}
}

#endif
