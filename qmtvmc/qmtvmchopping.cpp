#include "qmtconfiguration.h"



/*
qmt::vmc::QmtVmcHopping::QmtVmcHopping(qmt::vmc::QmtConfigurationState* from,
                                       qmt::vmc::QmtConfigurationState* to,
                                       const size_t id):from_state(from),to_state(to), _id(id)
{ }
*/


qmt::vmc::QmtVmcHopping::QmtVmcHopping(std::shared_ptr<qmt::vmc::QmtConfigurationState> from,
                                       std::shared_ptr<qmt::vmc::QmtConfigurationState> to,
                                       const size_t id,size_t f_id, size_t t_id,int sector,const double f):from_state(from),to_state(to), _id(id),from_id(f_id),to_id(t_id),_sector(sector),_factor(f)
{ }



const qmt::vmc::QmtConfigurationState&  qmt::vmc::QmtVmcHopping::get_from() const {
    return *from_state;
}

const qmt::vmc::QmtConfigurationState&  qmt::vmc::QmtVmcHopping::get_to() const {
    return *to_state;
}

std::shared_ptr<qmt::vmc::QmtConfigurationState>  qmt::vmc::QmtVmcHopping::get_from_ptr()  {
return from_state;}

std::shared_ptr<qmt::vmc::QmtConfigurationState> qmt::vmc::QmtVmcHopping::get_to_ptr()  {
    return to_state;
}



size_t qmt::vmc::QmtVmcHopping::get_id() const {
    return _id;
}

double qmt::vmc::QmtVmcHopping::get_factor() const {
    return _factor;
}

int qmt::vmc::QmtVmcHopping::get_sector() const {
    return _sector;
} 
