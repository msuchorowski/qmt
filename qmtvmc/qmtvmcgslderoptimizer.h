#ifndef QMT_VMC_GSL_DER_OPTIMIZER_H
#define QMT_VMC_GSL_DER_OPTIMIZER_H

#include "qmtvmcoptimizer.h"
#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include "../headers/qmtsimulatedannealing.h"

namespace qmt {
 namespace vmc {
   
   class QmtVmcGslDerOptimizer : public QmtVmcOptimizer {
   
   static double get_value(const gsl_vector * x, void * params);
    
   static void get_gradient(const gsl_vector *x, void *params, gsl_vector *df);

   static void get_value_and_gradient(const gsl_vector *x, void *params, double *out, gsl_vector *df);
 
   struct Params {
      QmtVmcData* d;
      QmtVmcAlgorithm* alg; 
      size_t steps;
      size_t probing_steps;
   };
   
   public:
     
     double optimize( QmtVmcData& d, 
		      QmtVmcAlgorithm& alg, 
                     const std::vector<std::tuple<double,double,double>>& in_par,
                     std::vector<double>& out_par,
		     size_t steps,
		     size_t probing_steps);
     
   };
   
 }    
}



#endif 
