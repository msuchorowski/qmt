#ifndef QMT_VMC_JASTROW
#define QMT_VMC_JASTROW

#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include "../headers/qmthbuilder.h"
#include <math.h>
#include <vector>
#include <set>
#include <tuple>
#include <map>
#include "../headers/qmtparsertools.h"
#include <stdlib.h>
#include <fstream>
#include<iostream>

namespace qmt {
namespace vmc {
class QmtVmcJastrow {

     struct SiteConections {
     std::vector<size_t> sp_ids; //numbers spin-band
     std::vector<size_t> param_ids; //Jastrow paramters indicies
     };

    std::vector<std::tuple<size_t,size_t,size_t,size_t>> definitions;
    std::vector<SiteConections> conections;
#ifdef SPIN_DEPENDENT_JASTROW
    std::map<size_t,size_t> spin_map;
    size_t params_size;
    size_t u_params_size;
#endif

public:

    QmtVmcJastrow(const std::vector<std::tuple<size_t,size_t,size_t,size_t>>& defs);

    virtual double operator()(const std::vector<double>& parameters,
                              const qmt::vmc::QmtConfigurationState& configuration) const;
			      
    virtual double operator()(const std::vector<double>& parameters,
                              const qmt::vmc::QmtConfigurationState& from, const qmt::vmc::QmtConfigurationState& to) const;

    virtual void update(const std::vector<double>& parameters,const qmt::vmc::QmtConfigurationState& state){};

    virtual void reset(const std::vector<double> &lambdas){};    

    virtual size_t get_number_of_params() const;
    
    virtual ~QmtVmcJastrow(){}

};



class QmtVmcTwoBodyJastrow {
  
protected:

    struct SiteConections {
     std::vector<size_t> sp_ids; //numbers spin-band
     std::vector<size_t> param_ids; //Jastrow paramters indicies
     std::vector<double> factors; //factor before the term
     size_t size;
     };





    struct Indicies {
    int i;
    int j;
    int k;
    int l;
    };

   
   

   size_t N;                                        //size of the system, i.e. spin sector size which length equals the number of orbitals
   size_t** F;                                        //cut_off matrix
   int **lambda_ids;
   std::vector<double> T;                           //T vector - must be updated each time configuration (see below) changed
   qmt::vmc::QmtConfigurationState *configuration;  //actual configuration - must be checked each time if it changed
   std::vector<SiteConections> conections;
   std::map<size_t,size_t> lambda_map;
   std::vector<double> param_sum;

   bool bcs;
 
  

public:
   
      QmtVmcTwoBodyJastrow(const std::vector<qmt::QmtInteractionDefinition>& defs,size_t sector_size,bool sc = false);
       QmtVmcTwoBodyJastrow(std::ifstream &file,size_t sector_size, bool sc = false);
      virtual void update(const std::vector<double>& parameters,const qmt::vmc::QmtConfigurationState& state);  
      virtual void reset(const std::vector<double> &lambdas);
  //    virtual double operator()(const std::vector<double>& parameters,
    //                          const qmt::vmc::QmtConfigurationState& from, const qmt::vmc::QmtConfigurationState& to);

      virtual double operator()(const std::vector<double>& parameters, const qmt::vmc::QmtConfigurationState& from, const qmt::vmc::QmtConfigurationState& to) const;
      virtual double operator()(const std::vector<double>& parameters, size_t l,size_t k) const;
      virtual double operator()(const std::vector<double>& parameters,size_t i,size_t j,size_t k, size_t l) const;

      virtual size_t get_number_of_params() const;
      virtual int get_jastrow_id(size_t i, size_t j) const;
      virtual size_t get_sector_size() const;
      virtual ~QmtVmcTwoBodyJastrow();

};


////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef SPIN_DEP

class QmtVmcSpinJastrow {


    struct SiteConections {
     std::vector<size_t> sp_id; //numbers spin-band
     std::vector<size_t> param_ids; //Jastrow paramters indicies
     std::vector<double> factors; //factor before the term
     size_t size;
     };





    struct Indicies {
    int i;
    int j;
    int k;
    int l;
    };

   
   

   size_t N;                                        //size of the system, i.e. spin sector size which length equals the number of orbitals
   size_t** F;                                        //cut_off matrix
   int **lambda_ids;
   std::vector<double> T;                           //T vector - must be updated each time configuration (see below) changed
   qmt::vmc::QmtConfigurationState *configuration;  //actual configuration - must be checked each time if it changed
   std::vector<SiteConections> conections;
   std::map<size_t,size_t> lambda_map;
   std::vector<double> param_sum;
   size_t parallel_spin_size;

  
 

public:

      QmtVmcSpinJastrow(std::ifstream &file,size_t sector_size);


      virtual void update(const std::vector<double>& parameters,const qmt::vmc::QmtConfigurationState& state);  
      virtual void reset(const std::vector<double> &lambdas);
      virtual double operator()(const std::vector<double>& parameters, const qmt::vmc::QmtConfigurationState& from, const qmt::vmc::QmtConfigurationState& to) const;
      virtual double operator()(const std::vector<double>& parameters, size_t l,size_t k) const;
      virtual double operator()(const std::vector<double>& parameters,size_t i,size_t j,size_t k, size_t l) const;
      virtual size_t get_number_of_params() const;



};

#endif

class QmtVmcPairOperatorCorrelator {

public:

    double operator()(const std::vector<double>& parameters,
                              const qmt::vmc::QmtConfigurationState& state) const;

};

}
}



#endif // qmtvmcjastrow.h
