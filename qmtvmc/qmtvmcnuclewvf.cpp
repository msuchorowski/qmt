#include "qmtvmcnuclewvf.h"

qmt::vmc::QmtVmcNuclearWaveFunction(const std::vector<QmtVmcIon>& ions, 
				    std::vector<std::tuple<size_t,size_t,int>> upp_params, std::vector<std::tuple<size_t,int>> c_params){

///////////map_matrix creation/////////////

std::vector<size_t> temp;

for(const auto &item : upp_params){
	temp.push_back(std::get<0>(item);
	temp.push_back(std::get<1>(item);
}

std::sort(temp.begin(),temp.end());
auto it = std::unique(temp.begin(),temp.end());
temp.resize(std::distance(temp.begin(),it));

for(size_t i = 0U; i < temp.size();++i)
 if( temp[i] != i) throw std::logic_error("qmt::vmc::QmtVmcNuclearWaveFunction upp paramaters not mapped properly");

map_size = temp.size();

map_matrix = new double* [map_size] ;
for(size_t i = 0U; i < map.size; ++i)
map_matrix[i] = new double [map_size];

for(const auto &item : upp_params){
	auto i = std::get<0>(item);
  	auto j = std::get<1>(item);
        auto v = std::get<2>(item);
      
        map_matrix[i][j] = v;
        map_matrix[j][i] = v;
}

///////////map_matrix creation/////////////

TO DO: c, adressing of pairs etc.


auto fn = [&](const std::tuple<size_t,size_t>& t1, const std::tuple<size_t,size_t>& t2)->bool {

size_t xl = std::get<0>(t1);
size_t yl = std::get<1>(t1);

size_t xr = std::get<0>(t2);
size_t yr = std::get<1>(t2);

size_t temp;

if(xl > yl) {
 temp = yl;
 yl = xl;
 xl = temp;
}

if(xr > yr) {
 temp = yr;
 yr = xr;
 xr = temp;
}


return xl<xr && yl<yr;
};

std::set<std::tuple<size_t,size_t>,std::function<bool(const std::tuple<size_t,size_t>&, const std::tuple<size_t,size_t>&)>> s(fn);

double eps = 0.001;

for(const auto &item : ions){
 for(const auto &item2 : item.neigbours)
  s.insert(std::make_tuple(item.id,item2.id));

ids.push_back(item.id);
lpositions.push_back(item.position);
dpositions.push_back(item.position - qmt::QmtVector(eps,eps,eps));
site_relative_positions.push_back(qmt::QmtVector::norm(item.position - qmt::QmtVector(eps,eps,eps)));
masses.push_back(item.mass);
charges.push_back(item.charge);
}
  
for(const auto &item : s){
  pairs.push_back(std::make_tuple(std::get<0>(item),std::get<1>(item));
  pair_distances.push_back(qmt::QmtVector::norm(dpositions[std::get<0>(item)] - dpositions[std::get<1>(item)]);
}


//Associate pairs with Rs

for(size_t i = 0; i < lpositions.size();++i){
associations.push_back(std::vector<size_t>>);

 for(const auto &item : s){
  if(std::get<0>(item) == i || std::get<1>(item) == i)
  associations[i].push_back(i);
 }
}

}

//////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc::QmtVmcNuclearWaveFunction get_value() const {

double exponent = 0.0;

for(size_t i = 0U; i < pairs.size();++)
 exponent += pair_distances[i] * u_pp[i];

for(size_t i = 0U; i < site_relative_positions.size();++i)
 exponent += site_relative_positions[i] * c[i];


return exp(-exponent);
}

qmt::vmc::QmtVmcNuclearWaveFunction ~QmtVmcNuclearWaveFunction() {
 if(map_matrix != NULL){
   for(size_t i = 0U; i < map.size; ++i)
   delete [] map_matrix[i];

   delete [] map_matrix;
 }
 

}

