#include "qmtvmcrandomhopping.h"

qmt::vmc::QmtVmcRandomHopping::QmtVmcRandomHopping(std::shared_ptr<qmt::vmc::QmtConfigurationState> st){
 prepare_seed();

  N=st->get_size();

 for(size_t i=0U; i < N; ++i){
  if(st->is_occupied_up(i))
    up_ones.push_back(i);
  else
    up_zeros.push_back(i);

  if(st->is_occupied_down(i))
    down_ones.push_back(i);
  else
    down_zeros.push_back(i);

 }

 state=st;
}

void qmt::vmc::QmtVmcRandomHopping::prepare_seed()
{
        myclock::time_point beginning = myclock::now();
        myclock::duration d = myclock::now() - beginning;
        seed = d.count();
	generator.seed(seed);
}


size_t qmt::vmc::QmtVmcRandomHopping::rnd(size_t max)
{
  std::uniform_int_distribution<size_t> distribution(0,max);
  return distribution(generator);
}


std::shared_ptr<qmt::vmc::QmtConfigurationState> qmt::vmc::QmtVmcRandomHopping::get() {

  auto p=std::shared_ptr<qmt::vmc::QmtConfigurationState>(state->get_copy());
//std::cout<<"OK"<<std::endl;
  if(rnd(1U) < 1U){
    size_t from = up_ones[rnd(up_ones.size()-1U)];
    size_t to = up_zeros[rnd(up_zeros.size()-1U)];
    p->set_bit(from,false);
    p->set_bit(to,true);
    last_sector = 0U;
//std::cout<<"UP from = "<<from<<" to = "<<to<<std::endl;
    last_one = to;
    last_zero = from;
  }
  else{
    size_t from = down_ones[rnd(down_ones.size()-1U)];
    size_t to = down_zeros[rnd(down_zeros.size()-1U)];
    p->set_bit(from + N,false);
    p->set_bit(to + N,true);
    last_sector = 1U;
//std::cout<<"DOWN from = "<<from<<" to = "<<to<<std::endl;
    last_one = to;
    last_zero = from;

  }
 return p;
}

void  qmt::vmc::QmtVmcRandomHopping::update(){

 size_t off = 0U;
 if(last_sector > 0U)
  off = N;
 
 state->set_bit(last_zero + off,false);
 state->set_bit(last_one + off,true);

 up_ones.clear();
 up_zeros.clear();

 down_ones.clear();
 down_zeros.clear();


 for(size_t i=0U; i < N; ++i){
  if(state->is_occupied_up(i))
    up_ones.push_back(i);
  else
    up_zeros.push_back(i);

  if(state->is_occupied_down(i))
    down_ones.push_back(i);
  else
    down_zeros.push_back(i);

 }

}
