#ifndef QMTVMCSINGLEPARTICLEPROBLEMSOLVER_H
#define QMTVMCSINGLEPARTICLEPROBLEMSOLVER_H
#ifdef GSL_LIBRARY	// to use GSL

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>

#include <iostream>
#include <vector>
#include<gsl/gsl_min.h>


#include "../headers/qmtmatrixformula.h"
#include "../headers/MatrixAlgebras/GSLAlgebra.h"
#include "../headers/qmtgslwrappers.h"
#include "qmtslaterstate.h"

#include <time.h>
#include <stdlib.h>

namespace qmt{
namespace vmc{

template<class MyHamiltonian>
class QmtVmcSingleParticleProblemSolver{

	typedef typename LocalAlgebras::GSLalgebra LA;
	typedef typename LA::Matrix Matrix;
	typedef typename LA::Vector Vector;
	typedef typename MyHamiltonian::NState NState;

	MyHamiltonian* hamiltonian;
	Matrix Hamiltonian_Matrix;
	Matrix eigenvectors;
	Vector eigenvalues;

	qmt::QmtGeneralMatrixFormula<MyHamiltonian,LA> formula;

	gsl_eigen_symmv_workspace * QRworkspace;

	std::vector<NState> basis;

public:
	QmtVmcSingleParticleProblemSolver(MyHamiltonian* _hamiltonian, std::vector<NState>& _basis){
		hamiltonian=_hamiltonian;
		basis=_basis;
		LA::InitializeMatrix(Hamiltonian_Matrix,basis.size(),basis.size());
		LA::InitializeMatrix(eigenvectors,basis.size(),basis.size());
		LA::InitializeVector(eigenvalues,basis.size());

		#ifdef _QMT_VMC_DEBUG
		std::cout<<"QmtVmcSingleParticleProblemSolver:: setting hamiltonian..."<<std::endl;
		#endif

		formula.set_hamiltonian(*hamiltonian);
		#ifdef _QMT_VMC_DEBUG
		std::cout<<"Done"<<std::endl;
		#endif
		
		formula.set_States(basis);

		QRworkspace=gsl_eigen_symmv_alloc(basis.size());
	}

	~QmtVmcSingleParticleProblemSolver(){
		LA::DeinitializeMatrix(Hamiltonian_Matrix);
		LA::DeinitializeMatrix(eigenvectors);
		LA::DeinitializeVector(eigenvalues);
		gsl_eigen_symmv_free(QRworkspace);
	}

	void get_single_particle_eigenstates(unsigned int number_of_electrons,size_t nups, size_t ndowns,qmt::vmc::QmtSlaterState& uncorrelated_state,const std::vector<double>& microscopic_parameters,const std::vector<double> &hopping_lines = std::vector<double>(0)){

                

		#ifdef _QMT_VMC_DEBUG
		std::cout<<"QmtVmcSingleParticleProblemSolver::solving system..."<<std::endl;
		#endif
		
		if(microscopic_parameters.size() != hamiltonian->number_of_one_body()){
			throw std::logic_error("QmtVmcSingleParticleProblemSolver: get_single_particle_eigenstates: Number of microscopic parameters must be the same in both hamiltonian and vector of values!");
		}
		uncorrelated_state.reset();
                srand (time(NULL));
                
		for (unsigned int i=0; i<microscopic_parameters.size(); i++){
                auto param = microscopic_parameters[i];
			formula.set_microscopic_parameter(1,i,param);
		}

		formula.get_Hamiltonian_Matrix(Hamiltonian_Matrix);

                std::ifstream file("hopplines.dat");

                std::vector<std::string> definitions;  
                if(hopping_lines.size() > 0 && !file.fail()) {
                      std::string line;
                 if (file.is_open())
                 {
                  while ( std::getline (file,line) )
                  {
                    definitions.push_back(line);

                  }
                }
              }


              for(const auto& definition : definitions) {
                   auto def = qmt::parser::get_bracketed_words(definition,'<','>',true);
                   size_t i = std::stoul(def[0]);
                   auto items = qmt::parser::get_bracketed_words(definition,'(',')',true);
                 for(const auto& item : items) {
                   auto pair = qmt::parser::get_delimited_words(",",item);
                   size_t j = std::stoul(pair[0]);
                   double val = hopping_lines[std::stoul(pair[1])];
                    double actual_v1 = gsl_matrix_get(Hamiltonian_Matrix,i,j);
                    double actual_v2 = gsl_matrix_get(Hamiltonian_Matrix,i + basis.size()/2,j + basis.size()/2);
                   gsl_matrix_set(Hamiltonian_Matrix,i,j,val + actual_v1);
                   gsl_matrix_set(Hamiltonian_Matrix,i + basis.size()/2,j + basis.size()/2,val + actual_v2);

                 }
              }
		

		for (int i = 0; i < basis.size(); i++){
		    for (int j = 0; j < basis.size(); j++){
               // if(fabs(gsl_matrix_get (Hamiltonian_Matrix, i, j) - gsl_matrix_get (Hamiltonian_Matrix, j, i))/fabs(gsl_matrix_get (Hamiltonian_Matrix, i, j))> 1.e-10)     
		if(fabs(gsl_matrix_get (Hamiltonian_Matrix, i, j) - gsl_matrix_get (Hamiltonian_Matrix, j, i))> 1.e-7) {
			std::cerr<<gsl_matrix_get (Hamiltonian_Matrix, i, j)<<" , ->";
                     std::cerr<<gsl_matrix_get (Hamiltonian_Matrix, j, i)<<" ,";
                     std::cerr << i*basis.size()+j << " " << fabs(gsl_matrix_get (Hamiltonian_Matrix, i, j) - gsl_matrix_get (Hamiltonian_Matrix, j, i)) << std::endl << fabs(gsl_matrix_get (Hamiltonian_Matrix, i, j) - gsl_matrix_get (Hamiltonian_Matrix, j, i))/fabs(gsl_matrix_get (Hamiltonian_Matrix, i, j)) << std::endl;    

std::cerr<<"Matrix is not symmetric!!!"<<std::endl; exit(0);}
                   }
		
		}


		
		gsl_eigen_symmv (Hamiltonian_Matrix, eigenvalues, eigenvectors , QRworkspace);
		gsl_eigen_symmv_sort ( eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
		
		std::vector<int> up_indices;
		std::vector<int> down_indices;
		
		for( int i = 0; i < basis.size();i++){
		   double spin_sum = 0.0;
		   for( int j = 0; j < basis.size()/2;j++) {
		     double entry = LA::GetMatrixE(eigenvectors,j,i);
		     spin_sum += entry * entry;
		   }
		   if(spin_sum >= 0.98) {up_indices.push_back(i); /*std::cout<<"up:"<<i<<std::endl;*/}
		   else {down_indices.push_back(i); /*std::cout<<"down:"<<i<<std::endl;*/}
		   spin_sum = 0.0;
		}
double totet=0;
// 		for (int i = 0; i < 20; i++){
 //		  std::cout<<"Eigen#"<<i<<" = "<<gsl_vector_get(eigenvalues,i)<<std::endl;
//                  totet +=gsl_vector_get(eigenvalues,i);
 //		}
//std::cout<<totet<<" ";
// 		std::cerr<<totet/50; exit(0);
/*  	std::cout<<"Eigen vectors"<<std::endl;
  		for (int i = 0; i < 24; i++){
  		  
  		    for (int j = 0; j < 24; j++){
                     double vvv=gsl_matrix_get (eigenvectors, i, j);
                     if(fabs(vvv) > 1e-10)
                       std::cout<<gsl_matrix_get (eigenvectors, i, j)<<",";
                     else
                       std::cout<<0<<",";
                    }
  		    std::cout<<std::endl;
  		}
		
*/			
		std::vector<double> temp;
		for (size_t i = 0U; i<nups;i++){
			std::vector<double> ups;
			std::vector<double> downs;
			for (size_t j = 0U; j<basis.size()/2;j++){
				if (fabs(LA::GetMatrixE(eigenvectors,j,up_indices[i]))>=1e-16)
					ups.push_back(LA::GetMatrixE(eigenvectors,j,up_indices[i]));
				else
					ups.push_back(0.0);
			downs.push_back(0.0);
			}
		    uncorrelated_state.add(new QmtParticleStateExpansion(ups,downs));
		}
		
		for (size_t i = 0U; i<ndowns;i++){
			std::vector<double> ups;
			std::vector<double> downs;
			for (size_t j = basis.size()/2; j<basis.size();j++){
				if (fabs(LA::GetMatrixE(eigenvectors,j,down_indices[i]))>=1e-16)
					downs.push_back(LA::GetMatrixE(eigenvectors,j,down_indices[i]));
				else
					downs.push_back(0.0);
			ups.push_back(0.0);
			}
		    uncorrelated_state.add(new QmtParticleStateExpansion(ups,downs));
		 }
	#ifdef _QMT_VMC_DEBUG
	std::cout<<"...solved"<<std::endl;
	#endif
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////sc pairing/////////////////////////////////////////////////////////////////////////

void get_single_particle_eigenstates_bcs(unsigned int number_of_electrons, size_t n_sector1,size_t n_sector2,qmt::vmc::QmtSlaterState& uncorrelated_state,
					 const std::vector<double>& microscopic_parameters,const std::vector<double> &hopping_lines = std::vector<double>(0),const std::vector<double>& deltas = std::vector<double>(0),
                                         const std::vector<double> &chemical_potential =   std::vector<double>({-2,0,2}))
                                         {
if(deltas.size() < 1) {get_single_particle_eigenstates(number_of_electrons, n_sector1,n_sector2,uncorrelated_state,microscopic_parameters,hopping_lines);
return;
}
#ifdef _QMT_VMC_DEBUG
		std::cout<<"QmtVmcSingleParticleProblemSolver::solving system..."<<std::endl;
		#endif
		
		if(microscopic_parameters.size() != hamiltonian->number_of_one_body()){
			throw std::logic_error("QmtVmcSingleParticleProblemSolver: get_single_particle_eigenstates_bcs: Number of microscopic parameters must be the same in both hamiltonian and vector of values!");
		}
		uncorrelated_state.reset();
                srand (time(NULL));
               	size_t L = basis.size()/2;


                std::ifstream file("hopplines.dat");

                std::vector<std::string> definitions;  
                if(hopping_lines.size() > 0 && !file.fail()) {
                      std::string line;
                 if (file.is_open())
                 {
                  while ( std::getline (file,line) )
                  {
                    definitions.push_back(line);

                  }
                }
              }
              std::cout<<"Getting deltas...";
              std::vector<std::string> deldefinitions;
              std::ifstream file_d("dellines.dat");

            
                if(deltas.size() > 0 && !file_d.fail()) {
                      std::string line;
                 if (file_d.is_open())
                 {
                  while ( std::getline (file_d,line) )
                  {
                    deldefinitions.push_back(line);

                  }
                }
              }
            std::cout<<"found "<<deldefinitions.size()<<" definition lines.."<<std::endl;

gsl_function_pp Fp([=](double mi)->double{

	       double s1 = 0;
               double s2 = 0;
// double mi=chemical_potential[1];             
                
		for (unsigned int i=0; i<microscopic_parameters.size(); i++){
                auto param = microscopic_parameters[i];
			formula.set_microscopic_parameter(1,i,param);
		}

		formula.get_Hamiltonian_Matrix(Hamiltonian_Matrix);

                for(size_t i = basis.size()/2; i < basis.size();i++){
                 double val = gsl_matrix_get(Hamiltonian_Matrix,i,i);
                 gsl_matrix_set(Hamiltonian_Matrix,i,i, -val);

                }

                for(const auto& definition : definitions) {
                   auto def = qmt::parser::get_bracketed_words(definition,'<','>',true);
                   size_t i = std::stoul(def[0]);
                   auto items = qmt::parser::get_bracketed_words(definition,'(',')',true);
                 for(const auto& item : items) {
                   auto pair = qmt::parser::get_delimited_words(",",item);
                   size_t j = std::stoul(pair[0]);
                   double val = hopping_lines[std::stoul(pair[1])];
                    double actual_v1 = gsl_matrix_get(Hamiltonian_Matrix,i,j);
                    double actual_v2 = gsl_matrix_get(Hamiltonian_Matrix,i + basis.size()/2,j + basis.size()/2);
                   gsl_matrix_set(Hamiltonian_Matrix,i,j,val + actual_v1);
                   gsl_matrix_set(Hamiltonian_Matrix,i + basis.size()/2,j + basis.size()/2,val + actual_v2);

                 }
              }


 for(size_t i = basis.size()/2; i < basis.size();i++){
   for(size_t j = basis.size()/2; j < basis.size();j++){
    if(i != j){
      double val = gsl_matrix_get(Hamiltonian_Matrix,i,j);
      gsl_matrix_set(Hamiltonian_Matrix,i,j,-val);
    }
   
  }
}

/*
 for(size_t i = 0; i < basis.size();i++){
   for(size_t j = 0; j < basis.size();j++){

    std::cout<<gsl_matrix_get(Hamiltonian_Matrix,i,j)<<", ";
    }
    std::cout<<std::endl;
   
  }


exit(0);
*/

//settting chemical potential


                for(size_t i = 0U; i < basis.size()/2;i++){
                      double entry = gsl_matrix_get(Hamiltonian_Matrix,i,i);
                      gsl_matrix_set(Hamiltonian_Matrix,i,i,entry - mi);
                }

                for(size_t i = basis.size()/2; i < basis.size();i++){
                      double entry = gsl_matrix_get(Hamiltonian_Matrix,i,i);
                      gsl_matrix_set(Hamiltonian_Matrix,i,i,entry + mi);
                }

//setting deltas



              for(const auto& definition : deldefinitions) {
                   auto def = qmt::parser::get_bracketed_words(definition,'<','>',true);
                   size_t i = std::stoul(def[0]);
                   auto items = qmt::parser::get_bracketed_words(definition,'(',')',true);
                 for(const auto& item : items) {
                   auto pair = qmt::parser::get_delimited_words(",",item);
                   size_t j = std::stoul(pair[0]);
                   double val = deltas[std::stoul(pair[1])];
                    double actual_v1 = gsl_matrix_get(Hamiltonian_Matrix,i+L,j);
                    double actual_v2 = gsl_matrix_get(Hamiltonian_Matrix,i,j + L);
                   gsl_matrix_set(Hamiltonian_Matrix,i+L,j,val + actual_v1);
                   gsl_matrix_set(Hamiltonian_Matrix,i,j + L,val + actual_v2);

                 }
              }


	            
		gsl_eigen_symmv (Hamiltonian_Matrix, eigenvalues, eigenvectors , QRworkspace);
		gsl_eigen_symmv_sort ( eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
 

                s1 = 0;
                s2 = 0;
               for(size_t i=0U; i < n_sector1+n_sector2;i++){
                   for(size_t j = 0; j < L;j++) {
           
                    s1 += LA::GetMatrixE(eigenvectors,j,i) * LA::GetMatrixE(eigenvectors,j,i);
                    s2 += LA::GetMatrixE(eigenvectors,j+L,i)* LA::GetMatrixE(eigenvectors,j+L,i);
                   }
               }
//              std::cout<<"s1 = "<<s1<<" s2 = "<<s2<<" mi = "<<mi<<" f = "<<fabs(s1 - double(n_sector1)) + fabs(s2 - double(n_sector2))<<std::endl;
              return fabs(s1 - double(n_sector1)) + fabs(s2 - double(n_sector2));
//              return (s1 - double(n_sector1)) * (s1 - double(n_sector1)) + (s2 - double(n_sector2)) * (s2 - double(n_sector2));
            });
              





int status;
  int iter = 0, max_iter = 1000;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;



  double m = chemical_potential[1];
  double a = chemical_potential[0], b = chemical_potential[2];
  gsl_function* F;


  F = static_cast<gsl_function*>(&Fp); 
  T = gsl_min_fminimizer_goldensection;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, F, m, a, b);



  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status 
        = gsl_min_test_interval (a, b, 0.000000001, 0.0);

    }
  while (status == GSL_CONTINUE && iter < max_iter);
  gsl_min_fminimizer_free (s);




              for (size_t i = 0U; i<number_of_electrons;i++){
			std::vector<double> ups;
			std::vector<double> downs;
			for (size_t j = 0U; j< L;j++){
				{
					ups.push_back(LA::GetMatrixE(eigenvectors,j,i));
                                 	downs.push_back(LA::GetMatrixE(eigenvectors,j+L,i));
                                }
				
			}
		    uncorrelated_state.add(new QmtParticleStateExpansion(ups,downs));
		}
}

}; // end of class QmtVmcSingleParticleProblemSolver

} // end namespace vmc
} // end namespace qmt

#endif // end if GSL
#endif // end if QMTOCVMCSINGLEPARTICEPROBLEMSOLVER_H
