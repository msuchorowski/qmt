#ifndef QMTVMCLOCALNN_H
#define QMTVMCLOCALNN_H
#include "qmtvmclocalobs.h"

namespace qmt {
namespace vmc {

class QmtVmcLocalNN : public QmtVmcLocalObservable {

size_t _id1;
size_t _id2;

std::ostream& print(std::ostream& s) const;

public:

QmtVmcLocalNN(size_t id1, size_t id2);

//QmtVmcLocalObservable interface

double get_value(const QmtConfigurationState& x);

};

}
}
#endif
