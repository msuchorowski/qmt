#ifndef QMTVMCLOCALSS_H
#define QMTVMCLOCALSS_H
#include "qmtvmclocalobs.h"

namespace qmt {
namespace vmc {

class QmtVmcLocalSS : public QmtVmcLocalObservable {

size_t _id1;
size_t _id2;
std::ostream& print(std::ostream& s) const;
public:

QmtVmcLocalSS(size_t id1, size_t id2);

//QmtVmcLocalObservable interface

double get_value(const QmtConfigurationState& x);

};

}
}
#endif
