#ifndef QMTVMCLOCALPAIRING_H
#define QMTVMCLOCALPAIRING_H

#include "qmtvmclocalobs.h"
#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include "qmtvmcprob.h"
#include <memory>

namespace qmt {
namespace vmc {

class QmtVmcLocalPairing : public QmtVmcLocalObservable {

size_t _id1;
size_t _id2;

int _sp1;  //up: spin >= 0,down spin  < 0 
int _sp2;


const QmtVmcProbability* _probability;

std::ostream& print(std::ostream& ) const;

public:

QmtVmcLocalPairing(size_t id1, size_t id2, int s1, int s2, 
                   const QmtVmcProbability* probability);

//QmtVmcLocalObservable interface

double get_value(const QmtConfigurationState& x);



};

}
}
#endif
