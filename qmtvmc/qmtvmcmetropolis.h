#ifndef QMT_VMC_METROPOLIS_H
#define QMT_VMC_METROPOLIS_H

#include "qmtvmcalgorithm.h"

#include "qmtslaterstate.h"
#include "qmtvmcjastrow.h"
#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include "qmtvmcprobabilityGSL.h"
#include "qmtvmchamiltonianenergy.h"
#include "qmtvmcrandomhopping.h"

/*Stochastic reconfiguration needs it*/

#include "qmtvmclocald.h"
#include "qmtvmclocalnn.h"


namespace qmt {
namespace vmc {
class QmtVmcMetropolis : public QmtVmcAlgorithm {

    unsigned seed;
    qmt::vmc::QmtVmcData& data;
    size_t steps;
    std::shared_ptr<qmt::vmc::QmtConfigurationState> starting_state;
    
    std::default_random_engine generator;
    typedef std::chrono::high_resolution_clock myclock;

    void prepare_seed();
    double rnd();
    int rnd(int max);
    
    double engine(size_t n_steps,size_t option); //option: 0 - energy, else variance
    
public:

    QmtVmcMetropolis(QmtVmcData& d, std::shared_ptr<qmt::vmc::QmtConfigurationState> st);


    double get_local_energy(size_t n_steps);
    double get_variance_estimator(size_t n_steps);
    void run(size_t n_steps);
    size_t get_number_of_steps() const;

};

/////////////////////////////////////////////////////////////////////////////////////////////////////

struct LambdaRecord{
size_t id;

static void set_observable(size_t i, size_t j,LambdaRecord* record){
if(i == j){
record->obs.push_back(new QmtVmcLocalD(i));
record->factors.push_back(1.0);
}
else{
 record->obs.push_back(new QmtVmcLocalNN(i,j));
 record->factors.push_back(0.5);
}
}



std::vector<QmtVmcLocalObservable*> obs;
std::vector<double> values;
std::vector<double> factors;

void reset(){
 factors.clear();  
 values.clear();
}

~LambdaRecord(){
for(size_t i=0U; i < obs.size();++i)
 delete obs[i];
}



};



class QmtVmcMetropolisCorrelatedSampling : public QmtVmcAlgorithm {
    


    unsigned seed;
    qmt::vmc::QmtVmcData& data;
    size_t steps;
    std::shared_ptr<qmt::vmc::QmtConfigurationState> starting_state;
    bool blocked;
    bool PREPARED;    

    std::default_random_engine generator;
    typedef std::chrono::high_resolution_clock myclock;

    void prepare_seed();
    double rnd();
    int rnd(int max);
    
    std::vector<double> engine(size_t n_steps,size_t option, qmt::vmc::QmtVmcStat *stats = NULL); //option: 0 - energy, 1 - variance, 2 - return energy and collect stats
    
    std::vector<std::shared_ptr<qmt::vmc::QmtConfigurationState>> states;
    std::vector<size_t> repetitions;
    std::vector<double> jastrows;

    std::vector<std::vector<double>> hopping_energies;
    std::vector<double> on_site_energies;
    std::vector<double> interaction_energies;
    
    void update(std::shared_ptr<qmt::vmc::QmtConfigurationState> ptr,bool changed);



    std::vector<LambdaRecord*> lambda_records;

public:

     QmtVmcMetropolisCorrelatedSampling(QmtVmcData& d, std::shared_ptr<qmt::vmc::QmtConfigurationState> st);
     ~QmtVmcMetropolisCorrelatedSampling();
  

    void block();
    void unblock();

    double get_local_energy(size_t n_steps);
    double get_variance_estimator(size_t n_steps);
    double get_observables(size_t n_steps,qmt::vmc::QmtVmcStat& stats);
    void run(size_t n_steps);
    size_t get_number_of_steps() const;
    double stochastic_reconfiguaration_input(gsl_vector *f, gsl_matrix *s);

};




}
}

#endif // qmtvmcmetropolis.h

