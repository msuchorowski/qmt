#include "qmtvmcsimulatedannealing.h"

double qmt::vmc::QmtVmcSimulatedAnnealing::get_value(const std::vector<double>& x, void* params)
{
  Params *p = static_cast<Params*>(params);
  
  
  std::vector<double> v_p;
    std::copy(x.begin(), x.end(),
              std::back_inserter(v_p));

//  v_p.push_back(0.0);

  p->d->prob_calc->set_parameters(v_p);
  p->alg->run(p->steps - p->probing_steps);
  return  p->alg->get_variance_estimator(p->probing_steps);
}



double qmt::vmc::QmtVmcSimulatedAnnealing::optimize( qmt::vmc::QmtVmcData& d, 
					       qmt::vmc::QmtVmcAlgorithm& alg,
					      const std::vector< std::tuple< double, double, double > >& in_par, 
					      std::vector< double > out_par,
					      size_t steps, 
					      size_t probing_steps) {
  Params p;
  p.d = &d;
  p.alg = &alg;
  p.probing_steps=probing_steps;
  p.steps = steps;

std::vector<double> lower;
std::vector<double> upper;

for(const auto & iter : in_par){
	lower.push_back(std::get<0>(iter));
	upper.push_back(std::get<2>(iter));
	
}
auto F = [&](std::vector<double> v)->double {
  return get_value(v,&p); 
};


std::vector<double> entry_lower;
std::vector<double> entry_upper;

for(auto i = 0U; i < in_par.size();++i){
	entry_lower.push_back(0);
	entry_upper.push_back(0.1);
}

static bool flag = false;

if(!flag){
qmt::QmtSimulatedAnnealing solver(1,in_par.size(),std::function<double(std::vector<double>)>(F),entry_lower,entry_upper,10.0,0.5);

double min_val;
solver.run(500,0.01,min_val,out_par);


flag = true;
}

out_par.clear();

qmt::QmtSimulatedAnnealing solver(1,in_par.size(),std::function<double(std::vector<double>)>(F),lower,upper,10.0,0.5);

double min_val;
solver.run(500,0.01,min_val,out_par);




/*
for(auto x : out_par)  {std::cout<<"  "<<x;}
std::cout<<std::endl;
*/
 p.d->prob_calc->set_parameters(out_par);
  p.alg->run(p.steps);
  return  p.alg->get_local_energy(p.probing_steps);
//return min_val;

}
