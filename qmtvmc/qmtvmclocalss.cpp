#include "qmtvmclocalss.h"


qmt::vmc::QmtVmcLocalSS::QmtVmcLocalSS(size_t id1,size_t id2):_id1(id1),_id2(id2){

}

/*
/QmtVmcLocalObservable implementation
*/

double qmt::vmc::QmtVmcLocalSS::get_value(const QmtConfigurationState& x){
double v1 = 0;
double v2 = 0;
	if(x.is_occupied_up(_id1)) v1 += 1.0;
	if(x.is_occupied_down(_id1)) v1 -= 1.0;
	if(x.is_occupied_up(_id2)) v2 += 1.0;
	if(x.is_occupied_down(_id2)) v2 -= 1.0;

 return v1*v2;
}

/*
/print
*/


std::ostream& qmt::vmc::QmtVmcLocalSS::print(std::ostream& s) const {
 s<<"<S"<<_id1<<"S,"<<_id2<<">";
return s;
}

