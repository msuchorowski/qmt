#include "qmtvmclocaln.h"


qmt::vmc::QmtVmcLocalN::QmtVmcLocalN(size_t id):_id(id){

}

/*
/QmtVmcLocalObservable implementation
*/

double qmt::vmc::QmtVmcLocalN::get_value(const QmtConfigurationState& x){
double v = 0;
	if(x.is_occupied_up(_id)) v += 1.0;
	if(x.is_occupied_down(_id)) v += 1.0;
 return v;
}

/*
/print
*/


std::ostream& qmt::vmc::QmtVmcLocalN::print(std::ostream& s) const {
 s<<"<N"<<_id<<">";
return s;
}

