#include "qmtslaterstate.h"
#include <algorithm>
#include <iostream>
#include "qmtconfbitset.h"
#include <omp.h>
/*
 * qmt::vmc::QmtParticleStateExpansion implementation
 */

qmt::vmc::QmtParticleStateExpansion::QmtParticleStateExpansion(const std::vector< double >& ups,
        const std::vector< double >& downs) {
    std::copy(ups.begin(), ups.end(), std::back_inserter(up_c));
    std::copy(downs.begin(), downs.end(), std::back_inserter(down_c));

}


bool qmt::vmc::QmtParticleStateExpansion::operator<(const qmt::vmc::QmtParticleStateExpansion& right) const
{
    return energy < right.energy ? true : false;
}


double qmt::vmc::QmtParticleStateExpansion::get_up_c(size_t index) const
{
    return up_c[index];
}

double qmt::vmc::QmtParticleStateExpansion::get_down_c(size_t index) const
{
    return down_c[index];
}

double qmt::vmc::QmtParticleStateExpansion::get_energy(void) const
{
    return energy;
}

size_t qmt::vmc::QmtParticleStateExpansion::get_size_up(void) const
{
    return up_c.size();
}


size_t qmt::vmc::QmtParticleStateExpansion::get_size_down(void) const
{
    return down_c.size();
}


/*
 * qmt::vmc::QmtSlaterState implementation
 */

void qmt::vmc::QmtSlaterState::add(qmt::vmc::QmtParticleStateExpansion* state) {
    states.push_back(state);
/*   
 auto ptr_comparer = [](const qmt::vmc::QmtParticleStateExpansion* p1, const qmt::vmc::QmtParticleStateExpansion* p2) {
        return p1->get_energy() < p2->get_energy();
    };
   
    
    std::sort(states.begin(),states.end(),ptr_comparer);*/
}

void qmt::vmc::QmtSlaterState::reset()
{
  for(auto i = 0U; i < states.size();++i)
    delete states[i];
  
  states.clear();
  
}


std::vector< double > qmt::vmc::QmtSlaterState::get_D_matrix_column(size_t col_index,
        const QmtConfigurationState& configuration) const {
    std::vector<double> output;
    // std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<16>&>(configuration)<<" col_index = "<<col_index<<std::endl;
    for(size_t i = 0; i < configuration.get_size(); ++i ) {
        if(configuration.is_occupied_up(i)) {
       //std::cout<<"1,";
            output.push_back(states[col_index]->get_up_c(i));
    //    std::cout<<"c#"<<i<<" = "<<states[col_index]->get_up_c(i)<<std::endl;
        }
        //else
      //std::cout<<"0,";
    }

     //std::cout<<"|x<";
    for(size_t i = 0; i < configuration.get_size(); ++i ) {
        if(configuration.is_occupied_down(i)) {
    //  std::cout<<"1,";
            output.push_back(states[col_index]->get_down_c(i));
    //    std::cout<<"c#"<<i<<" = "<<states[col_index]->get_up_c(i)<<std::endl;
        }
        //else
      //std::cout<<"0,";
    }
   // std::cout<<std::endl;
    return output;
}

/*
/ gets electron number which "jumps"
*/

size_t  qmt::vmc::QmtSlaterState::get_beta_index(const QmtConfigurationState& conf_from,const QmtConfigurationState& conf_to) const{
int n = conf_from.get_size(); 

   size_t beta = 0U;
   
   int ind_up = -1;
   int ind_down = -1;
   
   for(auto i = 0; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_up(i);
     auto is2 =  conf_to.is_occupied_up(i);
       if(is1 != is2 && is1) ind_up = i;
   }
   
   for(auto i = 0; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_down(i);
     auto is2 =  conf_to.is_occupied_down(i);
       if(is1 != is2 && is1) ind_down = i;
    }  

    if(ind_up >= 0){
      for(auto i = 0; i < ind_up; ++i) {
       auto is1 =  conf_from.is_occupied_up(i);
       if(is1) beta++;
      }
    }  
    

    if(ind_down >= 0){
      for(auto i = 0; i < n; ++i) {
      auto is1 =  conf_from.is_occupied_up(i);
       if(is1) beta++;
      }
      for(auto i = 0; i < ind_down; ++i) {
       auto is1 =  conf_from.is_occupied_down(i);
       if(is1) beta++;
      }  
     }
 return beta;
}

/*
/ gets index of spin-band where electron jumps to
*/

size_t qmt::vmc::QmtSlaterState::get_to_index(const QmtConfigurationState& conf_from,const QmtConfigurationState& conf_to) const {

  auto n = conf_from.get_size(); 

   size_t to_id = 0U;

   
   for(auto i = 0U; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_up(i);
     auto is2 =  conf_to.is_occupied_up(i);
       if(is1 != is2 && is2) {to_id = i;}

   }


   for(auto i = 0U; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_down(i);
     auto is2 =  conf_to.is_occupied_down(i);
       if(is1 != is2 && is2) {to_id = i + conf_from.get_size();}

   }

return to_id;

}




/*
/generate W matrix - for the first time and for refresh
*/

void qmt::vmc::QmtSlaterState::recalculate_W_matrix(const QmtConfigurationState& conf_from){


//double t0 = omp_get_wtime();

 size_t Nel = 0U;
 auto n = conf_from.get_size(); 

 for(auto i = 0U; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_up(i);
     auto is2 =  conf_from.is_occupied_down(i);
     if(is1) Nel++;
     if(is2) Nel++;
   }




 size_t m_size = Nel;
 size_t n_size = 2 * conf_from.get_size();

electron_ids.clear();

 for(auto i = 0U; i < n; ++i) {
     auto is =  conf_from.is_occupied_up(i);
     if(is1) electron_ids.push_back(i);
   }

 for(auto i = 0U; i < n; ++i) {
     auto is =  conf_from.is_occupied_down(i);
     if(is1) electron_ids.push_back(i + n);
   }




 if(W == NULL || W_copy == NULL) {
   W = gsl_matrix_alloc (m_size, n_size);
   W_copy = gsl_matrix_alloc (m_size, n_size);
   W_generated = true;
  }
 




gsl_matrix *d_T = gsl_matrix_alloc (m_size, m_size);




   for(auto i = 0U; i < m_size;++i) {
     auto column = get_D_matrix_column(i,conf_from);
       for(auto j  = 0U; j < column.size();++j)
        gsl_matrix_set(d_T,i,j,column[j]);
   }


   gsl_vector *x = gsl_vector_alloc (m_size);
   gsl_vector *b = gsl_vector_alloc (m_size);
   gsl_matrix *m_T = gsl_matrix_alloc (m_size, n_size);


   for(size_t i = 0; i < n_size/2; ++i ) {
       for(size_t j = 0U; j < m_size; ++j){
          gsl_matrix_set(m_T,j,i,states[j]->get_up_c(i));
          gsl_matrix_set(m_T,j,i + n_size/2,states[j]->get_down_c(i));
          }
    }





  int s;

  
for(auto i = 0U; i < n_size;++i) {
  for(auto j = 0U; j < m_size; ++j){
     gsl_vector_set(b,j,gsl_matrix_get(m_T,j,i));

     }


gsl_permutation * p = gsl_permutation_alloc (m_size);


//gsl_set_error_handler_off();



 gsl_linalg_LU_decomp (d_T, p, &s);
 gsl_linalg_LU_solve (d_T, p, b, x);



gsl_permutation_free(p);

for(auto q = 0U; q < m_size;++q) {
     auto column = get_D_matrix_column(q,conf_from);
       for(auto r  = 0U; r < column.size();++r)
        gsl_matrix_set(d_T,q,r,column[r]);
   }

  for(auto k = 0U; k < m_size; ++k)
    gsl_matrix_set(W,k,i,gsl_vector_get(x,k));
  }

   gsl_vector_free (x);
   gsl_vector_free(b);
   gsl_matrix_free(m_T);  
   gsl_matrix_free(d_T);  



}



/*
/updates W matrix
*/


double qmt::vmc::QmtSlaterState::update_W_matrix(const QmtConfigurationState& conf_from,const QmtConfigurationState& conf_to){

if(!W_generated){

calculate_W_matrix(conf_from);


//electron map generation
// std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<128>&>(conf_from)<<std::endl; 
// std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<128>&>(conf_to)<<std::endl;
electron_map.clear();

int cntr = 0;
 for(auto i = 0U; i < conf_from.get_size(); ++i) {
     if(conf_from.is_occupied_up(i)){
       electron_map.push_back(cntr);
      ++cntr;
     }
     else
      electron_map.push_back(-1); //empty site
}
 for(auto i = 0U; i < conf_from.get_size(); ++i) {
     if(conf_from.is_occupied_down(i)){
       electron_map.push_back(cntr);
      ++cntr;
     }
      else
      electron_map.push_back(-1); //empty site

}

W_generated = true;
 size_t beta = get_beta_index(conf_from,conf_to);
  size_t l = get_to_index(conf_from,conf_to);

  return  gsl_matrix_get(W,beta,l);


}

else {
//check if configuration changed and then update map

int  from_id = -1;
int  to_id = -1;
 for(auto i = 0U; i < conf_from.get_size(); ++i) {
     if
    
     if(conf_from.is_occupied_up(i) && electron_map[i] < 0){
         to_id = i;
     }

     if(!conf_from.is_occupied_up(i) && electron_map[i] >= 0){
         from_id = i;
     }

     if(conf_from.is_occupied_down(i) && electron_map[i +  conf_from.get_size()] < 0){
         to_id = i + conf_from.get_size();
     }

     if(!conf_from.is_occupied_down(i) && electron_map[i +  conf_from.get_size()] >= 0){
         from_id = i + conf_from.get_size();
     }
     

  }

 
  if(from_id >= 0 && to_id >=0) {

//Here update our matrix....
  
   size_t electron_number = 0U ;
   for(size_t i = 0U; i <=electron_ids.size();++i) {
     if(electron_ids[i] == from_id) {electron_ids[i] = to_id; 
     electron_number = i;
     break;}
   }
  
   
   for(size_t i = 0U; i < W->size1;i++){
     for(size_t alpha = 0U; alpha < W->size2;alpha++){
      double w_i_alpha = gsl_matrix_get(W,i,alpha);
      double w_i_beta = gsl_matrix_get(W,i,electron_number);
      double w_l_beta = gsl_matrix_get(W,to_id,electron_number);
      double w_l_alpha = gsl_matrix_get(W,to_id,alpha);
      auto kronecker = [&](size_t l,size_t l)->double {return l==t ? 1.0  : 0.0; };
      double w_prim = w_i_alpha - (w_i_beta/w_l_beta)*(w_l_alpha - kronecker(alpha,electron_number));
      gsl_matrix_set(W,i,alpha,w_prim);
    }
   }  



  }
//////////////////////////////


   size_t electron_number = 0U ;
   for(size_t i = 0U; i <=electron_ids.size();++i) {
     if(electron_ids[i] == from_id) { 
     electron_number = i;
     break;}



//    size_t beta =  get_beta_index(conf_from,conf_to);
   // beta = electron_map[beta];

  //  size_t l =  get_to_index(conf_from,conf_to);
    double v = gsl_matrix_get(W,electron_number,from_id);


    return v;//gsl_matrix_get(W,beta,l);
  
  
  }
 }
}




double qmt::vmc::QmtSlaterState::get_W_ratio(const QmtConfigurationState& conf_from,
		        const QmtConfigurationState& conf_to) const {

   int n = conf_from.get_size(); 


  
   int to_id = 0;
   size_t Nel = 0U;




   
   for(auto i = 0; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_up(i);
     auto is2 =  conf_to.is_occupied_up(i);
      if(is1 != is2 && is2) {to_id = i;} else to_id = 0;
 
   }


   for(auto i = 0; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_down(i);
     auto is2 =  conf_to.is_occupied_down(i);
       if(is1 != is2 && is2) {to_id = i + conf_from.get_size();} else to_id = 0;
   }


   size_t beta = 0U;
   
   int ind_up = -1;
   int ind_down = -1;
   ;
   for(auto i = 0; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_up(i);
     auto is2 =  conf_to.is_occupied_up(i);
       if(is1 != is2 && is1) ind_up = i;
   }
   
   for(auto i = 0; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_down(i);
     auto is2 =  conf_to.is_occupied_down(i);
       if(is1 != is2 && is1) ind_down = i;
    }  

    if(ind_up >= 0){
      for(auto i = 0; i < ind_up; ++i) {
       auto is1 =  conf_from.is_occupied_up(i);
       if(is1) beta++;
      }
    }  
    

    if(ind_down >= 0){
      for(auto i = 0; i < n; ++i) {
      auto is1 =  conf_from.is_occupied_up(i);
       if(is1) beta++;
      }
      for(auto i = 0; i < ind_down; ++i) {
       auto is1 =  conf_from.is_occupied_down(i);
       if(is1) beta++;
      }  
     }

   
   for(auto i = 0; i < n; ++i) {
     auto is1 =  conf_from.is_occupied_up(i);
     auto is2 =  conf_from.is_occupied_down(i);
     if(is1) Nel++;
     if(is2) Nel++;
   }




   /*
   / GSL....
   */
   size_t m_size = Nel;
   size_t n_size = 2 * conf_from.get_size();


   gsl_matrix *d_T = gsl_matrix_alloc (m_size, m_size);

   for(auto i = 0U; i < m_size;++i) {
     auto column = get_D_matrix_column(i,conf_from);
       for(auto j  = 0U; j < column.size();++j)
        gsl_matrix_set(d_T,i,j,column[j]);
   }


   gsl_vector *x = gsl_vector_alloc (m_size);
   gsl_vector *b = gsl_vector_alloc (m_size);
   gsl_matrix *m_T = gsl_matrix_alloc (m_size, n_size);

   for(size_t i = 0; i < n_size/2; ++i ) {
       for(size_t j = 0U; j < m_size; ++j){
          gsl_matrix_set(m_T,j,i,states[j]->get_up_c(i));
          gsl_matrix_set(m_T,j,i + n_size/2,states[j]->get_down_c(i));
          }
    }


  int s;

  gsl_permutation * p = gsl_permutation_alloc (m_size);

  for(auto j = 0U; j < m_size; ++j){
     gsl_vector_set(b,j,gsl_matrix_get(m_T,j,to_id));

     }

  gsl_linalg_LU_decomp (d_T, p, &s);

  gsl_linalg_LU_solve (d_T, p, b, x);



  gsl_permutation_free (p);

   double result = gsl_vector_get(x,beta);// * sgn;

   gsl_vector_free (x);
   gsl_vector_free(b);
   gsl_matrix_free(m_T);  
   gsl_matrix_free(d_T);  

    return result;
}

bool qmt::vmc::QmtSlaterState::check_configuration(const QmtConfigurationState& configuration) {


size_t Nel = 0U;
auto n = configuration.get_size(); 

 for(auto i = 0U; i < n; ++i) {
     auto is1 =  configuration.is_occupied_up(i);
     auto is2 =  configuration.is_occupied_down(i);
     if(is1) Nel++;
     if(is2) Nel++;
   }

 size_t m_size = Nel;



gsl_matrix *d_T = gsl_matrix_alloc (m_size, m_size);
   for(auto i = 0U; i < m_size;++i) {
     auto column = get_D_matrix_column(i,configuration);
       for(auto j  = 0U; j < column.size();++j)
        gsl_matrix_set(d_T,i,j,column[j]);
   }

return true;

}


