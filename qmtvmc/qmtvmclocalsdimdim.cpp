#include "qmtvmclocalsdimdim.h"


qmt::vmc::QmtVmcLocalSDimDim::QmtVmcLocalSDimDim(size_t id1, size_t id2):_id1(id1),_id2(id2){

}

/*
/QmtVmcLocalObservable implementation
*/

double qmt::vmc::QmtVmcLocalSDimDim::get_value(const QmtConfigurationState& x){
 double v1 = 0;
 double v2 = 0;
 double v3 = 0;
 double v4 = 0;


	if(x.is_occupied_up(_id1)) v1 += 1.0;
	if(x.is_occupied_down(_id1)) v1 -= 1.0;
        if(x.is_occupied_up(_id1+1U)) v2 += 1.0;
	if(x.is_occupied_down(_id1+1U)) v2 -= 1.0;

	if(x.is_occupied_up(_id2)) v3 += 1.0;
	if(x.is_occupied_down(_id2)) v3 -= 1.0;
        if(x.is_occupied_up(_id2+1U)) v4 += 1.0;
	if(x.is_occupied_down(_id2+1U)) v4 -= 1.0;

 return v1*v2*v3*v4;
}

/*
/print
*/

std::ostream& qmt::vmc::QmtVmcLocalSDimDim::print(std::ostream& s) const {
 s<<"<S"<<_id1<<"S"<<_id1+1U<<"S"<<_id2<<"S"<<_id2+1U<<">";
return s;
}
