#ifndef QMTVMCSINGLEPARTICLEPROBLEMSOLVER_H
#define QMTVMCSINGLEPARTICLEPROBLEMSOLVER_H
#ifdef GSL_LIBRARY	// to use GSL

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>

#include <iostream>
#include <vector>
#include <algorithm>

#include "../headers/qmtmatrixformula.h"
#include "../headers/MatrixAlgebras/GSLAlgebra.h"
#include "qmtslaterstate.h"


namespace qmt{
namespace vmc{

template<class MyHamiltonian>
class QmtVmcSingleParticleProblemSolver{

	typedef typename LocalAlgebras::GSLalgebra LA;
	typedef typename LA::Matrix Matrix;
	typedef typename LA::Vector Vector;
	typedef typename MyHamiltonian::NState NState;

	MyHamiltonian* hamiltonian;
	Matrix Hamiltonian_Matrix;
	Matrix eigenvectors;
	Vector eigenvalues;

	qmt::QmtGeneralMatrixFormula<MyHamiltonian,LA> formula;

	gsl_eigen_symmv_workspace * QRworkspace;

	std::vector<NState> basis;

public:
	QmtVmcSingleParticleProblemSolver(MyHamiltonian* _hamiltonian, std::vector<NState>& _basis){
		hamiltonian=_hamiltonian;
		basis=_basis;
		LA::InitializeMatrix(Hamiltonian_Matrix,basis.size(),basis.size());
		LA::InitializeMatrix(eigenvectors,basis.size(),basis.size());
		LA::InitializeVector(eigenvalues,basis.size());

		formula.set_hamiltonian(*hamiltonian);
		formula.set_States(basis);

		QRworkspace=gsl_eigen_symmv_alloc(basis.size());
	}

	~QmtVmcSingleParticleProblemSolver(){
		LA::DeinitializeMatrix(Hamiltonian_Matrix);
		LA::DeinitializeMatrix(eigenvectors);
		LA::DeinitializeVector(eigenvalues);
		gsl_eigen_symmv_free(QRworkspace);
	}

	void get_single_particle_eigenstates(unsigned int number_of_electrons, qmt::vmc::QmtSlaterState& uncorrelated_state, std::vector<double>& one_body_microscopic_parameters,std::vector<double>& hubbard_body_microscopic_parameters){
		if(microscopic_parameters.size() != hamiltonian->number_of_one_body()){
//			std::cout<<"Hamiltoniani one_body:"<<hamiltonian->number_of_one_body()<<" list size:"<<microscopic_parameters.size()<<std::endl;
			throw std::logic_error("QmtVmcSingleParticleProblemSolver: get_single_particle_eigenstates: Number of microscopic parameters must be the same in both hamiltonian and vector of values!");
		}
		uncorrelated_state.reset();
	
                std::vector<double> varparams;
                varparams.resize(2 * basis.size());
                std::fill(varparams.begin(),varparams.end(),0.5);

        	for (unsigned int i=0; i<microscopic_parameters.size(); i++){
			formula.set_microscopic_parameter(1,i,microscopic_parameters[i]);
		}

		formula.get_Hamiltonian_Matrix(Hamiltonian_Matrix);
/*		
		for (int i = 0; i < 64; i++){
		    for (int j = 0; j < 64; j++)
                     std::cout<<gsl_matrix_get (Hamiltonian_Matrix, i, j)<<" ,";
		    std::cout<<std::endl;
		}*/


/*		for (int i = 0; i < 33; i++){
		     for (int j = 0; j < 33; j++)
                     gsl_matrix_set(Hamiltonian_Matrix, i, j,gsl_matrix_get (Hamiltonian_Matrix, i, j) + 0.5);
		}*/


		gsl_eigen_symmv (Hamiltonian_Matrix, eigenvalues, eigenvectors , QRworkspace);
		gsl_eigen_symmv_sort ( eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
/*
 		for (int i = 0; i < 64; i++){
 		  std::cout<<"Eigen#"<<i<<" = "<<gsl_vector_get(eigenvalues,i)<<std::endl;
 		}*/
/*  		std::cout<<"Eigen vectors"<<std::endl;
  		for (int i = 0; i < 64; i++){
  		  
  		    for (int j = 0; j < 64; j++)
                       std::cout<<gsl_matrix_get (eigenvectors, i, j)<<"         ,";
  		    std::cout<<std::endl;
  		}*/
		
		for (size_t i = 0U; i<number_of_electrons;i++){
			std::vector<double> ups;
			std::vector<double> downs;
			for (size_t j = 0U; j<basis.size()/2;j++){
				if (fabs(LA::GetMatrixE(eigenvectors,j,i))>=1e-16)
					ups.push_back(LA::GetMatrixE(eigenvectors,j,i));
				else
					ups.push_back(0.0);
			}
			for (size_t j = basis.size()/2; j<basis.size();j++){
				if (fabs(LA::GetMatrixE(eigenvectors,j,i))>=1e-16)
					downs.push_back(LA::GetMatrixE(eigenvectors,j,i));
				else
					downs.push_back(0.0);
			}
			uncorrelated_state.add(new QmtParticleStateExpansion(ups,downs));
		}
	}

}; // end of class QmtVmcSingleParticleProblemSolver

} // end namespace vmc
} // end namespace qmt

#endif // end if GSL
#endif // end if QMTVMCSINGLEPARTICEPROBLEMSOLVER_H
