#ifndef QMT_VMC_DATA_H
#define QMT_VMC_DATA_H

#include <memory>
#include "qmtslaterstate.h"
#include "qmtvmcjastrow.h"
#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include "qmtvmchamiltonianenergy.h"
#include "qmtvmcstat.h"

namespace qmt {
namespace vmc {
struct QmtVmcData {

    QmtSlaterState* slater;
    QmtVmcHoppingsMap* map;
    QmtVmcGetEnergy* energy_calc;
    QmtVmcProbability* prob_calc;
    QmtVmcStat* stat;

};
}
}

#endif //end of qmt_vmc_data.h
