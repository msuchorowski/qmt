#include "qmtvmcmetropolis.h"
#include <omp.h>

void qmt::vmc::QmtVmcMetropolis::prepare_seed()
{
        myclock::time_point beginning = myclock::now();
        myclock::duration d = myclock::now() - beginning;
        seed = d.count();
	generator.seed(seed);
}

double qmt::vmc::QmtVmcMetropolis::rnd()
{
  std::uniform_real_distribution<double> distribution(0.0,1.0);
  return distribution(generator);
}

int qmt::vmc::QmtVmcMetropolis::rnd(int max)
{
  std::uniform_int_distribution<int> distribution(0,max);
  return distribution(generator);
}

qmt::vmc::QmtVmcMetropolis::QmtVmcMetropolis(qmt::vmc::QmtVmcData& d,std::shared_ptr<qmt::vmc::QmtConfigurationState> st):seed(0),
    data(d),steps(0U),starting_state(st)
{
  prepare_seed();
}


double qmt::vmc::QmtVmcMetropolis::engine(size_t n_steps,size_t option){

  double energy = 0.0;
 std::vector<double> vals;
#ifdef _QMT_VMC_DEBUG
 std::cout<<"qmt::vmc::QmtVmcMetropolis::get_local_energy(n_steps) started"<<std::endl;
#endif
 double acc_rate = 0;

  
//  auto det0 = data.prob_calc->product(starting_state.get());
/*  while(fabs(det0) < 1.0e-9) {
    starting_state = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());
    det0 = data.prob_calc->product(starting_state.get());
   }*/
   double val = 0.0;
    for(size_t i = 0U; i < n_steps; i++) {
//double time = omp_get_wtime();
//std::cout<<"Eneter energy"<<std::endl;        	
      auto hoppings_tr = starting_state->get_hoppings(*data.map);


      auto r1 = rnd(hoppings_tr.size() - 1);
      auto to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(hoppings_tr[r1].get_to_ptr());
        /* auto to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());//hopps_tr[r].get_to_ptr();
        
        
       det0 = data.prob_calc->product(to.get());
	while(fabs(det0) < 1.0e-9) {
	  to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());
	  det0 = data.prob_calc->product(to.get());
	}*/
	  
        
        double r2 = rnd();
	if(starting_state->compare_configuration(*(to.get()))) { i -= 1; continue;}

	double prob = data.prob_calc->probability(starting_state.get(),to.get());
//std::cout<<"Probaility calculated"<<std::endl;
	if(r2 < prob ) {
	     starting_state = to;
	     acc_rate  += 1.0;
	             //std::cout<<"Accedpted in engine"<<std::endl;
        }
        if( r2 < prob || 0 == i)
        val = data.energy_calc->getEnergy(*starting_state);
	
//	if(r2 < prob) 	 std::cout<<"Energy calculated = "<<val<<" In step#"<<i<<std::endl;
//     
        vals.push_back(val);
        energy += val;

//std::cout<<"Val = "<<val<<std::endl;        
        steps++;
//time = omp_get_wtime() - time;        
//std::cout<<"Local energy time:"<<time<<std::endl;
    }
    double std = 0.0;

    for(auto v : vals)
      std += (energy/n_steps - v)*(energy/n_steps -v);
      
      std = std/(n_steps - 1);
//     if(std < 1.0e-4) std = 100;
//    std::cout<<"Local_Energy/step = "<<energy/n_steps<< " variance = "<<std<<" ar = "<<acc_rate/n_steps<<std::endl;
//   

if(option > 0){
      return std;// + energy/n_steps;
    }
//    #ifdef _QMT_VMC_DEBUG
//std::cout<<"Local_Energy/step = "<<energy/n_steps<< " variance = "<<std<<" ar = "<<acc_rate/n_steps<<std::endl;
//    #endif

return energy/n_steps;
}


double qmt::vmc::QmtVmcMetropolis::get_local_energy(size_t n_steps){
return engine(n_steps,0);
}


double qmt::vmc::QmtVmcMetropolis::get_variance_estimator(size_t n_steps){
return engine(n_steps,1);
}



void qmt::vmc::QmtVmcMetropolis::run(size_t n_steps)
{
    /*auto det0 = data.prob_calc->product(starting_state.get());
    while(fabs(det0) < 1.0e-9) {
    starting_state = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());
    det0 = data.prob_calc->product(starting_state.get());
    
//        std::cout<<"det0 calculated: det0 = "<<det0<<std::endl;

  }*/


#ifdef _QMT_VMC_DEBUG
 std::cout<<"qmt::vmc::QmtVmcMetropolis::run(n_steps) started"<<std::endl;
#endif
 
for(size_t i = 0U; i < n_steps; i++) {
//double time = omp_get_wtime();
      auto hoppings_tr = starting_state->get_hoppings(*data.map);
//std::cout<<"Hopping size:"<<hoppings_tr.size()<<std::endl;
      auto r1 = rnd(hoppings_tr.size() - 1);
      auto to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(hoppings_tr[r1].get_to_ptr());

        /*auto to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());//hopps_tr[r].get_to_ptr();
         det0 = data.prob_calc->product(to.get());

	 while(fabs(det0) < 1.0e-9) {
	  to = std::shared_ptr<qmt::vmc::QmtConfigurationState>(starting_state->get_similar());
	  det0 = data.prob_calc->product(to.get());
	 }*/
        
        double r2 = rnd();
        #ifdef _QMT_VMC_DEBUG
        std::cout<<"Step#"<<i<<" probability = ";
        #endif
	double prob = 0.0;
	if(starting_state->compare_configuration(*(to.get()))) { i -= 1; continue;}

        prob = data.prob_calc->probability(starting_state.get(),to.get());

        #ifdef _QMT_VMC_DEBUG
         std::cout<<prob<<std::endl;
        #endif

	if(r2 < prob) {
            starting_state = to;

        #ifdef _QMT_VMC_DEBUG
            std::cout<<"QmtVmcMetropolis::run() Accedpted move"<<std::endl;
	#endif
        }
         steps++;
// time = omp_get_wtime() - time;        
//std::cout<<"Run energy time:"<<time<<std::endl;


    }
}

size_t qmt::vmc::QmtVmcMetropolis::get_number_of_steps() const
{
  return steps;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc::QmtVmcMetropolisCorrelatedSampling::prepare_seed()
{
        myclock::time_point beginning = myclock::now();
        myclock::duration d = myclock::now() - beginning;
        seed = d.count();
	generator.seed(seed);



}

double qmt::vmc::QmtVmcMetropolisCorrelatedSampling::rnd()
{
  std::uniform_real_distribution<double> distribution(0.0,1.0);
  return distribution(generator);
}

int qmt::vmc::QmtVmcMetropolisCorrelatedSampling::rnd(int max)
{
  std::uniform_int_distribution<int> distribution(0,max);
  return distribution(generator);
}

qmt::vmc::QmtVmcMetropolisCorrelatedSampling::QmtVmcMetropolisCorrelatedSampling(qmt::vmc::QmtVmcData& d,std::shared_ptr<qmt::vmc::QmtConfigurationState> st):seed(0),
    data(d),steps(0U),starting_state(st),blocked(false),PREPARED(false)
{
        #ifdef _QMT_VMC_DEBUG
	std::cerr<<std::endl<<" qmt::vmc::QmtVmcMetropolisCorrelatedSampling::QmtVmcMetropolisCorrelatedSampling - creating seed...";
	#endif


  prepare_seed();

        #ifdef _QMT_VMC_DEBUG
	std::cerr<<"done"<<std::endl;
	#endif




}


struct AvrgOneSite{
size_t i;

AvrgOneSite(size_t index):i(index){}

double operator()(const qmt::vmc::QmtConfigurationState& state) {
 double v = 0;
	if(state.is_occupied_up(i)) v += 1.0;
	if(state.is_occupied_down(i)) v += 1.0;
 return v;
}

};


struct AvrgTwoSite{
size_t i;
size_t j;

AvrgTwoSite(size_t index1, size_t index2):i(index1),j(index2){}

double operator()(const qmt::vmc::QmtConfigurationState& state) {
 double v1 = 0;
 double v2 = 0;
	if(state.is_occupied_up(i)) v1 += 1.0;
	if(state.is_occupied_down(i)) v1 += 1.0;
        if(state.is_occupied_up(j)) v2 += 1.0;
	if(state.is_occupied_down(j)) v2 += 1.0;
 return v1*v2;
}

};



std::vector<double> qmt::vmc::QmtVmcMetropolisCorrelatedSampling::engine(size_t n_steps,size_t option,qmt::vmc::QmtVmcStat *stats){

data.prob_calc->reset();
std::vector<double> result;
  double energy = 0.0;
 std::vector<double> vals;
#ifdef _QMT_VMC_DEBUG
 std::cout<<"qmt::vmc::QmtVmcMetropolis::get_local_energy(n_steps) started"<<std::endl;
#endif

// double acc_rate = 0;



   double val = 0.0;
   double tot_w = 0.0;
    double tot_avg = 0;
   double kinetic_energy = 0;
//   double interaction_energy = 0;
//   double on_site_energy = 0;


   std::vector<double> factors;


   AvrgOneSite one_site_1(0U);
   AvrgOneSite one_site_2(5U);
   AvrgTwoSite two_site(0U,5U);
   double one_avr1;
   double one_avr2;
   double two_avr; 
    for(size_t i = 0U; i < states.size(); i++) {
	//double exponent = data.prob_calc->jastrow(states[i].get()) - jastrows[i];
        double factor = 1;//exp(exponent);
       data.prob_calc->update(states[i].get());
       double k_e = data.energy_calc->getOneBodyEnergy(*states[i])*repetitions[i];   

       val = data.energy_calc->getOnSiteEnergy(*states[i]) * repetitions[i]+data.energy_calc->getTwoBodyEnergy(*states[i]) * repetitions[i]+k_e;
//std::cout<<"Energy compute"<<std::endl;
//std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<4>&>(*states[i])<<"  "<<std::endl; 
 //std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<4>&>(conf_to)<<std::endl;
//        kinetic_energy = data.energy_calc->getOnSiteEnergy(*states[i]) * repetitions[i];
/*
     if(!PREPARED) {
        hopping_energies.push_back(data.energy_calc->getHoppingEnergies(*states[i]));
        on_site_energies.push_back(data.energy_calc->getOnSiteEnergy(*states[i]));
        interaction_energies.push_back(data.energy_calc->getTwoBodyEnergy(*states[i]));
     }
        val = (data.energy_calc->getHoppingEnergyByList(*states[i],hopping_energies[i]) + on_site_energies[i] + interaction_energies[i]) * repetitions[i];*/
      
         factors.push_back(factor);
//       std::cout<<"val = "<<val<< " repetitions ="<<repetitions[i]<<" "<<*(static_cast<qmt::vmc::QmtConfigurationStateBitset<20>*>(states[i].get()))<<std::endl;     
	tot_w += repetitions[i] * factor * factor;
        
        vals.push_back(val);
         
        energy += val * factor * factor;
        kinetic_energy +=k_e*factor*factor;        

//        kinetic_energy_total += kinetic_energy*factor*factor;
        
        steps++;
     double prefloc = repetitions[i] * factor * factor;

     if(option == 2 && stats != NULL) {
       //data.prob_calc->reset();
//       if(i%20==0){
       stats->collect(*states[i], prefloc);
//       tot_avg+= repetitions[i] * factor * factor;
//       }
     }   
      one_avr1 += one_site_1(*states[i])*prefloc;
      one_avr2 += one_site_2(*states[i])*prefloc;
      two_avr  += two_site(*states[i])*prefloc;
    }
    double std = 0.0;
    double abs = 0.0;
//    std::cout<<"Vector size:"<<vals.size()<<std::endl;
    for(size_t j = 0U; j < vals.size(); ++j) {
      std += repetitions[j] * factors[j] * factors[j] * (energy/tot_w - vals[j]/repetitions[j])*(energy/tot_w -vals[j]/repetitions[j]);
      abs += factors[j] * fabs(energy/tot_w - vals[j]/repetitions[j]);
     }
    PREPARED = true;
      std = std/(tot_w);
      abs = abs/(tot_w);

double optf = /*std*/ energy/tot_w;
//    std::cout<<energy/tot_w<<" tot_w = "<<tot_w<<" n_steps =  "<<n_steps<< " "<<std<<" "<<std + energy/tot_w<<std::endl;
//exit(0);
data.prob_calc->reset();
if(option == 1){
     result.push_back(optf);
     return result;
    }
else {
//  std::cout<<"<aa> = "<<one_avr1/tot_w<< " <bb> = "<<one_avr2/tot_w<<" <aabb> = "<<two_avr/tot_w<<" <aabb> - <aa><bb> = "<<
//  two_avr/tot_w-one_avr1*one_avr2/(tot_w*tot_w)<<std::endl;
blocked = false;
// if(option == 2)
// std::cout<<"E_kin = "<<kinetic_energy_total/(20*tot_w)<<std::endl;
 if(option == 2 && stats != NULL){
//    std::cout<<optf<<" ";
//    std::cout<<energy/tot_w<<" ";
    stats->normalize(tot_w);
 }
}

 


result.push_back(optf);
result.push_back(energy/tot_w);
if(option == 2)
result.push_back(kinetic_energy/tot_w);

return result;
}


double qmt::vmc::QmtVmcMetropolisCorrelatedSampling::get_local_energy(size_t n_steps){
return engine(n_steps,0)[0];
}


double qmt::vmc::QmtVmcMetropolisCorrelatedSampling::get_variance_estimator(size_t n_steps){
return engine(n_steps,1)[0]; //XXXXXXXXXXXXXXXXXXXXX tutaj [0] jeśli wariancja
}

double qmt::vmc::QmtVmcMetropolisCorrelatedSampling::get_observables(size_t n_steps,qmt::vmc::QmtVmcStat& stats){
auto result = engine(n_steps,2,&stats);
//std::cout<<result[1]<<" ";
return result[1];
}




void qmt::vmc::QmtVmcMetropolisCorrelatedSampling::run(size_t n_steps)
{

data.prob_calc->update(starting_state.get());
data.prob_calc->reset();
data.prob_calc->update(starting_state.get());
#ifdef _QMT_VMC_DEBUG
 std::cerr<<"qmt::vmc::QmtVmcMetropolis::run(n_steps) started"<<std::endl;
#endif
PREPARED = false;
double stat = 0.0;
//double ener = 0.0;

qmt::vmc::QmtVmcRandomHopping random_hoppings_provider=qmt::vmc::QmtVmcRandomHopping(starting_state);

if(/*!blocked*/true) { 
       states.clear();
       repetitions.clear();
      
  //     jastrows.clear();
for(size_t i = 0U; i < n_steps; i++) {

      auto hoppings_tr = starting_state->get_hoppings(*data.map);
      auto r1 = rnd(hoppings_tr.size() - 1);
      auto to = random_hoppings_provider.get();//std::shared_ptr<qmt::vmc::QmtConfigurationState>(hoppings_tr[r1].get_to_ptr());

        double r2 = rnd();
        
        #ifdef _QMT_VMC_DEBUG
        std::cout<<"Step#"<<i<<" probability = ";
        #endif
	double prob = 0.0;
	if(starting_state->compare_configuration(*(to.get()))) {/* i -= 1;*/
	 /*std::cout<<"hs = "<<hoppings_tr.size()<<" r1 = "<<r1<<"  id = "<<hoppings_tr[r1].get_id()<<"  ";
	 std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<24>*>(starting_state.get()))<<"   -->      "; 
	  std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<24>*>(to.get()))<<std::endl; */
	prob = -1.0;
	}
	else
        prob = data.prob_calc->probability(starting_state.get(),to.get());

/*std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<24>*>(starting_state.get()))<<"   -->      "; 
	  std::cout<<*(static_cast<const qmt::vmc::QmtConfigurationStateBitset<24>*>(to.get()))<<std::endl; */


        #ifdef _QMT_VMC_DEBUG
        std::cout<<prob<<std::endl;
        #endif

//        if( n_steps/2 == i)
//           update(starting_state,true);

	if(r2 < prob) {
//        std::cout<<"jumped"<<std::endl;
            starting_state = to;
    	    stat += 1.0;
            random_hoppings_provider.update();
        if( i > n_steps/2)
//         if( i%20 == 0)
        update(starting_state,true);
        #ifdef _QMT_VMC_DEBUG
            std::cout<<"QmtVmcMetropolis::run() Accedpted move"<<std::endl;
	#endif
        data.prob_calc->update(starting_state.get());

        }
         else {
         if(i > n_steps/2 && states.size() > 0 )
//         if( i%20 == 0) 
          update(starting_state,false);
//          std::cout<<"DONT"<<std::endl;
         }
         steps++;
         //Prevent round
         if(steps%500 == 0){
                data.prob_calc->reset();
         }
        
//         std::cout<<"Ener in = "<< data.energy_calc->getEnergy(*starting_state)<<" "<<*(static_cast<qmt::vmc::QmtConfigurationStateBitset<20>*>(starting_state.get()))<<" "<<data.prob_calc->jastrow(starting_state.get())<<std::endl;


    }
  }

blocked = true;


//std::cout<<ener/n_steps;exit(0);
//std::cout<<"Ratio:"<<stat/n_steps<<std::endl;
}

size_t qmt::vmc::QmtVmcMetropolisCorrelatedSampling::get_number_of_steps() const
{
  return steps;
}

void qmt::vmc::QmtVmcMetropolisCorrelatedSampling::block() {
 blocked = true;
}

void qmt::vmc::QmtVmcMetropolisCorrelatedSampling::unblock() {
 blocked = false;
}


void qmt::vmc::QmtVmcMetropolisCorrelatedSampling::update(std::shared_ptr<qmt::vmc::QmtConfigurationState> ptr,bool changed) {
      if(changed){
//       std::cout<<"Pushing"<<*(static_cast<qmt::vmc::QmtConfigurationStateBitset<4>*>(ptr.get()))<<std::endl;
       states.push_back(ptr);
       repetitions.push_back(1U);
//       jastrows.push_back(data.prob_calc->jastrow(ptr.get()));
      }
      else {
      size_t glob_id = 0;
      if(states.size() > 0)
        glob_id = states.size() - 1U;
      
      repetitions[glob_id] += 1U;
      }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc::QmtVmcMetropolisCorrelatedSampling::stochastic_reconfiguaration_input(gsl_vector *f, gsl_matrix *s){

if(lambda_records.size() == 0U){
for(size_t i = 0U; i < data.prob_calc->get_number_of_params();++i){
   lambda_records.push_back(new LambdaRecord());
}

for(size_t i=0U; i < data.prob_calc->get_sector_size();++i){
  for(size_t j=0U; j < data.prob_calc->get_sector_size();++j){
   int id = data.prob_calc->get_lambda(i,j);
   if(id >= 0) {
    LambdaRecord::set_observable(i,j,lambda_records[id]);
  }
  }
}
}

data.prob_calc->reset();
std::vector<double> local_energy;
double tot_w = 0.0;
double energy = 0.0;

for(auto item : lambda_records)
      item->reset();


    for(size_t i = 0U; i < states.size(); i++) {
       data.prob_calc->update(states[i].get());
       double k_e = data.energy_calc->getOneBodyEnergy(*states[i])*repetitions[i];   

       local_energy.push_back(data.energy_calc->getOnSiteEnergy(*states[i]) * repetitions[i]+data.energy_calc->getTwoBodyEnergy(*states[i]) * repetitions[i]+k_e);     

       for(size_t j = 0U; j < lambda_records.size();++j){
         double val = 0.0;
          for(size_t k = 0U; k < lambda_records[j]->obs.size();++k){
            val+=repetitions[i]*lambda_records[j]->obs[k]->get_value(*states[i])*lambda_records[j]->factors[k];
          }
          lambda_records[j]->values.push_back(val);
       } 

	tot_w += repetitions[i];
        energy+=local_energy[i];
    }


double *single_avgs = new double [lambda_records.size()];

for(size_t i = 0U; i < lambda_records.size();++i)
   single_avgs[i] = 0.0;

double **double_avgs = new double* [lambda_records.size()];

for(size_t i = 0U; i < lambda_records.size();++i)
   double_avgs[i] = new double [lambda_records.size()];

for(size_t i = 0U; i < lambda_records.size();++i){
 for(size_t j = 0U; j < lambda_records.size();++j)
  double_avgs[i][j] = 0.0;
}

double *avgener_avgs = new double [lambda_records.size()];


 for(size_t j = 0U; j < lambda_records.size();++j)
  avgener_avgs[j] = 0.0;



/*singles*/

    for(size_t i = 0U; i < states.size(); i++) {
       for(size_t j = 0U; j < lambda_records.size();++j){
       single_avgs[j] += lambda_records[j]->values[i];
      }
    }

   for(size_t j = 0U; j < lambda_records.size();++j){
     single_avgs[j] /= tot_w;
   }

/*doubles*/


    for(size_t i = 0U; i < states.size(); i++) {
       for(size_t j = 0U; j < lambda_records.size();++j){
               for(size_t k = 0U; k < lambda_records.size();++k){
        double_avgs[j][k] += lambda_records[j]->values[i]*lambda_records[k]->values[i]/repetitions[i];
        }
      }
    }

   for(size_t j = 0U; j < lambda_records.size();++j){
     for(size_t k = 0U; k < lambda_records.size();++k){
     double_avgs[j][k] /= tot_w;
//     std::cout<<"avg("<<j<<","<<k<<") = "<<double_avgs[j][k];
   }
//   std::cout<<std::endl;
 }
/*avgeners*/

    for(size_t i = 0U; i < states.size(); i++) {
       for(size_t j = 0U; j < lambda_records.size();++j){
              
        avgener_avgs[j] += lambda_records[j]->values[i]*local_energy[i]/repetitions[i];
        }
      }
    

   for(size_t j = 0U; j < lambda_records.size();++j){
     avgener_avgs[j] /= tot_w;
   }
 

energy /= tot_w;


   for(size_t i = 0U; i < lambda_records.size();++i){
      gsl_vector_set(f,i,-2*(single_avgs[i]*energy - avgener_avgs[i]));
   }

  for(size_t i = 0U; i < lambda_records.size();++i){
    for(size_t j = 0U; j < lambda_records.size();++j){
      gsl_matrix_set(s,i,j,double_avgs[i][j] -single_avgs[i]*single_avgs[j]);
   }
 }

delete [] single_avgs;

for(size_t i = 0U; i < lambda_records.size();++i)
delete  double_avgs[i];

delete [] double_avgs;

delete [] avgener_avgs;


return energy;

}



qmt::vmc::QmtVmcMetropolisCorrelatedSampling::~QmtVmcMetropolisCorrelatedSampling(){
for(size_t i = 0U; i < lambda_records.size();i++)
delete lambda_records[i];
}


