#ifndef QMTVMCHAMILTONIANENERGY_H
#define QMTVMCHAMILTONIANENERGY_H

#include "qmtvmcgetenergy.h"
#include "qmtslaterstate.h"
#include "qmtconfiguration.h"
#include "qmtconfbitset.h"
#include "qmtvmcprob.h"
#include "../headers/qmthubbard.h"
#include "../headers/qmtstate.h"
#include "../headers/qmthbuilder.h"
#include <omp.h>

#include <bitset>
#include <vector>
#include <memory>
#include <thread>

namespace qmt {
namespace vmc {

class QmtVmcHamiltonianEnergy: public QmtVmcGetEnergy
{

    QmtSlaterState uncorrelated_state;
#ifndef QMTVMC_MULTICENTER_ENERGY
    std::vector<std::tuple<size_t,size_t,size_t,size_t>> interaction_defs;
    typedef std::vector<std::tuple<size_t,size_t,size_t,size_t>> InteractionDefsType;
#else
     std::vector<qmt::QmtInteractionDefinition> interaction_defs;
     typedef  std::vector<qmt::QmtInteractionDefinition> InteractionDefsType;
#endif
    std::vector<double> interaction_parameters;
    std::vector<unsigned int> single_particle_energy;
    const QmtVmcHoppingsMap* hopping_map;
    const QmtVmcProbability* probability;
    std::vector<double> hopping_amplitudes;
    std::vector<std::tuple<size_t,size_t,size_t,size_t,size_t,double>> multi_center_configuration;
    std::vector<double> multi_center_amplitudes;
    bool _bcs;

public:
    QmtVmcHamiltonianEnergy(const QmtSlaterState _uncorrelated_state,
                            const InteractionDefsType &_interaction_defs,
                            const std::vector<double>& _interaction_parameters,
                            const std::vector<unsigned int>& _single_particle_energy,
                            const std::vector<double>& _hopping_amplitudes,
                            const QmtVmcHoppingsMap* _hopping_map,
                            const QmtVmcProbability* _probability, bool bcs = false) {
        uncorrelated_state=_uncorrelated_state;
        interaction_defs=_interaction_defs;
        interaction_parameters=_interaction_parameters;
        single_particle_energy=_single_particle_energy;
        hopping_amplitudes = _hopping_amplitudes;
        hopping_map = _hopping_map;
        probability = _probability;
        _bcs = bcs;
        
    }




double getEnergy(QmtConfigurationState& x) {
      return getOneBodyEnergy(x) + getTwoBodyEnergy(x) + getOnSiteEnergy(x);
    }

double getOneBodyEnergy(QmtConfigurationState& x) {
 std::vector<qmt::vmc::QmtVmcHopping> hoppings = x.get_hoppings(*hopping_map);

        double energy=0.0;
        int cntr = 0;
        for (qmt::vmc::QmtVmcHopping& hopping : hoppings) {
	double prob = 0.0;
	
//std::cout<<"Hopping size:"<<hoppings.size()<<std::endl;
//double t0 = omp_get_wtime();
         if(hopping.get_from().compare_configuration((hopping.get_to()))){ prob = 0.5 ;//* hopping.get_from().get_phase()/hopping.get_to().get_phase();
         /* 0.5 factor comes from the  spin-up and spin-down "double-counting" */  cntr++;}
  	 else prob = probability->density_ratio(&hopping.get_from(),&hopping.get_to()) * (hopping.get_to().get_phase() / hopping.get_from().get_phase()) ;
//   std::cout<<"Hopping time = "<<omp_get_wtime() - t0<<std::endl;
         if(_bcs && hopping.get_sector() == 1){
            energy-= hopping_amplitudes[hopping.get_id()]*prob;
            }
         if(!_bcs || hopping.get_sector() == 0)
            energy+= hopping_amplitudes[hopping.get_id()]*prob;
        }
//std::cout<<"Hopping state"<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>&>(x)<<std::endl;

   return energy;
  }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double getOnSiteEnergy(QmtConfigurationState& x) {
double energy = 0.0;
for(size_t i=0U; i<x.get_size(); i++) {
            if(!_bcs){
            if(x.is_occupied_up(i)) energy+=hopping_amplitudes[single_particle_energy[i]];
            }
            else {
            if(x.is_occupied_up(i)){
                 
                 energy+=hopping_amplitudes[single_particle_energy[i]];
                 if(single_particle_energy[i]==0)
                 energy +=8.5;
                 if(single_particle_energy[i] ==3)
                 energy +=4.1;
                 
                }
                
            }
            if(_bcs){
              if(x.is_occupied_down(i)) energy-=hopping_amplitudes[single_particle_energy[i + x.get_size()]];
              }
            else
             if(x.is_occupied_down(i)) energy+=hopping_amplitudes[single_particle_energy[i + x.get_size()]];
            
        }

return energy;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef QMTVMC_MULTICENTER_ENERGY
double getTwoBodyEnergy(QmtConfigurationState& x) {
 double energy = 0.0;
 auto converter = [&](bool p)->size_t {return p ? 1 : 0;};


        for (const auto & interaction : interaction_defs) {
            if(std::get<3>(interaction)) {
                energy+=0.5*interaction_parameters[std::get<2>(interaction)]*
                        (converter(x.is_occupied_up(std::get<0>(interaction))) + converter(x.is_occupied_down(std::get<0>(interaction))))*
                        (converter(x.is_occupied_up(std::get<1>(interaction))) + converter(x.is_occupied_down(std::get<1>(interaction))))/4;

	    }
            else{

                energy+= interaction_parameters[std::get<2>(interaction)]*
                        (converter(x.is_occupied_up(std::get<0>(interaction)))* converter(x.is_occupied_down(std::get<1>(interaction))))/2;
            }

        }

//std::cout<<"Energy = "<<energy<<std::endl;
 return energy;
  }
#else
double getTwoBodyEnergy(QmtConfigurationState& x) {


double result = 0;

for (auto const &item : interaction_defs) {
 
    bool flag = true;
    //if(fabs(interaction_parameters[item.term]) < 0.02) continue;
/*
    if(sector_1 == 1 && sector_2 == 1) {
      if(!x.is_occupied_up(i) && x.is_occupied_up(j) &&   !x.is_occupied_up(k) && x.is_occupied_up(l)){
     flag = true;
       std::cout<<"SECTOR 1 1"<<std::endl;
      }
    }

    if(sector_1 == 1 && sector_2 == -1) {
      if(!x.is_occupied_up(i) && x.is_occupied_up(j) &&   !x.is_occupied_down(k) && x.is_occupied_down(l)){
     flag = true;
             std::cout<<"SECTOR 1 -1"<<std::endl;
      }
    }

     if(sector_1 == -1 && sector_2 == 1) {
      if(!x.is_occupied_down(i) && x.is_occupied_down(j) &&   !x.is_occupied_up(k) && x.is_occupied_up(l)){
     flag = true;
               std::cout<<"SECTOR -1 1"<<std::endl;
     }
    }

    if(sector_1 == -1 && sector_2 == -1) {
      if(!x.is_occupied_down(i) && x.is_occupied_down(j) &&   !x.is_occupied_down(k) && x.is_occupied_down(l)){
     flag = true;
              std::cout<<"SECTOR -1 -1"<<std::endl;
      }
    }
*/
   if(flag) {
   size_t p,q,r,s,id;
   double factor;
   
  
   
    p=item.i; 
    q=item.j;
    r=item.k; //k
    s=item.l; //l  
    

    if(p==r && q!=p) continue;
    if(q==s && r!=q) continue;
//      std::cout<<"term = "<<item.term<<" factor = "<<item.factor<<std::endl;
     
    id=item.term;
    factor = item.factor;
 //      std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>&>(x)<<std::endl; 
//    std::cout<<"p="<<p<<" q="<<q<<" r="<<r<<" s="<<s<<" id = "<<id<<" intsize = "<<interaction_parameters.size()<<std::endl;
   //std::cout<<" p = "<<probability->two_nodes(p,q,r,s)<<std::endl;
//if(p!=q && r !=s)
 //  std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>&>(x)<<std::endl; 
   
   result+=probability->two_nodes(p,q,r,s)*factor *  interaction_parameters[id];

//////////////////////////////////////////////////////
/*
double true_prob = probability->two_nodes(p,q,r,s);  
//std::cout<<"SECTION START--------------------------------------------------"<<std::endl;
   QmtConfigurationState* y = x.get_copy();
      y->set_bit(s,false);
      y->set_bit(r,true);

   int gs = s < x.get_size() ? s : s - x.get_size();  
   int gr = r < x.get_size() ? r : r - x.get_size();  
   int gq = q < x.get_size() ? q : q - x.get_size();  
   int gp = p < x.get_size() ? p : p - x.get_size();  

   if(s >= x.get_size() && !x.is_occupied_down(gs)) {if(fabs(0 - true_prob)>1e-8) std::cout<<"WRONG!"<<std::endl;continue;}
   if(s >= x.get_size() && x.is_occupied_down(gs) && x.is_occupied_down(gr) && gr!=gs) {if(fabs(0 - true_prob)>1e-8) std::cout<<"WRONG!"<<std::endl;continue;}
   if(s  < x.get_size() && !x.is_occupied_up(gs))  {if(fabs(0 - true_prob)>1e-8) std::cout<<"WRONG!"<<std::endl;continue;}
   if(s  < x.get_size() && x.is_occupied_up(gs) && x.is_occupied_up(gr) && gr!=gs) {if(fabs(0 - true_prob)>1e-8) std::cout<<"WRONG!"<<std::endl;continue;}

   QmtConfigurationState* xp = x.get_copy();
 
//std::cout<<"p="<<p<<" q="<<q<<" r="<<r<<" s="<<s<<std::endl;
  // std::cout<<"X = "<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>&>(*xp)<<std::endl; 
//  std::cout<<"Y = "<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>&>(*y)<<std::endl;   
   double prob1=0;
   if(xp->compare_configuration(*y))
    prob1 = 1;
   else
    prob1 = probability->density_ratio(xp,y);
   QmtConfigurationState* z = y->get_copy();
      z->set_bit(q,false);
      z->set_bit(p,true);
   if(q >= x.get_size() && !(y->is_occupied_down(gq)) ) {if(fabs(0 - true_prob)>1e-8) std::cout<<"WRONG!"<<std::endl;continue;}
   if(q >= x.get_size() && y->is_occupied_down(gq) && y->is_occupied_down(gp) && gp != gq ){if(fabs(0 - true_prob)>1e-8) std::cout<<"WRONG!"<<std::endl;continue;}
   if(q  < x.get_size() && !(y->is_occupied_up(gq)) ) {if(fabs(0 - true_prob)>1e-8) std::cout<<"WRONG!"<<std::endl;continue;}
   if(q  < x.get_size() && y->is_occupied_up(gq) && y->is_occupied_up(gp) && gp != gq ) {if(fabs(0 - true_prob)>1e-8) std::cout<<"WRONG!"<<std::endl;continue;}

   //std::cout<<"Z = "<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>&>(*z)<<std::endl; 
   //std::cout<<"J1 = "<<probability->jastrow(xp,y)<<" J2 = "<<probability->jastrow(y,z);  
   double prob2=0;
   if(y->compare_configuration(*z))
    prob2 = 1;
   else {
   
    prob2 = probability->density_ratio(y,z);

   }


 //     std::cout<<"gs = "<<gs<<" gr = "<<gr<<" gq = "<<gq<<" gp = "<<gp<<std::endl;
 //  std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<20>&>(*y)<<std::endl; 
   //std::cout<<"p = "<<prob1*prob2<<" tru_prob = "<<true_prob<<std::endl;
   if(fabs(prob1*prob2 - true_prob)>1e-8)
     std::cout<<"WRONG!"<<std::endl;
   if(!(xp->compare_configuration(*y)))
   probability->density_ratio(xp,y);

    delete y;
    delete z;
    delete xp;
//std::cout<<"SECTION END--------------------------------------------------"<<std::endl;
///////////////////////////////////////////////////////
*/


   }
}

/*
if(x.is_occupied_up(1) && x.is_occupied_down(1))
 std::cout<<"TWO CENTER P = "<<probability->two_nodes(0,1,3,3)<<std::endl;
*/
//exit(0);
//std::cout<<static_cast<const qmt::vmc::QmtConfigurationStateBitset<4>&>(x)<<" result = "<<result<<std::endl; 

return result;
}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
   /*
std::vector<double> getHoppingEnergies(qmt::vmc::QmtConfigurationState& x) {

          std::vector<qmt::vmc::QmtVmcHopping> hoppings = x.get_hoppings(*hopping_map);
          std::vector<double> values;

        for (qmt::vmc::QmtVmcHopping& hopping : hoppings) {
	double prob = 0.0;
         if(hopping.get_from().compare_configuration((hopping.get_to()))){
		 prob = 0.5 * hopping.get_from().get_phase()/hopping.get_to().get_phase();
	         // 0.5 factor comes from the  spin-up and spin-down "double-counting" 
	 }
  	 else 
		prob = probability->density_ratio(&hopping.get_from(),&hopping.get_to());
         
         prob *= exp(probability->jastrow(&hopping.get_to(),&hopping.get_from()));
         
         values.push_back(prob * hopping_amplitudes[hopping.get_id()]);           
        }
 return values;
 }
 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double getHoppingEnergyByList(qmt::vmc::QmtConfigurationState& x,const std::vector<double>& defs) {
          std::vector<qmt::vmc::QmtVmcHopping> hoppings = x.get_hoppings(*hopping_map);

        size_t cntr = 0U;
        double energy = 0.0;
        for (qmt::vmc::QmtVmcHopping& hopping : hoppings) {
	double prob = 0.0;
         if(hopping.get_from().compare_configuration((hopping.get_to()))){
		 energy += defs[cntr];
	 }
  	 else 
		energy += defs[cntr]* exp(probability->jastrow(&hopping.get_from(),&hopping.get_to()));
         ++cntr;
        }
 return energy;
}

*/

};

} // end of namespace vmc
} //end of namespace qmt
#endif // QMTVMCHAMILTONIANENERGY_H
