#include "qmtvmclocalpairing.h"


qmt::vmc::QmtVmcLocalPairing::QmtVmcLocalPairing(size_t id1, size_t id2, int s1, int s2,
                   const QmtVmcProbability* probability):_id1(id1),_id2(id2),_sp1(s1),_sp2(s2),
		   _probability(probability)  {

	}



/////////////////////////////////////////////////////////////////////////////////////////////////////////


double qmt::vmc::QmtVmcLocalPairing::get_value(const QmtConfigurationState& x){

size_t to = _id1;
size_t from = _id2;
if(_sp1 == -1) to+=x.get_size();
if(_sp2 == -1) from+=x.get_size();
    return  _probability->two_nodes(to,from);

}




/*
/print
*/

std::ostream& qmt::vmc::QmtVmcLocalPairing::print(std::ostream& s) const {
if(_sp1 > 0 && _sp2 > 0)
 s<<"<cup"<<_id1<<"_cup"<<_id2<<">";
if(_sp1 > 0 && _sp2 < 0)
 s<<"<cup"<<_id1<<"_cdown"<<_id2<<">";
if(_sp1 < 0 && _sp2 > 0)
 s<<"<cdown"<<_id1<<"_cup"<<_id2<<">";
if(_sp1 < 0 && _sp2 < 0)
 s<<"<cdown"<<_id1<<"_cdown"<<_id2<<">";

return s;
}
