#include "qmtvmcoptimizer.h"
#include<gsl/gsl_errno.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include "qmtvmcmetropolis.h"


namespace qmt{
 namespace vmc{
class QmtVmcStochRecOptimizer : public QmtVmcOptimizer {


struct MannKendallDataItem{

std::vector<double> vals;



MannKendallDataItem(size_t n){
vals.resize(n);
}

};


std::vector<double> zeta;

std::vector<MannKendallDataItem> mkset;
size_t length;

void initialize_zeta(size_t dim);

void update_mkset(std::vector<double> vals);

public:

double optimize(QmtVmcData& d, 
		     QmtVmcAlgorithm& alg, 
                     const std::vector<std::tuple<double,double,double>>& in_par,
                     std::vector<double>& out_par,
		     size_t steps,
		     size_t probing_steps);

}; 
}
}
