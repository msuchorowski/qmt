#ifndef QMTVMCEFFICIENTPROBABILITYGSL_H
#define QMTVMCEFFICIENTPROBABILITYGSL_H

#include "qmtvmcprob.h"

#include "qmtconfiguration.h"
#include "qmtvmcjastrow.h"
#include "qmtconfbitset.h"
#include "qmtslaterstate.h"
#include <vector>

#define _QMTVMC_GSL

#ifdef _QMTVMC_GSL
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_permutation.h>
#endif

#ifdef _QMTVMC_GSL
namespace qmt {
namespace vmc {
template<size_t NUM_SITES>
class QmtVmcProbabilityGSL : public QmtVmcProbability {
private:
    std::vector<double> parameters;
    QmtVmcJastrow jastrow_correlator;
    const QmtSlaterState* uncorrelated_state;
    bool PARAMETERS_SET;

    gsl_matrix * D;
    gsl_matrix * Dprim;

    size_t D_matrix_size;

    gsl_permutation * permutation;


public:
    QmtVmcProbabilityGSL(size_t num_of_electrons, QmtVmcJastrow _jastrow_correlator, const QmtSlaterState* _uncorrelated_state):jastrow_correlator(_jastrow_correlator) {
        uncorrelated_state=_uncorrelated_state;
        PARAMETERS_SET=false;

        D_matrix_size=num_of_electrons;

        D	=gsl_matrix_alloc(D_matrix_size,D_matrix_size);
        Dprim	=gsl_matrix_alloc(D_matrix_size,D_matrix_size);
        permutation = gsl_permutation_alloc (D_matrix_size);
    }

    virtual ~QmtVmcProbabilityGSL() {
     
        gsl_matrix_free (D);
        gsl_matrix_free (Dprim);
        gsl_permutation_free (permutation);
    }

    
    virtual double product(const QmtConfigurationState* x) {
         int sgn;
        

        for(unsigned int i=0; i<D_matrix_size; i++) {
            std::vector<double> D_column = uncorrelated_state->get_D_matrix_column(i,*x);


            for(unsigned int j=0; j<D_column.size(); j++)
                gsl_matrix_set(D,j,i,D_column[j]);

            
        }
//         std::cout<<"D-matrix"<<std::endl;
//         	for (int i = 0; i < 4; i++){
//  		  
//  		    for (int j = 0; j < 4; j++)
//                       std::cout<<gsl_matrix_get (D, i, j)<<"         ,";
//  		    std::cout<<std::endl;
//  		}
 		gsl_linalg_LU_decomp (D, permutation, &sgn);
		auto det =   gsl_linalg_LU_det (D, sgn);
      //  std::cout<<" det = "<<det<<std::endl;
        return det;// gsl_linalg_LU_decomp (D, permutation, &sgn);
    }
    
virtual double probability(const QmtConfigurationState* x, const QmtConfigurationState* xprim) {
        if(!PARAMETERS_SET) {
            throw std::invalid_argument ("QmtVmcProbablityGSL::probability: parameters not set!");
        }


        int sgn;
        int sgnprim;

        double jastrow_coeff = jastrow_correlator(parameters,*xprim)/jastrow_correlator(parameters,*x);

        for(unsigned int i=0; i<D_matrix_size; i++) {
            std::vector<double> D_column = uncorrelated_state->get_D_matrix_column(i,*x);


            for(unsigned int j=0; j<D_column.size(); j++)
                gsl_matrix_set(D,j,i,D_column[j]);

            D_column = uncorrelated_state->get_D_matrix_column(i,*xprim);

            for(unsigned int j=0; j<D_column.size(); j++)
                gsl_matrix_set(Dprim,j,i,D_column[j]);
        }

        gsl_linalg_LU_decomp (D, permutation, &sgn);
        gsl_linalg_LU_decomp (Dprim, permutation, &sgnprim);

	
auto det1 = gsl_linalg_LU_det (D, sgn);
auto det2 = gsl_linalg_LU_det (Dprim, sgnprim);

#ifdef _QMT_VMC_DEBUG
std::cout<<"det1 = "<<det1<<" det2 = "<<det2<<std::endl;
#endif
double ratio = det2/det1;
//if(fabs(det1) <1.0e-64) return 0;
//if(fabs(det1) <1.0e-7 && fabs(det2) <1.0e-7) ratio = 0;
//if(fabs(det2) >1.0e-7 && fabs(det1) <1.0e-7) ratio = 0;	

        double prob = pow(jastrow_coeff * (xprim->get_phase() / x->get_phase()) * (ratio),2);
//          std::cout<<"prob = "<<prob <<"ratio = "<<ratio<<" det1 = "<<det1<<" det2 = "<<det2<<std::endl;
        return  prob>1.0 ? 1.0 : prob;
    }

    virtual double density_ratio(const QmtConfigurationState* x, const QmtConfigurationState* xprim) const {
        if(!PARAMETERS_SET) {
            throw std::invalid_argument ("QmtVmcProbablityGSL::density_ratio: parameters not set!");
        }

        int sgn;
        int sgnprim;

        double jastrow_coeff = jastrow_correlator(parameters,*xprim)/jastrow_correlator(parameters,*x);

        for(unsigned int i=0; i<D_matrix_size; i++) {
            std::vector<double> D_column = uncorrelated_state->get_D_matrix_column(i,*x);

	    
          
            for(unsigned int j=0; j<D_column.size(); j++)
                gsl_matrix_set(D,j,i,D_column[j]);

            D_column = uncorrelated_state->get_D_matrix_column(i,*xprim);

            for(unsigned int j=0; j<D_column.size(); j++)
                gsl_matrix_set(Dprim,j,i, D_column[j]);
        }
     

        gsl_linalg_LU_decomp (D, permutation, &sgn);
        gsl_linalg_LU_decomp (Dprim, permutation, &sgnprim);

auto det1 = gsl_linalg_LU_det (D, sgn);
auto det2 = gsl_linalg_LU_det (Dprim, sgnprim);

//  std::cout<<*static_cast<const qmt::vmc::QmtConfigurationStateBitset<16>*>(x)<<"       "<<det1<<std::endl;  
//         for(int i = 0; i < D_matrix_size;++i){
// 	  for(int j = 0; j < D_matrix_size;++j) 
// 	    std::cout<<gsl_matrix_get(D,i,j)<<",   	";
// 	  std::cout<<std::endl;
// 	}
double ratio = det2/det1;

//if(fabs(det1) <1.0e-64) return 0;
//if(fabs(det2) >1.0e-7 && fabs(det1) <1.0e-7) ratio =  0;	
	
//std::cout<<"ratio = "<<det1/det2<<std::endl;

//std::cout<<*static_cast<const qmt::vmc::QmtConfigurationStateBitset<16>*>(x)<<"       "<<det1<<std::endl;
//std::cout<<"jastrow:"<<jastrow_coeff<<" det1:"<<det1<<" det2:"<<det2<<std::endl;
        double prob = jastrow_coeff* (xprim->get_phase() / x->get_phase()) * (ratio);
/*        if(fabs(prob) > 100){ 
        std::cout<<"density_prob = "<<prob<<" det2 = "<<det2<<" det1 = "<<det1<<" jastrow coeff = "<<jastrow_coeff<<" phases_ratio = "<<xprim->get_phase() / x->get_phase()<<std::endl;
        std::cout<<"Jastrow params"<<std::endl;
         for(const auto &x : parameters)
          std::cout<<x<<" "<<std::endl;
        }*/
        return prob;
    }

    virtual void set_parameters(const std::vector<double>& _parameters) {
        PARAMETERS_SET=true;
        parameters=_parameters;
    }

}; // end of class QmtVmcProbablityGSL

} // end of namespace vmc
} //end of namespace qmt
#endif //_QMTVMC_GSL

#endif // QMTVMCEFFICIENTPROBABILITYGSL_H
