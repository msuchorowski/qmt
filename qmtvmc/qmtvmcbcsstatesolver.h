#ifndef QMTVMC_BCS_STATE_SOLVER_H
#define QMTVMC_BCS_STATE_SOLVER_H
#ifdef GSL_LIBRARY	// to use GSL

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>

#include <iostream>
#include <vector>
#include <tuple>


#include "../headers/qmtmatrixformula.h"
#include "../headers/MatrixAlgebras/GSLAlgebra.h"
#include "qmtslaterstate.h"

#include <time.h>
#include <stdlib.h>

namespace qmt{
namespace vmc{

template<class MyHamiltonian>
class QmtVmcBcsStateSolver{

	typedef typename LocalAlgebras::GSLalgebra LA;
	typedef typename LA::Matrix Matrix;
	typedef typename LA::Vector Vector;
	typedef typename MyHamiltonian::NState NState;

	MyHamiltonian* hamiltonian;
	Matrix Hamiltonian_Matrix;
	Matrix eigenvectors;
	Vector eigenvalues;

	qmt::QmtGeneralMatrixFormula<MyHamiltonian,LA> formula;

	gsl_eigen_symmv_workspace * QRworkspace;

	std::vector<NState> basis;
	std::vector<std::tuple<size_t,size_t,double>> bcs_delta;

public:
	QmtVmcBcsStateSolver(MyHamiltonian* _hamiltonian, std::vector<NState>& _basis, std::vector<std::tuple<size_t,size_t,double> delta_ids){
		hamiltonian =_hamiltonian;
		basis =_basis;
                bcs_delta = delta_ids;
		LA::InitializeMatrix(Hamiltonian_Matrix,basis.size(),basis.size());
		LA::InitializeMatrix(eigenvectors,basis.size(),basis.size());
		LA::InitializeVector(eigenvalues,basis.size());

		formula.set_hamiltonian(*hamiltonian);
		formula.set_States(basis);

		QRworkspace=gsl_eigen_symmv_alloc(basis.size());
	}

	~QmtVmcBcsStateSolver(){
		LA::DeinitializeMatrix(Hamiltonian_Matrix);
		LA::DeinitializeMatrix(eigenvectors);
		LA::DeinitializeVector(eigenvalues);
		gsl_eigen_symmv_free(QRworkspace);
	}

	void get_single_particle_eigenstates(unsigned int number_of_electrons,size_t nups, size_t ndowns, qmt::vmc::QmtSlaterState& uncorrelated_state, std::vector<double>& microscopic_parameters){
		#ifdef _QMT_VMC_DEBUG
		std::cout<<"QmtVmcSingleParticleProblemSolver::solving system..."<<std::endl;
		#endif
		
		if(microscopic_parameters.size() != hamiltonian->number_of_one_body()){
			throw std::logic_error("QmtVmcSingleParticleProblemSolver: get_single_particle_eigenstates: Number of microscopic parameters must be the same in both hamiltonian and vector of values!");
		}
		uncorrelated_state.reset();
                srand (time(NULL));
                
		for (unsigned int i=0; i<microscopic_parameters.size(); i++){
                auto param = microscopic_parameters[i];
			formula.set_microscopic_parameter(1,i,param);
		}

		formula.get_Hamiltonian_Matrix(Hamiltonian_Matrix);

/////////////////////BCS STATE//////////////////////

		for(const auto item& : bcs_delta) {
                size_t idl = std::get<0>(item) + basis.size()/2;
                size_t jdl = std::get<1>(item);

                size_t iur = std::get<0>(item);
                size_t jur = std::get<1>(item)  + basis.size()/2;;

		double delta = std::get<2>(item);

		gsl_matrix_set(Hamiltonian_Matrix,idl,jdl,delta);
		gsl_matrix_set(Hamiltonian_Matrix,iur,jur,delta);
		}


		for(size_t i = basis.size()/2; i < basis.size();i++){
		   for(size_t i = basis.size()/2; j < basis.size();j++) {
                     double t = gsl_matrix_get(Hamiltonian_Matrix,i,j);
                     gsl_matrix_set(Hamiltonian_Matrix,i,j,-t);
                   }
		}

/////////////////////BCS STATE////////////////////////		
  
/*
		for (int i = 0; i < 8; i++){
		    for (int j = 0; j < 8; j++)
                     std::cout<<gsl_matrix_get (Hamiltonian_Matrix, i, j)<<" ,";
		    std::cout<<std::endl;
		}
*/

/*		for (int i = 0; i < 33; i++){
		     for (int j = 0; j < 33; j++)
                     gsl_matrix_set(Hamiltonian_Matrix, i, j,gsl_matrix_get (Hamiltonian_Matrix, i, j) + 0.5);
		}*/
		
		gsl_eigen_symmv (Hamiltonian_Matrix, eigenvalues, eigenvectors , QRworkspace);
		gsl_eigen_symmv_sort ( eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);
		
		std::vector<int> up_indices;
		std::vector<int> down_indices;
		
		for( int i = 0; i < basis.size();i++){
		   double spin_sum = 0.0;
		   for( int j = 0; j < basis.size()/2;j++) {
		     double entry = LA::GetMatrixE(eigenvectors,j,i);
		     spin_sum += entry * entry;
		   }
		   if(spin_sum >= 0.98) {up_indices.push_back(i); /*std::cout<<"up:"<<i<<std::endl;*/}
		   else {down_indices.push_back(i); /*std::cout<<"down:"<<i<<std::endl;*/}
		   spin_sum = 0.0;
		}
/*
 		for (int i = 0; i < 8; i++){
 		  std::cout<<"Eigen#"<<i<<" = "<<gsl_vector_get(eigenvalues,i)<<std::endl;
 		}
  		std::cout<<"Eigen vectors"<<std::endl;
  		for (int i = 0; i < 8; i++){
  		  
  		    for (int j = 0; j < 8; j++)
                       std::cout<<gsl_matrix_get (eigenvectors, i, j)<<"         ,";
  		    std::cout<<std::endl;
  		}*/
		
			
		std::vector<double> temp;
		for (size_t i = 0U; i<nups;i++){
			std::vector<double> ups;
			std::vector<double> downs;
			for (size_t j = 0U; j<basis.size()/2;j++){
				if (fabs(LA::GetMatrixE(eigenvectors,j,up_indices[i]))>=1e-16)
					ups.push_back(LA::GetMatrixE(eigenvectors,j,up_indices[i]));
				else
					ups.push_back(0.0);
			downs.push_back(0.0);
			}
		    uncorrelated_state.add(new QmtParticleStateExpansion(ups,downs));
		}
		
		for (size_t i = 0U; i<ndowns;i++){
			std::vector<double> ups;
			std::vector<double> downs;
			for (size_t j = basis.size()/2; j<basis.size();j++){
				if (fabs(LA::GetMatrixE(eigenvectors,j,down_indices[i]))>=1e-16)
					downs.push_back(LA::GetMatrixE(eigenvectors,j,down_indices[i]));
				else
					downs.push_back(0.0);
			ups.push_back(0.0);
			}
		    uncorrelated_state.add(new QmtParticleStateExpansion(ups,downs));
		 }
	#ifdef _QMT_VMC_DEBUG
	std::cout<<"...solved"<<std::endl;
	#endif
	}

}; // end of class QmtVmcBcsStateSolver

} // end namespace vmc
} // end namespace qmt

#endif // end if GSL
#endif // end if QMTVMC_BCS_STATE_SOLVER_H
