#include "qmtvmcstreoptimizer.h"

#include <gsl/gsl_eigen.h>

void qmt::vmc:: QmtVmcStochRecOptimizer::initialize_zeta(size_t dim){

zeta.clear();
zeta.resize(dim);
auto signum = [&](double x)->double{

return x > 0 ? 1.0 :(x == 0.0 ? 0.0 : -1.0);
};
for(size_t i = 0U; i < mkset.size()-1;++i){
 for(size_t j = 0U;j < dim;++j){
  for(size_t k = i+1U;k <mkset.size(); ++k){
   zeta[j]+=signum(mkset[i].vals[j]-mkset[k].vals[j]);
   }
 }
}

for(size_t j = 0U;j < dim;++j){
   zeta[j]/=((mkset.size()*(mkset.size()-1)));
   zeta[j]=fabs(zeta[j]);
//   std::cout<<"zeta["<<j<<"] = "<<zeta[j]<<std::endl;
 }

}


///////////////////////////////////////////////////////////////////////////////////

void qmt::vmc:: QmtVmcStochRecOptimizer::update_mkset(std::vector<double> vals) {


auto item = MannKendallDataItem(vals.size());
for(size_t i = 0U; i < vals.size();++i)
item.vals[i] = vals[i];

mkset.push_back(item);

if(mkset.size() > length)
mkset.erase(mkset.begin());


}


///////////////////////////////////////////////////////////////////////////////////

double qmt::vmc:: QmtVmcStochRecOptimizer::optimize( qmt::vmc::QmtVmcData& d, 
					       qmt::vmc::QmtVmcAlgorithm& alg,
					      const std::vector< std::tuple< double, double, double > >& in_par, 
					      std::vector< double >& out_par,
					      size_t steps, 
					      size_t probing_steps) {

 std::ofstream log("optimization.log");
 
 size_t dim = in_par.size();

 gsl_matrix *s = gsl_matrix_alloc (dim, dim);
 gsl_vector *f = gsl_vector_alloc(dim);
gsl_vector *scaler = gsl_vector_alloc(dim);
 out_par.clear();

 bool final_flag = false;
 QmtVmcMetropolisCorrelatedSampling &algorithm = static_cast<QmtVmcMetropolisCorrelatedSampling &>(alg);

size_t cntr = 0U;
size_t final_cntr = 0U;

std::vector<double> input;

length = 100U;
size_t MAX_ITER = 100000U;
double mpl = 0.001;

for(const auto& item : in_par)
   input.push_back(std::get<1>(item));


std::ifstream JastrowFile("input_jastrows.dat");
 if(!JastrowFile.fail()){
 input.clear();
 for(size_t i =0U; i < in_par.size(); ++i){
   double val;
  JastrowFile>>val;
  input.push_back(val);
 }
 
}

//input[0]=1.5;
//input[1]=-0.15;
//input[2] = -0.12;

out_par.clear();
for(size_t i = 0U; i < in_par.size();++i)
   out_par.push_back(0.0);


double old_energy = 0.0;
double energy = 0.0;
while(cntr < MAX_ITER)
{


// std::cout<<std::endl;

 d.prob_calc->set_parameters(input);

 algorithm.run(steps);
 old_energy = energy;
 energy = algorithm.stochastic_reconfiguaration_input(f,s);



/*
 
for(size_t i = 0U; i < dim;i++){
  for(size_t j = 0U; j < dim;j++){
 if(i!=j){
   double lv = gsl_matrix_get(s,i,j);
   double li = gsl_matrix_get(s,i,i);
   double lj = gsl_matrix_get(s,j,j);
  gsl_matrix_set(s,i,j,lv/sqrt(li*lj));
 }
 }
 gsl_vector_set(scaler,i,gsl_matrix_get(s,i,i));
  }



for(size_t i = 0U; i < dim;i++){
   double lv = gsl_matrix_get(s,i,i);
  gsl_vector_set(f,i,lv/(sqrt(lv)));
}

 for(size_t j = 0U; j < dim;j++){
  gsl_matrix_set(s,j,j,1.0);
 }

*/

  for(size_t i = 0U; i < dim;i++){
   double lv = gsl_matrix_get(s,i,i);
   double leps = 1.0e-3;
  gsl_matrix_set(s,i,i,lv+leps);
  }


 

 /*
if(cntr>0){
 if(old_energy < energy)
 continue;
}
*/
gsl_vector *x = gsl_vector_alloc (dim);
  
  int r;

  gsl_permutation * p = gsl_permutation_alloc (dim);
  gsl_linalg_LU_decomp (s, p, &r);


/*
std::vector<size_t> removed;

if(cntr==0U){

for(size_t round = 0U;round < 4;round++){

gsl_matrix *temp_s =gsl_matrix_alloc (dim-removed.size(), dim-removed.size());

size_t cntr_i = 0U;
size_t cntr_j = 0U;

for(size_t i = 0U; i < dim;++i) {
  cntr_j = 0U;
  if(std::find(removed.begin(), removed.end(), i) != removed.end()) continue;
  for(size_t j = 0U; j < dim;++j) {
    if(std::find(removed.begin(), removed.end(), j) != removed.end()) continue;
  //  std::cout<<"cntr_i = "<<cntr_i<<" cntr_j = "<<cntr_j<<" i = "<<i<<" j = "<<j<<std::endl;
    

   gsl_matrix_set(temp_s,cntr_i,cntr_j,gsl_matrix_get(s,i,j));

 //std::cout<<"cntr_i = "<<cntr_i<<" cntr_j = "<<cntr_j<<" i = "<<i<<" j = "<<j<<" temp_s = "<<gsl_matrix_get(temp_s,cntr_i,cntr_j)<< " s = "<<gsl_matrix_get(s,i,j)<<std::endl;
      cntr_j+=1U;
    
  }
  cntr_i+=1U;

}


  

gsl_permutation *pp =gsl_permutation_alloc (dim - removed.size());

int rr;


gsl_matrix *invert_m =gsl_matrix_alloc (dim-removed.size(), dim - removed.size());
gsl_matrix *temp_s_cp=gsl_matrix_alloc (dim-removed.size(), dim - removed.size());

gsl_matrix_memcpy(temp_s_cp,temp_s);

gsl_eigen_symm_workspace *sys =  gsl_eigen_symm_alloc (dim-removed.size());
gsl_vector *eigen = gsl_vector_alloc(dim-removed.size());

gsl_eigen_symm (temp_s_cp,eigen,sys);

gsl_linalg_LU_decomp (temp_s, pp, &rr);
gsl_linalg_LU_invert (temp_s, pp,invert_m);
 
std::vector<double> diag_elements;

for(size_t i =0U; i < dim-removed.size();i++)
  diag_elements.push_back(gsl_matrix_get(invert_m,i,i));

auto max_index = std::distance(diag_elements.begin(),std::max_element(diag_elements.begin(), diag_elements.end()));
int signum;
double det =  gsl_linalg_LU_det (temp_s, rr);
std::cout<<"MAX index at:"<<max_index<<" det = "<<det<<std::endl;
removed.push_back(max_index);

for(size_t i =0U;i < dim-removed.size()+1U;++i)
std::cout<<"EIGEN#"<<i<<" = "<<gsl_vector_get(eigen,i);

gsl_matrix_free(temp_s);

}
exit(0);

}
*/

  gsl_linalg_LU_solve (s, p, f, x);

  for(size_t i=0U;i< dim;i++)
    input[i] = input[i]+gsl_vector_get(x,i)*mpl;///(sqrt(gsl_vector_get(scaler,i)));
//if(mpl > 0.001)
 mpl*=1;//0.97;
/*
  printf ("x = \n");
  gsl_vector_fprintf (stdout, x, "%g");
*/
  gsl_permutation_free (p);
  gsl_vector_free (x);

cntr++;
update_mkset(input);


  if(cntr >= length){
   initialize_zeta(input.size());
//    for(size_t i = 0U; i <input.size();++i)
  //   std::cout<<"zeta"<<i<<" = "<<zeta[i]<<std::endl;
   }
 
 log<<"ITERATION#"<<cntr<<" "<<energy<<" ";
 for(size_t i = 0U; i < input.size();++i){
  log<<input[i]<<" ";
  
 }
 log<<std::endl;
  bool flag = false;
  if(cntr >= length){
   flag = true;
  for(size_t i =0U; i < input.size();++i){
    if(zeta[i] > 0.4) {
    flag = false;
    break;
   }
  }
  }

  if(flag || final_flag) {
   if(final_flag==false)
    log<<"-----------------------CONVERGED--------------------"<<"\n"<<"averaging parameters..."<<std::endl;
    final_flag = true;
   for(size_t i = 0U; i < input.size();++i){
    out_par[i]+=input[i];
   }
   final_cntr++;
   }
   
   if(final_cntr ==100U) {
   for(size_t i = 0U; i < input.size();++i){
     out_par[i]=out_par[i]/final_cntr;
   }
    log<<"done... values:"<<std::endl;
    for(size_t i = 0U; i < input.size();++i){
     log<<"LAMBDA#"<<i<<" = "<<out_par[i]<<std::endl;
   }
    

   break;
  }


 }

 d.prob_calc->set_parameters(out_par);
 algorithm.run(steps);
 log.close();
 return algorithm.get_observables(steps,*(d.stat));
}
