#include "qmtconfiguration.h"
#include <stdexcept>
#include <iostream>
/*
 * Implementation of qmt::vmc::QmtVmcHoppingsMap
 */


qmt::vmc::QmtVmcHoppingsMap::QmtVmcHoppingsMap(std::vector<std::tuple<size_t,size_t,size_t>>& map):n(map.size()) {

#ifdef _QMT_VMC_DEBUG
for(int i = 0; i < map.size();++i)
 std::cout<<std::get<0>(map[i])<<" "<<std::get<1>(map[i])<<" "<<std::get<2>(map[i])<<std::endl;
#endif

    for(const auto& item : map) {
        hopping_ids.insert(std::get<2>(item));
        auto it = hoppings_map.find(std::make_tuple(std::get<0>(item),std::get<1>(item)));
        if(it == hoppings_map.end()) {
            hoppings_map[std::make_tuple(std::get<0>(item),std::get<1>(item))] = std::vector<size_t> {std::get<2>(item)};
        }
        else
            hoppings_map[std::make_tuple(std::get<0>(item),std::get<1>(item))].push_back(std::get<2>(item));

    }
}


size_t qmt::vmc::QmtVmcHoppingsMap::get_size() const {
    return n;
}


void qmt::vmc::QmtVmcHoppingsMap::set_jump(size_t i, size_t j, size_t index) {


    auto it = hoppings_map.find(std::make_tuple(i,j));
    if(it == hoppings_map.end()) {
        hoppings_map[std::make_tuple(i,j)] = std::vector<size_t> {index};
    }
    else
        hoppings_map[std::make_tuple(i,j)].push_back(index);
    hopping_ids.insert(index);
}

const std::vector<size_t>& qmt::vmc::QmtVmcHoppingsMap::is_allowed(size_t i,size_t j) const {

    auto key = std::make_tuple(i,j);
    if (hoppings_map.count(key))
      return hoppings_map.at(key);
    else{
      return empty_v;
      }

}

size_t qmt::vmc::QmtVmcHoppingsMap:: get_number_of_hoppings() const {
 return hopping_ids.size();
}



///////////////////////////////QmtVmcTwoCentresMap////////////////////////////

size_t qmt::vmc::QmtVmcTwoCentresMap::generate_key(size_t i ,size_t j,size_t k,size_t l) const {

size_t key = i*1000 + j*100 + k*10 + l;

return key;
}

size_t qmt::vmc::QmtVmcTwoCentresMap::generate_key(const configuration& conf) const {

size_t i,j,k,l;

std::tie(i,j,k,l,std::ignore,std::ignore) = conf;

return generate_key(i,j,k,l);
}

qmt::vmc::QmtVmcTwoCentresMap::QmtVmcTwoCentresMap(std::vector<configuration>& map) {
  
   
}

