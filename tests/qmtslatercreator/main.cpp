#include "../../headers/qmtslatercreator.h"
#include "../../headers/qmtslaterorbitals.h"

int main() {

qmt::QmtSlaterCreator creator1s("1s, 0, 0, 0, 0, 9");
qmt::QmtSlaterCreator creator2s("2s, 0, 0, 0, 0, 9");
qmt::QmtSlaterCreator creator3s("3s, 0, 0, 0, 0, 9");

//std::cout<<creator1s<<std::endl;
//std::cout<<creator2p<<std::endl;

qmt::SlaterOrbital* psi0_1s = creator1s.Create(0.874);
qmt::SlaterOrbital* psi1_1s = creator1s.Create(1.323);
qmt::SlaterOrbital* psi1_2s = creator2s.Create(1.323);
qmt::SlaterOrbital* psi1_3s = creator3s.Create(1.323);

double a = qmt::SlaterOrbital::overlap(*psi1_1s,*psi1_2s);
double b = qmt::SlaterOrbital::overlap(*psi1_2s,*psi0_1s);
double c = qmt::SlaterOrbital::overlap(*psi0_1s,*psi1_1s);

std::cout<<"<psi1_1s|psi1_2s> =  a =  "<<qmt::SlaterOrbital::overlap(*psi1_2s,*psi1_1s)<<std::endl;
std::cout<<"<psi0_1s|psi1_2s> =  b =  "<<qmt::SlaterOrbital::overlap(*psi0_1s,*psi1_2s)<<std::endl;
std::cout<<"<psi0_1s|psi1_1s> =  c =  "<<qmt::SlaterOrbital::overlap(*psi0_1s,*psi1_1s)<<std::endl;
std::cout<<"<psi1_1s|psi1_3s> =       "<<qmt::SlaterOrbital::overlap(*psi1_1s,*psi1_3s)<<std::endl;
std::cout<<"<psi1_3s|psi1_3s> =       "<<qmt::SlaterOrbital::overlap(*psi1_3s,*psi1_3s)<<std::endl;

qmt::SltOrb_2s slt(*(static_cast<qmt::SltOrb_2s*>(psi1_2s)));
double beta1 = c/sqrt(-2*a*b*c + b*b + c+c);
double beta2 = -b/sqrt(-2*a*b*c + b*b + c+c);
std::cout<<"Beta1 = "<<beta1<<std::endl<<"Beta2 = "<<beta2<<std::endl;
return 0;
}
