#include <iostream>
#include <vector>

#define GSL_LIBRARY

#include "../../headers/qmthubbard.h"
#include "../../headers/qmthbuilder.h"
#include "../../headers/qmtstate.h"
#include "../../headers/qmtbasisgenerator.h"
#include "../../qmtvmc/qmtvmcsingleparticleproblemsolver.h"
#include "../../qmtvmc/qmtslaterstate.h"


#define S 4

int main(){
	
	typedef qmt::QmtNState<std::bitset<S>, int> qmt_nstate;
	typedef qmt::QmtHubbard<qmt::SqOperator<qmt_nstate >> qmt_hamiltonian;
	

	// get hamiltonian
	qmt_hamiltonian* hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian("ham1s.dat");
	
	// create single-particle basis
	qmt::QmtBasisGenerator generator;
	std::vector<qmt_nstate> basis;

	generator.generate<S>(1,S/2,basis);


	// solve single-particle problem

	qmt::vmc::QmtVmcSingleParticleProblemSolver<qmt_hamiltonian> problem_solver(hamiltonian,basis);
	qmt::vmc::QmtSlaterState uncorrelated_state;
	std::vector<double> microscopic_parameters({-1.75,-0.71});

	problem_solver.get_single_particle_eigenstates(2,uncorrelated_state,microscopic_parameters);

	return 0;
}
