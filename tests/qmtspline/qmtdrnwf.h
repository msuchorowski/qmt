#include "../../headers/qmtspline.h"
//#include "../../headers/qmtparsertools"
#include <fstream>
#include <math.h>
#include <iostream>
template <typename Radial>
struct QmtDrnWaveFunction {

Radial *radial;
int _l;
double _d;
size_t size;
double max_xy,max_z;

QmtDrnWaveFunction(const char* radial_file, size_t size,int l, double d):_l(l), _d(d){

max_xy = 138 * 138;

max_z = _d/2;

double *X = new double [size];
double *Y = new double [size];

std::ifstream wf(radial_file);
       if (wf.is_open())
       {
        for(int i = 0; i <size;++i) {
	    double x,y;
	    wf>>x>>y;
	    X[i] = x;
            Y[i] = y;	    
	  }	    
	}
	wf.close();
 radial = new Radial(X,Y, size);
 delete [] X;
 delete [] Y;
}

double  get_value(double x, double y, double z) {

double distance2 = x*x + y*y ;
//std::cout<<"z = "<<z<<std::endl;

if(distance2 >= max_xy ||  z <= -2.5 || z >= 2.5) return 0;
//std::cout<<"_d = "<<_d<<std::endl;
double angular = 1.0;
if(_l < 0)
  angular= sin(fabs(_l)* atan2(y,x))*sqrt(2);
if(_l > 0)
  angular= cos(fabs(_l)* atan2(y,x))*sqrt(2);
// std::cout<<"angular = "<<angular<<std::endl;
return angular * (radial->get_value(sqrt(x*x + y*y)));// * cos(M_PI*z/_d) * sqrt(2/_d);
}



};
