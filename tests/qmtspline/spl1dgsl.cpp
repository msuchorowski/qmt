#include "../../headers/qmtspline.h"
#include "qmtdrnwf.h"

#include <iostream>
#include <math.h>
int main() {

QmtDrnWaveFunction<qmt::QmtSpline1dGsl> wf1("R0_l1.dat",5000,-1,2.5);

QmtDrnWaveFunction<qmt::QmtTableFunction1d> wf2("R0_l1.dat",5000,-1,2.5);
/*
for(double x = -139; x <= 139; x+=0.125) {
 for(double y = -139; y <= 139; y+=0.125)
   std::cout<<x<<" "<<y<<" "<<wf1.get_value(x,y,1)<<std::endl;
 std::cout<<std::endl;
}
*/

//for(double x = 0.1; x <= 139;x+=0.1)
  std::cout<<wf1.get_value(13.125,49,0.0)<<std::endl;
  std::cout<<wf2.get_value(13.125,49,0.0)<<std::endl;
/*
double M[100];// = new double[100];
double W[100];// = new double[100];

double x = 0;

for(int  i = 0; i <100; ++i) {
  x = i * 0.1 -5;
//  std::cout<<"x = "<<x<<std::endl;
  M[i] = x;
  W[i] = cos(x * x);
//X[i+50] = x;
//Y[i+50] = x * x;
}

//for(int i = 0;i < 100;++i)
// std::cout<<M[i]<<" "<<W[i]<<std::endl;

qmt::QmtSpline1dGsl spline1(M,W,100);


for(int i = 0;i < 100;++i) {
   x = i * 0.05 -5.0;

   std::cout<<x<<" "<<spline1.get_value(x)<<std::endl;
}
//delete  [] X;
//delete [] Y;
*/
return 0;
};