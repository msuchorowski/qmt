#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include "../../headers/qmtwannierbasis.h"
#include "../../headers/qmtvector.h"
#include <vector>


int main() {

//H2 molecule 1s approach


qmt::QmtWannierSlaterBasedCreator creator1s_1("[0] [0,0,0] (0) [1s, 0, 0, 0, 0, 9] (1) [1s,1000000.43, 0, 0, 0, 9]  ");
qmt::QmtWannierSlaterBasedCreator creator1s_2("[1] [1000000.43,0,0] (1) [1s, 0, 0, 0, 0, 9] (0) [1s,1000000.43, 0, 0, 0, 9] ");

std::cout<< creator1s_1<<std::endl<<creator1s_2<<std::endl;

std::vector<qmt::QmtWannierSlaterBasedCreator*> creators;
creators.push_back(&creator1s_1);
creators.push_back(&creator1s_2);

std::vector< std::tuple<int,int,int> > equations;
equations.push_back(std::make_tuple(0,0,1));
equations.push_back(std::make_tuple(0,1,0)); //should be done in parser

std::vector<double> starting_betas;
starting_betas.push_back(1.0);
starting_betas.push_back(-0.5);


qmt::QmtWannierBasis<qmt::QmtWannierSlaterBasedCreator> basis(creators,equations,starting_betas,1.19);

std::cout<<"<W_1|W_2>= "<<basis.overlap(0,1)<<std::endl<<std::flush;
std::cout<<"<W_1|W_1>= "<<basis.overlap(0,0)<<std::endl<<std::flush;
std::cout<<"<W_2|W_2>= "<<basis.overlap(0,0)<<std::endl<<std::flush;

std::vector< qmt::QmtVector > SuperCell;
SuperCell.push_back(qmt::QmtVector(0,0,0));
SuperCell.push_back(qmt::QmtVector(1000000.43,0,0));

double eps = 0.0;
double t =0.0;

eps = basis.kinetic_integral(0,0);
t = basis.kinetic_integral(0,1,SuperCell[0],SuperCell[1]);

for (const auto& vec : SuperCell){
	eps += basis.attractive_integral(0,0,vec);
	t += basis.attractive_integral(0,1,vec,SuperCell[0],SuperCell[1]);
}

std::cout<<"<W_1|H1|W_1>= "<<eps<<std::endl;
std::cout<<"<W_1|H1|W_2>= "<<t<<std::endl;
std::cout<<"<W_1 W_1|I|W_1 W_1>= "<<basis.v_integral(0,0,0,0)<<std::endl;
std::cout<<"<W_1 W_2|I|W_1 W_2>= "<<basis.v_integral(0,1,0,1,SuperCell[0],SuperCell[1],SuperCell[0],SuperCell[1])<<std::endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::cout<<std::endl<<"*****************************************************************************************"<<std::endl<<std::endl;

qmt::QmtWannierSlaterBasedCreator creator2s_1("[2] [0,0,0] (2) [1s, 0, 0, 0, 1, 9] (3) [1s,1000000.43, 0, 0, 1, 9] (4) [2s, 0, 0, 0, 1, 9] (5) [2s,1000000.43, 0, 0, 1, 9]  ");
qmt::QmtWannierSlaterBasedCreator creator2s_2("[3] [1000000.43,0,0] (3) [1s, 0, 0, 0, 1, 9] (2) [1s,1000000.43, 0, 0, 1, 9] (5) [2s, 0, 0, 0, 1, 9] (4) [2s,1000000.43, 0, 0, 1, 9]  ");
qmt::QmtWannierSlaterBasedCreator creator2p_1("[4] [0,0,0] (6) [1s, 0, 0, 0, 1, 9] (7) [1s,1000000.43, 0, 0, 1, 9] (8) [2s, 0, 0, 0, 1, 9] (9) [2s,1000000.43, 0, 0, 1, 9] (10) [2p_x, 0, 0, 0, 1, 9] (11) [2p_x,1000000.43, 0, 0, 1, 9]  ");
qmt::QmtWannierSlaterBasedCreator creator2p_2("[5] [1000000.43,0,0] (7) [1s, 0, 0, 0, 1, 9] (6) [1s,1000000.43, 0, 0, 1, 9] (9) [2s, 0, 0, 0, 1, 9] (8) [2s,1000000.43, 0, 0, 1, 9] (11) [2p_x, 0, 0, 0, 1, 9] (10) [2p_x,1000000.43, 0, 0, 1, 9]  ");


std::cout<< creator1s_1<<std::endl<<creator1s_2<<std::endl;
std::cout<< creator2s_1<<std::endl<<creator2s_2<<std::endl;
std::cout<< creator2p_1<<std::endl<<creator2p_2<<std::endl;

std::vector<qmt::QmtWannierSlaterBasedCreator*> creators2;
creators2.push_back(&creator1s_1);
creators2.push_back(&creator1s_2);
creators2.push_back(&creator2s_1);
creators2.push_back(&creator2s_2);
creators2.push_back(&creator2p_1);
creators2.push_back(&creator2p_2);

std::vector< std::tuple<int,int,int> > equations2;
equations2.push_back(std::make_tuple(0,0,1));
equations2.push_back(std::make_tuple(2,2,1));
equations2.push_back(std::make_tuple(4,4,1));
equations2.push_back(std::make_tuple(0,1,0));
equations2.push_back(std::make_tuple(0,2,0));
equations2.push_back(std::make_tuple(0,3,0));
equations2.push_back(std::make_tuple(0,4,0));
equations2.push_back(std::make_tuple(0,5,0));
equations2.push_back(std::make_tuple(2,3,0));
equations2.push_back(std::make_tuple(2,4,0));
equations2.push_back(std::make_tuple(2,5,0));
equations2.push_back(std::make_tuple(4,5,0)); //should be done in parser

std::vector<double> starting_betas2;
starting_betas2.push_back(1.0);
starting_betas2.push_back(-0.5);
starting_betas2.push_back(0.5);
starting_betas2.push_back(-0.1);
starting_betas2.push_back(2.0);
starting_betas2.push_back(-0.5);
starting_betas2.push_back(0.5);
starting_betas2.push_back(-0.1);
starting_betas2.push_back(0.5);
starting_betas2.push_back(-0.1);
starting_betas2.push_back(2.0);
starting_betas2.push_back(-0.5);

std::vector<double> alphas;
alphas.push_back(1.0);
alphas.push_back(1.0/2.0);

qmt::QmtWannierBasis<qmt::QmtWannierSlaterBasedCreator> basis2(creators2,equations2,starting_betas2,alphas);

std::cout<<"<W_1|W_2>= "<<basis2.overlap(0,1)<<std::endl<<std::flush;
std::cout<<"<W_1|W_3>= "<<basis2.overlap(0,2)<<std::endl<<std::flush;
std::cout<<"<W_1|W_4>= "<<basis2.overlap(0,3)<<std::endl<<std::flush;
std::cout<<"<W_3|W_4>= "<<basis2.overlap(2,3)<<std::endl<<std::flush;
std::cout<<"<W_1|W_1>= "<<basis2.overlap(0,0)<<std::endl<<std::flush;
std::cout<<"<W_3|W_3>= "<<basis2.overlap(2,2)<<std::endl<<std::flush;
std::cout<<"<W_5|W_5>= "<<basis2.overlap(4,4)<<std::endl<<std::flush;


std::vector< qmt::QmtVector > SuperCell2;
SuperCell2.push_back(qmt::QmtVector(0,0,0));
SuperCell2.push_back(qmt::QmtVector(1000000.43,0,0));

eps = 0.0;
t =0.0;
double eps2 = 0.0;
double t2 =0.0;
double V0 =0.0;
double eps3 = 0.0;
double t3 =0.0;
double V03 =0.0;

eps = basis2.kinetic_integral(0,0);
t = basis2.kinetic_integral(0,1,SuperCell2[0],SuperCell2[1]);
eps2 = basis2.kinetic_integral(2,2);
t2 = basis2.kinetic_integral(2,3,SuperCell2[2],SuperCell2[3]);
V0 = basis2.kinetic_integral(0,2,SuperCell2[0],SuperCell2[2]);
eps3 = basis2.kinetic_integral(4,4);
t3 = basis2.kinetic_integral(4,5,SuperCell2[4],SuperCell2[5]);
V03 = basis2.kinetic_integral(0,4,SuperCell2[0],SuperCell2[4]);

for (const auto& vec : SuperCell2){
	eps += basis2.attractive_integral(0,0,vec);
	t += basis2.attractive_integral(0,1,vec,SuperCell2[0],SuperCell2[1]);
	eps2 += basis2.attractive_integral(2,2,vec);
	t2 += basis2.attractive_integral(2,3,vec,SuperCell2[2],SuperCell2[3]);
	V0 += basis2.attractive_integral(0,2,vec,SuperCell2[0],SuperCell2[2]);
	eps3 += basis2.attractive_integral(4,4,vec);
	t3 += basis2.attractive_integral(4,5,vec,SuperCell2[4],SuperCell2[5]);
	V03 += basis2.attractive_integral(0,4,vec,SuperCell2[0],SuperCell2[4]);
}

std::cout<<"<W_1|H1|W_1>= "<<eps<<std::endl;
std::cout<<"<W_1|H1|W_2>= "<<t<<std::endl;
std::cout<<"<W_3|H1|W_3>= "<<eps2<<std::endl;
std::cout<<"<W_3|H1|W_4>= "<<t2<<std::endl;
std::cout<<"<W_1|H1|W_3>= "<<V0<<std::endl;
std::cout<<"<W_5|H1|W_5>= "<<eps3<<std::endl;
std::cout<<"<W_5|H1|W_6>= "<<t3<<std::endl;
std::cout<<"<W_1|H1|W_5>= "<<V03<<std::endl;
std::cout<<"<W_1 W_1|I|W_1 W_1>= "<<basis2.v_integral(0,0,0,0)<<std::endl;
std::cout<<"<W_1 W_2|I|W_1 W_2>= "<<basis2.v_integral(0,1,0,1,SuperCell2[0],SuperCell2[1],SuperCell2[0],SuperCell2[1])<<std::endl;
std::cout<<"<W_3 W_3|I|W_3 W_3>= "<<basis2.v_integral(2,2,2,2)<<std::endl;
std::cout<<"<W_3 W_4|I|W_3 W_4>= "<<basis2.v_integral(2,3,2,3,SuperCell2[2],SuperCell2[3],SuperCell2[2],SuperCell2[3])<<std::endl;
std::cout<<"<W_5 W_5|I|W_5 W_5>= "<<basis2.v_integral(4,4,4,4)<<std::endl;
std::cout<<"<W_5 W_6|I|W_5 W_6>= "<<basis2.v_integral(4,5,4,5,SuperCell2[4],SuperCell2[5],SuperCell2[4],SuperCell2[5])<<std::endl;

return 0;
}
