#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include "../../headers/qmtlowdin.h"

int main() {

//He


qmt::QmtWannierSlaterBasedCreator creator1s_1("[0] [0,0,0] (0) [1s, 0, 0, 0, 0, 9] (1) [2s,0, 0, 0, 1, 9] (2) [2p_x,0, 0, 0, 1, 9] ");
qmt::QmtWannierSlaterBasedCreator creator1s_2("[1] [0,0,0] (0) [1s, 0, 0, 0, 0, 9] (1) [2s,0, 0, 0, 1, 9] (2) [2p_x,0, 0, 0, 1, 9] ");


std::cout<< creator1s_1<<std::endl<<creator1s_2<<std::endl;

std::vector<double> alphas;

alphas.push_back(1.0);
alphas.push_back(1.5);

auto Wannier1 = creator1s_1.Create(alphas);
auto Wannier2 = creator1s_2.Create(alphas);

/*
Wannier1 -> set_c_by_id(0,1.25581);
Wannier1 -> set_c_by_id(1,-0.481696);


Wannier2 -> set_c_by_id(0,1.25581);
Wannier2 -> set_c_by_id(1,-0.481696);
*/

int s = 3;

double **M = new double* [s];
for(auto i = 0; i <s;i++)
   M[i] = new double;

qmt::QmtWannierSlaterBasedCreator::Wannier::multiply(*Wannier1,*Wannier1,M,s);

for(auto i = 0; i <s;i++){
  for(auto j = 0; j <s;j++)
    std::cout<<M[i][j]<<" ";
  std::cout<<std::endl;
}

qmt::qmtLowdinOrthogonalization(M, s);

std::cout<<"M after Lowdin:"<<std::endl;

for(auto i = 0; i <s;i++){
  for(auto j = 0; j <s;j++)
    std::cout<<M[i][j]<<" ";
  std::cout<<std::endl;
}

Wannier1 -> set_c_by_id(0,M[0][0]);
Wannier1 -> set_c_by_id(1,M[0][1]);
Wannier1 -> set_c_by_id(2,M[0][2]);

Wannier2 -> set_c_by_id(0,M[1][0]);
Wannier2 -> set_c_by_id(1,M[1][1]);
Wannier2 -> set_c_by_id(2,M[1][2]);


std::cout<<std::endl<<"<W_1|W_2>= "<<qmt::QmtWannierSlaterBasedCreator::Wannier::overlap(*Wannier1,*Wannier2)<<std::endl;
std::cout<<std::endl<<"<W_1|W_1>= "<<qmt::QmtWannierSlaterBasedCreator::Wannier::overlap(*Wannier1,*Wannier1)<<std::endl;
std::cout<<std::endl<<"<W_2|W_2>= "<<qmt::QmtWannierSlaterBasedCreator::Wannier::overlap(*Wannier2,*Wannier2)<<std::endl;

delete Wannier1;
delete Wannier2;

return 0;
}
