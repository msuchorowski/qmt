#include "../../qmtvmc/qmtslaterstate.h"
#include "../../qmtvmc/qmtvmcjastrow.h"
#include "../../qmtvmc/qmtconfiguration.h"
#include "../../qmtvmc/qmtconfbitset.h"
#include "../../qmtvmc/qmtvmcprobabilityGSL.h"
#include "../../qmtvmc/qmtvmchamiltonianenergy.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <math.h>
#include <stdio.h>

int main()
{
  // Two-site model with U and K
  
	std::vector<std::tuple<size_t, size_t, size_t,size_t>> jastrow_definition;
	
	jastrow_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(0,0,0,1)); // U on site 0
	jastrow_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(1,1,0,1)); // U on site 1
	jastrow_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(0,1,1,0)); // K
  
	qmt::vmc::QmtVmcJastrow jastrow(jastrow_definition);
	
	qmt::vmc::QmtSlaterState uncorrelated_state;
	double rsq = 1.0/sqrt(2);
	qmt::vmc::QmtParticleStateExpansion gamma1u (std::vector<double>({1*rsq,1*rsq}),std::vector<double>({0,0}));
	qmt::vmc::QmtParticleStateExpansion gamma1d (std::vector<double>({0,0}),std::vector<double>({1*rsq,1*rsq}));
	
	uncorrelated_state.add(&gamma1u);
	uncorrelated_state.add(&gamma1d);
	
	qmt::vmc::QmtVmcProbability *probability;
	probability = new qmt::vmc::QmtVmcProbabilityGSL<4>(2,jastrow,&uncorrelated_state);
	probability->set_parameters(std::vector<double>({-.2,0.0}));
	
	
	qmt::vmc::QmtConfigurationState* x = new qmt::vmc::QmtConfigurationStateBitset<4>(1u,1u);
	qmt::vmc::QmtConfigurationState* xprim = new qmt::vmc::QmtConfigurationStateBitset<4>(1u,1u);
	
	
	std::vector<std::tuple<size_t,size_t,size_t>> hop_map_vec;
	
	hop_map_vec.push_back(std::make_tuple(0,1,1));
	hop_map_vec.push_back(std::make_tuple(1,0,1));
	
	qmt::vmc::QmtVmcHoppingsMap *map = new qmt::vmc::QmtVmcHoppingsMap(hop_map_vec);
	
	
	std::vector<std::tuple<size_t, size_t, size_t,size_t>> interaction_definition;
	
	interaction_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(0,0,0,1)); // U on site 0
	interaction_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(1,1,0,1)); // U on site 1
	interaction_definition.push_back(std::make_tuple<size_t,size_t,size_t,size_t>(0,1,1,0)); // K
	
	std::vector<double> interactions ({1.65321, 0.956691});
	std::vector<double> hoppings ({-1.75079,-0.727647});
	std::vector<unsigned int> sp_energy ({0,0,0,0});

	
	/*QmtVmcHamiltonianEnergy(const QmtSlaterState _uncorrelated_state, 
			const std::vector<std::tuple<size_t,size_t,size_t>>& _interaction_defs,
			const std::vector<double>& _interaction_parameters, 
			const std::vector<double>& _single_particle_energy, 
			const std::vector<double>& _hopping_amplitudes,
			const QmtVmcHoppingsMap* _hopping_map,
			const QmtVmcProbability* _probability){
			*/
	qmt::vmc::QmtVmcHamiltonianEnergy energy_calculator(uncorrelated_state,interaction_definition,interactions,sp_energy,hoppings,map,probability);
	
	

//	qmt::vmc::QmtConfigurationState* xx = new qmt::vmc::QmtConfigurationState(*x);
//	const auto xx = x;
//	qmt::vmc::QmtConfigurationStateBitset<4> from(*static_cast<qmt::vmc::QmtConfigurationStateBitset<4>*>(x));
//	qmt::vmc::QmtConfigurationStateBitset<4>  to(1u,1u);
	std::shared_ptr<qmt::vmc::QmtConfigurationState> from(x);
	double enrg = 0.0;
	for(int l = 0;l < 200; l++) {
		for(int m = 0;m < 1; m++) {
	for(int i = 0; i < 500000;i++) {
		probability->set_parameters(std::vector<double>({-1.2 + l *0.01,m * 0.02}));
//	qmt::vmc::QmtConfigurationState   xx(*static_cast<qmt::vmc::QmtConfigurationStateBitset<4>*>(x));
		auto hopps_tr = from->get_hoppings(*map);

	int r = (double(rand())/RAND_MAX) * hopps_tr.size();
//	qmt::vmc::QmtConfigurationStateBitset<4> pp =(hopps_tr[r].get_to())

	auto to = hopps_tr[r].get_to_ptr();
//	qmt::vmc::QmtConfigurationStateBitset<4>* ppp = static_cast<qmt::vmc::QmtConfigurationStateBitset<4>*>(from);
//	qmt::vmc::QmtConfigurationStateBitset<4>* pppp = static_cast<qmt::vmc::QmtConfigurationStateBitset<4>*>(to);
//	std::cout<<"from:"<<*ppp<<std::endl;
//	std::cout<<"to:"<<*pppp<<std::endl;
//	for(int j = 0; j < hopps_tr.size();++j) {
//	qmt::vmc::QmtConfigurationStateBitset<4>* pp = static_cast<qmt::vmc::QmtConfigurationStateBitset<4>*>(hopps_tr[j].get_to_ptr());
//	std::cout<<*pp<<std::endl;
//	}


	double r2 = (double(rand())/RAND_MAX);
	double prob = probability->probability(from.get(),to.get());
//	std::cout<<"p = "<<prob<<std::endl;
	if(r2 < prob ) {from = to; }
	enrg += energy_calculator.getEnergy(*from)+2.0/1.43042;
//	std::cout<<" E="<<energy_calculator.getEnergy(*from)+2.0/1.43042<<std::endl;
	}
		std::cout<<-1.2 + l*0.01<< " "<< m * 0.02<<" "<<0.5*enrg/500000<<std::endl;
		enrg = 0.0;
	}
//	std::cout<<std::endl;
	}

	return 0;
}
