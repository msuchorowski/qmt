#include "../../headers/qmtstate.h"
#include "../../headers/qmtbasisgenerator.h"
#include <vector>
#include "omp.h"

int main() {
std::vector<qmt::QmtNState <qmt::uint, int> > states;
qmt:: QmtBasisGenerator generator;      
generator.generate(12,24,states);



//for(const auto &s : states)
//std::cout<<s<<std::endl;
std::cerr<<"size = "<<states.size()<<std::endl;

qmt::QmtStatesCombination<qmt::QmtNState <qmt::uint, int> > combinations(states);

double t = omp_get_wtime(); 
std::cerr<<"Perfoming calculations..."<<std::endl;
for(int i = 0; i < 1; ++i)
 for(int j = 0; j < 1; ++j)
   for(int k = 0; k < 12; ++k)
     for(int l = 0; l < 12; ++l) {
combinations.two_body_action(qmt::QmtSpin::down,qmt::QmtSpin::down,i,j,k,l);
combinations.two_body_action(qmt::QmtSpin::up,qmt::QmtSpin::up,i,j,k,l);
combinations.two_body_action(qmt::QmtSpin::up,qmt::QmtSpin::down,i,j,k,l);
combinations.two_body_action(qmt::QmtSpin::down,qmt::QmtSpin::up,i,j,k,l);

 }
std::cerr<<"...done in time:"<<omp_get_wtime() - t<<"s"<<std::endl;

return 0;
}