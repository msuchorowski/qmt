
#include "../../headers/qmtsystem.h"
#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"
#include ../../headers/"qmtmicrotrans.h"
#include "../../headers/qmthubbard.h"
   
   typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 
   typedef  qmt::QmtMicroscopicTransInvariant<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> qmt_standard_system;
   
   int main () {
     
     qmt_standard_system system("fake", "fake", 
		       "fake", "fake",std::vector<double>());
     
    return 0; 
   }