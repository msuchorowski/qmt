#include <iostream>
#include "../../headers/qmthubbard.h"
#include "../../headers/qmtstate.h"
#include "../../headers/qmtbasisgenerator.h"
#include "../../headers/qmtmclanczos.h"
#include <bitset>
#include <cmath>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>

#define S 24
#define LANCZOS 6
#define MCHF 6
typedef qmt::QmtNState<std::bitset<S>, int> qmt_nstate;
typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<std::bitset<S>, int> >> qmt_hamiltonian; 
typedef qmt::SqOperator<qmt::QmtNState<std::bitset<S>, int> > qmt_operator;


/*
typedef qmt::QmtNState<unsigned int, int> qmt_nstate;
typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<unsigned int, int> >> qmt_hamiltonian; 
typedef qmt::SqOperator<qmt::QmtNState<unsigned int, int> > qmt_operator;
*/


bool my_compare(qmt_nstate one, qmt_nstate two){
	return fabs(one.getPhase())>fabs(two.getPhase());
}

int main() {
  
  double t0 = -0.727647;
  double e0 = -1.75079;
  double U  = 1.65321;
  double K = 0.956691;


//  t0=0.1;
//  e0 = -1.;
//  U = 0;
//  K = 0.5;

  
  std::vector<double> one_body({0.0,t0,e0});

  std::vector<double> two_body({U,K});
//  t0 = 0.;
//  e0 = 1.;
//  U  = 0.;
//  K = 0.;
  
  qmt_hamiltonian hamiltonian;
  
  hamiltonian.addOneBodyTerm(t0);
    
  for(int i = 0; i < S/2;++i){
  for(int j = 0; j < i;++j){
	if(i!=j){
  hamiltonian.addOneBody(0,qmt_operator::get_up_creator(i),qmt_operator::get_up_anihilator(j));
  hamiltonian.addOneBody(0,qmt_operator::get_up_creator(j),qmt_operator::get_up_anihilator(i));
  
  hamiltonian.addOneBody(0,qmt_operator::get_down_creator(i),qmt_operator::get_down_anihilator(j));
  hamiltonian.addOneBody(0,qmt_operator::get_down_creator(j),qmt_operator::get_down_anihilator(i));
}
}
}

 hamiltonian.addOneBodyTerm(e0);
    
  for(int i = 0; i < S/2;++i){
  hamiltonian.addOneBody(1,qmt_operator::get_up_creator(i),qmt_operator::get_up_anihilator(i));
  
  hamiltonian.addOneBody(1,qmt_operator::get_down_creator(i),qmt_operator::get_down_anihilator(i));
}
  
  hamiltonian.addTwoBodyTerm(U);
  hamiltonian.addTwoBodyTerm(K);
  
  for(int i = 0; i < S/2;++i)
  hamiltonian.addTwoBody(0,qmt_operator::get_up_creator(i),qmt_operator::get_up_anihilator(i),qmt_operator::get_down_creator(i),qmt_operator::get_down_anihilator(i));
  
  for(int i = 0; i < S/2;++i){
    for(int j = i; j < S/2;++j){
     if( i != j) {
      hamiltonian.addTwoBody(1,qmt_operator::get_up_creator(i),qmt_operator::get_up_anihilator(i),qmt_operator::get_up_creator(j),qmt_operator::get_up_anihilator(j));
      hamiltonian.addTwoBody(1,qmt_operator::get_up_creator(i),qmt_operator::get_up_anihilator(i),qmt_operator::get_down_creator(j),qmt_operator::get_down_anihilator(j));
      hamiltonian.addTwoBody(1,qmt_operator::get_down_creator(i),qmt_operator::get_down_anihilator(i),qmt_operator::get_down_creator(j),qmt_operator::get_down_anihilator(j));
      hamiltonian.addTwoBody(1,qmt_operator::get_down_creator(i),qmt_operator::get_down_anihilator(i),qmt_operator::get_up_creator(j),qmt_operator::get_up_anihilator(j));
     }
    }
  }
 	  


  qmt::QmtBasisGenerator generator;
  std::vector<qmt_nstate> states;
  std::vector<qmt_nstate> output;
  
  //generator.generate<S>(S/2,S,states);
//  std::cout<<"Basis generated size:"<<states.size()<<std::endl; 

  
  std::vector<qmt_nstate> states2;

/*
	auto size=states.size();
	for(int i=0U; i < MCHF; i++)
		states2.push_back(states[(size/MCHF)*i]);
	
	double norm=0.0;
	
	for (const auto& state : states2)
		norm += pow(state.getPhase(),2);

	norm=1.0/sqrt(norm);

	for (auto& state : states2)
		state.setPhase(state.getPhase()*norm);
*/

	qmt_nstate start(S/2,0);

	for(int i=0; i<S/2; i++){
		if (i%2)
			start.qmt_nstate::create_down(i);
		else
			start.qmt_nstate::create_up(i);
	}
	states2.push_back(start);
	start= qmt_nstate(S/2,0);
	for(int i=0; i<S/2; i++){
		start.qmt_nstate::create_up(i);
	}
	states2.push_back(start);

for(int i=1; i<=6; i++){
  qmt::QmtMCLanczosDiagonalizer<qmt_hamiltonian,qmt_nstate> qmt_diag(hamiltonian,states2,2,2,1000*i,300);
  qmt_diag.Diagonalize(one_body,two_body);
}

return 0;
}
