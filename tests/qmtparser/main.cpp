#include "../../headers/qmtparsertools.h"
#include "../../headers/qmtvector.h"
#include <vector>

int main() {

std::vector<std::string>  output = qmt::parser::get_delimited_words(",.:/ }","//.,Ania:..Basia,Tomek,Ba rtek,:");

for(const auto &out : output)
  std::cout<<out<<std::endl; 

qmt::parser::stringToOrbitalType("3s");

qmt::QmtVector one = qmt::parser::get_QmtVector_from_line("(0,0,0)");

std::cout<<one<<std::endl;

std::vector<qmt::QmtVector> myvectors = qmt::parser::get_QmtVector_from_file("vectors.dat");

for (const auto& vec : myvectors)
   std::cout<<vec<<std::endl;

return 0;
}
