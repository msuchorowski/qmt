#include "../../headers/qmtwanniercreator.h"
#include "../../headers/qmtslaterorbitals.h"

int main() {

//H2 molecule 1s approach


qmt::QmtWannierSlaterBasedCreator creator1s_1("[0] [0,0,0] (0) [1s, 0, 0, 0, 0, 9] (1) [1s,1.43, 0, 0, 0, 9]  ");
qmt::QmtWannierSlaterBasedCreator creator1s_2("[1] [1.43,0,0] (1) [1s, 0, 0, 0, 0, 9] (0) [1s,1.43, 0, 0, 0, 9] ");


std::cout<< creator1s_1<<std::endl<<creator1s_2<<std::endl;

auto Wannier1 = creator1s_1.Create(1.19);
auto Wannier2 = creator1s_2.Create(1.19);

Wannier1 -> set_c_by_id(0,1.25581);
Wannier1 -> set_c_by_id(1,-0.481696);


Wannier2 -> set_c_by_id(0,1.25581);
Wannier2 -> set_c_by_id(1,-0.481696);


std::cout<<std::endl<<"<W_1|W_2>= "<<qmt::QmtWannierSlaterBasedCreator::Wannier::overlap(*Wannier1,*Wannier2)<<std::endl;
std::cout<<std::endl<<"<W_1|W_1>= "<<qmt::QmtWannierSlaterBasedCreator::Wannier::overlap(*Wannier1,*Wannier1)<<std::endl;
std::cout<<std::endl<<"<W_2|W_2>= "<<qmt::QmtWannierSlaterBasedCreator::Wannier::overlap(*Wannier2,*Wannier2)<<std::endl;

delete Wannier1;
delete Wannier2;

return 0;
}
