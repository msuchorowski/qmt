#include <math.h>
#include "../headers/node.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

/****************************************************************************************************************************/

static double Slater1sIntegral(double r, double alpha) {
    double denominator = alpha * r;
    double rev = 1/denomiator ? denominator > 0 : 0.0;

    return -2*alpha*( rev - exp(-2*alpha*r) *(1 + rev));

}

/****************************************************************************************************************************/

template <typename Orbital>
static double t1Pi(double r, double alpha,const Node<Orbital>& wannier1,const Node<Orbital>& wannier2, int m, int n) {

    double i1 = 0;
    double i2 = 0;

    for(int i = 0; i < m; ++i)
        i1+=Slater1sIntegral(wannier1.get_self().distance(wannier1.get_neighbor(i)), alpha);

    for(int i = 0; i < n; ++i)
        i2+=Slater1sIntegral(wannier2.get_self().distance(wannier2.get_neighbor(i)), alpha);

    return alpha*alpha * exp(-alpha * r)*(1 + alpha * r - alpha * alpha* r* r/3) -
           4 * alpha * exp(-alpha * r)*(1 + alpha * r) + i1 + i2;
}


static void show_matrix(gsl_matrix *matrix) {
    int m = matrix->size1;
    int n = matrix->size2;

    for(int i =0; i < m; ++i) {
        for(int j = 0; j < n; ++j)
            std::cout<<gsl_matrix_get(m,i,j)<<" ";
        std::cout<<std::endl;
    }

};


/****************************************************************************************************************************/


template<typename Orbital>
double hopping(gsl_matrix *output,gsl_vector* beta,gsl_vector* temp, Node<Orbital> wannier1, Node<Orbital> wannier2, int N, double alpha) {

    for(int i = 0; i < N; ++i) {
        for(int j = 0; j < N; ++j) {
            double r;
            r = wannier1.get_neighbor(i).distance(wannier2.get_neigbor(j));
            gsl_matrix_set(output, i,j,t1Pi(r, alpha, wannier1, wannier2, N, N));

        }
    }

    double result;
    show_matrix(output);
    exit(0);
    gsl_blas_dgemv (CblasNoTrans, 1.0, output, beta, 0.0, temp);
    gsl_blas_ddot  (beta, temp, &result);
    return result;
}

/****************************************************************************************************************************/
