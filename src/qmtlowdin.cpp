#include "../headers/qmtlowdin.h"
namespace qmt {



void qmtLowdinOrthogonalization(double **M, size_t s) {
 gsl_vector* eigenvalues=gsl_vector_calloc(s);
			gsl_matrix* eigenvectors = gsl_matrix_calloc(s,s);
                 	gsl_matrix* eigenvectors_trans = gsl_matrix_calloc(s,s);
                        gsl_matrix* smhalf = gsl_matrix_calloc(s,s);
			gsl_matrix* matrix = gsl_matrix_calloc(s,s);
			
			gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(
					s);
for(size_t i = 0; i <s; ++i) {
  for(size_t j = 0; j <s; ++j)
    gsl_matrix_set(matrix,i,j,M[i][j]);
}
			
			gsl_eigen_symmv(matrix, eigenvalues, eigenvectors, workspace);

			gsl_eigen_symmv_free(workspace);
	
//			gsl_eigen_symmv_sort(eigenvalues, eigenvectors, GSL_EIGEN_SORT_VAL_ASC);

gsl_matrix_memcpy(eigenvectors_trans,eigenvectors);
gsl_matrix_transpose(eigenvectors_trans);

gsl_matrix_set_zero (smhalf);
gsl_matrix_set_zero (matrix);

for(size_t i = 0; i < s;++i) {
   gsl_matrix_set(smhalf,i,i,1.0/sqrt(gsl_vector_get(eigenvalues,i)));
}

 gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                  1.0, eigenvectors, smhalf,
                  0.0, matrix);

 gsl_matrix_set_zero (smhalf);
 
 gsl_blas_dgemm (CblasNoTrans, CblasTrans,
                  1.0, matrix, eigenvectors,
                  0.0, smhalf);


 for(size_t i = 0; i <s; ++i) {
  for(size_t j = 0; j <s; ++j){
   M[i][j] =  gsl_matrix_get(smhalf,i,j);
#ifdef _QMT_DEBUG
   //std::cout<<M[i][j]<<" ";
#endif
   }
#ifdef _QMT_DEBUG
   //std::cout<<std::endl;
#endif
}


	                gsl_matrix_free(matrix);
			gsl_matrix_free(eigenvectors);
			gsl_matrix_free(eigenvectors_trans);
			gsl_matrix_free(smhalf);
			gsl_vector_free(eigenvalues);

}



}

