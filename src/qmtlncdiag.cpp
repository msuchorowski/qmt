#include "../headers/qmtlncdiag.h"

qmt::QmtLanczosDiagonalizer::QmtLanczosDiagonalizer(const char* file_name, int option): _lanczos_steps(2000), _lanczos_eps(1.0e-9) {
  
  std::ifstream cfile;       
  cfile.open(file_name); 
  
  std::string content( (std::istreambuf_iterator<char>(cfile) ),
                       (std::istreambuf_iterator<char>()    ) );
  
   std::string hamiltonian_fn = qmt::parser::get_bracketed_words(content,"hamiltonian_file_name","hamiltonian_file_name_end")[0];
   problem_size = std::stoi(qmt::parser::get_bracketed_words(content,"problem_size","problem_size_end")[0]);
   number_of_centers=problem_size;
   electron_number = std::stoi(qmt::parser::get_bracketed_words(content,"electron_number","electron_number_end")[0]);
   hamiltonian = qmt::QmtHamiltonianBuilder<qmt_hamiltonian>::getHamiltonian(hamiltonian_fn);
   generator.generate(electron_number,problem_size*2,states);
  #ifdef _QMT_DEBUG
   for(auto item : states)
   std::cout<<item<<std::endl;
  #endif
   formula.set_hamiltonian(*hamiltonian);
   formula.set_StatesOptional(states,option);  
   problem_size=states.size();
   LocalAlgebras::SparseAlgebra::InitializeMatrix(hamiltonian_M,problem_size,problem_size);
   LocalAlgebras::SparseAlgebra::InitializeVector(eigenVector,problem_size);
   LanczosSolver = new qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra>(problem_size,_lanczos_steps, _lanczos_eps); 
   cfile.close();
}

double qmt::QmtLanczosDiagonalizer::Diagonalize(const std::vector<double>& one_body_integrals, 
						const std::vector<double>& two_body_integrals,const std::vector<double>& params1,const std::vector<double>& params2,const std::vector<double>& params3) {
  double external_energy = one_body_integrals[0];
  
  #ifdef _QMT_DEBUG
  std::cout<<"Coulomb = "<<external_energy<<std::endl;
  #endif
  
  for(auto i = 1U; i < one_body_integrals.size() ;++i) {
     formula.set_microscopic_parameter(1,i-1,one_body_integrals[i]);
     
     #ifdef _QMT_DEBUG
     std::cout<<"I1["<<i-1<<"]= "<<one_body_integrals[i]<<std::endl;
     #endif
  }
  
  for(auto i = 0U; i < two_body_integrals.size() ;++i) {
     formula.set_microscopic_parameter(2,i,two_body_integrals[i]);
     #ifdef _QMT_DEBUG
     std::cout<<"I2["<<i<<"]= "<<two_body_integrals[i]<<std::endl;
     #endif
  }
  
  
  formula.get_Hamiltonian_Matrix(hamiltonian_M);
  #ifdef _QMT_DEBUG
  LocalAlgebras::SparseAlgebra::PrintMatrix(hamiltonian_M);
  #endif
  LanczosSolver->solve(hamiltonian_M);
  
  LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);
  #ifdef _QMT_DEBUG
  for(int i = 0; i < problem_size;i++){
  std::cout<<states[i]<<" "<<LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,i)<<std::endl;
  }
  #endif
 
  return LanczosSolver->get_minimal_eigenvalue() + external_energy;
}


  void qmt::QmtLanczosDiagonalizer::NWaveFunctionEssentials(std::vector<double>& averages){
    LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);
for(int i=0; i<number_of_centers; i++){
        for(int j=0; j<number_of_centers; j++){
            double average=0.0;
            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(j)(states[l])));
                }
            }
            averages.push_back(average);
        }
    }
    for(int i=0; i<number_of_centers; i++){
        for(int j=0; j<number_of_centers; j++){
            double average=0.0;
            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){
                    average+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(j)(states[l])));
                }
            }
            averages.push_back(average);
        }
    }

}

/*
/Double occupancy
*/

double qmt::QmtLanczosDiagonalizer::DoubleOccupancy(){

LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);


double d2=0.0;

for(int i=0; i<number_of_centers; i++){
            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){
                    d2+=LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(states[l])))));
                }
            }
    }

d2/=number_of_centers;

return d2;
}


void qmt::QmtLanczosDiagonalizer::DoubleOccupancy(std::vector<double> &v){

LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);



for(int i=0; i<number_of_centers; i++){
            v.push_back(0);
            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){
                    v[i]+=(LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(states[l]))))));
                }
            }
    }

}

/*
/Dimer-dimer
*/


double qmt::QmtLanczosDiagonalizer::DimerDimerAvs(size_t i, size_t j){

LanczosSolver->get_minimal_eigenvector(eigenVector,hamiltonian_M);


double uuuu = 0.0;
double uuud = 0.0;
double uudu = 0.0;
double uduu = 0.0;
double duuu = 0.0;

double uudd = 0.0;
double udud = 0.0;
double duud = 0.0;
double dudu = 0.0;
double dduu = 0.0;
double uddu = 0.0;

double dddd = 0.0;
double dddu = 0.0;
double ddud = 0.0;
double dudd = 0.0;
double uddd = 0.0;
 




            for(int k=0; k<problem_size; k++){
                for(int l=0; l<problem_size; l++){

uuuu += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

uuud -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));


uudu -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));


uduu -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));


duuu -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

 
uudd += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));


udud += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));

duud += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

dudu += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

dduu += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));

uddu += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));


dddd += LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));


dddu -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_up_creator(j+1)(sq_operator::get_up_anihilator(j+1)(states[l])))))))));


ddud -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_up_creator(j)(sq_operator::get_up_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));

dudd -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_down_creator(i)(sq_operator::get_down_anihilator(i)(sq_operator::get_up_creator(i+1)(sq_operator::get_up_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));


uddd -= LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,k)*LocalAlgebras::SparseAlgebra::GetVectorE(eigenVector,l)*NState::dot_product(states[k],sq_operator::get_up_creator(i)(sq_operator::get_up_anihilator(i)(sq_operator::get_down_creator(i+1)(sq_operator::get_down_anihilator(i+1)(sq_operator::get_down_creator(j)(sq_operator::get_down_anihilator(j)(sq_operator::get_down_creator(j+1)(sq_operator::get_down_anihilator(j+1)(states[l])))))))));
               }
            }
  


return  0.5*(uuuu + uuud + uudu + uduu + duuu + uudd + udud + duud + dudu + dduu + uddu + dddd + dddu + ddud + dudd + uddd);


}






qmt::QmtLanczosDiagonalizer::~QmtLanczosDiagonalizer() {
  delete LanczosSolver;
}
