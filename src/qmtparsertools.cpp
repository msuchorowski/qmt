#include "../headers/qmtparsertools.h"

namespace qmt {

namespace parser {



std::vector<std::string> get_delimited_words(const std::string& delimiters, const std::string& phrase) {
    std::vector<std::string> output;
    std::string word;
    for(const auto &letter : phrase) {

        if(std::any_of(delimiters.begin(),delimiters.end(),[&](const char c)->bool { return c==letter;}) || &letter == &phrase[phrase.size() - 1]) {
            if(word.size() > 0 && &letter != &phrase[phrase.size() - 1] )
                output.push_back(word);
            else {
                if(!std::any_of(delimiters.begin(),delimiters.end(),[&](const char c)->bool { return c==letter;}))
                    word = word + letter;
                if(word.size() > 0)
                    output.push_back(word);
            }

            word.clear();
            continue;
        }
        word = word + letter;
    }
    return output;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<std::string> get_bracketed_words(const std::string& input, const char& open, const char& close, bool CLEAR_WHITE_SPACES) {
    if (open == close) {
        throw std::logic_error("qmt::parser::get_bracketed_words: \'open\' and \'close\' marks can not be the same!");
    }
    if (isspace(open)) { // function is ignoring strings consiting only of white spaces
        throw std::logic_error("qmt::parser::get_bracketed_words: \'open\' mark can not be a white space!");
    }
    if (isspace(close)) {
        throw std::logic_error("qmt::parser::get_bracketed_words: \'close\' mark can not be a white space!");
    }
    std::vector<std::string> output;

    bool IF_IN_BRACKETS=false;
    std::string tmp_str;

    for (auto iter=input.begin(); iter!=input.end(); iter++) {
        if (CLEAR_WHITE_SPACES && isspace(*iter)) continue; //if CLEAR_WHITE_SPACES is switched, function will ignore white spaces
        if (*iter == open && IF_IN_BRACKETS) continue; // ignore double opening

        if (*iter == close && IF_IN_BRACKETS) { // closing brackets; note: all open, but not closed brackets will not contribute to the resultant output
            IF_IN_BRACKETS=false;
            if (tmp_str.length()) {
                bool IS_EMPTY=true; // checking if string is not empty (is not consisted of white spaces)
                for (auto iter2=tmp_str.begin(); iter2!=tmp_str.end(); iter2++) {
                    if (!isspace(*iter2))
                        IS_EMPTY=false ;
                }
                if(!IS_EMPTY) output.push_back(tmp_str);
            }
            tmp_str.clear();
        }

        if (IF_IN_BRACKETS) {
            tmp_str+=*iter;
        }

        if (*iter == open && !IF_IN_BRACKETS) { // opening brackets
            IF_IN_BRACKETS=true;
        }
    }
    return output;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::string> get_bracketed_words(const std::string& input, const std::string& open, const std::string& close) {
    
  if (open == close) {
        throw std::logic_error("qmt::parser::get_bracketed_words: \'open\' and \'close\' marks can not be the same!");
    }
    
    std::vector<std::string> output;
    std::string current = input;
    current.erase( std::remove_if(  current.begin(),  current.end(), ::isspace ),  current.end() );
    size_t open_size = open.size();
    size_t close_size = close.size();
    
    while(1) {
      size_t begin = current.find(open);
      size_t end = current.find(close);
      if(end <=begin || end == std::string::npos || begin == std::string::npos)
	break;
      output.push_back(current.substr(begin + open_size,end - begin - open_size));
      current = current.substr(end + close_size);    
    }
    return output;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////

qmt::OrbitalType stringToOrbitalType(const std::string& orbital) {
    if(orbital == "1s") return qmt::OrbitalType::_1s;
    else if (orbital == "2s")  return qmt::OrbitalType::_2s;
    else if (orbital == "2p_x")  return qmt::OrbitalType::_2p_x;
    else if (orbital == "2p_xp")  return qmt::OrbitalType::_2p_xp;
    else if (orbital == "2p_xm")  return qmt::OrbitalType::_2p_xm;
    else if (orbital == "2p_y")  return qmt::OrbitalType::_2p_y;
    else if (orbital == "2p_yp")  return qmt::OrbitalType::_2p_yp;
    else if (orbital == "2p_ym")  return qmt::OrbitalType::_2p_ym;
    else if (orbital == "2p_z")  return qmt::OrbitalType::_2p_z;
    else if (orbital == "2p_zp")  return qmt::OrbitalType::_2p_zp;
    else if (orbital == "2p_zm")  return qmt::OrbitalType::_2p_zm;
    else if (orbital == "3s")    return qmt::OrbitalType::_3s;
    else if (orbital == "3p_x")  return qmt::OrbitalType::_3p_x;
    else if (orbital == "3p_y")  return qmt::OrbitalType::_3p_y;
    else if (orbital == "3p_z")  return qmt::OrbitalType::_3p_z;
    else if (orbital == "3d_z2") return qmt::OrbitalType::_3d_z2;
    else if (orbital == "3d_xz") return qmt::OrbitalType::_3d_xz;
    else if (orbital == "3d_yz")  return qmt::OrbitalType::_3d_yz;
    else if (orbital == "3d_xy")  return qmt::OrbitalType::_3d_xy;
    else if (orbital == "3d_x2y2")return qmt::OrbitalType::_3d_x2y2;
    else
        throw std::logic_error("qmt::parser::stringToOrbitalType: unknown OrbitalType: " + std::string(orbital));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

std::string orbitalTypeToString(qmt::OrbitalType orbital) {
    switch (orbital) {
    case qmt::OrbitalType::_1s:
        return std::string("1s");
    case qmt::OrbitalType::_2s:
        return std::string("2s");

    case qmt::OrbitalType::_2p_x:
        return std::string("2p_x");
    case qmt::OrbitalType::_2p_xm:
        return std::string("2p_xm");
    case qmt::OrbitalType::_2p_xp:
        return std::string("2p_xp");
    

    case qmt::OrbitalType::_2p_y:
        return std::string("2p_y");
    case qmt::OrbitalType::_2p_ym:
        return std::string("2p_ym");
    case qmt::OrbitalType::_2p_yp:
        return std::string("2p_yp");
        
    case qmt::OrbitalType::_2p_z:
        return std::string("2p_z");
    case qmt::OrbitalType::_2p_zm:
        return std::string("2p_zm");
    case qmt::OrbitalType::_2p_zp:
        return std::string("2p_zp");
        
    case qmt::OrbitalType::_3s:
        return std::string("3s");
    case qmt::OrbitalType::_3p_x:
        return std::string("3p_x");
    case qmt::OrbitalType::_3p_y:
        return std::string("3p_y");
    case qmt::OrbitalType::_3p_z:
        return std::string("3p_z");
    case qmt::OrbitalType::_3d_z2:
        return std::string("3d_z2");
    case qmt::OrbitalType::_3d_xz:
        return std::string("3d_xz");
    case qmt::OrbitalType::_3d_yz:
        return std::string("3d_yz");
    case qmt::OrbitalType::_3d_xy:
        return std::string("3d_xy");
    case qmt::OrbitalType::_3d_x2y2:
        return std::string("3d_x2y2");
    }
    throw std::logic_error("qmt::orbitalTypeToString: unknown OrbitalType");
}

std::vector<int> get_atomic_numbers(const std::string& file_name){
  
   std::ifstream input_file(file_name.c_str());
      std::string   content( (std::istreambuf_iterator<char>(input_file) ),
                     (std::istreambuf_iterator<char>()    ) );
      input_file.close();
      
   auto words_to_parse = get_bracketed_words(content,"open_atomic_numbers","close_atomic_numbers");
   
   auto words_numbers = get_delimited_words(",",words_to_parse[0]);
   std::vector<int> values;
   
   for(const auto& word : words_numbers) {
     auto lword = word;
     lword.erase( std::remove_if( lword.begin(), lword.end(), ::isspace ), lword.end() ); //remove blanks
     values.push_back(std::stoi(lword));
   }
   
   return values;
}

QmtVector get_QmtVector_from_line(const std::string& line){
 auto bracketed = get_bracketed_words(line,"(",")");
 
 auto myvector = get_delimited_words(",",bracketed[0]);
 
 if(myvector.size()!=3)
   throw std::logic_error("qmt::parser::get_QmtVector_from_line : expression in brackets is not a 3D-vector!");
 
 return QmtVector(std::stod(myvector[0]),std::stod(myvector[1]),std::stod(myvector[2]));
   
}

std::vector<qmt::QmtVector> get_QmtVector_from_file(const std::string& file_name){

    std::vector<qmt::QmtVector> result;
  
    std::ifstream input_file(file_name.c_str());
       
    std::string line;
    while (std::getline(input_file, line)){
      // removing comments
      std::size_t pos = line.find("#");
      if (pos<line.length())
	line.erase(line.begin()+pos,line.end());
      
      //removing white-spaces if line was not erased
      if(line.length()){
	std::size_t found = line.find_first_of(" \t");
	while (found!=std::string::npos)
	{
	  line.erase(line.begin()+found);
	  found=line.find_first_of(" \t",found+1);
	}
      }
      if(line.find("(")==0) //if line starts with bracket
	result.push_back(get_QmtVector_from_line(line));
    }
    
      input_file.close();
      
      return result;
}

} //end namespace parser
} //end namespace qmt
