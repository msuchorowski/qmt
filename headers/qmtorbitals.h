#include "qmvars.h"
#include "qmtvector.h"
#include <omp.h>
#include <cmath>
#include <math.h>
#include <vector>
#include <algorithm>
#include <utility>
#include <tuple>
#include <memory>
//#ifdef GSL_SUPPORT
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_sf_erf.h>

//#endif

#include <math.h>
#include <stdexcept>


#ifndef QMTORBITALS_H_INCLUDED
#define QMTORBITALS_H_INCLUDED

class Slater1S {

    double _x;
    double _y;
    double _z;

    double _ri;
    double _alpha;
public:

    Slater1S(double x, double y, double z, double alpha ):_x(x), _y(y), _z(z), _alpha(alpha) {
        _ri = sqrt(_x*_x + _y*_y + _z*_z);
    }


    double distance(const Slater1S& slater) const {

        double dx = _x - slater._x;
        double dy = _y - slater._y;
        double dz = _z - slater._z;

        return sqrt(dx*dx + dy*dy + dz*dz);
    }

    static double overlap(const Slater1S& slater1, const Slater1S& slater2)  {
        double d = slater1.distance(slater2);
        double alpha = slater1._alpha;
        return exp(-alpha * d) * (1 + alpha * d + alpha*alpha *d *d /3);
    }

};

//////////////////////////////////////////////////////////////////////////////////////////////

class Gauss {


    //double r_x;
    // double r_y;
    // double r_z;

    qmt::QmtVector v;
    double _gamma;
    double prefactor;

    static double get_prefactor(double gamma) {
        return  pow(2 * gamma *gamma /3.14159265, 0.75);
    }

    /******************************************************************************/

    static double two_site_distance(const Gauss& g_i, const Gauss& g_j) {


        return qmt::QmtVector::norm(g_i.v - g_j.v);
    }

    /******************************************************************************/

    static double three_site_distance(const Gauss& g_i,const Gauss& g_j,
                                      double rx, double ry, double rz) {

        double x = g_j.v.get_x()- g_i.v.get_x();
        double y = g_j.v.get_y()- g_i.v.get_y();
        double z = g_j.v.get_z()- g_i.v.get_z();

        double inv = 1/(1+ (g_i._gamma * g_i._gamma/(g_j._gamma * g_j._gamma)));

        x = rx - (x * inv + g_i.v.get_x());
        y = ry - (y * inv + g_i.v.get_y());
        z = rz - (z * inv + g_i.v.get_z());

        return sqrt( x * x + y * y + z * z);

    }


    static double three_site_distance(const Gauss& g_i,const Gauss& g_j,
                                      const qmt::QmtVector& i_translation,const qmt::QmtVector& j_translation,
                                      double rx, double ry, double rz) {

        qmt::QmtVector dv = g_j.v + j_translation - g_i.v - i_translation;
        qmt::QmtVector dr(rx,ry,rz);

        double inv = 1/(1+ (g_i._gamma * g_i._gamma/(g_j._gamma * g_j._gamma)));

        dv = inv * dv + g_i.v + i_translation;

        return qmt::QmtVector::norm(dr - dv);

    }

    /******************************************************************************/

    static double four_site_distance(const Gauss& g_i, const Gauss& g_j, const Gauss& g_k, const Gauss& g_l) {

        double g_ik = g_i._gamma / g_k._gamma;
        double g_jl = g_j._gamma / g_l._gamma;

        double d_ik = 1.0 / ( 1.0 + g_ik * g_ik );
        double d_jl = 1.0 / ( 1.0 + g_jl * g_jl );

        double x1 = g_i.v.get_x() + (g_k.v.get_x() - g_i.v.get_x()) * d_ik;
        double y1 = g_i.v.get_y() + (g_k.v.get_y() - g_i.v.get_y()) * d_ik;
        double z1 = g_i.v.get_z() + (g_k.v.get_z() - g_i.v.get_z()) * d_ik;

        double x2 = g_j.v.get_x() + (g_l.v.get_x() - g_j.v.get_x()) * d_jl;
        double y2 = g_j.v.get_y() + (g_l.v.get_y() - g_j.v.get_y()) * d_jl;
        double z2 = g_j.v.get_z() + (g_l.v.get_z() - g_j.v.get_z()) * d_jl;

        double x = x1 - x2;
        double y = y1 - y2;
        double z = z1 - z2;


        return sqrt( x * x + y * y + z * z);
    }


    static double four_site_distance(const Gauss& g_i, const Gauss& g_j, const Gauss& g_k, const Gauss& g_l,
                                     const qmt::QmtVector& i_translation,const qmt::QmtVector& j_translation,
                                     const qmt::QmtVector& k_translation,const qmt::QmtVector& l_translation) {

        double g_ik = g_i._gamma / g_k._gamma;
        double g_jl = g_j._gamma / g_l._gamma;

        double d_ik = 1.0 / ( 1.0 + g_ik * g_ik );
        double d_jl = 1.0 / ( 1.0 + g_jl * g_jl );

        qmt::QmtVector v1 = g_i.v + i_translation + (g_k.v + k_translation - g_i.v - i_translation)*d_ik;
        qmt::QmtVector v2 = g_j.v + j_translation + (g_l.v + l_translation - g_j.v - j_translation)*d_jl;

        return qmt::QmtVector::norm(v1 - v2);
    }


    /******************************************************************************/

public:

    Gauss(double x, double y, double z, double gamma): _gamma(gamma) {
        v = qmt::QmtVector(x,y,z);
        prefactor = get_prefactor(gamma);
    }

    Gauss(const Gauss& gauss) {
        _gamma = gauss._gamma;
        v = gauss.v;
        prefactor = gauss.prefactor;
    }

    static double distance(const Gauss& g1, const Gauss& g2)  {
        return qmt::QmtVector::norm(g1.v - g2.v);
    }

    static double sq_distance(const Gauss& g1, const Gauss& g2)  {

        return qmt::QmtVector:: sq_norm(g1.v - g2.v);
    }

    void set_gamma(double gamma) {
        _gamma = gamma;
        prefactor = get_prefactor(gamma);
    }


    void set_r(double x, double y, double z) {
        v.set_x(x);
        v.set_y(y);
        v.set_z(z);
    }

    void set_r(const qmt::QmtVector& vector) {
        v = vector;
    }

    /********************************************************************************/

    double get_x() const {
        return v.get_x();
    }
    double get_y() const {
        return v.get_y();
    }
    double get_z() const {
        return v.get_z();
    }

    /********************************************************************************/
    /*                            <Psi_i(r)|Psi_j(r)> integral                      */
    /********************************************************************************/


    static double overlap(const Gauss& g1, const Gauss& g2) {

        double g = sqrt(g1._gamma * g2._gamma);
        double G = sqrt((g1._gamma * g1._gamma + g2._gamma*g2._gamma)*0.5);

        return g*g*g*exp( (-0.5*g*g*g*g/(G*G)) * sq_distance(g1,g2))/(G*G*G);
    }

    static double overlap(const Gauss& g1, const Gauss& g2,const qmt::QmtVector& v1, const qmt::QmtVector& v2) {

        double g = sqrt(g1._gamma * g2._gamma);
        double G = sqrt((g1._gamma * g1._gamma + g2._gamma*g2._gamma)*0.5);
//        std::cout<<"R = "<<v2 + g2.v<<" "<< g1.v + v1<<" "<<qmt::QmtVector::sq_norm(v2 + g2.v - g1.v - v1)<<std::endl;
        return   g*g*g*exp( (-0.5*g*g*g*g/(G*G)) *  qmt::QmtVector::sq_norm(v2 + g2.v - g1.v - v1))/(G*G*G);
    }

    /********************************************************************************/
    /*                        <Psi_i(r)|-nabla^2|Psi_j(r)> integral                 */
    /********************************************************************************/

    static double kinetic_integral(const Gauss& g1, const Gauss& g2) {

        double g = sqrt(g1._gamma * g2._gamma);
        double G = sqrt((g1._gamma * g1._gamma + g2._gamma*g2._gamma)*0.5);
        double R = distance(g1,g2);
        return  pow(g/G,7) * (3*G*G - R*R*pow(g,4))*exp( -0.5*pow((g*g/G),2)*R*R);

    }


    static double kinetic_integral(const Gauss& g1, const Gauss& g2,
                                   const qmt::QmtVector& v1, const qmt::QmtVector& v2) {

        double g = sqrt(g1._gamma * g2._gamma);
        double G = sqrt((g1._gamma * g1._gamma + g2._gamma*g2._gamma)*0.5);
        double R = qmt::QmtVector::norm(g2.v + v2 -  g1.v - v1);
        return pow(g/G,7) * (3*G*G - R*R*pow(g,4))*exp( -0.5*pow((g*g/G),2)*R*R);

    }

    /********************************************************************************/
    /*                       <Psi_i(r)|1/{|r-R_k|}|Psi(r)> integral                 */
    /********************************************************************************/

    static double attractive_integral(const Gauss& g_i, const Gauss& g_j, double rx, double ry, double rz) {
//three_site_distance
        double r = three_site_distance(g_i, g_j, rx, ry, rz);
        double G = sqrt((g_i._gamma * g_i._gamma + g_j._gamma*g_j._gamma)*0.5);
        if(r > 0)
            // r = gsl_sf_erf( sqrt(2) * G  *r) / r;
            r = std::erf( sqrt(2) * G  *r) / r;
        else
            r =  2*sqrt(2) * G / sqrt(3.14159265);

        return -2*overlap(g_i, g_j) * r;
    }


    static double attractive_integral(const Gauss& g_i, const Gauss& g_j,
                                      const qmt::QmtVector& v1, const qmt::QmtVector& v2,
                                      double rx, double ry, double rz) {

        double r = three_site_distance(g_i, g_j, v1, v2, rx, ry, rz);
        double G = sqrt((g_i._gamma * g_i._gamma + g_j._gamma*g_j._gamma)*0.5);
        if(r > 0)
            //r = gsl_sf_erf(sqrt(2) * G  * r) / r;
            r = std::erf(sqrt(2) * G  * r) / r;
        else
            r =  2*sqrt(2) * G / sqrt(3.14159265);

        return -2*overlap(g_i, g_j, v1, v2) * r;
    }

    /********************************************************************************/
    /*                            Four operator integrals                           */
    /********************************************************************************/

    static double v_integral(const Gauss& g_i, const Gauss& g_j, const Gauss& g_k, const Gauss& g_l) {

        double r = four_site_distance(g_i, g_j, g_k, g_l);

        double gi2 = g_i._gamma * g_i._gamma;
        double gj2 = g_j._gamma * g_j._gamma;
        double gk2 = g_k._gamma * g_k._gamma;
        double gl2 = g_l._gamma * g_l._gamma;

        double gac = sqrt((gi2 + gk2)/2);
        double gbd = sqrt((gj2 + gl2)/2);

        double a = gac * gbd  / sqrt( (gi2 + gj2 + gk2 + gl2) * 0.25);
        if(r > 0)
            //r = gsl_sf_erf(   a * r) / r;
            r = std::erf(   a * r) / r;
        else
            r =   2 * a / sqrt(3.14159265);

        return 2 * overlap(g_i, g_k) * overlap(g_j, g_l) * r;
    }


    static double v_integral(const Gauss& g_i, const Gauss& g_j, const Gauss& g_k, const Gauss& g_l,
                             const qmt::QmtVector& i_translation,const qmt::QmtVector& j_translation,
                             const qmt::QmtVector& k_translation,const qmt::QmtVector& l_translation) {

        double r = four_site_distance(g_i, g_j, g_k, g_l, i_translation, j_translation, k_translation, l_translation);

        double gi2 = g_i._gamma * g_i._gamma;
        double gj2 = g_j._gamma * g_j._gamma;
        double gk2 = g_k._gamma * g_k._gamma;
        double gl2 = g_l._gamma * g_l._gamma;

        double gac = sqrt((gi2 + gk2)/2);
        double gbd = sqrt((gj2 + gl2)/2);

        double a = gac * gbd  / sqrt( (gi2 + gj2 + gk2 + gl2) * 0.25);
        if(r > 0)
            //r = gsl_sf_erf(   a * r) / r;
            r = std::erf(   a * r) / r;
        else
            r =   2 * a / sqrt(3.14159265);

        return 2 * overlap(g_i, g_k, i_translation, k_translation) * overlap(g_j, g_l, j_translation, l_translation) * r;
    }

    static double get_value(const Gauss& g,const qmt::QmtVector& position,const qmt::QmtVector& translation = qmt::QmtVector(0,0,0)){
        auto d = qmt::QmtVector::sq_norm(position + translation - g.v);
       return g.prefactor * exp( - g._gamma*g._gamma * d);
    }
};


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

template <typename Orbital, int Parallel = QmVariables::QmParallel::no>
class QmExpansion {

    /* I decided to agregate pointers - why? - it contradicts RAII, however, it is much easier
    to handle coherently orbitals and QmExpoansions when kind of refreshment of their states is demanded*/


    /*NOTE: Only symmetric case is under consideration */

    typedef std::tuple<int,double,std::shared_ptr<Orbital>,int> c_orbital;

    std::vector<c_orbital> orbitals;
    int cntr;

    double r_x;
    double r_y;
    double r_z;
    int _id;
    double _c;

    static bool compare(const c_orbital& orb1, const c_orbital& orb2) {
        return std::get<0>(orb1) < std::get<0>(orb2);
    }

public:

    /********************************************************************************/

    QmExpansion(double x, double y, double z, int id = 0, double c = 0):cntr(0),r_x(x), r_y(y), r_z(z), _id(id),_c(c) {}
    QmExpansion(qmt::QmtVector r, int id = 0, double c = 0):cntr(0),r_x(r.get_x()), r_y(r.get_y()), r_z(r.get_z()),_id(id),_c(c) {}

    QmExpansion(int id = 0, double c = 0):cntr(0),r_x(0), r_y(0), r_z(0), _id(id),_c(c) {}

    QmExpansion(const QmExpansion<Orbital,Parallel>& expansion) {
        r_x = expansion.r_x;
        r_y = expansion.r_y;
        r_z = expansion.r_z;
        _id = expansion._id;
        _c = expansion._c;
        cntr = expansion.cntr;
        for(const auto &orbital : expansion.orbitals)
            orbitals.push_back(c_orbital(std::get<0>(orbital),std::get<1>(orbital), std::shared_ptr<Orbital>(new Orbital(*std::get<2>(orbital))), std::get<3>(orbital)));
    }

    /********************************************************************************/

    void clear_elements() {
        orbitals.clear();
        cntr = 0;
    }



    void add_element(int id, double c, Orbital& orbital) {

        orbitals.push_back( c_orbital(id,c, std::shared_ptr<Orbital>(new Orbital(orbital)),0));
        std::sort(orbitals.begin(), orbitals.end(), compare);
        cntr++;
    }

    void add_element_with_cindex(int cid, double c, Orbital& orbital) {
        int id = cntr;
        orbitals.push_back( c_orbital(id,c, std::shared_ptr<Orbital>(new Orbital(orbital)),cid));
        cntr++;
    }

    void add_element(int id, double c, Orbital* orbital) {

        orbitals.push_back( c_orbital(id,c, std::shared_ptr<Orbital>(orbital),0));
        std::sort(orbitals.begin(), orbitals.end(), compare);
        cntr++;
    }

    void add_element_with_cindex(int cid, double c, Orbital* orbital) {
        int id = cntr;
        orbitals.push_back( c_orbital(id,c, std::shared_ptr<Orbital>(orbital),cid));
        cntr++;
    }


    /********************************************************************************/

    const Orbital& get_element(int id) const {
        if(id > cntr || id < 0) throw std::length_error("QmExpansion::get_element()::invalid length");
        return *(std::get<2>(orbitals.at(id)));

    }

    int get_element_cid(int id) const {
        if(id > cntr || id < 0) throw std::length_error("QmExpansion::get_element()::invalid length");
        return (std::get<3>(orbitals.at(id)));

    }
    /*
        const Orbital& get_element_by_id(int id) const {

          for(int i = 0; i < cntr; i++){
             if(std::get<0>(orbitals.at(i)) == id)
              return *(std::get<2>(orbitals.at(i)));
            }
          throw std::logic_error("QmExpansion::get_element_by_id()::no element found")
        }
    */

    const double get_c(int id) const {
        if(id > cntr || id < 0) throw std::length_error("QmExpansion::get_element()::invalid length");
        return (std::get<1>(orbitals.at(id)));

    }

    /********************************************************************************/

    int get_size() const {
        return cntr;
    }

    /********************************************************************************/

    void set_r(double x, double y, double z) {
        r_x = x;
        r_y = y;
        r_z = z;
    }

    /********************************************************************************/

    void set_c(double c) {
        _c = c;
    }

    void set_c_by_id(int cid, double c) {
        for(int i = 0; i < cntr; i++) {
            if(std::get<3>(orbitals.at(i)) == cid) {
                (std::get<1>(orbitals.at(i)) = c);
            }
        }
    }

    /********************************************************************************/

    double get_x() const {
        return r_x;
    }
    double get_y() const {
        return r_y;
    }
    double get_z() const {
        return r_z;
    }

    int get_id() const {
        return _id;
    }

     qmt::QmtVector get_position() const {
	return  qmt::QmtVector(r_x,r_y,r_z);
    }

    /********************************************************************************/

    static double overlap(const QmExpansion<Orbital,Parallel>& qe1, const QmExpansion<Orbital,Parallel>& qe2) {
        int size1 = qe1.get_size();
        int size2 = qe2.get_size();

        double result = 0.0;

        for(int i = 0; i < size1; ++i) {
            for(int j = 0; j < size2; ++j) {
                result += qe1.get_c(i) * qe2.get_c(j) * Orbital::overlap(qe1.get_element(i), qe2.get_element(j));
            }
        }

        return result;
    }

    /////////////////////
    //Translated version//
    /////////////////////


    static double overlap(const QmExpansion<Orbital,Parallel>& qe1, const QmExpansion<Orbital,Parallel>& qe2,
                          const qmt::QmtVector& translation1, const qmt::QmtVector& translation2) {
        int size1 = qe1.get_size();
        int size2 = qe2.get_size();


        double result = 0.0;

        for(int i = 0; i < size1; ++i) {
            for(int j = 0; j < size2; ++j) {
                result += qe1.get_c(i) * qe2.get_c(j) * Orbital::overlap(qe1.get_element(i), qe2.get_element(j),
                          translation1, translation2);
            }
        }

        return result;
    }

    /********************************************************************************/

    static double kinetic_integral(const QmExpansion<Orbital,Parallel>& qe1, const QmExpansion<Orbital,Parallel>& qe2) {
        int size1 = qe1.get_size();
        int size2 = qe2.get_size();

        double result = 0.0;

        for(int i = 0; i < size1; ++i) {
            for(int j = 0; j < size2; ++j) {
                result += qe1.get_c(i) * qe2.get_c(j) * Orbital::kinetic_integral(qe1.get_element(i), qe2.get_element(j));
            }
        }

        return result;
    }

    /////////////////////
    //Translated version//
    /////////////////////

    static double kinetic_integral(const QmExpansion<Orbital,Parallel>& qe1, const QmExpansion<Orbital,Parallel>& qe2,
                                   const qmt::QmtVector& translation1, const qmt::QmtVector& translation2) {
        int size1 = qe1.get_size();
        int size2 = qe2.get_size();


        double result = 0.0;

        for(int i = 0; i < size1; ++i) {
            for(int j = 0; j < size2; ++j) {
                result += qe1.get_c(i) * qe2.get_c(j) * Orbital::kinetic_integral(qe1.get_element(i), qe2.get_element(j),
                          translation1, translation2);
            }
        }

        return result;
    }

    /********************************************************************************/

    static double attractive_integral(const QmExpansion<Orbital, Parallel>& qe1,
                                      const QmExpansion<Orbital, Parallel>& qe2, double rx, double ry, double rz) {

        int size1 = qe1.get_size();
        int size2 = qe2.get_size();


        double result = 0.0;

        for(int i = 0; i < size1; ++i) {
            for(int j = 0; j < size2; ++j) {
                result += qe1.get_c(i) * qe2.get_c(j) * Orbital::attractive_integral(qe1.get_element(i), qe2.get_element(j), rx, ry, rz);
            }
        }

        return result;
    }

    /////////////////////
    //Translated version//
    /////////////////////


    static double attractive_integral(const QmExpansion<Orbital, Parallel>& qe1,
                                      const QmExpansion<Orbital, Parallel>& qe2, const qmt::QmtVector& translation1, const qmt::QmtVector& translation2,
                                      double rx, double ry, double rz) {

        int size1 = qe1.get_size();
        int size2 = qe2.get_size();


        double result = 0.0;

        for(int i = 0; i < size1; ++i) {
            for(int j = 0; j < size2; ++j) {
                result += qe1.get_c(i) * qe2.get_c(j) * Orbital::attractive_integral(qe1.get_element(i), qe2.get_element(j),
                          translation1, translation2, rx, ry, rz);
            }
        }

        return result;
    }


    /********************************************************************************/

    static double v_integral(const QmExpansion<Orbital,  Parallel>& qe1, const QmExpansion<Orbital,  Parallel>& qe2,
                             const QmExpansion<Orbital,  Parallel>& qe3, const QmExpansion<Orbital,  Parallel>& qe4) {

        int size1 = qe1.get_size();
        int size2 = qe2.get_size();
        int size3 = qe3.get_size();
        int size4 = qe4.get_size();


        double result = 0.0;
        int p = Parallel;
        int i,j,k,l;
        #pragma omp parallel for if(p==1) private(i,j,k,l)\
shared(qe1,qe2,qe3,qe4) reduction(+:result)\
        collapse(4)

        for( i = 0; i < size1; ++i ) {
            for( j = 0; j < size3; ++j ) {
                for( k = 0; k < size2; ++k) {
                    for( l = 0; l < size4; ++l) {
                        result += qe1.get_c(i) * qe2.get_c(k) * qe3.get_c(j) * qe4.get_c(l) *
                                  Orbital::v_integral(qe1.get_element(i), qe3.get_element(j), qe2.get_element(k), qe4.get_element(l));
//                    printf("th#%d\n",omp_get_thread_num());
                    }
                }
            }
        }
        return result;
    }
    /////////////////////
    //Translated version//
    /////////////////////


    static double v_integral(const QmExpansion<Orbital,  Parallel>& qe1, const QmExpansion<Orbital,  Parallel>& qe2,
                             const QmExpansion<Orbital,  Parallel>& qe3, const QmExpansion<Orbital,  Parallel>& qe4,
                             const qmt::QmtVector& translation1, const qmt::QmtVector& translation2,
                             const qmt::QmtVector& translation3, const qmt::QmtVector& translation4) {

        int size1 = qe1.get_size();
        int size2 = qe2.get_size();
        int size3 = qe3.get_size();
        int size4 = qe4.get_size();


        double result = 0.0;
        int p = Parallel;
        int i,j,k,l;
        #pragma omp parallel for if(p==1) private(i,j,k,l)\
shared(qe1,qe2,qe3,qe4) reduction(+:result)\
        collapse(4)

        for( i = 0; i < size1; ++i ) {
            for( j = 0; j < size3; ++j ) {
                for( k = 0; k < size2; ++k) {
                    for( l = 0; l < size4; ++l) {
                        result += qe1.get_c(i) * qe2.get_c(k) * qe3.get_c(j) * qe4.get_c(l) *
                                  Orbital::v_integral(qe1.get_element(i), qe3.get_element(j), qe2.get_element(k), qe4.get_element(l),
                                                      translation1, translation2, translation3, translation4 );
                        //                    printf("th#%d\n",omp_get_thread_num());
                    }
                }
            }
        }
        return result;
    }

   static double get_value(const QmExpansion<Orbital, Parallel>& q,const qmt::QmtVector& position,const qmt::QmtVector& translation = qmt::QmtVector(0,0,0)) {
     double result = 0.0;
     for( int i = 0; i < q.get_size(); ++i)  
       result += q.get_c(i) * Orbital::get_value(q.get_element(i),position,translation);
     return result;
  }

    static void multiply(const QmExpansion<Orbital,  Parallel>&  w1, const QmExpansion<Orbital,  Parallel>&  w2, double** result, int size) {


        for(int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                result[i][j] = 0.0;
            }
        }

        for(int i = 0; i < w1.get_size(); ++i) {
            for (int j = 0; j < w2.get_size(); ++j) {
                int b1 = w1.get_element_cid(i);
                int b2 = w2.get_element_cid(j);
                double overlap = Orbital::overlap(w1.get_element(i),w2.get_element(j));
                result[b1][b2]+=overlap;
            }
        }

    }



};


/////////////////////////////////////////////////////////////////////////////////////////////////////
//It makes sense for finding expansion coefficients only! 24.04.2015 - Obsolete - use QmExpansion!!//
/////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename Orbital>
class Wannier {

    typedef typename std::pair<Orbital,int> orbital_pair;
    std::vector <orbital_pair> orbitals;

    qmt::QmtVector position;

public:
    Wannier():position(0,0,0) {}

    Wannier(double x, double y, double z):position(x,y,z) {}

    Wannier(const qmt::QmtVector& v):position(v) {}


    int size() const {
        return orbitals.size();
    }

    void addOrbital(const Orbital& orbital, int beta_index) {
        orbitals.push_back( orbital_pair(orbital, beta_index));
    }

    static void multiply(const Wannier& w1, const Wannier& w2, double** result, int size) {



        for(int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                result[i][j] = 0.0;
            }
        }

        for(int i = 0; i < w1.size(); ++i) {
            for (int j = 0; j < w2.size(); ++j) {
                int b1 = w1.orbitals[i].second;
                int b2 = w2.orbitals[j].second;
                double overlap = Orbital::overlap(w1.orbitals[i].first,w2.orbitals[j].first);

                result[b1][b2]+=overlap;

            }
        }

    }

//#ifdef GSL_SUPPORT
    static void multiply(const Wannier& w1, const Wannier& w2, gsl_matrix *result) {


        for(int i = 0; i < result->size1; ++i) {
            for (int j = 0; j < result->size2; ++j) {
                gsl_matrix_set(result,i,j,0);
            }
        }

        for(int i = 0; i < w1.size(); ++i) {
            for (int j = 0; j < w2.size(); ++j) {
                if(i >= w1.orbitals.size() || j >=w2.orbitals.size()) return;
                int b1 = w1.orbitals[i].second;
                int b2 = w2.orbitals[j].second;

                double overlap = Orbital::overlap(w1.orbitals[i].first,w2.orbitals[j].first);

                gsl_matrix_set (result, b1, b2, gsl_matrix_get(result,b1, b2) + overlap);

            }
        }
    }


//#endif

    static double  overlap(const Wannier& w1, const Wannier& w2) {

        double result = 0;
        for(int i = 0; i < w1.size(); ++i) {
            for (int j = 0; j < w2.size(); ++j) {
                int b1 = w1.orbitals[i].second;
                int b2 = w2.orbitals[j].second;
                result += Orbital::overlap(w1.orbitals[i].first,w2.orbitals[j].first);


            }
        }

        return result;
    }


    double get_x() const {
        return position.get_x();
    }
    double get_y() const {
        return position.get_y();
    }
    double get_z() const {
        return position.get_z();
    }

};


#endif // QMTORBITALS_H_INCLUDED
