#ifndef QMT_WANNIER_CREATOR_H_INCLUDED
#define QMT_WANNIER_CREATOR_H_INCLUDED

#include "qmtslatercreator.h"
#include <tuple>

namespace qmt {

class QmtWannierSlaterBasedCreator {

    std::vector<std::tuple<int,qmt::QmtSlaterCreator*>> slater_creators;
    int id;
    qmt::QmtVector position;

    QmtWannierSlaterBasedCreator(const QmtWannierSlaterBasedCreator&);
    QmtWannierSlaterBasedCreator& operator=(const QmtWannierSlaterBasedCreator&);
public:

    QmtWannierSlaterBasedCreator(const std::string& input);
    ~QmtWannierSlaterBasedCreator();

    typedef QmExpansion<qmt::SlaterOrbital,QmVariables::QmParallel::omp> Wannier;

    Wannier* Create(double alpha, const qmt::QmtVector& scale=qmt::QmtVector(1,1,1)) const;
    Wannier* Create(const std::vector<double> &alphas,  const qmt::QmtVector& scale=qmt::QmtVector(1,1,1)) const;

    int get_max_beta_index() const;

    friend std::ostream& operator<<(std::ostream &stream, const qmt::QmtWannierSlaterBasedCreator& creator);
};


}



#endif //QMT_WANNIER_CREATOR_H_INCLUDED
