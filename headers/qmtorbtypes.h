#ifndef QMTORBTYPES_H_INCLUDED
#define QMTORBTYPES_H_INCLUDED
namespace qmt {
enum class OrbitalType {
    _1s,
    _2s,_2p_x,_2p_y,_2p_z,_2p_xp,_2p_xm,_2p_yp,_2p_ym,_2p_zp,_2p_zm,
    _3s,_3p_x,_3p_y,_3p_z,_3d_z2,_3d_xz,_3d_yz,_3d_xy,_3d_x2y2,
//	4s,4p_x,4p_y,4p_z,4d_z2,4d_xz,4d_yz,4d_xy,4d_x2y2,4f_z3,4f_xz2,4f_yz2,4_xyz,4f_z_x2y2,4f_x_x2_3y2,4f_y_3x2_y2,
//       	5s,5p_x,5p_y,5p_z,5d_z2,5d_xz,5d_yz,5d_xy,5d_x2y2,
//	6s,6px,6py,6pz,
//	7s
};
}
#endif //QMTORBTYPES_H_INCLUDED
