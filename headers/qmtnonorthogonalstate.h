#ifndef QMTNONORTHOGONALSTATE_H_INCLUDED
#define QMTNONORTHOGONALSTATE_H_INCLUDED

#include<vector>
#include<numeric>
#include<utility>
#include<algorithm>
#include<stdexcept>
#include<iostream>
#include<bitset>
#include "qmtbitsettools.h"
#include "qmtstate.h"
#include<tuple>
#include<map>


namespace qmt {

///////////////////////////////////////////////////////////////////////////////////////
//				BITSET


template <std::size_t _size>
class QmtNonOrthogonalNState : public QmtNState<std::bitset<_size>,int> {


public:
    QmtNonOrthogonalNState() : QmtNState<std::bitset<_size>,int>(){
    }

    QmtNonOrthogonalNState(ulong n) : QmtNState<std::bitset<_size>,int>(n) {
    }

    QmtNonOrthogonalNState(ulong n, std::bitset<_size> s, double p = 1 ) : QmtNState<std::bitset<_size>,int>( n, s, p ) {

    }


/*
    std::bitset<_size> state;
    ulong size;   // band
    double phase; //-1, +1, 0 "zero state"

    ulong mask_decomposer(std::bitset<_size> number, ulong length) {
       
	std::bitset<_size> mask=std::bitset<_size>(0);
        for(ulong i = 0; i < length; ++i) {
		mask.set(i);
        }

        return (mask & number).count();
    }

public:

    QmtNState<std::bitset<_size>,int>() : state(0), size(0), phase(0) {
    }

    QmtNState<std::bitset<_size>,int>(ulong n) : state(0), size(n), phase(1) {
    }

    QmtNState<std::bitset<_size>,int>(ulong n, std::bitset<_size> s, double p = 1 ) : state(s), size(n), phase(p) {

    }

    ulong get_size() const {
        return size;
    }

    double getPhase() const {
        return phase;
    }
    void setPhase(double p) {
        phase = p;
    }

    std::bitset<_size> integerRepresentation() const {
        return state;
    }

    void anihilate_up(ulong band) {
        if(!state[band]) {
            phase = 0;
            state = 0;
            return;
        }

        else {
           phase = phase * (mask_decomposer(state, band)%2 == 0 ? 1 : -1);
            state[band]=0;
        }
    }



    void anihilate_down(ulong band) {
        ulong offset = band + size; //TO DO: offset to constructor?

       if(!state[offset])  {
            phase = 0;
            state = 0;
            return;
        }

        else {
            phase = phase * (mask_decomposer(state, offset)%2 == 0 ? 1 : -1);
            state[offset]=0;
        }
    }



    void create_up(ulong band) {
         if(state[band]) {
            phase = 0;
            state = 0;
            return;
        }
        else {
            phase = phase * (mask_decomposer(state, band)%2 == 0 ? 1 : -1);
            state.set(band);
        }
    }


    void create_down(ulong band) {
        ulong offset = band + size;

        if(state[offset]) {
            phase = 0;
            state = 0;
            return;
        }
        else {
            phase = phase * (mask_decomposer(state, offset)%2 == 0 ? 1 : -1);
            state.set(offset);
        }
    }


    bool is_occupied(size_t index) const {    
	return state[index];
    }
    
 
    void set_bit(size_t index, bool value) {
       state[index] = value;
    }
    
    bool get_bit(size_t index) const{
       return state[index];
    }

    friend std::ostream& operator<<(std::ostream& stream,const QmtNState<std::bitset<_size>,int>& state) {

        std::bitset<_size> n = state.state;
        std::bitset<_size> one = std::bitset<_size>(1);
        if(state.phase < 0 )
            stream<<"-";
        if(state.phase == 0) {
            stream<<"zero state";
            return stream;
        }
        stream<<"|";
        for(uint i = 0; i < state.size; ++i) {
            if(((one<<i) & n)!= std::bitset<_size>(0)) stream<<"1";
            else stream<<"0";
        }
        std::cout<<"> x ";

        stream<<"|";
        for(uint i =state.size ; i < 2 * state.size ; ++i) {
            if(((one<<i) & n)!= std::bitset<_size>(0)) stream<<"1";
            else stream<<"0";
        }
        stream<<">";
        return stream;
    }


    static double dot_product(const QmtNState<std::bitset<_size>,int>& state1, const QmtNState<std::bitset<_size>,int>& state2) {

        if( state1.size != state2.size) throw std::length_error("QmtNState<std::bitset<_size>,int>::dot_product");

        double p = state1.phase * state2.phase;
        int result = state1.state == state2.state ? 1 : 0;

        return p * result;
    }

    static double dot_product(const std::vector<QmtNState<std::bitset<_size>,int>>& states1, const std::vector<QmtNState<std::bitset<_size>,int>>& states2) {
	double result=0.0;

	unsigned int i=0;
	unsigned int j=0;

	while(i<states1.size() && j<states2.size()){
	if(compare_configuration(states1[i],states2[j])){
		result += states1[i].phase * states2[j].phase;
		i++;
		j++;
		}
	else if(gt_configuration(states1[i],states2[j]))
		j++;
	else
		i++;
	}

        
        return result;
    }

    static bool compare_configuration(const QmtNState<std::bitset<_size>,int>& first, const QmtNState<std::bitset<_size>,int>& second){
		return qmt::bitsettools::eq(first.state,second.state);
    }

    static bool gt_configuration(const QmtNState<std::bitset<_size>,int>& first, const QmtNState<std::bitset<_size>,int>& second){
		return qmt::bitsettools::gt(first.state,second.state);
    }

    static bool lt_configuration(const QmtNState<std::bitset<_size>,int>& first, const QmtNState<std::bitset<_size>,int>& second){
		return qmt::bitsettools::lt(first.state,second.state);
    }

*/
};





}

#endif // QMTNONORTHOGONALSTATE_H_INCLUDED
