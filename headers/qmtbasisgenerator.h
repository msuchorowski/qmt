/*
 * qmtbasisgenerator.h
 *
 *  Created on: 30 cze 2014
 *      Author: andrzej kądzielawa
 */

#ifndef QMTBASISGENERATOR_H_
#define QMTBASISGENERATOR_H_

#include <iostream>
#include <ostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <limits>
#include "qmtstate.h"

namespace qmt {


class QmtBasisGenerator {

public:
template <typename T = uint>
    void generate(uint number_of_fermions, std::vector<QmtNState<T, int> > &output) {
        if (output.size()>0) output.clear();

        for(uint i=0; i<(1<<8); i++) {
            if(generator(i) == number_of_fermions) {
                QmtNState<T,int> state(number_of_fermions, i);
                output.push_back(state);
            }
        }
    }
template <typename T = uint>
    void generate(uint number_of_fermions, uint number_of_bands, std::vector<QmtNState<T, int> > &output, bool sort_spins=false) {
        if (output.size()>0) output.clear();
        
        T one = 1;
        uint max = number_of_bands/2 >=  sizeof(T)*8/2 ? sizeof(T) * 8 - 1 : number_of_bands;
	//std::cout << "TEST " << number_of_bands/2 << " " << sizeof(T)*8/2 << std::endl;
        T limit = static_cast<T>(one<<max);
        if( number_of_bands == sizeof(T) * 8) limit = limit - 1 +limit; 
        for(T i = 0; i < limit; i++) {
            if(generator<T>(i) == number_of_fermions) {

                QmtNState<T,int> state(number_of_bands/2, i);
                output.push_back(state);
            }
        }
	if(sort_spins)
	        std::sort(output.begin(), output.end(),compare<T>);
    }

template <std::size_t N>
    void generate(uint number_of_fermions, uint number_of_bands, std::vector<QmtNState<std::bitset<N>, int> > &output, bool sort_spins=false) {
        if (output.size()>0) output.clear();
        //std::cout << "TEST2" << std::endl;
        std::bitset<N> limit = 0;

	for(unsigned int i=0; i<N; i++) //setting limit with ones
		limit.set(i);
	if (limit.count() < number_of_fermions){
		std::cerr<<"QmtBasisGenerator::generate<std::bitset<N>>: number_of_fermions exceed number of sites!"<<std::endl;
		return;
	}
	else if (limit.count() == number_of_fermions){
		QmtNState<std::bitset<N>,int> state(N/2, limit);
		output.push_back(state);
		return;
	}

        for(std::bitset<N> i = 0; !(i.all()); increment<N>(i)) {
            if(i.count() == number_of_fermions) {
                QmtNState<std::bitset<N>,int> state(N/2, i);
                output.push_back(state);
            }
        }
//	if(sort_spins)
//	        std::sort(output.begin(), output.end(),compare<T>);
    }

template <std::size_t N>
    void generate_single_particle(std::vector<QmtNState<std::bitset<N>, int> > &output) {
        if (output.size()>0) output.clear();

	output.reserve(N);
        
        for(unsigned int i=0; i<N; i++) {
		std::bitset<N> mask=0;
		mask.set(i);
                QmtNState<std::bitset<N>,int> state(N/2, mask);
                output.push_back(state); 
        }


    }

template <std::size_t N, std::size_t M> // N - size of the state; M - number of free fermions
    void generate(uint number_of_fermions, uint number_of_bands, std::vector<QmtNState<std::bitset<N>, int> > &output, std::bitset<N> mask, bool sort_spins=false) {
	//std::cout << "TEST3" << std::endl;
        if (output.size()>0) output.clear();
        
	if (mask.count() > number_of_fermions)	throw std::logic_error("QmtBasisGenerator::generate<std::bitset<N>>: number_of_fermions is smaller than the number of fermions in the mask!");

        std::bitset<N> limit = 0;
	for(unsigned int i=0; i<N; i++) //setting limit with ones
		limit.set(i);

	if (limit.count() < number_of_fermions)	throw std::logic_error("QmtBasisGenerator::generate<std::bitset<N>>: number_of_fermions exceed number of sites!");

	else if (limit.count() == number_of_fermions){
		QmtNState<std::bitset<N>,int> state(N/2, limit);
		output.push_back(state);
		return;
	}

	double* dictonary;
	dictonary = new double[M];
	
	unsigned int cntr=0;
	for(unsigned int i=0; i<N; i++){
		if (!mask[i]){			
			if(cntr==M)	throw std::logic_error("QmtBasisGenerator::generate<std::bitset<N>>: Number of available spaces is greater than set by M!");

			dictonary[cntr]=i;
			++cntr;
		}
	}
		if(cntr!=M)	throw std::logic_error("QmtBasisGenerator::generate<std::bitset<N>>: Number of available spaces is smaller than set by M!");

        for(std::bitset<M> i = 0; !(i.all()); increment<M>(i)) {
            if(i.count() == (number_of_fermions-N+M)) {
		std::bitset<N> new_entry=mask;
		for(unsigned int j=0; j<M; j++)
			if (i[j]) new_entry.set(dictonary[j]);
                QmtNState<std::bitset<N>,int> state(N/2, new_entry);
                output.push_back(state);
            }
        }
//	if(sort_spins)
//	        std::sort(output.begin(), output.end(),compare<T>);
    }


template <typename T = uint>
    void generate(uint number_of_fermions, uint number_of_bands, int total_spin, std::vector<QmtNState<T, int> > &output, bool sort_spins=false) {
	        
	if (output.size()>0) output.clear();
        T one = 1;
        uint max = number_of_bands/2 >=  sizeof(T)*8/2 ? sizeof(T) * 8 - 1 : number_of_bands;
        T limit = static_cast<T>(one<<max);
        if( number_of_bands == sizeof(T) * 8) limit = limit - 1 +limit; 
        for(T i = 0; i < limit; i++) {
	   // std::cout << generator<T>(i) << " " << number_of_fermions << " " << spin_checker(i,total_spin,max/2) << std::endl; 
            if(generator<T>(i) == number_of_fermions && spin_checker(i,total_spin,max/2)) {
                QmtNState<T,int> state(number_of_bands/2, i);
                output.push_back(state);
            }
        }
	if(sort_spins)
	        std::sort(output.begin(), output.end(),compare<T>);

    }

private:
template <typename T = uint>
    T generator(T number) {
        T counter=0;
        T one = 1;

        for(T i=0; i< sizeof(T) * 8; i++) {
            
            if((T)(one << i) & (number)) counter++;

        }
        return counter;
    }

	template<std::size_t N>
	bool greater_than(const std::bitset<N>& x, const std::bitset<N>& y)
	{
    	for (int i = N-1; i >= 0; i--) {
       		if (x[i] ^ y[i]) return x[i];
    		}
    	return false;
	}

	template <size_t N>
	void increment (std::bitset<N>& in ) { // from http://stackoverflow.com/questions/16761472/how-can-i-increment-stdbitset
	//  add 1 to each value, and if it was 1 already, carry the 1 to the next.
    	for ( size_t i = 0; i < N; ++i ) {
        	if ( in[i] == 0 ) {  // There will be no carry
        	    in[i] = 1;
        	    break;
        	    }
       	 in[i] = 0;  // This entry was 1; set to zero and carry the 1
       	 }
    	}


template <typename T = uint>
    static bool compare(const QmtNState<T, int>& s1, const QmtNState<T, int>& s2) {
        return total_spin_z(s1.integerRepresentation(),s1.get_size()) < total_spin_z( s2.integerRepresentation(), s2.get_size());
    }
template <typename T = uint>
    static long int total_spin_z(T number, uint nbands) {
        int counter1=0;
        int counter2=0;
        T one = 1;
        for(uint i=0; i<nbands; i++) {
            if((one << i) & (number)) counter1++;

        }
        for(uint i=nbands; i<nbands * 2; i++) {
            if((one << i) & (number)) counter2++;

        }
        return counter1 - counter2;
    }

template <typename T = uint>
    bool spin_checker(T number, long int total_spin, uint nbands) {
        int counter1=0;
        int counter2=0;
        T one = 1U;
        for(uint i=0; i<nbands; i++) {
            if((one << i) & (number)) counter1++;
        }
        for(uint i=nbands; i<nbands * 2; i++) {
            if((one << i) & (number)) counter2++;
        }
        return (counter1 - counter2) == total_spin ? true : false;
    }

}; // end of class  QmtBasisGenerator





} // end of namespace qmt
#endif /* QMTBASISGENERATOR_H_ */
