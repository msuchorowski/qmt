#ifndef QMT_SIMULATED_ANNEALING_H
#define QMT_SIMULATED_ANNEALING_H

#include <iostream>
#include <vector>
#include<functional>
#include<random>
#include <algorithm>
#include <cmath>

#ifdef _MC_OPT_MPI
#include <mpi.h>
#endif

namespace qmt {

class QmtSimulatedAnnealing {
  
size_t N;					//number of solvers  
size_t dim;					//number of dimensions of minimization
std::function<double(std::vector<double>)> F;	//function to optimize
double beta;					//susceptibility to change
double beta0;
double beta_step;				//susceptibility step
double range;					//magnitude of single change
std::vector<double> lower_limits;
std::vector<double> upper_limits;		// lower and upper limits of minimization procedure

std::vector<std::vector<double>> variables;	// variables to optimize
std::vector<double> values;			// values

std::random_device rand_dev;
std::mt19937 generator;
std::uniform_real_distribution<> distrubution;
  
  
public:
  QmtSimulatedAnnealing(const size_t _N, const size_t _dim, std::function<double(std::vector<double>)> _F, std::vector<double> _lower_limits, std::vector<double> _upper_limits, double _beta=1.0, double _range=0.01):
  N(_N),dim(_dim),F(_F),beta(1.0),beta0(1.0),beta_step(_beta),range(_range),lower_limits(_lower_limits),upper_limits(_upper_limits){
    
    
    
    generator=std::mt19937(rand_dev());
    distrubution=std::uniform_real_distribution<>(0,1);
    
  //reset();
     
    
  }
    QmtSimulatedAnnealing(const size_t _N, const size_t _dim, std::function<double(std::vector<double>)> _F,std::vector<std::vector<double>> _variables, std::vector<double> _lower_limits, std::vector<double> _upper_limits, double _beta=1.0, double _range=0.01):
  N(_N),dim(_dim),F(_F),beta(1.0),beta0(1.0),beta_step(_beta),range(_range),lower_limits(_lower_limits),upper_limits(_upper_limits),variables(_variables){
        
    
    generator=std::mt19937(rand_dev());
    distrubution=std::uniform_real_distribution<>(0,1);
   
    for(const auto& variable : variables)
      values.push_back(F(variable));
     
  } 
  
private:
  void reset(){
    beta=beta0;
    variables.clear();
    values.clear();
        
    for(size_t i=0; i<N; i++){
      std::vector<double> tmp;
     for(size_t j=0; j<dim; j++){
       double min=lower_limits[j];
       double max=upper_limits[j];
       tmp.push_back((max-min)*distrubution(generator) + min);
     }
     variables.push_back(tmp);
     
  
    }
        for(const auto& variable : variables)
       values.push_back(F(variable));
    
  }
  
  void metropolis(const size_t max_steps, const double eps){
    for (size_t i=0; i<max_steps; i++){
      //#pragma omp parallel for TO SOLVE: segmentation fault!
#ifdef   _MC_OPT_MPI
      N = 1; 
#endif      
      
      for(size_t j=0; j<N; j++){
	
	// prepare move naive approach
/*
	std::vector<double> new_variables;
	for(size_t k=0; k<dim; k++){
	  double dr=range*(2.0*distrubution(generator)-1.0);
	  double tmp=variables[j][k]+dr;
	  if (tmp<lower_limits[k])
	     tmp=lower_limits[k]-dr;
	  else if (tmp>upper_limits[k])
	     tmp=upper_limits[k]-dr;
	  new_variables.push_back(tmp);
	}
	
	double new_value=F(new_variables);
	
	
	if(values[j]>new_value){
	  for(size_t k=0; k<dim; k++){
	    variables[j][k]=new_variables[k];
	  }
	    values[j]=new_value;
	}
	else if (exp(beta*(values[j]-new_value)) >= distrubution(generator)){
	  for(size_t k=0; k<dim; k++){
	    variables[j][k]=new_variables[k];
	  }
	    values[j]=new_value;
	}
	beta*=beta_step;
     }
*/
	// prepare move ASA approach
	std::vector<double> new_variables;
	for(size_t k=0; k<dim; k++){
		double u=distrubution(generator);
		double nv = variables[j][k];
//		std::cout<<pow(1.0+beta,fabs(2.0*u-1.0))<<std::endl;
		    if(u<0.5)
			nv-=((pow(1+beta,fabs(2.0*u-1.0))-1)/beta)*(upper_limits[k]-lower_limits[k]);
		    else
			nv+=((pow(1+beta,fabs(2.0*u-1.0))-1)/beta)*(upper_limits[k]-lower_limits[k]);
//		std::cout<<nv<<std::endl;
		if(nv>upper_limits[k])
			new_variables.push_back(upper_limits[k]);
		else if(nv<lower_limits[k])
			new_variables.push_back(lower_limits[k]);
		else
			new_variables.push_back(nv);
	}
	double new_value=F(new_variables);

	if(values[j]>new_value){
	  for(size_t k=0; k<dim; k++){
	    variables[j][k]=new_variables[k];
	  }
	    values[j]=new_value;
	    //	beta*=beta_step;
	beta=beta0*pow(beta_step,pow((double)i,1.0/dim));

	}
	else if (exp(beta*(values[j]-new_value)) >= distrubution(generator)){
	  for(size_t k=0; k<dim; k++){
	    variables[j][k]=new_variables[k];
	  }
	    values[j]=new_value;
	    //beta*=beta_step;
	beta=beta0*pow(beta_step,pow((double)i,1.0/dim));

	}
//	else	beta*=sqrt(beta_step);

      }

    }
            
  }

#ifdef   _MC_OPT_MPI  
struct qmt_double_int {
            double val;
            int   rank;
        };   
#endif  
  
public:
  void run(const size_t max_steps, const double eps, std::vector<double>& point_values, std::vector<std::vector<double>>& points){
    reset();
    
    metropolis(max_steps,eps);

    point_values = values;
    points = variables;
        
  }
  
  void run(const size_t max_steps, const double eps, double& point_value, std::vector<double>& points){
    reset();
    
    metropolis(max_steps,eps);

#ifdef   _MC_OPT_MPI
       int n, myrank;
       MPI_Comm_size(MPI_COMM_WORLD, &n);
       MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
       double *mpi_vars_global = new double[n*dim];
       double *mpi_vars = new double[dim];
       qmt_double_int mpi_value;
       mpi_value.val = values[0];
       mpi_value.rank = myrank;
       
       for(int i = 0; i < dim; ++i)
	 mpi_vars[i] = variables[0][i];
       
      qmt_double_int mpi_min_result;
       
       MPI_Allreduce(&mpi_value, &mpi_min_result, 1, MPI_DOUBLE_INT,MPI_MINLOC,MPI_COMM_WORLD);
       MPI_Allgather(mpi_vars,dim,MPI_DOUBLE,mpi_vars_global,dim,MPI_DOUBLE, MPI_COMM_WORLD);
#endif      
      
#ifndef _MC_OPT_MPI    
    auto min_val = std::min_element(std::begin(values),std::end(values));
    point_value = *min_val;
    points = variables[std::distance(std::begin(values),min_val)];
#endif

#ifdef _MC_OPT_MPI    
      point_value = mpi_min_result.val;
      
      points.clear();
      for(int i = 0; i < dim; ++i) {
	points.push_back(mpi_vars_global[mpi_min_result.rank * dim + i]);
      }
      
#endif
    
        
    
#ifdef   _MC_OPT_MPI
       delete [] mpi_vars_global;
       delete [] mpi_vars;
#endif 
  }
    
  ~QmtSimulatedAnnealing(){}
};

}
#endif // QMT_SIMULATED_ANNEALING_H