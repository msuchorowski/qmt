#ifndef QMTSTATE_H_INCLUDED
#define QMTSTATE_H_INCLUDED

#include<vector>
#include<numeric>
#include<utility>
#include<algorithm>
#include<stdexcept>
#include<iostream>
#include<bitset>
#include "qmtbitsettools.h"
#include<tuple>

namespace qmt {

typedef unsigned int uint;

enum class QmtSpin {
    up, down
};


enum class Occupation {
    vaccum,zero, up, down, up_down
};



/////////////////////////////////////////////////////////////////////////////////////////

class QmtOrbitalState {

public:


private:

    Occupation _occupation;

public:

    QmtOrbitalState(): _occupation(Occupation::zero) {}
    QmtOrbitalState( Occupation occupation):  _occupation(occupation) {}

    void set_state(Occupation occ) {
        _occupation = occ;
    }
    Occupation get_state() const {
        return _occupation;
    }

    static QmtOrbitalState vaccum_state() {
        return  QmtOrbitalState(Occupation::vaccum);
    }
    static QmtOrbitalState zero_state()   {
        return  QmtOrbitalState(Occupation::zero);
    }
    static QmtOrbitalState up_state()     {
        return  QmtOrbitalState(Occupation::up);
    }
    static QmtOrbitalState down_state()   {
        return  QmtOrbitalState(Occupation::down);
    }
    static QmtOrbitalState double_state() {
        return  QmtOrbitalState(Occupation::up_down);
    }



    static double dot_product(const QmtOrbitalState& s1, const QmtOrbitalState& s2) {

        if( s1._occupation == s2._occupation  && s1._occupation != Occupation::vaccum) {
            return 1.0;
        }
        else
            return 0;
    }

    friend std::ostream& operator<<(std::ostream& stream, const QmtOrbitalState& state) {
        switch(state._occupation) {
        case Occupation::vaccum:
            stream<<"(vaccum)";
            break;
        case Occupation::zero:
            stream<<"(0)";
            break;
        case Occupation::up:
            stream<<"(1)";
            break;
        case Occupation::down:
            stream<<"(-1)";
            break;
        case Occupation::up_down:
            stream<<"(1,-1)";
            break;
        }

        return stream;
    }

    static void anihilate_up( QmtOrbitalState& state ) {
        switch(state._occupation) {
        case Occupation::vaccum :
            break;
        case Occupation::zero:
            state._occupation = Occupation::vaccum;
            break;
        case Occupation::down:
            state._occupation = Occupation::vaccum;
            break;
        case Occupation::up:
            state._occupation = Occupation::zero;
            break;
        case Occupation::up_down:
            state._occupation = Occupation::down;
            break;
        }
    }

    static void anihilate_down( QmtOrbitalState& state ) {
        switch(state._occupation) {
        case Occupation::vaccum :
            break;
        case Occupation::zero:
            state._occupation = Occupation::vaccum;
            break;
        case Occupation::up:
            state._occupation = Occupation::vaccum;
            break;
        case Occupation::down:
            state._occupation = Occupation::zero;
            break;
        case Occupation::up_down:
            state._occupation = Occupation::up;
            break;
        }
    }

    static void create_up(QmtOrbitalState& state) {
        switch(state._occupation) {
        case Occupation::vaccum :
            break;
        case Occupation::zero:
            state._occupation = Occupation::up;
            break;
        case Occupation::up:
            state._occupation = Occupation::vaccum;
            break;
        case Occupation::down:
            state._occupation = Occupation::up_down;
            break;
        case Occupation::up_down:
            state._occupation = Occupation::vaccum;
            break;
        }

    }

    static void create_down(QmtOrbitalState& state) {
        switch(state._occupation) {
        case Occupation::vaccum :
            break;
        case Occupation::zero:
            state._occupation = Occupation::down;
            break;
        case Occupation::up:
            state._occupation = Occupation::up_down;
            break;
        case Occupation::down:
            state._occupation = Occupation::vaccum;
            break;
        case Occupation::up_down:
            state._occupation = Occupation::vaccum;
            break;
        }

    }

};


/*
 It is intended to collect here states which are single term tensor product of local states: |1> x |2> x |3> ...x |n>
 so no linear combinations are considered
*/


/////////////////////////////////////////////////////////////////////////////////////////
//NEEDS REVISION!!! PHASE SIGN PROBLEM
/////////////////////////////////////////////////////////////////////////////////////////
template <typename State, typename Out = double>
class QmtNState {
public:

    typedef QmtNState<State,Out> qmtState;

private:
    typedef unsigned int uint;

// QmtNState(const qmtState&) = delete;
// qmtState& operator=(const qmtState) = delete;

    uint size;
    double phase;
    std::vector<State> states;

    static Out accumalate(const Out& v1, const Out& v2) {
        return v1 * v2; //Out must be additive and should be commutative
    }

    void set_phase(uint band, QmtSpin spin) {
        uint p = 0;

        if(spin == QmtSpin::up) {
            for(uint i = 0; i < band; ++i) {
                if(states[i].get_state() == Occupation::up || states[i].get_state() == Occupation::up_down) {
                    p++;
                }
            }
        }

        if(spin == QmtSpin::down) {
            for(uint i = 0; i < size; ++i)
                if(states[i].get_state() == Occupation::up || states[i].get_state() == Occupation::up_down) {
                    p++;
                }

            for(uint i = 0; i < band; ++i) {
                if(states[i].get_state() == Occupation::down || states[i].get_state() == Occupation::up_down) {
                    p++;
                }
            }
        }

//	  std::cout<<"p = "<<p<<std::endl;
        if(p % 2  != 0)
            phase = phase * (-1);
        else
            phase =  1;
        //std::cout<<"phase = "<<phase<<std::endl;
    }


public:

    typedef State SingleState;

    void setPhase(double p) {
        phase = p;
    }

    double getPhase() const {
        return phase;
    }
    QmtNState(uint N):size(N), phase(1) {
        states.reserve(N);
        std::fill(states.begin(),states.end(), State());
    }

    QmtNState(const QmtNState& state) {
        size = state.size;
        states.reserve(size);
        std::fill(states.begin(),states.end(), State());
        for(int i = 0; i < size; ++i)
            states[i] = State(state.states[i]);
        phase = state.phase;
    }

    qmtState& operator=(const qmtState& state) {
        if( &state != this) {
            size = state.size;
            states.reserve(size);
            std::fill(states.begin(),states.end(), State());
            for(int i = 0; i < size; ++i)
                states[i] = State(state.states[i]);
            phase = state.phase;
        }

        return *this;
    }

    State& operator[](uint N) {
        if( N >= size) throw std::length_error( "QmtNState::operator[] length error exception");
        return states[N];
    }


    void anihilate_up(uint band) {
        set_phase(band,QmtSpin::up);
        SingleState::anihilate_up(states[band]);
    }

    void anihilate_down(uint band) {
        set_phase(band, QmtSpin::down);
        SingleState::anihilate_down(states[band]);
    }

    void create_up(uint band) {
        set_phase(band, QmtSpin::up);
        SingleState::create_up(states[band]);
    }

    void create_down(uint band) {
        set_phase(band, QmtSpin::down);
        SingleState::create_down(states[band]);
    }


    static Out dot_product(const qmtState& state1, const qmtState& state2) {
        Out p = state1.phase * state2.phase;
        return p *  std::inner_product(state1.states.begin(), state1.states.begin() + state1.size,
                                       state2.states.begin(), Out() + 1, qmtState::accumalate, State::dot_product );
    }


    friend std::ostream& operator<<(std::ostream& stream,const qmtState& state) {
        if(state.phase > 0)
            stream<<"+";
        else
            stream<<"-";

        stream<<"|";
        for(int i = 0; i < state.size; ++i)
            stream<<state.states[i];
        stream<<">";
        return stream;
    }

};

/*
 *  QmtNState template specialization: |100100....> coded on single unsigned long and unsigned  int
 */
 
 
typedef unsigned long ulong;

template <>
class QmtNState<ulong,int> {

    ulong state;
    ulong size;   // band
    double phase; //-1, +1, 0 "zero state"

    ulong mask_decomposer(ulong number, ulong length) {

        ulong n = 0;
        
        for(ulong i = 0; i < length; ++i) {
            if( (1UL<<i) & number) n++;
        }

        return n;
    }

public:

    QmtNState<ulong,int>() : state(0), size(0), phase(0) {
    }

    QmtNState<ulong,int>(ulong n) : state(0), size(n), phase(1) {
    }

    QmtNState<ulong,int>(ulong n, ulong s, int p = 1 ) : state(s), size(n), phase(p) {

    }

    ulong get_size() const {
        return size;
    }

    double getPhase() const {
        return phase;
    }
    void setPhase(double p) {
        phase = p;
    }

    ulong integerRepresentation() const {
        return state;
    }
    void anihilate_up(ulong band) {
        
        ulong mask = 1UL<<band;
        ulong new_state = state ^ mask;

        if(new_state > state) {
            phase = 0;
            state = 0;
            return;
        }

        else {
            phase = phase * (mask_decomposer(state, band)%2 == 0 ? 1 : -1);
            state = new_state;
        }
    }



    void anihilate_down(ulong band) {
        ulong offset = band + size; //TO DO: offset to constructor?
        ulong mask = 1UL<<(offset);
        ulong new_state = state ^ mask;

        if(new_state > state) {
            phase = 0;
            state = 0;
            return;
        }

        else {
            phase = phase * (mask_decomposer(state, offset)%2 == 0 ? 1 : -1);
            state = new_state;
        }
    }



    void create_up(ulong band) {
        ulong mask = 1UL<<band;
        ulong new_state = state | mask;

        if(new_state == state) {
            phase = 0;
            state = 0;
            return;
        }
        else {
            phase = phase * (mask_decomposer(state, band)%2 == 0 ? 1 : -1);
            state = new_state;
        }
    }


    void create_down(ulong band) {
        ulong offset = band + size;
        ulong mask = 1UL<<offset;
        ulong new_state = state | mask;

        if(new_state == state) {
            phase = 0;
            state = 0;
            return;
        }
        else {
            phase = phase * (mask_decomposer(state, offset)%2 == 0 ? 1 : -1);
            state = new_state;
        }
    }


    friend std::ostream& operator<<(std::ostream& stream,const QmtNState<ulong,int>& state) {

        ulong n = state.state;
        ulong one = 1;
        if(state.phase < 0 )
            stream<<"-";
        if(state.phase == 0) {
            stream<<"zero state";
            return stream;
        }
        stream<<"|";
        for(uint i = 0; i < state.size; ++i) {
            if((one<<i) & n) stream<<"1";
            else stream<<"0";
        }
        std::cout<<"> x ";

        stream<<"|";
        for(uint i =state.size ; i < 2 * state.size ; ++i) {
            if((one<<i) & n) stream<<"1";
            else stream<<"0";
        }
        stream<<">";
        return stream;
    }


    static double dot_product(const QmtNState<ulong,int>& state1, const QmtNState<ulong,int>& state2) {

        if( state1.size != state2.size) throw std::length_error("QmtNState<uint,int>::dot_product");

        double p = state1.phase * state2.phase;
        int result = state1.state == state2.state ? 1 : 0;

        return p * result;
    }

    static bool compare_configuration(const QmtNState<ulong,int>& first, const QmtNState<ulong,int>& second){
		return (first.state == second.state);
    }

    static bool gt_configuration(const QmtNState<ulong,int>& first, const QmtNState<ulong,int>& second){
		return (first.state > second.state);
    }

    static bool lt_configuration(const QmtNState<ulong,int>& first, const QmtNState<ulong,int>& second){
		return (first.state < second.state);
    }

};


////////////////////////////////////////////////////////////////////////////////////////
//				BITSET


template <std::size_t _size>
class QmtNState<std::bitset<_size>,int> {

    std::bitset<_size> state;
    ulong size;   // band
    double phase; //-1, +1, 0 "zero state"

    ulong mask_decomposer(std::bitset<_size> number, ulong length) {
       
	std::bitset<_size> mask=std::bitset<_size>(0);
        for(ulong i = 0; i < length; ++i) {
		mask.set(i);
        }

        return (mask & number).count();
    }

public:

    QmtNState<std::bitset<_size>,int>() : state(0), size(0), phase(0) {
    }

    QmtNState<std::bitset<_size>,int>(ulong n) : state(0), size(n), phase(1) {
    }

    QmtNState<std::bitset<_size>,int>(ulong n, std::bitset<_size> s, double p = 1 ) : state(s), size(n), phase(p) {

    }

    ulong get_size() const {
        return size;
    }

    double getPhase() const {
        return phase;
    }
    void setPhase(double p) {
        phase = p;
    }

    std::bitset<_size> integerRepresentation() const {
        return state;
    }

    void anihilate_up(ulong band) {
        if(!state[band]) {
            phase = 0;
            state = 0;
            return;
        }

        else {
           phase = phase * (mask_decomposer(state, band)%2 == 0 ? 1 : -1);
            state[band]=0;
        }
    }



    void anihilate_down(ulong band) {
        ulong offset = band + size; //TO DO: offset to constructor?

       if(!state[offset])  {
            phase = 0;
            state = 0;
            return;
        }

        else {
            phase = phase * (mask_decomposer(state, offset)%2 == 0 ? 1 : -1);
            state[offset]=0;
        }
    }



    void create_up(ulong band) {
         if(state[band]) {
            phase = 0;
            state = 0;
            return;
        }
        else {
            phase = phase * (mask_decomposer(state, band)%2 == 0 ? 1 : -1);
            state.set(band);
        }
    }


    void create_down(ulong band) {
        ulong offset = band + size;

        if(state[offset]) {
            phase = 0;
            state = 0;
            return;
        }
        else {
            phase = phase * (mask_decomposer(state, offset)%2 == 0 ? 1 : -1);
            state.set(offset);
        }
    }


    bool is_occupied(size_t index) const {    
	return state[index];
    }
    
 
    void set_bit(size_t index, bool value) {
       state[index] = value;
    }
    
    bool get_bit(size_t index) const{
       return state[index];
    }

    friend std::ostream& operator<<(std::ostream& stream,const QmtNState<std::bitset<_size>,int>& state) {

        std::bitset<_size> n = state.state;
        std::bitset<_size> one = std::bitset<_size>(1);
        if(state.phase < 0 )
            stream<<"-";
        if(state.phase == 0) {
            stream<<"zero state";
            return stream;
        }
        stream<<"|";
        for(uint i = 0; i < state.size; ++i) {
            if(((one<<i) & n)!= std::bitset<_size>(0)) stream<<"1";
            else stream<<"0";
        }
        std::cout<<"> x ";

        stream<<"|";
        for(uint i =state.size ; i < 2 * state.size ; ++i) {
            if(((one<<i) & n)!= std::bitset<_size>(0)) stream<<"1";
            else stream<<"0";
        }
        stream<<">";
        return stream;
    }


    static double dot_product(const QmtNState<std::bitset<_size>,int>& state1, const QmtNState<std::bitset<_size>,int>& state2) {

        if( state1.size != state2.size) throw std::length_error("QmtNState<std::bitset<_size>,int>::dot_product");

        double p = state1.phase * state2.phase;
        int result = state1.state == state2.state ? 1 : 0;

        return p * result;
    }

    static double dot_product(const std::vector<QmtNState<std::bitset<_size>,int>>& states1, const std::vector<QmtNState<std::bitset<_size>,int>>& states2) {
	double result=0.0;

	unsigned int i=0;
	unsigned int j=0;

	while(i<states1.size() && j<states2.size()){
	if(compare_configuration(states1[i],states2[j])){
		result += states1[i].phase * states2[j].phase;
		i++;
		j++;
		}
	else if(gt_configuration(states1[i],states2[j]))
		j++;
	else
		i++;
	}

        
        return result;
    }

    static bool compare_configuration(const QmtNState<std::bitset<_size>,int>& first, const QmtNState<std::bitset<_size>,int>& second){
		return qmt::bitsettools::eq(first.state,second.state);
    }

    static bool gt_configuration(const QmtNState<std::bitset<_size>,int>& first, const QmtNState<std::bitset<_size>,int>& second){
		return qmt::bitsettools::gt(first.state,second.state);
    }

    static bool lt_configuration(const QmtNState<std::bitset<_size>,int>& first, const QmtNState<std::bitset<_size>,int>& second){
		return qmt::bitsettools::lt(first.state,second.state);
    }
};





////////////////////////////////////////////////////////////////////////////////////////

template <>
class QmtNState<uint,int> {

    uint state;
    uint size;   // band
    double phase; //-1, +1, 0 "zero state"

    uint mask_decomposer(uint number, uint length) {

        uint n = 0;

        for(uint i = 0; i < length; ++i) {
            if( (1<<i) & number) n++;
        }

        return n;
    }

public:

    QmtNState<uint,int>() : state(0), size(0), phase(0) {
    }

    QmtNState<uint,int>(uint n) : state(0), size(n), phase(1) {
    }

    QmtNState<uint,int>(uint n, uint s, int p = 1 ) : state(s), size(n), phase(p) {

    }

    uint get_size() const {
        return size;
    }

    double getPhase() const {
        return phase;
    }
    void setPhase(double p) {
        phase = p;
    }

    uint integerRepresentation() const {
        return state;
    }
    void anihilate_up(uint band) {
        uint mask = 1<<band;
        uint new_state = state ^ mask;

        if(new_state > state) {
            phase = 0;
            state = 0;
            return;
        }

        else {
            phase = phase * (mask_decomposer(state, band)%2 == 0 ? 1 : -1);
            state = new_state;
        }
    }



    void anihilate_down(uint band) {
        uint offset = band + size; //TO DO: offset to constructor?
        uint mask = 1<<(offset);
        uint new_state = state ^ mask;

        if(new_state > state) {
            phase = 0;
            state = 0;
            return;
        }

        else {
            phase = phase * (mask_decomposer(state, offset)%2 == 0 ? 1 : -1);
            state = new_state;
        }
    }



    void create_up(uint band) {
        uint mask = 1<<band;
        uint new_state = state | mask;

        if(new_state == state) {
            phase = 0;
            state = 0;
            return;
        }
        else {
            phase = phase * (mask_decomposer(state, band)%2 == 0 ? 1 : -1);
            state = new_state;
        }
    }


    void create_down(uint band) {
        uint offset = band + size;
        uint mask = 1<<offset;
        uint new_state = state | mask;

        if(new_state == state) {
            phase = 0;
            state = 0;
            return;
        }
        else {
            phase = phase * (mask_decomposer(state, offset)%2 == 0 ? 1 : -1);
            state = new_state;
        }
    }


    friend std::ostream& operator<<(std::ostream& stream,const QmtNState<uint,int>& state) {

        uint n = state.state;

        if(state.phase < 0 )
            stream<<"-";
        if(state.phase == 0) {
            stream<<"zero state";
            return stream;
        }
        stream<<"|";
        for(uint i = 0; i < state.size; ++i) {
            if((1<<i) & n) stream<<"1";
            else stream<<"0";
        }
        std::cout<<"> x ";

        stream<<"|";
        for(uint i =state.size ; i < 2 * state.size ; ++i) {
            if((1<<i) & n) stream<<"1";
            else stream<<"0";
        }
        stream<<">";
        return stream;
    }


    static double dot_product(const QmtNState<uint,int>& state1, const QmtNState<uint,int>& state2) {

        if( state1.size != state2.size) throw std::length_error("QmtNState<uint,int>::dot_product");

        double p = state1.phase * state2.phase;
        int result = state1.state == state2.state ? 1 : 0;

        return p * result;
    }

    static bool compare_configuration(const QmtNState<uint,int>& first, const QmtNState<uint,int>& second){
		return (first.state == second.state);
    }

    static bool gt_configuration(const QmtNState<uint,int>& first, const QmtNState<uint,int>& second){
		return (first.state > second.state);
    }

    static bool lt_configuration(const QmtNState<uint,int>& first, const QmtNState<uint,int>& second){
		return (first.state < second.state);
    }
};


/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
template <typename NState, typename T = double, int Parallel = 0>
 class QmtStatesCombination {
 std::vector<T> coefs;
 std::vector<NState> basis_states;

 std::vector<std::vector <std::tuple<unsigned long ,int,int>> > stats;
 
 

 public:
   
static   bool compare(const NState& s1,const NState& s2) {
	        return s1.integerRepresentation() < s2.integerRepresentation(); }
    
    QmtStatesCombination(const std::vector<NState> &basis) {
       std::copy(basis.begin(), basis.end(), std::back_inserter(basis_states));                   	
       std::sort(basis_states.begin(),basis_states.end(),compare);
       coefs.reserve(basis_states.size());
       stats.reserve(basis_states.size());
       std::fill(stats.begin(),stats.end(),std::vector<std::tuple<unsigned long ,int,int>>());
       std::fill(coefs.begin(),coefs.end(),T());
    }
   
   
   
   long size() const {return basis_states.size();}
   
   static T dot_product(const QmtStatesCombination &state1, const QmtStatesCombination &state2) {

        if( state1.size != state2.size) throw std::length_error("QmtStatesCombination::dot_product");
        
	T result = T();
	
	for(const auto index : state1.size)
	  result += state1.coefs[index] * state2.coefs[index]*NState::dot_product(state1.basis_states[index], state2.basis_states[index]);
	
    return result;
        
    }
    
   void one_body_action(QmtSpin spin, uint band1,uint band2, double integral = 1.0 ) {
     for(const auto &state : basis_states) {
	  auto temp_state(state);
	  if(spin == QmtSpin::up) {
	    temp_state.anihilate_up(band2);
	    temp_state.create_up(band1);	  
	  }
	  if(spin == QmtSpin::down) {
	    temp_state.anihilate_down(band2);
	    temp_state.create_down(band1);
	  }
	  
	  T phase = temp_state.getPhase();
	  uint int_repr = temp_state.integerRepresentation();
	 auto index =  (std::lower_bound(basis_states.begin(),basis_states.end(),temp_state) - basis_states.begin());
	  coefs[index] += phase * integral;
	}
     
   }
  

   void two_body_action(QmtSpin spin,QmtSpin spinp, uint band1,uint band2,uint band3,uint band4, int integral_id = 0, double integral = 1.0 ) {
     long cntr = 0;
     for(const auto &state : basis_states) {
	  auto temp_state(state);
	  if(spin == QmtSpin::up && spinp == QmtSpin::up) {
	    temp_state.anihilate_up(band4);
	    temp_state.anihilate_up(band3);	  
	    temp_state.create_up(band2);
	    temp_state.create_up(band1);	  
          //  continue;
	  }
	  
	  if(spin == QmtSpin::up && spinp == QmtSpin::down) {
	    temp_state.anihilate_up(band4);
	    temp_state.anihilate_down(band3);	  
	    temp_state.create_down(band2);
	    temp_state.create_up(band1);	  
         //   continue;
	  }
	  
	  
	  if(spin == QmtSpin::down && spinp == QmtSpin::down) {
	    temp_state.anihilate_down(band4);
	    temp_state.anihilate_down(band3);	  
	    temp_state.create_down(band2);
	    temp_state.create_down(band1);	  
          //  continue;
	  }
	  
	  if(spin == QmtSpin::down && spinp == QmtSpin::up) {
	    temp_state.anihilate_down(band4);
	    temp_state.anihilate_up(band3);	  
	    temp_state.create_up(band2);
	    temp_state.create_down(band1);	  
          //  continue;
	  }
	  
	  T phase = temp_state.getPhase();
	  //uint int_repr = temp_state.integerRepresentation();
	int index = static_cast<int>(std::lower_bound(basis_states.begin(),basis_states.end(), temp_state,compare) - basis_states.begin());
	//   coefs[index] += phase * integral;
         if(phase != 0) {
        // std::cout<<"Inserting:("<<index<<", "<<integral_id<<", "<<phase<<") for state"<<state<<std::endl;
         stats[cntr].push_back(std::make_tuple(index,integral_id,phase)); 
}
         cntr++;
	}
     
   }

   
};

/////////////////////////////////////////////////////////////////////////////////////////

template < typename NState >
class SqOperator {

public:
    enum Rule { anihilator, creator};

private:
    Rule _rule;
    unsigned int _band;
    QmtSpin _spin;

public:
    SqOperator< NState > (SqOperator::Rule rule, unsigned int id, QmtSpin spin):
        _rule(rule), _band(id), _spin(spin) {
    }

    typedef NState SqOperatorNState;

    /*
    Again: Should we rely on Return Value Optimization? Neverless, it will not be used many times...
    */
    static SqOperator get_up_creator(unsigned int id) {
        return SqOperator<NState>(Rule::creator, id, QmtSpin::up);
    }

    static SqOperator get_down_creator(unsigned int id) {
        return SqOperator<NState>(Rule::creator, id, QmtSpin::down);
    }

    static SqOperator get_up_anihilator(unsigned int id)  {
        return SqOperator<NState>(Rule::anihilator, id, QmtSpin::up);
    }

    static SqOperator get_down_anihilator(unsigned int id) {
        return SqOperator<NState>(Rule::anihilator, id, QmtSpin::down);
    }

    NState operator()(const NState &in_state) {
        NState state(in_state);
        if(_rule == Rule::anihilator) {
            if(_spin == QmtSpin::up)
                state.anihilate_up(_band);
            if(_spin == QmtSpin::down)
                state.anihilate_down(_band);
        }

        if(_rule == Rule:: creator) {
            if(_spin == QmtSpin::up)
                state.create_up(_band);
            if(_spin == QmtSpin::down)
                state.create_down(_band);
        }
        return state;
    }

};


template <typename NState, class T = double>
class QmtNStateLC : public NState {

    std::vector<std::tuple<T, NState>> n_states;
    typedef typename std::vector<std::tuple<T, NState>>::iterator n_iterator;
    typedef typename std::vector<std::tuple<T, NState>>::const_iterator n_const_iterator;



public:
    QmtNStateLC< NState,T >() {}

    uint get_size() const {
        return n_states.size();
    }

    void set(const T& c, const NState& nstate) {
        n_states.push_back(std::make_tuple(T(c),NState(nstate)));
    }


    void anihilate_up(uint band) {
        for(n_iterator i = n_states.begin(); i != n_states.end(); ++i)
            std::get<1>(*i).anihilate_up(band);
    }



    void anihilate_down(uint band) {
        for(n_iterator i = n_states.begin(); i != n_states.end(); ++i)
            std::get<1>(*i).anihilate_down(band);
    }



    void create_up(uint band) {
        for(n_iterator i = n_states.begin(); i != n_states.end(); ++i)
            std::get<1>(*i).create_up(band);
    }


    void create_down(uint band) {
        for(n_iterator i = n_states.begin(); i != n_states.end(); ++i)
            std::get<1>(*i).create_down(band);
    }



    static T dot_product(const QmtNStateLC<NState,T>& state1, const QmtNStateLC<NState,T>& state2) {

        T result;
        for(n_const_iterator i = state1.n_states.begin(); i != state1.n_states.end(); ++i ) {
            for(n_const_iterator j = state2.n_states.begin(); j != state2.n_states.end(); ++j ) {
                result += std::get<0>(*i) * std::get<0>(*j) *
                          static_cast<T>(NState::dot_product(std::get<1>(*i),std::get<1>(*j)));
            }
        }
        return result;
    }

    friend std::ostream& operator<<(std::ostream& stream, const QmtNStateLC<NState,T> state) {
        for(uint i =  0; i < state.n_states.size(); ++i ) {
            stream<<std::get<0>(state.n_states[i])<<" x ["<<std::get<1>(state.n_states[i])<<"] "<<std::endl;
        }
        return stream;
    }


};

}

#endif // QMTSTATE_H_INCLUDED
