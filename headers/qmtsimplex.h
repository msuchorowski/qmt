#ifndef QMT_SIMPLEX_H
#define QMT_SIMPLEX_H

#include <iostream>
#include <vector>
#include <functional>
#include <random>
#include <algorithm>
#include <cmath>
#include <exception>

#ifdef _MC_OPT_MPI
#include <mpi.h>
#endif

namespace qmt {

class QmtSimplex {
  
size_t N;					//number of solvers  
size_t dim;					//number of dimensions of minimization
std::function<double(std::vector<double>)> F;	//function to optimize
std::vector<std::vector<double>> variables;	// variables to optimize
std::vector<double> values;			// values

std::random_device rand_dev;
std::mt19937 generator;
std::uniform_real_distribution<> distrubution;

private:
	class amoeba{
		std::vector<std::vector<double>> vertices;
		std::vector<double> values_at_vertices;
		std::function<double(std::vector<double>)> F;
		size_t highest_value_index;
		size_t second_highest_value_index;
		size_t lowest_value_index;


		public:
		amoeba(const size_t N, double** points, std::function<double(std::vector<double>)> _F):F(_F){
			//assigning vertices
			for(size_t i=0; i<N+1; i++)
				for(size_t j=0; j<N; j++)
					vertices[i][j] = points[i][j];

			// calculating values
			for(const auto& vertex : vertices)
				values_at_vertices.push_back(F(vertex));

			// finding max and min values
			highest_value_index = 0;
			second_highest_value_index=0;
			lowest_value_index = 0;
			for(unsigned int i=0; i<values_at_vertices.size(); i++){
				if(values_at_vertices[i]>values_at_vertices[highest_value_index])
					highest_value_index=i;
				if(values_at_vertices[i]<values_at_vertices[lowest_value_index])
					lowest_value_index=i;
			}
			for(unsigned int i=0; i<values_at_vertices.size(); i++){
				if(i==highest_value_index)
					continue;
				if(values_at_vertices[i]>values_at_vertices[highest_value_index])
					second_highest_value_index=i;
			}	

		}

		amoeba(const size_t N, std::vector<std::vector<double>>& points, std::function<double(std::vector<double>)> _F):F(_F){
			if(points.size()!=N+1)
				std::logic_error("QmtSimplex::amoeba: One must provide amoeba with N+1 points!");
			for(const auto& point : points)
				if(point.size()!=N)
					std::logic_error("QmtSimplex::amoeba: Points must be N-dimensional!");

			//assigning vertices
			vertices=points;

			// calculating values
			for(const auto& vertex : vertices)
				values_at_vertices.push_back(F(vertex));

			// finding max and min values
			highest_value_index = 0;
			second_highest_value_index=0;
			lowest_value_index = 0;
			for(unsigned int i=0; i<values_at_vertices.size(); i++){
				if(values_at_vertices[i]>values_at_vertices[highest_value_index])
					highest_value_index=i;
				if(values_at_vertices[i]<values_at_vertices[lowest_value_index])
					lowest_value_index=i;
			}
			for(unsigned int i=0; i<values_at_vertices.size(); i++){
				if(i==highest_value_index)
					continue;
				if(values_at_vertices[i]>values_at_vertices[highest_value_index])
					second_highest_value_index=i;
			}	
		}

		void try_me(){
			double value;
			std::vector<double> vertex;

			reflection(value,vertex);

			if(values_at_vertices[lowest_value_index]>=value){
				reflection_and_expansion(2.0,value,vertex);
				set_vertex(highest_value_index,value,vertex);
			}
			else if	(values_at_vertices[second_highest_value_index]<=value){
				contraction(0.5,value,vertex);
				if(value >= values_at_vertices[second_highest_value_index])
					multiple_contraction(0.5);
				else
					set_vertex(highest_value_index,value,vertex);
			}
			else
				set_vertex(highest_value_index,value,vertex);

		}

		private:
		void reflection(double& value, std::vector<double>& vertex){
			vertex = vertices[highest_value_index];
			for(unsigned int i=0; i<vertices.size(); i++){
				if(i==highest_value_index)
					continue;

				for(unsigned int j=0; j<vertices[i].size(); j++){
					vertex[j]+=vertices[i][j]-vertices[highest_value_index][j];
				}
			}

			value = F(vertex);

		}

		void reflection_and_expansion(double delta, double& value, std::vector<double>& vertex){
			if(delta<=1.0)
				std::logic_error("QmtSimplex::amoeba: Scaling in reflection_and_expansion must be greater than 1.0");

			vertex = vertices[highest_value_index];
			for(unsigned int i=0; i<vertices.size(); i++){
				if(i==highest_value_index)
					continue;

				for(unsigned int j=0; j<vertices[i].size(); j++){
					vertex[j]+=delta*(vertices[i][j]-vertices[highest_value_index][j]);
				}
			}

			value = F(vertex);

		}

		void contraction(double delta, double& value, std::vector<double>& vertex){
			if(delta>=1.0)
				std::logic_error("QmtSimplex::amoeba: Scaling in contraction must be less than 1.0");

			vertex = vertices[highest_value_index];
			for(unsigned int i=0; i<vertices.size(); i++){
				if(i==highest_value_index)
					continue;

				for(unsigned int j=0; j<vertices[i].size(); j++){
					vertex[j]+=0.5*(1.0-delta)*(vertices[i][j]-vertices[highest_value_index][j]);
				}
			}

			value = F(vertex);

		}


		void multiple_contraction(double delta){
			if(delta>=1.0)
				std::logic_error("QmtSimplex::amoeba: Scaling in multiple_contraction must be less than 1.0");

			for(unsigned int i=0; i<vertices.size(); i++){
				if(i==lowest_value_index)
					continue;

				for(unsigned int j=0; j<vertices[i].size(); j++){
					vertices[i][j]+=delta*(vertices[highest_value_index][j]-vertices[i][j]);					
				}

				values_at_vertices[i] = F(vertices[i]);
			}
			highest_value_index = 0;
			second_highest_value_index=0;
			lowest_value_index = 0;
			for(unsigned int i=0; i<values_at_vertices.size(); i++){
				if(values_at_vertices[i]>values_at_vertices[highest_value_index])
					highest_value_index=i;
				if(values_at_vertices[i]<values_at_vertices[lowest_value_index])
					lowest_value_index=i;
			}
			for(unsigned int i=0; i<values_at_vertices.size(); i++){
				if(i==highest_value_index)
					continue;
				if(values_at_vertices[i]>values_at_vertices[highest_value_index])
					second_highest_value_index=i;
			}

		}

		void set_vertex(unsigned int index, double value, std::vector<double>& vertex){
			vertices[index]=vertex;
			values_at_vertices[index] = value;

			highest_value_index = 0;
			second_highest_value_index=0;
			lowest_value_index = 0;
			for(unsigned int i=0; i<values_at_vertices.size(); i++){
				if(values_at_vertices[i]>values_at_vertices[highest_value_index])
					highest_value_index=i;
				if(values_at_vertices[i]<values_at_vertices[lowest_value_index])
					lowest_value_index=i;
			}
			for(unsigned int i=0; i<values_at_vertices.size(); i++){
				if(i==highest_value_index)
					continue;
				if(values_at_vertices[i]>values_at_vertices[highest_value_index])
					second_highest_value_index=i;
			}
		}

		public:

		double get_amoeba_size(){
			return(2.0*fabs(values_at_vertices[highest_value_index]-values_at_vertices[lowest_value_index])/(fabs(values_at_vertices[highest_value_index])+fabs(values_at_vertices[lowest_value_index])+1e-16));
		}

		void get_minimal_value(double& min){ min = values_at_vertices[lowest_value_index];}
		void get_minimal_value(double& min, std::vector<double>& args){
			min = values_at_vertices[lowest_value_index];
			args= vertices[lowest_value_index];
		}

	}; // end of class amoeba

  std::vector<amoeba> amoebas;
  
public:
  QmtSimplex(const size_t _N, const size_t _dim, std::function<double(std::vector<double>)> _F, std::vector<std::vector<std::vector<double>>> points):
  N(_N),dim(_dim),F(_F){   
 
    for(unsigned int i=0; i<N; i++){
	amoebas.push_back(amoeba(dim,points[i],F));
	}

  }
  QmtSimplex(const size_t _N, const size_t _dim, std::function<double(std::vector<double>)> _F, double lower_limits, double upper_limits):
  N(_N),dim(_dim),F(_F){   
 
    generator=std::mt19937(rand_dev());
    distrubution=std::uniform_real_distribution<>(lower_limits,upper_limits);


    for(unsigned int i=0; i<N; i++){
	std::vector<std::vector<double>> tmp1;
	for(unsigned int j=0; j<dim+1;j++){
	   std::vector<double> tmp2;
	   for(unsigned int k=0; k<dim; k++)
		tmp2.push_back(distrubution(generator));
	   tmp1.push_back(tmp2);
	}
	amoebas.push_back(amoeba(dim,tmp1,F));
	}

  }

  
private:
  void reset(){

  }
  
  void simplex_method(const size_t max_steps, const double eps){
	std::vector<bool> RUN_ME(N,true);
    for (size_t i=0; i<max_steps; i++){

      //#pragma omp parallel for TO SOLVE: segmentation fault!
#ifdef   _MC_OPT_MPI
      N = 1; 
#endif      

	bool KILL_ME=true;
	for(size_t j=0; j<N; j++)
		if(RUN_ME[j])
			KILL_ME=false;

	if(KILL_ME)
		break;

	
      for(size_t j=0; j<N; j++){
	if(RUN_ME[j]){
		amoebas[j].try_me();
		if(amoebas[j].get_amoeba_size()<eps) RUN_ME[j]=false;
	}
      }

    }
            
  }


#ifdef   _MC_OPT_MPI  
struct qmt_double_int {
            double val;
            int   rank;
        };   
#endif  

 
public:
  void run(const size_t max_steps, const double eps, double& point_value, std::vector<double>& points){
    reset();
    
    simplex_method(max_steps,eps);

    for(size_t j=0; j<N; j++){
	double val;
	std::vector<double> args;
	amoebas[j].get_minimal_value(val,args);
	variables.push_back(args);
	values.push_back(val);
      }


#ifdef   _MC_OPT_MPI
       int n, myrank;
       MPI_Comm_size(MPI_COMM_WORLD, &n);
       MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
       double *mpi_vars_global = new double[n*dim];
       double *mpi_vars = new double[dim];
       qmt_double_int mpi_value;
       mpi_value.val = values[0];
       mpi_value.rank = myrank;
       
       for(int i = 0; i < dim; ++i)
	 mpi_vars[i] = variables[0][i];
       
      qmt_double_int mpi_min_result;
       
       MPI_Allreduce(&mpi_value, &mpi_min_result, 1, MPI_DOUBLE_INT,MPI_MINLOC,MPI_COMM_WORLD);
       MPI_Allgather(mpi_vars,dim,MPI_DOUBLE,mpi_vars_global,dim,MPI_DOUBLE, MPI_COMM_WORLD);
#endif      
      
#ifndef _MC_OPT_MPI    

    auto min_val = std::min_element(std::begin(values),std::end(values));
    point_value = *min_val;
    points = variables[std::distance(std::begin(values),min_val)];

#endif
#ifdef _MC_OPT_MPI    
      point_value = mpi_min_result.val;
      
      points.clear();
      for(int i = 0; i < dim; ++i) {
	points.push_back(mpi_vars_global[mpi_min_result.rank * dim + i]);
      }
      
#endif
    
        
    
#ifdef   _MC_OPT_MPI
       delete [] mpi_vars_global;
       delete [] mpi_vars;
#endif 


  }
    
  ~QmtSimplex(){}
};

}
#endif // QMT_SIMPLEX_H
