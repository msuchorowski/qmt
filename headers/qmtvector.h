/*
 * qmtvector.h
 *
 *  Created on: 30 lip 2014
 *      Author: abiborski
 */
#include <iostream>
#include <math.h>
#ifndef QMTVECTOR_H_
#define QMTVECTOR_H_

namespace qmt {

/*
 * This class provides basic euclidean vector operations - it is not intedned
 * to be fast but rather ease to use in contexts where some preparation for
 * computations i s performed
 */

class QmtVector {
    double r_x;
    double r_y;
    double r_z;
    double epsilon; //comparison threshold between two vectors

public:
    QmtVector(double x,double y, double z, double eps=1e-07):r_x(x), r_y(y), r_z(z), epsilon(eps) {}
    QmtVector():r_x(0), r_y(0), r_z(0), epsilon(1e-07) {}
    QmtVector(const QmtVector& v):r_x(v.r_x),r_y(v.r_y),r_z(v.r_z),epsilon(v.epsilon){}

    bool operator==(const QmtVector& right) {
//		std::cout<<(sq_norm(*this - right))<<std::endl;
        return (sq_norm(*this - right)) < epsilon;
    }

    QmtVector operator+(const QmtVector& right) const {
        return QmtVector(r_x + right.r_x, r_y + right.r_y, r_z + right.r_z);
    }

    QmtVector operator-(const QmtVector& right) const {
        return QmtVector(r_x - right.r_x, r_y - right.r_y, r_z - right.r_z);
    }

    QmtVector& operator*=(double scalar) {
        r_x*= scalar;
        r_y*= scalar;
        r_z*= scalar;
        return *this;
    }

    QmtVector& operator/=(double scalar) {
        r_x/= scalar;
        r_y/= scalar;
        r_z/= scalar;
        return *this;
    }

    QmtVector& operator+=(const QmtVector& right) {
        r_x+= right.r_x;
        r_y+= right.r_y;
        r_z+= right.r_z;
        return *this;
    }

    QmtVector& operator-=(const QmtVector& right) {
        r_x-= right.r_x;
        r_y-= right.r_y;
        r_z-= right.r_z;
        return *this;
    }

    friend QmtVector operator-(const QmtVector& right) {
        return QmtVector(-right.r_x, -right.r_y, -right.r_z);
    }
    /*
     * Scalar product
     */

    double operator*(const QmtVector& right) const {
        return r_x*right.r_x +  r_y* right.r_y +  r_z * right.r_z;
    }
    
    /*
     * Vector product
     */

    QmtVector operator^(const QmtVector& right) const {
        return QmtVector(r_y*right.r_z - r_z*right.r_y, r_z*right.r_x - r_x*right.r_z, r_x*right.r_y - r_y*right.r_x);
    }

    /*
     * double * vector
     */

    static double norm(const QmtVector& v) {

        return sqrt(v.r_x * v.r_x + v.r_y * v.r_y + v.r_z * v.r_z);
    }

    static double sq_norm(const QmtVector& v) {
        return v.r_x * v.r_x + v.r_y * v.r_y + v.r_z * v.r_z;
    }

    friend QmtVector operator*(double factor, const QmtVector& right ) {
        return QmtVector(factor * right.r_x, factor * right.r_y, factor * right.r_z);
    }

    QmtVector operator*(double factor ) {
        return QmtVector(factor * r_x, factor * r_y, factor * r_z);
    }



    friend std::ostream& operator<<(std::ostream& stream, const QmtVector vector) {
        stream<<"["<<vector.r_x<<", "<< vector.r_y<<", "<<vector.r_z<<"]";
        return stream;
    }

    double get_x() const {
        return r_x;
    }
    double get_y() const {
        return r_y;
    }
    double get_z() const {
        return r_z;
    }

    void set_x(double x)  {
        r_x = x;
    }
    void set_y(double y)  {
        r_y = y;
    }
    void set_z(double z)  {
        r_z = z;
    }

};

}

#endif /* QMTVECTOR_H_ */
