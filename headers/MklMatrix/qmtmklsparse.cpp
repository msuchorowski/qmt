#include "qmtmklsparse.h"


QmtMklSparseMatrix:: QmtMklSparseMatrix(uint size):M(size),nnz(0),initialized(false){}

void QmtMklSparseMatrix::set(const std::vector<double>& values, const std::vector<uint>& rows,const std::vector<uint>& cols) {

 if(!initialized) {

  MKL_INT job[6];
  nnz = values.size();

  job[0] = 2; //coo -> csr
  job[1] = 0;
  job[2] = 0;
  job[4] = nnz;
  job[5] = 0;

  MKL_INT n = M;
  double *acsr = new double [nnz];
  ja = new MKL_INT[nnz];
  ia = new MKL_INT[n+1];

  double *acoo = new double[nnz];
  MKL_INT *rowind = new MKL_INT [nnz];
  MKL_INT *colind = new MKL_INT [nnz];

  std::copy(values.begin(), values.end(), acoo);
  std::copy(rows.begin(), rows.end(), rowind);
  std::copy(cols.begin(), cols.end(), colind);

  int info;
  MKL_INT lnnz = nnz;
  mkl_dcsrcoo(job, &n, acsr, ja, ia, &lnnz, acoo, rowind, colind, &info);
  _values = new double [nnz];
  _columns = new MKL_INT[nnz];
  _pointerB = new MKL_INT[n];
  _pointerE = new MKL_INT[n];
  
  std::copy(acsr, acsr + nnz, _values);
  std::copy(ja, ja + nnz, _columns);
  std::copy(ia, ia + n, _pointerB);
  std::copy(ia + 1, ia + n + 1, _pointerE);


  delete [] acoo;
  delete [] rowind;
  delete [] colind;

  delete [] acsr;

  if( 0 == info) initialized = true;
  else  {
   std::cout<<"QmtMklSparseMatrix::Set: error on matrix conversion"<<std::endl;
   delete [] _values;
   delete [] _columns;
   delete [] ja;
   delete [] ia;
   delete [] _pointerB;
   delete [] _pointerE;
  }
}

/*
void mkl_scsrcoo(int *job, int *n, float *acsr, int *ja,

int *ia, int *nnz, float *acoo, int *rowind, int *colind, int *info);

http://denali.princeton.edu/IntelXe2011/mkl/mkl_manual/  
*/


}

bool QmtMklSparseMatrix::isInitialized() const {return initialized;}

void QmtMklSparseMatrix::Multiply(const  QmtMklSparseMatrix& A, double* v,double* u, double alpha, double beta) {
 if(A.isInitialized() ) {
 char  transpose = 'n';
 char  matdescra[6];
 matdescra[0] = 'G';
 matdescra[3] = 'C';

 MKL_INT m = A.M;
 double alp = alpha;
 double bet = beta;
 mkl_dcsrmv(&transpose, &m, &m, &alp,matdescra, A._values, A._columns, A._pointerB, A._pointerE, v, &bet, u);
 }
}

void QmtMklSparseMatrix::Add(const  QmtMklSparseMatrix& A,const  QmtMklSparseMatrix&B,double alpha) {
 if(A.isInitialized() && B.isInitialized() ) {
   char  transpose = 'n';
   char  matdescra[6];
   matdescra[0] = 'G';
   matdescra[3] = 'C';

 MKL_INT m = A.M;
 double alp = alpha;
 double bet = beta;
//	mkl_dcsradd(&transpose, 0, 0, A.M, A.M, A._values, A.ja, A.ia, alpha, B._values, B.jb, B.ib, c, jc, ic, &nzmax, &info);
 }
}

QmtMklSparseMatrix:: ~QmtMklSparseMatrix() {
if(initialized) {
 delete [] _values;
 delete [] _columns;
 delete [] ja;
 delete [] ia;
 delete [] _pointerB;
 delete [] _pointerE;
}
}




