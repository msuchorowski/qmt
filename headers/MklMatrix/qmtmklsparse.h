#include "/opt/intel/composer_xe_2013.1.117/mkl/include/mkl_types.h"
#include "/opt/intel/composer_xe_2013.1.117/mkl/include/mkl_spblas.h"
#include <vector>
#include <iostream>
#include <algorithm>

class QmtMklSparseMatrix {
typedef unsigned int uint;

bool initialized;

uint  M;
uint nnz;

uint _nnzcntr;

double* _values;
MKL_INT* _columns;

MKL_INT* ia;
MKL_INT* ja;
MKL_INT* _pointerB;
MKL_INT* _pointerE; 

public:

bool isInitialized() const;

QmtMklSparseMatrix(uint size);
~QmtMklSparseMatrix();

//void Reset();

void set(const std::vector<double>& values, const std::vector<uint>& rows,const std::vector<uint>& cols);

/*
double get_value(uint i, uint j) const;
*/
//Matrix Vector

static void Multiply(const  QmtMklSparseMatrix& A,  double* v,double* u, double alpha, double beta);
static void Add(const  QmtMklSparseMatrix& A,const  QmtMklSparseMatrix&B,double alpha);
/*
//Matrix-Matrix multiplication
static void Multiply(const  QmtMklSparseMatrix& A,const  QmtMklSparseMatrix& B,  QmtMklSparseMatrix& C);

//Matrix + Matrix
static void Add(const  QmtMklSparseMatrix& A,const  QmtMklSparseMatrix& B,double alpha);


friend std::ostream& operator<<(std::ostream& stream, const QmtMklSparseMatrix& matrix);
*/
};
