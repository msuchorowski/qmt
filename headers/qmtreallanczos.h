#ifndef QMTREALLANCZOS_H
#define QMTREALLANCZOS_H

#include <iostream>
#include <vector>
#include <cmath>
#include <random>

namespace qmt {
/*
 * MatrixAlgebra must contain:
 * types:
 * 		(*) Vector
 * 		(*) Matrix
 * 		(*) Tridiagonal
 *
 * static functions:
 *		(*) Matrix-Vector Algebra:
 *				- void MultiplyMV(const Matrix& A, const Vector& b, Vector& c)
 *				- void LinearMultiplyMV(const Matrix& A, const Vector& b, Vector& c, double alpha, double beta)
 *		(*) Vector Algebra:
 *				- void MultiplyVV(const Vector& a, const Vector& b, double* result)
 *				- void CopyVV(const Vector& a, Vector& b)
 *				- void CopyVV(const Vector& a, Vector& b, double alpha)
 *				- void ScaleV(Vector& a, double alpha)
 *				- void SwapVV(Vector& a, Vector& b)
 *				- double NormV(const Vector& a)
 *				- void NormalizeV(Vector& a)
 *		(*) (De)Inicialization:
 *				- void InitializeMatrix(Matrix& A, unsigned int size1, unsigned int size2)
 *				- void DeinitializeMatrix(Matrix& A)
 *				- void InitializeVector(Vector& A, unsigned int size)
 *				- void DeinitializeVector(Vector& A)
 *				- void InitializeTridiagonal(Tridiagonal& Tridiagonal_matrix, unsigned int size, std::vector<double> Alphas,std::vector<double> Betas)
 *				- void DeinitializeTridiagonal(Tridiagonal& A)
 *		(*) Object Handling:
 *				- void PrintMatrix(const Matrix& A)
 *				- void PrintVector(const Vector& a)
 *				- void SetMatrix(Matrix& A, unsigned int i, unsigned int j, double value)
 *				- void SetVector(Vector& A, unsigned int i, double value)
 *				- double GetMatrixE(const Matrix& A, unsigned int i, unsigned int j)
 *				- void GetMatrixC(Vector& v, const Matrix& A, unsigned int i)
 *				- void GetMatrixR(Vector& v, const Matrix& A, unsigned int j)
 *				- double GetVectorE(const Vector& A, unsigned int i)
 *				- void SolveTridiagonal(Tridiagonal& TMatrix, Vector& eigenvalues, Matrix& eigenvectors)
 *
 */

template<typename MatrixAlgebra>
class QmtRealLanczos {
public:
    typedef typename MatrixAlgebra::Matrix Matrix;
    typedef typename MatrixAlgebra::Vector Vector;
    typedef typename MatrixAlgebra::Tridiagonal Tridiagonal;
    typedef std::vector<double> dvector;

private:
    Vector NMixedState;
    Vector NMixedStatePrev;

    unsigned int problem_size;

    Vector eigenvector;
    double eigenvalue;

//    Matrix eigenvectorsLanczos;
//    Vector eigenvalues;
//    Matrix transition_matrix;
//	  add to find_eigenvector function

    unsigned int lanczos_size;

    double alpha;
    double beta;

    dvector Alphas;
    dvector Betas;

    double zero;
    unsigned int fuse;

    bool IF_Solved;
    bool IF_Eigenvector_found;

public:
    QmtRealLanczos(unsigned int problem_size) {
        Initialize(problem_size,problem_size,pow(2.0,-5));
    }

    QmtRealLanczos(unsigned int problem_size, double zero) {
        Initialize(problem_size,problem_size,zero);
    }

    QmtRealLanczos(unsigned int problem_size, unsigned int fuse, double zero) {
        Initialize(problem_size,fuse,zero);
    }

    ~QmtRealLanczos() {
        MatrixAlgebra::DeinitializeVector(NMixedState);
        MatrixAlgebra::DeinitializeVector(NMixedStatePrev);
        MatrixAlgebra::DeinitializeVector(eigenvector);

    }

private:
    void Initialize(unsigned int _problem_size, unsigned int _fuse, double _zero) {
        problem_size = _problem_size;

        MatrixAlgebra::InitializeVector(NMixedState,problem_size);
        MatrixAlgebra::InitializeVector(NMixedStatePrev,problem_size);
        MatrixAlgebra::InitializeVector(eigenvector,problem_size);

        lanczos_size=0;

        zero=_zero;
        fuse=_fuse;

        IF_Solved=false;
        IF_Eigenvector_found=false;
    }

public:
    void solve(const Matrix& problem_matrix) {
        if (problem_size > 1) {
            IF_Eigenvector_found=false;
            IF_Solved=false;

            Alphas.clear();
            Betas.clear();

            beta = 0;

            // random number generator
            std::random_device random_seed;
            std::mt19937 generator(random_seed());
            std::uniform_real_distribution<> uniform_real(-2.0/sqrt(problem_size),2.0/sqrt(problem_size));

            for(unsigned int i=0; i<problem_size; ++i) {
                MatrixAlgebra::SetVector(NMixedState,i,uniform_real(generator));
            }

            MatrixAlgebra::NormalizeV(NMixedState);


            Vector remember_NMixedStatePrev;
            MatrixAlgebra::InitializeVector(remember_NMixedStatePrev,problem_size);

            for (unsigned int iter = 0; (iter < fuse) && (iter < problem_size); iter++) {
                Betas.push_back(beta);

                MatrixAlgebra::CopyVV(NMixedStatePrev,remember_NMixedStatePrev);
                MatrixAlgebra::LinearMultiplyMV(problem_matrix,NMixedState,NMixedStatePrev,1.0,-beta);
                MatrixAlgebra::MultiplyVV(NMixedState, NMixedStatePrev, &alpha);
                Alphas.push_back(alpha);

                MatrixAlgebra::CopyVV(NMixedState, NMixedStatePrev,-alpha);

                beta = MatrixAlgebra::NormV(NMixedStatePrev);


                if (beta < zero) {
//                	Betas.push_back(beta);
                    break;
                }


                MatrixAlgebra::ScaleV(NMixedStatePrev, pow(beta, -1));

                MatrixAlgebra::SwapVV(NMixedState, NMixedStatePrev);
            } // end of main loop

            MatrixAlgebra::CopyVV(remember_NMixedStatePrev,NMixedStatePrev);

            MatrixAlgebra::DeinitializeVector(remember_NMixedStatePrev);

            lanczos_size = Alphas.size();

            diagonalization(problem_matrix);
            IF_Solved=true;

        } else
            std::cerr << "QmtRealLanczos: solve(): No Problem Matrix given!\n";
    }

    double get_minimal_eigenvalue() {
        if(IF_Solved) {
            return eigenvalue;
        }
        else {
            std::cerr<<"QmtRealLanczos: System not yet solved!"<<std::endl;
            return 0;
        }
    }

    void get_minimal_eigenvector(Vector& vec, const Matrix& problem_matrix) {
        if(IF_Solved) {
            if(!IF_Eigenvector_found) {
                diagonalization(problem_matrix,true);
                IF_Eigenvector_found=true;
            }
            MatrixAlgebra::CopyVV(eigenvector,vec);
        }
        else {
            std::cerr<<"QmtRealLanczos: System not yet solved!"<<std::endl;
        }
    }

private:
    void diagonalization(const Matrix& problem_matrix, bool IF_Eigen_Vector=false) {

        Matrix eigenvectorsLanczos; // matrix of eigenvectors of tridiagonal matrix
        Vector eigenvalues; // vector of eigenvalues of tridiagonal matrix

        Tridiagonal Tridiagonal_matrix; // tridiagonal matrix

        Matrix transition_matrix; // transition matrix
        Vector LanczosVector; // minimal eigenvector of tridiagonal matrix

        // inicialization
        MatrixAlgebra::InitializeMatrix(eigenvectorsLanczos,lanczos_size,lanczos_size);
        MatrixAlgebra::InitializeVector(eigenvalues,lanczos_size);
        if(IF_Eigen_Vector)		MatrixAlgebra::InitializeVector(LanczosVector,lanczos_size);
        else				 	MatrixAlgebra::InitializeVector(LanczosVector,2);
        MatrixAlgebra::InitializeTridiagonal(Tridiagonal_matrix,lanczos_size,Alphas,Betas);

        // eigenvalue problem for tridiagonal matrix
        MatrixAlgebra::SolveTridiagonal(Tridiagonal_matrix,eigenvalues,eigenvectorsLanczos);
        // taking the minimal eigenvalue and eigenvector of tridiagonal matrix
        eigenvalue = MatrixAlgebra::GetVectorE(eigenvalues,0);
        if(IF_Eigen_Vector) MatrixAlgebra::GetMatrixC(LanczosVector,eigenvectorsLanczos,0);

        if(IF_Eigen_Vector) {
            // calculating transition matrix between tridiagonal matrix and given problem matrix
            MatrixAlgebra::InitializeMatrix(transition_matrix, problem_size, lanczos_size);
            find_transition_matrix(transition_matrix,problem_matrix);

            // calculating ACTUAL eigenvector
            MatrixAlgebra::MultiplyMV(transition_matrix,LanczosVector,eigenvector);

            MatrixAlgebra::NormalizeV(eigenvector);
        }

        // cleaning workspace
        if(IF_Eigen_Vector) MatrixAlgebra::DeinitializeMatrix(transition_matrix);
        MatrixAlgebra::DeinitializeVector(LanczosVector);
        MatrixAlgebra::DeinitializeMatrix(eigenvectorsLanczos);
        MatrixAlgebra::DeinitializeVector(eigenvalues);
        MatrixAlgebra::DeinitializeTridiagonal(Tridiagonal_matrix);

    }

    void find_transition_matrix(Matrix& transition_matrix, const Matrix& problem_matrix) {  // to reproduce the eigenvectors
        if (lanczos_size > 0) {

            // first vector is the last vector, the second is the second last
            for (unsigned int i = 0; i < problem_size; i++) {
                MatrixAlgebra::SetMatrix(transition_matrix, i, lanczos_size - 1,
                                         MatrixAlgebra::GetVectorE(NMixedState, i));
                if (lanczos_size > 1)
                    MatrixAlgebra::SetMatrix(transition_matrix, i, lanczos_size - 2,
                                             MatrixAlgebra::GetVectorE(NMixedStatePrev, i));
            }

            for (int iter = lanczos_size - 3; iter >= 0; iter--) {

                MatrixAlgebra::LinearMultiplyMV(problem_matrix,NMixedStatePrev,NMixedState,1.0,-Betas[iter+2]);

                MatrixAlgebra::CopyVV(NMixedStatePrev, NMixedState,-Alphas[iter + 1]);

                MatrixAlgebra::ScaleV(NMixedState, pow(Betas[iter + 1], -1));

                for (unsigned int i = 0; i < problem_size; i++) {
                    MatrixAlgebra::SetMatrix(transition_matrix, i, iter,
                                             MatrixAlgebra::GetVectorE(NMixedState, i));
                }

                MatrixAlgebra::SwapVV(NMixedState, NMixedStatePrev);
            } // end of main loop

        }
    }
};

}//end of namespace qmt

#endif
