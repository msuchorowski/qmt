#ifndef QMTGSLWRAPPERS_H
#define QMTGSLWRAPPERS_H

#include <functional>
#include <gsl/gsl_min.h>
namespace qmt {

class gsl_function_pp : public gsl_function
{
private:
   std::function<double(double)> _func;
   static double invoke(double x, void *params) {
   return static_cast<gsl_function_pp*>(params)->_func(x);
   }   

public:
   gsl_function_pp(std::function<double(double)> const& func) : _func(func){
   function=&gsl_function_pp::invoke;
   params=this;
   }     
   
};


}



#endif

