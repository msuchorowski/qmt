   

#include "qmtsystem.h"
#include "qmtwanniercreator.h"
#include "qmtslaterorbitals.h"
#include "qmtmicrotrans.h"
#include "qmthubbard.h"
   
   typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 
   typedef  qmt::QmtMicroscopicTransInvariant<qmt::QmtWannierSlaterBasedCreator, qmt_hamiltonian> qmt_standard_system;
   
   int main () {
     
     qmt_standard_system system("fake", "fake", 
		       "fake", "fake",std::vector<double>());
     
    return 0; 
   }