#ifndef QMTLANCZOS_H
#define QMTLANCZOS_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_eigen.h>
#include "qmvars.h"

#include <limits>

namespace qmt {

typedef std::vector<double> dvector;

template<class Matrix, class Vector, QmVariables::QmParallel Parallel =
QmVariables::QmParallel::no>
class QmtLanczosSolver {
    QmtLanczosSolver() {
    }
};

template<>
class QmtLanczosSolver<gsl_matrix, gsl_vector> {
private:
    void _QmtLanczosSolver(const gsl_matrix *_problem, bool IFMATRIXGIVEN) {
        problem_matrix = NULL;
        NMixedState = NULL;
        NMixedStatePrev = NULL;

        PROBLEM_MATRIX = false;
        NMIXEDSTATE = false;
        NMIXEDSTATEPREV = false;

        problem_size = 0;

        if (IFMATRIXGIVEN) {
            problem_size = _problem->size1;

            problem_matrix = gsl_matrix_alloc(problem_size, problem_size);
            gsl_matrix_memcpy(problem_matrix, _problem);

            NMixedState = gsl_vector_alloc(problem_size);
            NMixedStatePrev = gsl_vector_alloc(problem_size);

            PROBLEM_MATRIX = true;
            NMIXEDSTATE = true;
            NMIXEDSTATEPREV = true;

            generate_first_vector();
        }

        eigenvectors = NULL;
        eigenvectorsLanczos = NULL;
        eigenvalues = NULL;
        RandomGenerator = NULL;
        transition_matrix = NULL;

        EIGENVECTORS = false;
        EIGENVECTORSLANCZOS = false;
        EIGENVALUES = false;
        TRANSITION_MATRIX = false;

        lanczos_size = 0;
        alpha = 0;
        beta = 0;


        // values of zero, and fuse can be changed via public functions the default values are
        zero = 0.000125; // arbitral value
        fuse = std::numeric_limits<int>::max(); // maximal integer value

    }

public:
    QmtLanczosSolver(const gsl_matrix *_problem) {
        if (_problem->size1 == _problem->size2) {

            _QmtLanczosSolver(_problem, true);
        } else {
            std::cerr << "QmtLanczosSolver: Not a square matrix\n";

            _QmtLanczosSolver(NULL, false);
        }
    }
    QmtLanczosSolver() {
        _QmtLanczosSolver(NULL, false);
    }

    ~QmtLanczosSolver() {
        if (PROBLEM_MATRIX)
            gsl_matrix_free(problem_matrix);
        if (EIGENVECTORS)
            gsl_matrix_free(eigenvectors);
        if (EIGENVECTORSLANCZOS)
            gsl_matrix_free(eigenvectorsLanczos);
        if (NMIXEDSTATE)
            gsl_vector_free(NMixedState);
        if (NMIXEDSTATEPREV)
            gsl_vector_free(NMixedStatePrev);
        if (EIGENVALUES)
            gsl_vector_free(eigenvalues);
        if (TRANSITION_MATRIX)
            gsl_matrix_free(transition_matrix);

        betas.clear();
        alphas.clear();
    }

    void setProblemMatrix(const gsl_matrix* _problem) {
        if (PROBLEM_MATRIX && problem_matrix->size1 == _problem->size1) {

            if (EIGENVECTORS)
                gsl_matrix_free(eigenvectors);
            if (EIGENVECTORSLANCZOS)
                gsl_matrix_free(eigenvectorsLanczos);
            if (EIGENVALUES)
                gsl_vector_free(eigenvalues);
            if (TRANSITION_MATRIX)
                gsl_matrix_free(transition_matrix);

            EIGENVECTORS = false;
            EIGENVECTORSLANCZOS = false;
            EIGENVALUES = false;
            TRANSITION_MATRIX = false;

            betas.clear();
            alphas.clear();
            lanczos_size = 0;

            gsl_matrix_memcpy(problem_matrix, _problem);

            generate_first_vector();

        } else {
            if (PROBLEM_MATRIX)
                gsl_matrix_free(problem_matrix);

            if (_problem->size1 == _problem->size2) {
                _QmtLanczosSolver(_problem, true);
            } else {
                std::cerr << "QmtLanczosSolver: Not a square matrix\n";

                _QmtLanczosSolver(NULL, false);
            }
        }
    }

    void set_zero_tolerance(double _zero) {
        zero = fabs(_zero);
    }

    void run() {
        if (problem_size > 0) {
            beta = 0;
            betas.push_back(beta);

            gsl_vector *remember_NMixedStatePrev = gsl_vector_alloc(
                    problem_size);


            for (unsigned int iter = 0; (iter < fuse) && (iter < problem_size); iter++) {
                gsl_vector_memcpy(remember_NMixedStatePrev, NMixedStatePrev);

                gsl_blas_dgemv(CblasNoTrans, 1.0, problem_matrix, NMixedState,
                               -beta, NMixedStatePrev);

                gsl_blas_ddot(NMixedState, NMixedStatePrev, &alpha);
                alphas.push_back(alpha);

                gsl_blas_daxpy(-alpha, NMixedState, NMixedStatePrev);

                beta = gsl_blas_dnrm2(NMixedStatePrev);


                if (beta < zero) {
                    gsl_vector_memcpy(NMixedStatePrev,
                                      remember_NMixedStatePrev);
                    break;
                }

                betas.push_back(beta);

                gsl_vector_scale(NMixedStatePrev, pow(beta, -1));

                gsl_blas_dswap(NMixedState, NMixedStatePrev);
            } // end of main loop

            gsl_vector_free(remember_NMixedStatePrev);
            lanczos_size = alphas.size();
            diagonalization();
        } else
            std::cerr << "QmtLanczosSolver: No Problem Matrix given!\n";
    }
    void run(const int safety) {
        fuse = safety;
        run();
    }
    void run(const double _zero) {
        zero = fabs(_zero);
        run();
    }
    void run(const double _zero, const int safety) {
        zero = fabs(_zero);
        fuse = safety;
        run();
    }

    double get_minimal_eigenvalue() { // if Lanczos algorithm not performed return infinity
        if (EIGENVALUES) {
            return gsl_vector_get(eigenvalues, 0);
        }
        return std::numeric_limits<double>::infinity();
    }

    void print_eigenvalues() {
        if (EIGENVALUES) {
            std::cout << "Eigenvalues:\n";
            for (unsigned int i = 0; i < lanczos_size; i++)
                std::cout << gsl_vector_get(eigenvalues, i) << " ";

            std::cout << "\n";
        }
    }

    void print_eigenvectors() {
        if (lanczos_size > 0) {
            if (!EIGENVECTORS)
                find_eigenvectors();

            double test=0;

            std::cout << "Eigenvalues with Eigenvectors:\n";
            for (unsigned int i = 0; i < lanczos_size; i++) {
                std::cout << gsl_vector_get(eigenvalues, i) << ":\n( ";

                for (unsigned int j = 0; j < problem_size; j++) {
                    test = gsl_matrix_get(eigenvectors, j, i);
                    if(test<1e-15) test = 0.0;
                    std::cout << test << ", ";
                }
                std::cout << ")\n";
            }
            std::cout << "\n";
        }
    }

private:
    void diagonalization() {
        if (lanczos_size > 0) {
            if (EIGENVECTORSLANCZOS)
                gsl_matrix_free(eigenvectorsLanczos);
            if (EIGENVALUES)
                gsl_vector_free(eigenvalues);

            eigenvectorsLanczos = gsl_matrix_alloc(lanczos_size, lanczos_size);
            EIGENVECTORSLANCZOS = true;
            eigenvalues = gsl_vector_alloc(lanczos_size);
            EIGENVALUES = true;

            gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(
                    lanczos_size);
            gsl_matrix* tridiagonal = gsl_matrix_alloc(lanczos_size,
                                      lanczos_size);

            gsl_matrix_set_zero(tridiagonal);

            for (unsigned int i = 0; i < lanczos_size; i++) {
                gsl_matrix_set(tridiagonal, i, i, alphas[i]);
                if (i != 0) {
                    gsl_matrix_set(tridiagonal, i - 1, i, betas[i]);
                    gsl_matrix_set(tridiagonal, i, i - 1, betas[i]);
                }
            }

            gsl_eigen_symmv(tridiagonal, eigenvalues, eigenvectorsLanczos,
                            workspace);

            gsl_eigen_symmv_free(workspace);

            //Sorting
            gsl_eigen_symmv_sort(eigenvalues, eigenvectorsLanczos,
                                 GSL_EIGEN_SORT_VAL_ASC);

            gsl_matrix_free(tridiagonal);
        }
    }

    void generate_first_vector() {
        if (problem_size > 0) {
            // generating random first vector
            int number_of_non_zero = problem_size < 1000 ? problem_size : 1000;

            gsl_rng * RandomGenerator;

            gsl_rng_env_setup();

            RandomGenerator = gsl_rng_alloc(gsl_rng_default);
            gsl_vector_set_zero(NMixedState);

            for (int i = 0; i < number_of_non_zero; i++)
                gsl_vector_set(NMixedState, i,
                               gsl_ran_flat(RandomGenerator, -1.0, 1.0));

            beta = gsl_blas_dnrm2(NMixedState);
            if (beta != 0)
                gsl_vector_scale(NMixedState, pow(beta, -1));
            else
                gsl_vector_set_basis(NMixedState, 1);

            gsl_rng_free(RandomGenerator);
        }
    }

    void reverse_run() {  // to reproduce the eigenvectors
        if (lanczos_size > 0) {
            if (TRANSITION_MATRIX)
                gsl_matrix_free(transition_matrix);

            TRANSITION_MATRIX = true;
            transition_matrix = gsl_matrix_alloc(problem_size, lanczos_size);

            // first vector is the last vector, the second is the second last
            for (unsigned int i = 0; i < problem_size; i++) {
                gsl_matrix_set(transition_matrix, i, lanczos_size - 1,
                               gsl_vector_get(NMixedState, i));
                if(lanczos_size>1)
                    gsl_matrix_set(transition_matrix, i, lanczos_size - 2,
                                   gsl_vector_get(NMixedStatePrev, i));
            }

            for (int iter = lanczos_size - 3; iter >= 0; iter--) {

                gsl_blas_dgemv(CblasNoTrans, 1.0, problem_matrix,
                               NMixedStatePrev, -betas[iter + 2], NMixedState);
                gsl_blas_daxpy(-alphas[iter + 1], NMixedStatePrev, NMixedState);
                gsl_vector_scale(NMixedState, pow(betas[iter + 1], -1));

                for (unsigned int i = 0; i < problem_size; i++) {
                    gsl_matrix_set(transition_matrix, i, iter,
                                   gsl_vector_get(NMixedState, i));
                }

                gsl_blas_dswap(NMixedState, NMixedStatePrev);
            } // end of main loop

        }
    }

    void find_eigenvectors() {
        if (lanczos_size > 0) {
            reverse_run();

            if (EIGENVECTORS)
                gsl_matrix_free(eigenvectors);

            eigenvectors = gsl_matrix_alloc(problem_size, lanczos_size);
            EIGENVECTORS = true;

            gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, transition_matrix,
                           eigenvectorsLanczos, 0.0, eigenvectors); //Finding eigenvectors


        }

    }

private:
    gsl_matrix* problem_matrix;
    bool PROBLEM_MATRIX;
    gsl_matrix* eigenvectors;
    bool EIGENVECTORS;
    gsl_matrix* eigenvectorsLanczos;
    bool EIGENVECTORSLANCZOS;
    gsl_matrix* transition_matrix;
    bool TRANSITION_MATRIX;

    gsl_vector* NMixedState;
    bool NMIXEDSTATE;
    gsl_vector* NMixedStatePrev;
    bool NMIXEDSTATEPREV;
    gsl_vector* eigenvalues;
    bool EIGENVALUES;

    gsl_rng * RandomGenerator;

    double alpha;
    dvector alphas;

    double beta;
    dvector betas;

    unsigned int problem_size;
    unsigned int lanczos_size;

    unsigned int fuse;

    double zero;

public:
    QmtLanczosSolver (QmtLanczosSolver &QLS) = delete;
    QmtLanczosSolver& operator =(const QmtLanczosSolver in) = delete;

};
//end of class QmtLanczosSolver

}//end of namespace qmt

#endif
