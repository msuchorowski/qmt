#ifndef QMT_PARSER_TOOLS_H_INCLUDED
#define QMT_PARSER_TOOLS_H_INCLUDED

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdexcept> // std::logic_error()
#include <cctype> // isspace()
#include "qmtorbtypes.h"
#include "qmtvector.h"


namespace qmt {
namespace parser {
std::vector<std::string> get_delimited_words(const std::string& delimiters, const std::string& phrase);
std::vector<std::string> get_bracketed_words(const std::string& input, const char& open, const char& close, bool CLEAR_WHITE_SPACES = false);
std::vector<std::string> get_bracketed_words(const std::string& input, const std::string& open, const std::string& close);
std::string orbitalTypeToString(qmt::OrbitalType orbital);
qmt::OrbitalType stringToOrbitalType(const std::string& orbital);
std::vector<int> get_atomic_numbers(const std::string& file_name);
qmt::QmtVector get_QmtVector_from_line(const std::string& line);
std::vector<qmt::QmtVector> get_QmtVector_from_file(const std::string& file_name);
}
}
#endif //QMT_PARSER_TOOLS_H_INCLUDED
