#ifndef QMT_LNC_DIAG_H
#define QMT_LNC_DIAG_H

//#include <mpi.h>
#include <iostream>
#include "qmtdiagonalizer.h"
#include "qmthubbard.h"
#include "qmthbuilder.h"
#include "qmtbasisgenerator.h"
#include "MatrixAlgebras/SparseAlgebra.h"
#include "qmtmatrixformula.h"
#include "qmtreallanczos.h"
#include "qmtparsertools.h"



namespace qmt {
  
 class QmtLanczosDiagonalizer : public QmtDiagonalizer {
    typedef  qmt::QmtHubbard<qmt::SqOperator<qmt::QmtNState<qmt::uint, int> >> qmt_hamiltonian; 
    typedef qmt::QmtNState<qmt::uint, int> NState;                                             //Output - eigen state
    typedef qmt::SqOperator<NState> sq_operator;
    
    qmt_hamiltonian *hamiltonian;                                                       //Hamiltonian
    std::vector<NState> states;				       //Basis states in Fock space
    qmt:: QmtBasisGenerator generator;                                                         //Basis states generator
    qmt::QmtGeneralMatrixFormula<qmt_hamiltonian,LocalAlgebras::SparseAlgebra> formula; //Tranlate Hamiltonian to matrix 
                                                                              
    LocalAlgebras::SparseAlgebra::Matrix hamiltonian_M;                              //Hamiltonian Matrix
    LocalAlgebras::SparseAlgebra::Vector eigenVector;                                //Eigen vector (ground state) of matrix
    qmt::QmtRealLanczos<LocalAlgebras::SparseAlgebra> *LanczosSolver;                //Eigen problem solver
    
    int _lanczos_steps;
    int _lanczos_eps;
    
    int problem_size;
    int number_of_centers;
    int electron_number;
    int alphas_number;
 
  public:
  QmtLanczosDiagonalizer(const char* file_name,int option = 1);
  double Diagonalize(const std::vector<double>& one_body_integrals, const std::vector<double>& two_body_integrals,const std::vector<double>& params1=std::vector<double>(0),const std::vector<double>& params2=std::vector<double>(0),const std::vector<double>& params3=std::vector<double>(0));
  void NWaveFunctionEssentials(std::vector<double>& averages);
  double DoubleOccupancy();
  void DoubleOccupancy(std::vector<double>& v);
  double DimerDimerAvs(size_t i, size_t j);
  virtual ~QmtLanczosDiagonalizer();
}; 
  
  
}

#endif
