#ifndef QMT_DIAGONALIZER_H
#define QMT_DIAGONALIZER_H

#include <vector>

namespace qmt {

struct QmtDiagonalizer {
  /*
   * It must provide ONLY lowest eigen value
   */
  
  virtual double Diagonalize(const std::vector<double>& one_body_integrals, const std::vector<double>& two_body_integrals,const std::vector<double>& params1=std::vector<double>(0),const std::vector<double>& params2=std::vector<double>(0),const std::vector<double>& params3=std::vector<double>(0)) = 0;
  virtual ~QmtDiagonalizer() {};
};


}
#endif