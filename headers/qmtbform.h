#ifndef QMTBFORM_H_INCLUDED
#define QMTBFORM_H_INCLUDED

class BilinearForm {
    int n;
    double _c;
    gsl_matrix* _m;

public:

    BilinearForm(double** matrix, int size, double c=0):n(size), _c(c) {
        _m = gsl_matrix_alloc (n, n);
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                gsl_matrix_set(_m,i,j, matrix[i][j]);
            }
        }
    }



    BilinearForm(const gsl_matrix* matrix, double c=0):n(matrix->size1), _c(c) {
        _m = gsl_matrix_alloc (n, n);
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                gsl_matrix_set(_m,i,j, gsl_matrix_get(matrix,i,j));
            }
        }
    }

    BilinearForm(const BilinearForm& form):n(form.n),_c(form._c) {
        _m = gsl_matrix_alloc (n, n);
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                gsl_matrix_set(_m,i,j, gsl_matrix_get(form._m,i,j));
            }
        }
    }


    BilinearForm& operator=(const BilinearForm& form) {
        n = form.n;
        _c = form._c;
        _m = gsl_matrix_alloc (n, n);
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                gsl_matrix_set(_m,i,j, gsl_matrix_get(form._m,i,j));
            }
        }
        return *this;
    }

    double get_value(const double *m) const {
        double result = 0.0f;
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                result += gsl_matrix_get(_m,i,j) * m[i] * m[j];
            }
        }

        return result + _c;
    }

    double get_value(const gsl_vector *m) const {
        double result = 0.0f;
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                result += gsl_matrix_get(_m,i,j) * gsl_vector_get(m,i) * gsl_vector_get(m,j);
            }
        }

        return result + _c;
    }


    ~BilinearForm() {
        gsl_matrix_free(_m);
    }


};


#endif // QMTBFORM_H_INCLUDED
