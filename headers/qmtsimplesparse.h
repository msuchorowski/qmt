#include <vector>
#include <iostream>
#include <functional>
#include <algorithm>
#include <tuple>
#include <thread>         // std::thread
#include <mutex>          // std::mutex



namespace qmt {
/*
n1,n2 : zero entries in M1 and M2 respecitvely
Options:
0: general: M1+ alpha * M2  -> O(N^2)
1: row sorted:  M1+ alpha * M2 -> O(n1 + n2)
*/
template <typename T, unsigned int Option = 0>
class QmtSparseMatrix {

    typedef unsigned int uint;

    std::vector<T> _values;
    std::vector<uint> _columns;
    std::vector<uint> _rows;

    std::mutex mtx;

    uint nrows;
    uint ncols;

    uint get_row(uint i) const {
        return _rows[i];
    }
    uint get_column(uint i) const {
        return _columns[i];
    }

public:



    QmtSparseMatrix(uint rows, uint cols): nrows(rows), ncols(cols) { }
    ~QmtSparseMatrix() { }

    void Reset() {
        _values.clear();
        _columns.clear();
        _rows.clear();
    }

    void set_value(T value, uint i, uint j ) {
        mtx.lock();
        if(i  < nrows && j < ncols) {

            _values.push_back(value);
            _rows.push_back(i);
            _columns.push_back(j);

        }
        mtx.unlock();
    }


    void modify_value(T value, uint i, uint j, std::function<T(T,T)> func) {
        mtx.lock();
        if(i  < nrows && j < ncols) {

            for(uint k = 0; k < _values.size(); ++k)  {
                if( i == _rows[k] && j== _columns[k]) {
                    _values[k] = func(_values[k],value);
                    mtx.unlock();
                    return;
                }
            }

            _values.push_back(func(T(),value));
            _rows.push_back(i);
            _columns.push_back(j);


        }
        mtx.unlock();
    }

    void modify_all_values(std::function<T(T)> func) {
        mtx.lock();
        std::transform(_values.begin(),_values.end(),_values.begin(),func);
        mtx.unlock();
    }



    T get_value(uint i, uint j)const {
        if(i  < nrows && j < ncols) {
            uint cntr = 0;
            while(cntr < _values.size()) {
                if(_rows[cntr] == i && _columns[cntr] ==j) {
                    return _values[cntr] ;
                }

                cntr++;
            }

        }
        return T();
    }

//Matrix Vector

    static void Multiply(const QmtSparseMatrix<T>& A, const T* v,T* u, T alpha, T beta) {

        for( uint i = 0; i < A.nrows; ++i)  {
            u[i] *= beta;
        }

        for( uint i = 0; i < A._values.size(); ++i)  {
            u[A.get_row(i)] += alpha* A._values[i] * v[A.get_column(i)];
        }

    }

    static void Multiply(const QmtSparseMatrix<T>& A, const std::vector<T>& v,std::vector<T>& u, T alpha, T beta) {

        for( uint i = 0; i < A.nrows; ++i)  {
            u[i] = u[i] *beta;
        }

        for( uint i = 0; i < A._values.size(); ++i)  {
            u[A.get_row(i)] += alpha* A._values[i] * v[A.get_column(i)];
        }

    }


#ifdef GLS_LIBRARY
    static void Multiply(const QmtSparseMatrix<T>& A, const gsl_vector* v,gsl_vector* u, T alpha, T beta) {

        for( uint i = 0; i < A.nrows; ++i)  {
            gsl_vector_set(u,i, gsl_vector_get(u,i) * beta);
        }

        for( uint i = 0; i < A._values.size(); ++i)  {
            gsl_vector_set(u, A.get_row(i), gsl_vector_get(v,A.get_column(i)) * alpha *  A._values[i] + gsl_vector_get(u,A.get_row(i)));
        }

    }
#endif

//Matrix-Matrix multiplication

    static void Multiply(const QmtSparseMatrix<T>& A,const QmtSparseMatrix<T>& B, QmtSparseMatrix<T>& C) {
        for(auto i = 0; i < A.nrows; ++i) {
            for(auto j = 0; j < B.ncols; ++j) {
                auto v = T();
                for(auto k = 0; k < B.nrows; ++k) {
                    v += A.get_value(i,k) * B.get_value(k,j);
                }
                C.set_value(v,i,j);
            }
        }

    }


    static void Add(QmtSparseMatrix<T>& A,const QmtSparseMatrix<T>& B,T alpha) {
        if(A.nrows == B.nrows && A.ncols == B.ncols)  {
            for(uint i = 0; i <B._values.size(); ++i) {
                A.modify_value(B._values[i],B._rows[i], B._columns[i],[&](T arg1, T arg2)->T {return arg1 + arg2 * alpha;});
            }
        }

    }


    void get_id_cols(unsigned int* index) const {
        std::copy(_columns.begin(), _columns.end(),index);
    }

    void get_id_rows(unsigned int* index) const {
        std::copy(_rows.begin(), _rows.end(),index);
    }

    void get_values(T* values) const {
        std::copy(_values.begin(), _values.end(),values);
    }

    unsigned int get_size_cols() const {
        return ncols;
    }

    unsigned int get_size_rows() const {
        return nrows;
    }

    unsigned int get_size_non_zero() const {
        return _values.size();
    }


    friend std::ostream& operator<<(std::ostream& stream, const QmtSparseMatrix& matrix) {
        for(uint i = 0; i< matrix.nrows; ++i) {
            for(uint j = 0; j < matrix.ncols; ++j) {
                stream<<matrix.get_value(i,j)<<" ";
            }
            stream<<std::endl;
        }

        return stream;
    }

    void show_sparse() const {
        for(auto s = 0; s < _values.size(); ++s) {
            std::cout<<_rows[s]<<" "<<_columns[s]<<" "<<_values[s]<<std::endl;
        }
    }


};


/*
Option=1
*/

template <typename T>
class QmtSparseMatrix<T,1> {

    typedef unsigned int uint;

    std::vector<T> _values;
    std::vector<uint> _columns;
    std::vector<uint> _rows;
    std::mutex mtx;

    uint nrows;
    uint ncols;

    uint get_row(uint i) const {
        return _rows[i];
    }
    uint get_column(uint i) const {
        return _columns[i];
    }
    T get_value(uint i) const {
        return _values[i];
    }
public:



    QmtSparseMatrix(uint rows, uint cols): nrows(rows), ncols(cols) { }
    ~QmtSparseMatrix() { }

    void Reset() {
        _values.clear();
        _columns.clear();
        _rows.clear();
    }

    void set_value(T value, uint i, uint j ) {
        mtx.lock();
        if(i  < nrows && j < ncols) {
            _values.push_back(value);
            _rows.push_back(i);
            _columns.push_back(j);

        }
        mtx.unlock();
    }


    void modify_value(T value, uint i, uint j, std::function<T(T,T)> func) {
        mtx.lock();
        if(i  < nrows && j < ncols) {

            for(uint k = 0; k < _values.size(); ++k)  {
                if( i == _rows[k] && j== _columns[k]) {
                    _values[k] = func(_values[k],value);
                    mtx.unlock();
                    return;
                }
            }

            _values.push_back(func(T(),value));
            _rows.push_back(i);
            _columns.push_back(j);


        }
        mtx.unlock();
    }

    void modify_all_values(std::function<T(T)> func) {
        mtx.lock();
        std::transform(_values.begin(),_values.end(),_values.begin(),func);
        mtx.unlock();
    }



    T get_value(uint i, uint j)const {
        if(i  < nrows && j < ncols) {
            uint cntr = 0;
            while(cntr < _values.size()) {
                if(_rows[cntr] == i && _columns[cntr] ==j) {
                    return _values[cntr] ;
                }

                cntr++;
            }

        }
        return T();
    }

//Matrix Vector

    static void Multiply(const QmtSparseMatrix<T,1>& A, const T* v,T* u, T alpha, T beta) {

        for( uint i = 0; i < A.nrows; ++i)  {
            u[i] *= beta;
        }

        for( uint i = 0; i < A._values.size(); ++i)  {
            u[A.get_row(i)] += alpha* A._values[i] * v[A.get_column(i)];
        }

    }

    static void Multiply(const QmtSparseMatrix<T,1>& A, const std::vector<T>& v,std::vector<T>& u, T alpha, T beta) {

        for( uint i = 0; i < A.nrows; ++i)  {
            u[i] = u[i] *beta;
        }

        for( uint i = 0; i < A._values.size(); ++i)  {
            u[A.get_row(i)] += alpha* A._values[i] * v[A.get_column(i)];
        }

    }


#ifdef GLS_LIBRARY
    static void Multiply(const QmtSparseMatrix<T,1>& A, const gsl_vector* v,gsl_vector* u, T alpha, T beta) {

        for( uint i = 0; i < A.nrows; ++i)  {
            gsl_vector_set(u,i, gsl_vector_get(u,i) * beta);
        }

        for( uint i = 0; i < A._values.size(); ++i)  {
            gsl_vector_set(u, A.get_row(i), gsl_vector_get(v,A.get_column(i)) * alpha *  A._values[i] + gsl_vector_get(u,A.get_row(i)));
        }

    }
#endif

//Matrix-Matrix multiplication

    static void Multiply(const QmtSparseMatrix<T,1>& A,const QmtSparseMatrix<T,1>& B, QmtSparseMatrix<T,1>& C) {
        for(auto i = 0; i < A.nrows; ++i) {
            for(auto j = 0; j < B.ncols; ++j) {
                auto v = T();
                for(auto k = 0; k < B.nrows; ++k) {
                    v += A.get_value(i,k) * B.get_value(k,j);
                }
                C.set_value(v,i,j);
            }
        }

    }


    static void Add(QmtSparseMatrix<T,1>& A, QmtSparseMatrix<T,1>& B,T alpha) {
        QmtSparseMatrix<T,1> V(A.nrows, A.ncols);
        if(A.nrows == B.nrows && A.ncols == B.ncols)  {
            A.sort();
            B.sort();
            uint k1 = 0;
            uint k2 = 0;
            while( k1 < A.get_size_non_zero() && k2 < B.get_size_non_zero() ) {
                uint i1 = A.get_row(k1);
                uint j1 = A.get_column(k1);
                T v1 = A.get_value(k1);

                uint i2 = B.get_row(k2);
                uint j2 = B.get_column(k2);
                T v2 = B.get_value(k2);
                if(i1 == i2) {
                    if(j1 == j2) {
                        V.set_value(v1 + alpha * v2, i1, j1);
                        k1++;
                        k2++;
                    }
                    if(j1 < j2) {
                        V.set_value(v1,i1,j1);
                        k1++;
                    }
                    if(j1 > j2) {
                        V.set_value(v2 * alpha,i2,j2);
                        k2++;
                    }
                }
                if(i1 < i2) {
                    V.set_value(v1, i1, j1);
                    k1++;
                }
                if(i1 > i2) {
                    V.set_value(v2 * alpha, i2, j2);
                    k2++;
                }
            }

            if(k1 == A.get_size_non_zero() && k2 < B.get_size_non_zero()) {
                for(uint i = k2; i < B.get_size_non_zero(); ++i)
                    V.set_value(B.get_value(i) * alpha,B.get_row(i),B.get_column(i));
            }
            if(k1 < A.get_size_non_zero() && k2 == B.get_size_non_zero()) {
                for(uint i = k1; i < A.get_size_non_zero(); ++i)
                    V.set_value( A.get_value(i), A.get_row(i),A.get_column(i));  // NOTE: we DO NOT multiply by alpha : V = A + alpha * B
            }
            A._columns = V._columns;
            A._rows = V._rows;
            A._values = V._values;
        }

    }


    void get_id_cols(unsigned int* index) const {
        std::copy(_columns.begin(), _columns.end(),index);
    }

    void get_id_rows(unsigned int* index) const {
        std::copy(_rows.begin(), _rows.end(),index);
    }

    void get_values(T* values) const {
        std::copy(_values.begin(), _values.end(),values);
    }

    unsigned int get_size_cols() const {
        return ncols;
    }

    unsigned int get_size_rows() const {
        return nrows;
    }

    unsigned int get_size_non_zero() const {
        return _values.size();
    }


    friend std::ostream& operator<<(std::ostream& stream, const QmtSparseMatrix& matrix) {
        for(uint i = 0; i< matrix.nrows; ++i) {
            for(uint j = 0; j < matrix.ncols; ++j) {
                stream<<matrix.get_value(i,j)<<" ";
            }
            stream<<std::endl;
        }

        return stream;
    }


    void show_for_plot() const {
        for(uint i = 0; i< nrows; ++i) {
            for(uint j = 0; j < ncols; ++j) {
                if(fabs(get_value(i,j)) > 1.0e-6 )
                    std::cout<<i<<" "<<j<<" "<<1<<" "<<std::endl;
                else
                    std::cout<<i<<" "<<j<<" "<<0<<" "<<std::endl;
            }
            std::cout<<std::endl;
        }

    }


    void show_sparse() const {
        for(auto s = 0; s < _values.size(); ++s) {
            std::cout<<_rows[s]<<" "<<_columns[s]<<" "<<_values[s]<<std::endl;
        }
    }

    void sort() {
        typedef std::tuple<T, uint, uint> Mtype;
        std::vector<Mtype> tupled_matrix;

        for(int i=0; i<_values.size(); ++i)
            tupled_matrix.push_back(std::make_tuple(_values[i],_rows[i],_columns[i]));

        auto comparer = [](const Mtype& first, const Mtype& second)->bool {
            if(std::get<1>(first) < std::get<1>(second)) return true;
            if(std::get<1>(first) == std::get<1>(second)) {
                if(std::get<2>(first) < std::get<2>(second)) return true;
                return false;
            }
            return false;
        };

        std::sort(tupled_matrix.begin(), tupled_matrix.end(),comparer);
        for(int i = 0; i < _values.size(); ++i) {
            _values[i] = std::get<0>(tupled_matrix[i]);
            _rows[i] = std::get<1>(tupled_matrix[i]);
            _columns[i] = std::get<2>(tupled_matrix[i]);
        }
    }

};



}
