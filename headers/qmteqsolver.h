#ifndef QMTEQSOLVER_H_INCLUDED
#define QMTEQSOLVER_H_INCLUDED

#include <iostream>
#include <vector>
#include <algorithm>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_matrix.h>
//#include "../qmtgenopt/headers/qmtgenopt.h"




template<typename Form>
int f_ort_set (const gsl_vector * x, void *params,
               gsl_vector * f)
{

    std::vector<Form> *forms = (std::vector<Form> *)(params);

    int n = forms->size();

    for(int i = 0; i < n; ++i) {
        gsl_vector_set(f,i, (*forms)[i].get_value(x) );
    }

    return GSL_SUCCESS;
}

/*--------------------------------------------------------------------------*/
/*
template<typename Form>
double F(std::vector<double> args) {
  double 
}
*/

/*--------------------------------------------------------------------------*/
/*
void print_state (size_t iter, gsl_multiroot_fsolver * s,int N)
{


    std::cout<<"iter ="<<iter<<" ";

    for(int i = 0; i < N; ++i) {
        std::cout<<"x_"<<i<<" "<<gsl_vector_get(s->x,i)<<" ";
    }

    std::cout<<"  ";

    for(int i = 0; i < N; ++i) {
        std::cout<<"f_"<<i<<"({x}) = "<<gsl_vector_get(s->f,i)<<" ";
    }

    std::cout<<std::endl;

}
*/
/*--------------------------------------------------------------------------*/


template<typename Form>
int NonLinearSystemSolve(std::vector<Form>& forms, double* x_init, double* out, double eps = 1.0e-6, unsigned int max = 100000)
{

    size_t N = forms.size();
    
//     auto F = [&](std::vector<double> args)->double {
//        double result = 0.0;
//        int cntr = 0;
//       gsl_vector * v = gsl_vector_alloc (args.size());
//        
//        for(int i = 0; i < args.size();++i)
//         gsl_vector_set(v,i,args[i]);
// 	 
//       for(auto &item : forms){
// 	result+= item.get_value(v);
// 	cntr++;
//       }
//       return result;
//     };
//     
//     
//     qmt::QmtGenOpt<64> opt(500U,forms.size(), std::function<double(std::vector<double>)>(F));
//     auto result = opt.Run();
//     std::cout<<"Generic Result: ";
//     for(auto &item : result)
//       std::cout<<item<<" ";
//     std::cout<<std::endl;
    const gsl_multiroot_fsolver_type *T;
    gsl_multiroot_fsolver *s;

    int status;
    size_t iter = 0;

    gsl_multiroot_function f = {&f_ort_set<Form>, N, &forms};

    gsl_vector *x = gsl_vector_alloc (N);

    for(unsigned int i = 0; i < N; ++i) {
//        gsl_vector_set(x,i, x_init[i]/fabs(x_init[i])*sqrt(exp(-1.5*fabs(x_init[i]))*(1+1.5*fabs(x_init[i])+1.5*1.5*x_init[i]*x_init[i])));
	gsl_vector_set(x,i, x_init[i]);
    }

/*    bool RUN_ME=true;
    
    while(RUN_ME){
*/
    T = gsl_multiroot_fsolver_hybrids;
    s = gsl_multiroot_fsolver_alloc (T, N);
    gsl_multiroot_fsolver_set (s, &f, x);

#ifdef QMT_VERBOSE
    print_state (iter, s, N);
#endif

    do
    {
        iter++;
        status = gsl_multiroot_fsolver_iterate (s);

#ifdef QMT_VERBOSE
        print_state (iter, s, N);
#endif

        if (status)   /* check if solver is stuck */
            break;

        status =
            gsl_multiroot_test_residual (s->f, eps*1e-2);
    }
    while (status == GSL_CONTINUE && iter < max);

    for(unsigned int i = 0; i  < N; ++i)
        out[i] = gsl_vector_get(s->x,i);
        
/*
    double tmp=out[0];
    RUN_ME=false;
    for(unsigned int i = 1; i  < N; ++i) {
	if(fabs(out[i])>fabs(tmp))
	    RUN_ME=true;
	tmp=out[i];
    }

    std::vector<double> new_set_of_betas;
    for(unsigned int i = 0; i < N; ++i) {
	new_set_of_betas.push_back(out[i]);
    }
    
    struct myclass {
  bool operator() (double i, double j) { return fabs(i) > fabs(j); }
  } wayToSortBeta;

    std::sort(new_set_of_betas.begin(),new_set_of_betas.end(),wayToSortBeta);
    
    for(unsigned int i = 0; i < N; ++i) {
        gsl_vector_set(x,i, 0.5*new_set_of_betas[i]);
    }
    iter=0;
*/    
    gsl_multiroot_fsolver_free (s);

    
//    }
        gsl_vector_free (x);
    return status;
}

#endif // QMTEQSOLVER_H_INCLUDED
