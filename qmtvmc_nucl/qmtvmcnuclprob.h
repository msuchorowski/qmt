#ifndef QMT_VMC_NUCL_PROBABILITY
#define QMT_VMC_NUCL_PROBABILITY
namespace qmt{
namespace vmc_nucl{

class QmtVmcNuclProbability {

public:

 virtual double get_probability(size_t ion_index, qmt::QmtVector& displacement,QmtVmcNuclWf& wave_func);
  
};


 }
}

#endif
