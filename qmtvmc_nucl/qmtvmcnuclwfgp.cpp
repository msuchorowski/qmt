#include "qmtvmcnuclwfgp.h"


void qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::invertMatrix(gsl_matrix *M) {


gsl_permutation * p = gsl_permutation_alloc (r.size());
gsl_matrix *m_temp = gsl_matrix_alloc (r.size(), r.size());

for(size_t i = 0U;i < r.size();i++) {
 for(size_t j = 0U;j < r.size();j++)
  gsl_matrix_set(m_temp,i,j,gsl_matrix_get(M,i,j));
}

int s;

 gsl_linalg_LU_decomp (m_temp, p, &s);
 gsl_linalg_LU_invert (m_temp,p,M);
 gsl_matrix_free(m_temp);

}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::updateMatrix(gsl_matrix *M,size_t column,std::vector<double> values){

std::vector<double> jota;

double beta = 0;


for(size_t i = 0U;i < r.size();i++) {
 for(size_t j = 0U;j < r.size();j++){
  beta += values[j]*gsl_matrix_get(M,j,i);
 }
 jota.push_back(beta);
}

for(size_t alfa = 0U;alfa < r.size();alfa++) {
 for(size_t j = 0U;j < r.size();j++){
   if(column!=j){
    double U_alfa_j = gsl_matrix_get(M,alfa,j);
    double U_alfa_l = gsl_matrix_get(M,alfa,column);
    gsl_matrix_set(M,alfa,j,U_alfa_j - U_alfa_l * jota[j]/jota[column]);
   }
 }
}


}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::QmtVmcNuclWfGaussianSlater(const std::vector<qmt::QmtVector>& positions,
									const std::vector<double>& masses,
									const  std::vector<double>& params){
  r = positions;
  R = positions;
  m = masses;
  gamma = params;



  slater_m = gsl_matrix_alloc (m.size(), m.size());
 

  for(size_t i = 0U; i < R.size(); ++i)
   gaussians.push_back(Gauss(R[i].get_x(), R[i].get_y(), R[i].get_z(),gamma[i]));


  for(size_t i =0U;i < masses.size();i++){
      for(size_t j =0U;j < masses.size();j++){
      gsl_matrix_set(slater_m,i,j,Gauss::get_value(gaussians[i],r[j]));
    }
  }

invertMatrix(slater_m);

neigbors.resize(masses.size());

for(size_t i =0U;i < masses.size();i++){
      for(size_t j =0U;j < masses.size();j++){
       if(j != i){
       neigbors[i].ids.push_back(j);
       }
    }
  }


}

/*
qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater(const std::vector<qmt::QmtVector>& positions,
   					  const std::vector<double>& masses,
					  const  std::vector<double>& params, qmt::QmtVector& radius):QmtVmcNuclWfGaussianSlater(positions,masses,params){



}
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

size_t qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_size() const {
 return m.size();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_R(qmt::QmtVector& v,size_t id) const {
v.set_x(R[id].get_x());
v.set_y(R[id].get_y());
v.set_z(R[id].get_z());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_r(qmt::QmtVector& v,size_t id) const {
v.set_x(r[id].get_x());
v.set_y(r[id].get_y());
v.set_z(r[id].get_z());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_m(size_t id) const {
return m[id];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_param(size_t id) const {
return gamma[id];
}

size_t qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_params_sectors() const {
 return 1U;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_value() const {

double product = 1.0;

 for(size_t i = 0U; i < r.size(); i++){
   product*=Gauss::get_value(gaussians[i],r[i]);
 }
return product;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::set_R(const qmt::QmtVector& v,size_t id){
R[id]=v;
gaussians[id].set_r(v);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::set_r(const qmt::QmtVector& v,size_t id){
r[id]=v;
std::vector<double> values;
for(size_t i = 0U;i < gaussians.size(); ++i)
  values.push_back(Gauss::get_value(gaussians[i],r[id]));

updateMatrix(slater_m,id,values);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::set_m(double mass,size_t id){
 m[id]=mass;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::set_param(double p,size_t id){
gamma[id] = p;
gaussians[id].set_gamma(p);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_kinetic_energy() {
double result = 0.0;

for(size_t i = 0U; i < r.size(); i++){
   for(size_t j = 0U; j < m.size(); j++){

 result+= gamma[j]*gamma[j] *(1.0 - 2*gamma[j]*gamma[j]*((r[i].get_x() - R[j].get_x())*(r[i].get_x() - R[j].get_x())))*Gauss::get_value(gaussians[j],r[i])*gsl_matrix_get(slater_m,j,i)/m[j];
 result+= gamma[j]*gamma[j] *(1.0 - 2*gamma[j]*gamma[j]*((r[i].get_y() - R[j].get_y())*(r[i].get_y() - R[j].get_y())))*Gauss::get_value(gaussians[j],r[i])*gsl_matrix_get(slater_m,j,i)/m[j];
 result+= gamma[j]*gamma[j] *(1.0 - 2*gamma[j]*gamma[j]*((r[i].get_z() - R[j].get_z())*(r[i].get_z() - R[j].get_z())))*Gauss::get_value(gaussians[j],r[i])*gsl_matrix_get(slater_m,j,i)/m[j];
  }
}

return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_interaction_energy(){
double result = 0;

for(size_t i = 0U; i < neigbors.size();++i){
  for(size_t j = 0U; j < neigbors[i].ids.size();++j){
     result +=2/(qmt::QmtVector::norm(r[i] - r[neigbors[i].ids[j]])); 
    // std::cout<<"r1 = "<<r[i]<<" r2 = "<<r[neigbors[i].ids[j]]<<std::endl;
 }
}

//std::cout<<"E = "<<result/2<<std::endl;
return result/2;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc_nucl::QmtVmcNuclWfGaussianSlater::get_ratio(size_t ion_index, qmt::QmtVector& displacement){

double result = 0.0;

for(size_t i = 0U;i  < m.size();++i) {
result += Gauss::get_value(gaussians[i],r[ion_index]+displacement)*gsl_matrix_get(slater_m,i,ion_index);
}

double r_0;
double r_prim;
qmt::QmtVector r0_disp(0,0,0);
qmt::QmtVector r1_disp(0,0,0);

if(ion_index == 0U)
r0_disp = displacement;

if(ion_index == 1U)
r1_disp = displacement;



r_0 = Gauss::get_value(gaussians[0],r[0])*Gauss::get_value(gaussians[1],r[1])-Gauss::get_value(gaussians[1],r[0])*Gauss::get_value(gaussians[0],r[1]);
r_prim = Gauss::get_value(gaussians[0],r[0]+r0_disp)*Gauss::get_value(gaussians[1],r[1]+r1_disp)-Gauss::get_value(gaussians[1],r[0]+r0_disp)*Gauss::get_value(gaussians[0],r[1] + r1_disp);


std::cout<<"Result = "<<result<<" Ratio = "<<r_prim/r_0<<" "<<displacement<<" index = "<<ion_index<<" curent_r = "<<r[ion_index]<<std::endl;
//exit(0);

return r_prim/r_0;
}
