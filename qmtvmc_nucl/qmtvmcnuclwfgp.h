#ifndef QMT_VMC_NUCL_WF_GAUSSIAN_SLATER
#define QMT_VMC_NUCL_WF_GAUSSIAN_SLATER

#include <vector>
#include "../headers/qmtorbitals.h"
#include <gsl/gsl_linalg.h>
#include "qmtvmcnuclwf.h"

namespace qmt{
namespace vmc_nucl{

class QmtVmcNuclWfGaussianSlater : public QmtVmcNuclWf {
 std::vector<qmt::QmtVector> r;
 std::vector<qmt::QmtVector> R;
 std::vector<Gauss> gaussians;
 std::vector<double> m;
 std::vector<double> gamma;

 gsl_matrix *slater_m;

struct Neigs{

std::vector<size_t> ids;

};

std::vector<Neigs> neigbors;

void invertMatrix(gsl_matrix *m);
void updateMatrix(gsl_matrix *m,size_t column,std::vector<double> values);

public:

QmtVmcNuclWfGaussianSlater(const std::vector<qmt::QmtVector>& positions,
									const std::vector<double>& masses,
									const  std::vector<double>& params);

/*
QmtVmcNuclWfGaussianSlater(const std::vector<qmt::QmtVector>& positions,
									const std::vector<double>& masses,
									const  std::vector<double>& params, qmt::QmtVector& radius);


QmtVmcNuclWfGaussianSlater(const std::vector<qmt::QmtVector>& positions,
									const std::vector<double>& masses,
									const  std::vector<double>& params,qmt::QmtVector& v1,qmt::QmtVector& v2, qmt::QmtVector& v3, qmt::QmtVector& radius);
*/

size_t get_size() const;
void get_R(qmt::QmtVector&,size_t) const; 
void get_r(qmt::QmtVector&,size_t) const; 
double get_m(size_t) const; 
double get_param(size_t) const;
size_t get_params_sectors() const;
double get_value() const;
void set_R(const qmt::QmtVector&,size_t); 
void set_r(const qmt::QmtVector&,size_t); 
void set_m(double,size_t); 
void set_param(double,size_t);


//double get_overlap(qmt::vmc_nucl::QmtVmcNuclWf&);
double get_kinetic_energy();
double get_interaction_energy();
double get_ratio(size_t ion_index, qmt::QmtVector& displacement);


};

}
}


#endif
