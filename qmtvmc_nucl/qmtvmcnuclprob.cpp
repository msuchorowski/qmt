#include "qmtvmcnuclporb.h"


double QmtVmcNuclProbability::get_probability(size_t ion_index, qmt::QmtVector& displacement,QmtVmcNuclWf& wave_func){
     qmt::QmtVector current_position;
     double v_before = wave_func.get_value();
     wave_func.get_r(current_position,ion_position);
     wave_func.set_r(current_position + displacement);
     double v_after = wave_func.get_value();
     
    wave_func.set_r(current_position); 
  
    return v_after*v_after/(v_before*v_before);
  }
