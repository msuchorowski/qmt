#ifndef QMT_VMC_NUCL_WF
#define QMT_VMC_NUCL_WF

#include "../headers/qmtvector.h"

namespace qmt{
namespace vmc_nucl{

class QmtVmcNuclWf {

public:

virtual size_t get_size() const = 0;

virtual void get_R(qmt::QmtVector&,size_t) const = 0; 
virtual void get_r(qmt::QmtVector&,size_t) const = 0; 
virtual double get_m(size_t) const = 0; 

virtual double get_param(size_t) const = 0;

virtual size_t get_params_sectors() const = 0;

virtual double get_value() const = 0;

virtual void set_R(const qmt::QmtVector&,size_t) = 0; 
virtual void set_r(const qmt::QmtVector&,size_t) = 0; 
virtual void set_m(double,size_t) = 0; 
virtual void set_param(double,size_t) = 0;

//virtual double get_overlap(qmt::vmc_nucl::QmtVmcNuclWf&) = 0;
virtual double get_kinetic_energy() = 0;
virtual double get_interaction_energy() = 0;

virtual double get_ratio(size_t ion_index, qmt::QmtVector& displacement)=0;
};

 }
}
#endif
