#ifndef QMT_VMC_NUCL_METROPOLIS
#define QMT_VMC_NUCL_METROPOLIS

#include "qmtvmcnuclwf.h"
#include <chrono>
#include <random>
#include <math.h>

namespace qmt{
namespace vmc_nucl{



struct DataSet{
double kinetic_e;
double inter_e;
qmt::QmtVector displ;
size_t mpl;
std::vector<qmt::QmtVector> positions;


DataSet(double k,double c, qmt::QmtVector v, size_t m){
kinetic_e= k;
inter_e = c;
displ = v;
mpl = m;
}


DataSet(const DataSet& d){
kinetic_e= d.kinetic_e;
inter_e = d.inter_e;
displ = d.displ;
mpl = d.mpl;
positions = d.positions;
}

void increment(){
 mpl+=1U;
}

};

class QmtVmcNuclMetropolis {

qmt::vmc_nucl::QmtVmcNuclWf* wave_function;
size_t MCS;
unsigned seed;

std::vector<qmt::QmtVector> R;
std::vector<double> params;

std::default_random_engine generator;
typedef std::chrono::high_resolution_clock myclock;

void prepare_seed();
double rnd();
double grnd(size_t id);
int rnd(int max);


std::vector<DataSet> data;

public:

double get_average_kinetic_energy();
double get_average_interaction_energy();
void get_average_displacment(qmt::QmtVector &v);
void get_data(std::vector<qmt::vmc_nucl::DataSet>& d);
void run(qmt::QmtVector scale=qmt::QmtVector(0.1,0,0));

QmtVmcNuclMetropolis(size_t MC_steps, qmt::vmc_nucl::QmtVmcNuclWf* wf,std::vector<double> pars = std::vector<double>({}));

};



 }
}



#endif
