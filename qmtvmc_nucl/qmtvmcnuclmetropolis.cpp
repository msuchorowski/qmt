#include "qmtvmcnuclmetropolis.h"

void qmt::vmc_nucl::QmtVmcNuclMetropolis::prepare_seed()
{
        myclock::time_point beginning = myclock::now();
        myclock::duration d = myclock::now() - beginning;
        seed = d.count();
	generator.seed(seed);
}

////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc_nucl::QmtVmcNuclMetropolis::rnd()
{
  std::uniform_real_distribution<double> distribution(0.0,1.0);
  return distribution(generator);
}

////////////////////////////////////////////////////////////////////////////////////////////


double qmt::vmc_nucl::QmtVmcNuclMetropolis::grnd(size_t id)
{
  std::normal_distribution<double> distribution(0.0,0.07);
  double  number = distribution(generator);
 // std::cout<<"Number = "<<number<<std::endl;
 return number;
}

////////////////////////////////////////////////////////////////////////////////////////////

int qmt::vmc_nucl::QmtVmcNuclMetropolis::rnd(int max)
{
  std::uniform_int_distribution<int> distribution(0,max);
  return distribution(generator);
}

////////////////////////////////////////////////////////////////////////////////////////////


qmt::vmc_nucl::QmtVmcNuclMetropolis::QmtVmcNuclMetropolis(size_t MC_steps, qmt::vmc_nucl::QmtVmcNuclWf* wf,std::vector<double> pars){
prepare_seed();
MCS = MC_steps;
wave_function=wf;
params = pars;

for(size_t i=0U; i < wave_function->get_size();++i){
qmt::QmtVector v;
wave_function->get_R(v,i);
R.push_back(v);

}
}

////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc_nucl::QmtVmcNuclMetropolis::run(qmt::QmtVector scale){


size_t N=wave_function->get_size();

double x_disp = 0;
double y_disp = 0;
double z_disp = 0;

 data.push_back(DataSet(wave_function->get_kinetic_energy(),wave_function->get_interaction_energy(),qmt::QmtVector(0,0,0),1U));
 for(size_t j = 0U; j < wave_function->get_size();++j) {
  qmt::QmtVector q;
  wave_function->get_R(q,j);
  data.back().positions.push_back(q);
  }


for(size_t step=0U; step < MCS;++step) {

 size_t index = rnd(N-1);
 
 double r = rnd();

// qmt::QmtVector displ =  qmt::QmtVector(0.5*scale.get_x() - scale.get_x()*rnd(),0.5*scale.get_y() - scale.get_y()*rnd(),0.5*scale.get_z() - scale.get_z()*rnd());
  qmt::QmtVector displ =  qmt::QmtVector(grnd(index)*scale.get_x(),grnd(index)*scale.get_y(),grnd(index)*scale.get_z());
 //std::cout<<displ<<std::endl;
 double ratio = wave_function->get_ratio(index,displ);

  
  

 if(ratio*ratio >= r) {
 //std::cout<<"ratio^2 = "<<ratio * ratio<<" r = "<<r<<std::endl;
  qmt::QmtVector p;
  wave_function->get_R(p,index);
  wave_function->set_r(displ + p,index);
  
 for(size_t i =0U; i < N;++i){
  qmt::QmtVector R;
  qmt::QmtVector p;
  wave_function->get_R(R,i);
  wave_function->get_r(p,i);
  x_disp += pow(R.get_x() - p.get_x(),2);
  y_disp += pow(R.get_y() - p.get_y(),2);
  z_disp += pow(R.get_z() - p.get_z(),2);
 }
  
  data.push_back(DataSet(wave_function->get_kinetic_energy(),wave_function->get_interaction_energy(),qmt::QmtVector(x_disp,y_disp,z_disp),1U));
  for(size_t j = 0U; j < wave_function->get_size();++j) {
  qmt::QmtVector q;
  wave_function->get_r(q,j);
  data.back().positions.push_back(q);

  }

 }
 
 else{

 data.back().increment(); 
 std::cout<<"Rejected"<<std::endl;
  }
}


}

////////////////////////////////////////////////////////////////////////////////////////////

double  qmt::vmc_nucl::QmtVmcNuclMetropolis::get_average_kinetic_energy(){
double tot = 0;
  for(size_t i = 0U; i < data.size();++i){
   tot+=data[i].mpl * data[i].kinetic_e;
  }

return tot/data.size();
}

////////////////////////////////////////////////////////////////////////////////////////////

double qmt::vmc_nucl::QmtVmcNuclMetropolis::get_average_interaction_energy(){
double tot = 0;
  for(size_t i = 0U; i < data.size();++i){
   tot+=data[i].mpl * data[i].inter_e;
  }

return tot/data.size();

}
////////////////////////////////////////////////////////////////////////////////////////////

void qmt::vmc_nucl::QmtVmcNuclMetropolis::get_average_displacment(qmt::QmtVector &v){

double tot_x = 0;
double tot_y = 0;
double tot_z = 0;
  for(size_t i = 0U; i < data.size();++i){
   tot_x+=data[i].mpl * data[i].displ.get_x();
   tot_y+=data[i].mpl * data[i].displ.get_y();
   tot_z+=data[i].mpl * data[i].displ.get_z();
  }

v.set_x(tot_x/data.size());
v.set_y(tot_y/data.size());
v.set_z(tot_z/data.size());


}


void qmt::vmc_nucl::QmtVmcNuclMetropolis::get_data(std::vector<qmt::vmc_nucl::DataSet>& d){

for(const auto& item : data)
 d.push_back(item);

}

